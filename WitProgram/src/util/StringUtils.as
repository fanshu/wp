package util {
import flash.utils.Dictionary;

public class StringUtils {
    // format('My {animal} is named {name}.', {animal:'goat',name:'Eric'}) => 'My goat is named Eric.'
    // Tokens not contained in the dictionary will not be modified.
    public static function substitute(s:String, context:Dictionary):String {
        for (var token:String in context) {
            s = s.replace('{' + token + '}', context[token]);
        }
        return s;
    }
}
}
