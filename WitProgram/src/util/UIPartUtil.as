package util {
import flash.display.GradientType;
import flash.display.Graphics;
import flash.display.Shape;
import flash.display.Sprite;
import flash.geom.Matrix;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import gnu.as3.gettext.gettext;

import wp.editor.uiwidgets.IconButton;

use namespace gettext;

public class UIPartUtil {
    private static const _:Function = gettext;
    public static var cornerRadius:int = 8;

    public static function makeLabel(s:String, fmt:TextFormat, x:int = 0, y:int = 0):TextField {
        // Create a non-editable text field for use as a label.
        var tf:TextField = new TextField();
        tf.autoSize = TextFieldAutoSize.LEFT;
        tf.selectable = false;
        tf.defaultTextFormat = fmt;
        tf.text = s;
        tf.x = x;
        tf.y = y;
        return tf;
    }

    public static function drawTopBar(g:Graphics, colors:Array, path:Array, w:int, h:int, borderColor:int = -1):void {
        if (borderColor < 0) borderColor = CSS.borderColor;
        g.clear();
        drawBoxBkgGradientShape(g, Math.PI / 2, colors, [0x00, 0xFF], path, w, h);
        g.lineStyle(0.5, borderColor, 1, true);
        drawPath(path, g);
    }

    public static function drawSelected(g:Graphics, colors:Array, path:Array, w:int, h:int):void {
        g.clear();
        drawBoxBkgGradientShape(g, Math.PI / 2, colors, [0xDC, 0xFF], path, w, h);
        g.lineStyle(0.5, CSS.borderColor, 1, true);
        drawPath(path, g);
    }

    private static function drawBoxBkgGradientShape(g:Graphics, angle:Number, colors:Array, ratios:Array, path:Array, w:Number, h:Number):void {
        var m:Matrix = new Matrix();
        m.createGradientBox(w, h, angle, 0, 0);
        g.beginGradientFill(GradientType.LINEAR, colors, [100, 100], ratios, m);
        drawPath(path, g);
        g.endFill();
    }

    public static function getTopBarPath(w:int, h:int):Array {
        return [["m", 0, h], ["v", -h + cornerRadius], ["c", 0, -cornerRadius, cornerRadius, -cornerRadius],
            ["h", w - cornerRadius * 2], ["c", cornerRadius, 0, cornerRadius, cornerRadius],
            ["v", h - cornerRadius]];
    }

    /* Text Menu Buttons */

    public static function makeMenuButton(s:String, fcn:Function, hasArrow:Boolean = false, labelColor:int = 0xFFFFFF):IconButton {
        var onImg:Sprite = makeButtonLabel(_(s), CSS.buttonLabelOverColor, hasArrow);
        var offImg:Sprite = makeButtonLabel(_(s), labelColor, hasArrow);
        var btn:IconButton = new IconButton(fcn, onImg, offImg);
        btn.isMomentary = true;
        return btn;
    }

    public static function makeButtonLabel(s:String, labelColor:int, hasArrow:Boolean):Sprite {
        var label:TextField = makeLabel(s, CSS.topBarButtonFormat);
        label.textColor = labelColor;
        var img:Sprite = new Sprite();
        img.addChild(label);
        if (hasArrow) img.addChild(menuArrow(label.textWidth + 5, 6, labelColor));
        return img;
    }

    private static function menuArrow(x:int, y:int, c:int):Shape {
        var arrow:Shape = new Shape();
        var g:Graphics = arrow.graphics;
        g.beginFill(c);
        g.lineTo(8, 0);
        g.lineTo(4, 6);
        g.lineTo(0, 0);
        g.endFill();
        arrow.x = x;
        arrow.y = y;
        return arrow;
    }

    public static function drawPath(path:Array, g:Graphics):void {
        var startx:Number = 0, starty:Number = 0;
        var pathx:Number = 0, pathy:Number = 0;
        for each (var item:Array in path) {
            switch (item[0].toLowerCase()) {
                case 'm':
                    startx = item[1];
                    starty = item[2];
                    g.moveTo(pathx = startx, pathy = starty);
                    break;
                case 'l':
                    g.lineTo(pathx += item[1], pathy += item[2]);
                    break;
                case 'h':
                    g.lineTo(pathx += item[1], pathy);
                    break;
                case 'v':
                    g.lineTo(pathx, pathy += item[1]);
                    break;
                case 'c':
                    var cx:Number = pathx + item[1];
                    var cy:Number = pathy + item[2];
                    var px:Number = pathx + item[3];
                    var py:Number = pathy + item[4];
                    g.curveTo(cx, cy, px, py);
                    pathx += item[3];
                    pathy += item[4];
                    break;
                case 'z':
                    g.lineTo(pathx = startx, pathy = starty);
                    break;
                default:
                    trace('DrawPath command not implemented', item[0]);
            }
        }
    }
}
}
