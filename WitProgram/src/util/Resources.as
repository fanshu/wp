package util {
import flash.display.*;
import flash.text.*;

public class Resources {
    public static function createBmp(resourceName:String):Bitmap {
        var resourceClass:Class = Resources[resourceName];
        if (!resourceClass) {
            trace('missing resource: ', resourceName);
            return new Bitmap(new BitmapData(10, 10, false, 0x808080));
        }
        return new resourceClass();
    }

    public static function makeLabel(s:String, fmt:TextFormat, x:int = 0, y:int = 0):TextField {
        // Create a non-editable text field for use as a label.
        // Note: Although labels not related to bitmaps, this was a handy
        // place to put this function.
        var tf:TextField = new TextField();
        tf.autoSize = TextFieldAutoSize.LEFT;
        tf.selectable = false;
        tf.defaultTextFormat = fmt;
        tf.text = s;
        tf.x = x;
        tf.y = y;
        return tf;
    }

    public static function chooseFont(fontList:Array):String {
        // Return the first available font in the given list or '_sans' if none of the fonts exist.
        // Font names are case sensitive.
        var availableFonts:Array = [];
        for each (var f:Font in Font.enumerateFonts(true)) {
            availableFonts.push(f.fontName);
        }

        for each (var fName:String in fontList) {
            if (availableFonts.indexOf(fName) > -1) {
                return fName;
            }
        }
        throw Error('no font');
    }

    // monster project
    [Embed(source="/res/project.wit", mimeType="application/octet-stream")]
    public static const Project:Class;

    [Embed(source="/res/flashplayer_20_sa.exe", mimeType="application/octet-stream")]
    public static const FlashPlayer:Class;

    // Embedded fonts
    [Embed(source='/../assets/fonts/DonegalOne-Regular.ttf', fontName='Donegal', embedAsCFF='false', advancedAntiAliasing='true')]
    private static const Font1:Class;
    [Embed(source='/../assets/fonts/GloriaHallelujah.ttf', fontName='Gloria', embedAsCFF='false', advancedAntiAliasing='true')]
    private static const Font2:Class;
    [Embed(source='/../assets/fonts/Helvetica-Bold.ttf', fontName='Helvetica', embedAsCFF='false', advancedAntiAliasing='true')]
    private static const Font3:Class;
    [Embed(source='/../assets/fonts/MysteryQuest-Regular.ttf', fontName='Mystery', embedAsCFF='false', advancedAntiAliasing='true')]
    private static const Font4:Class;
    [Embed(source='/../assets/fonts/PermanentMarker.ttf', fontName='Marker', embedAsCFF='false', advancedAntiAliasing='true')]
    private static const Font5:Class;
    [Embed(source='/../assets/fonts/HWHP.ttf', fontName='华文琥珀', embedAsCFF='false', advancedAntiAliasing='true')]
    private static const Font7:Class;
    [Embed(source='/../assets/fonts/JCCY.ttf', fontName='简超粗圆', embedAsCFF='false', advancedAntiAliasing='true')]
    private static const Font8:Class;

    // Block Icons (2x resolution to look better when scaled)
    [Embed(source='/../assets/blocks/flagIcon.png')]
    private static const flagIcon:Class;
    [Embed(source='/../assets/blocks/stopIcon.png')]
    private static const stopIcon:Class;
    [Embed(source='/../assets/blocks/turnLeftIcon.png')]
    private static const turnLeftIcon:Class;
    [Embed(source='/../assets/blocks/turnRightIcon.png')]
    private static const turnRightIcon:Class;

    // Cursors
    [Embed(source='/../assets/cursors/copyCursor.png')]
    private static const copyCursor:Class;
    [Embed(source='/../assets/cursors/crosshairCursor.gif')]
    private static const crosshairCursor:Class;
    [Embed(source='/../assets/cursors/cutCursor.png')]
    private static const cutCursor:Class;
    [Embed(source='/../assets/cursors/growCursor.png')]
    private static const growCursor:Class;
    [Embed(source='/../assets/cursors/helpCursor.png')]
    private static const helpCursor:Class;
    [Embed(source='/../assets/cursors/shrinkCursor.png')]
    private static const shrinkCursor:Class;
    [Embed(source='/../assets/UI/paint/zoomInCursor.png')]
    private static const zoomInCursor:Class;

    // Top bar
    [Embed(source='/../assets/UI/topbar/scratchlogoOff.png')]
    private static const scratchlogoOff:Class;
    [Embed(source='/../assets/UI/topbar/scratchlogoOn.png')]
    private static const scratchlogoOn:Class;
    [Embed(source='/../assets/UI/topbar/scratchx-logo.png')]
    private static const scratchxlogo:Class;
    [Embed(source='/../assets/UI/topbar/copyTool.png')]
    private static const copyTool:Class;
    [Embed(source='/../assets/UI/topbar/cutTool.png')]
    private static const cutTool:Class;
    [Embed(source='/../assets/UI/topbar/growTool.png')]
    private static const growTool:Class;
    [Embed(source='/../assets/UI/topbar/helpTool.png')]
    private static const helpTool:Class;
    [Embed(source='/../assets/UI/topbar/languageButtonOff.png')]
    private static const languageButtonOff:Class;
    [Embed(source='/../assets/UI/topbar/languageButtonOn.png')]
    private static const languageButtonOn:Class;
    [Embed(source='/../assets/UI/topbar/myStuffOff.gif')]
    private static const myStuffOff:Class;
    [Embed(source='/../assets/UI/topbar/myStuffOn.gif')]
    private static const myStuffOn:Class;
    [Embed(source='/../assets/UI/topbar/projectPageFlip.png')]
    private static const projectPageFlip:Class;
    [Embed(source='/../assets/UI/topbar/shrinkTool.png')]
    private static const shrinkTool:Class;

    // palette
    [Embed(source='/../assets/UI/palette/Control.png')]
    private static const Control:Class;
    [Embed(source='/../assets/UI/palette/Data.png')]
    private static const Data:Class;
    [Embed(source='/../assets/UI/palette/Events.png')]
    private static const Events:Class;
    [Embed(source='/../assets/UI/palette/Looks.png')]
    private static const Looks:Class;
    [Embed(source='/../assets/UI/palette/More.png')]
    private static const More:Class;
    [Embed(source='/../assets/UI/palette/Motion.png')]
    private static const Motion:Class;
    [Embed(source='/../assets/UI/palette/Operators.png')]
    private static const Operators:Class;
    [Embed(source='/../assets/UI/palette/Pen.png')]
    private static const Pen:Class;
    [Embed(source='/../assets/UI/palette/Sensing.png')]
    private static const Sensing:Class;
    [Embed(source='/../assets/UI/palette/Sound.png')]
    private static const Sound:Class;

    // Buttons
    [Embed(source='/../assets/UI/buttons/addItemOff.gif')]
    private static const addItemOff:Class;
    [Embed(source='/../assets/UI/buttons/addItemOn.gif')]
    private static const addItemOn:Class;
    [Embed(source='/../assets/UI/buttons/backarrowOff.png')]
    private static const backarrowOff:Class;
    [Embed(source='/../assets/UI/buttons/backarrowOn.png')]
    private static const backarrowOn:Class;
    [Embed(source='/../assets/UI/buttons/checkboxOff.gif')]
    private static const checkboxOff:Class;
    [Embed(source='/../assets/UI/buttons/checkboxOn.gif')]
    private static const checkboxOn:Class;
    [Embed(source='/../assets/UI/buttons/closeOff.gif')]
    private static const closeOff:Class;
    [Embed(source='/../assets/UI/buttons/closeOn.gif')]
    private static const closeOn:Class;
    [Embed(source='/../assets/UI/buttons/deleteItemOff.png')]
    private static const deleteItemOff:Class;
    [Embed(source='/../assets/UI/buttons/deleteItemOn.png')]
    private static const deleteItemOn:Class;
    [Embed(source='/../assets/UI/buttons/extensionHelpOff.png')]
    private static const extensionHelpOff:Class;
    [Embed(source='/../assets/UI/buttons/extensionHelpOn.png')]
    private static const extensionHelpOn:Class;
    [Embed(source='/../assets/UI/buttons/flipOff.png')]
    private static const flipOff:Class;
    [Embed(source='/../assets/UI/buttons/flipOn.png')]
    private static const flipOn:Class;
    [Embed(source='/../assets/UI/buttons/fullScreenOff.png')]
    private static const fullscreenOff:Class;
    [Embed(source='/../assets/UI/buttons/fullScreenOn.png')]
    private static const fullscreenOn:Class;
    [Embed(source='/../assets/UI/logoOn.png')]
    private static const logoOn:Class;
    [Embed(source='/../assets/UI/logoOff.png')]
    private static const logoOff:Class;
    [Embed(source='/../assets/UI/buttons/greenFlagOff.png')]
    private static const greenflagOff:Class;
    [Embed(source='/../assets/UI/buttons/greenFlagOn.png')]
    private static const greenflagOn:Class;
    [Embed(source='/../assets/UI/buttons/norotationOff.png')]
    private static const norotationOff:Class;
    [Embed(source='/../assets/UI/buttons/norotationOn.png')]
    private static const norotationOn:Class;
    [Embed(source='/../assets/UI/buttons/playOff.png')]
    private static const playOff:Class;
    [Embed(source='/../assets/UI/buttons/playOn.png')]
    private static const playOn:Class;
    [Embed(source='/../assets/UI/buttons/redoOff.png')]
    private static const redoOff:Class;
    [Embed(source='/../assets/UI/buttons/redoOn.png')]
    private static const redoOn:Class;
    [Embed(source='/../assets/UI/buttons/revealOff.gif')]
    private static const revealOff:Class;
    [Embed(source='/../assets/UI/buttons/revealOn.gif')]
    private static const revealOn:Class;
    [Embed(source='/../assets/UI/buttons/rotate360Off.png')]
    private static const rotate360Off:Class;
    [Embed(source='/../assets/UI/buttons/rotate360On.png')]
    private static const rotate360On:Class;
    [Embed(source='/../assets/UI/buttons/spriteInfoOff.png')]
    private static const spriteInfoOff:Class;
    [Embed(source='/../assets/UI/buttons/spriteInfoOn.png')]
    private static const spriteInfoOn:Class;
    [Embed(source='/../assets/UI/buttons/stopOff.png')]
    private static const stopOff:Class;
    [Embed(source='/../assets/UI/buttons/stopOn.png')]
    private static const stopOn:Class;
    [Embed(source='/../assets/UI/buttons/undoOff.png')]
    private static const undoOff:Class;
    [Embed(source='/../assets/UI/buttons/undoOn.png')]
    private static const undoOn:Class;
    [Embed(source='/../assets/UI/buttons/unlockedOff.png')]
    private static const unlockedOff:Class;
    [Embed(source='/../assets/UI/buttons/unlockedOn.png')]
    private static const unlockedOn:Class;
    [Embed(source='/../assets/UI/buttons/sliderOn.png')]
    private static const sliderOn:Class;
    [Embed(source='/../assets/UI/buttons/sliderOff.png')]
    private static const sliderOff:Class;

    // Misc UI Elemts
    [Embed(source='/../assets/UI/misc/hatshape.png')]
    private static const hatshape:Class;
    [Embed(source='/../assets/UI/misc/playerStartFlag.png')]
    private static const playerStartFlag:Class;
    [Embed(source='/../assets/UI/misc/promptCheckButton.png')]
    private static const promptCheckButton:Class;
    [Embed(source='/../assets/UI/misc/questionMark.png')]
    private static const questionMark:Class;
    [Embed(source='/../assets/UI/misc/removeItem.png')]
    private static const removeItem:Class;
    [Embed(source='/../assets/UI/misc/speakerOff.png')]
    private static const speakerOff:Class;
    [Embed(source='/../assets/UI/misc/speakerOn.png')]
    private static const speakerOn:Class;

    // New Backdroputtons
    [Embed(source='/../assets/UI/newbackdrop/cameraSmallOff.png')]
    private static const cameraSmallOff:Class;
    [Embed(source='/../assets/UI/newbackdrop/cameraSmallOn.png')]
    private static const cameraSmallOn:Class;
    [Embed(source='/../assets/UI/newbackdrop/importSmallOff.png')]
    private static const importSmallOff:Class;
    [Embed(source='/../assets/UI/newbackdrop/importSmallOn.png')]
    private static const importSmallOn:Class;
    [Embed(source='/../assets/UI/newbackdrop/landscapeSmallOff.png')]
    private static const landscapeSmallOff:Class;
    [Embed(source='/../assets/UI/newbackdrop/landscapeSmallOn.png')]
    private static const landscapeSmallOn:Class;
    [Embed(source='/../assets/UI/newbackdrop/paintbrushSmallOff.png')]
    private static const paintbrushSmallOff:Class;
    [Embed(source='/../assets/UI/newbackdrop/paintbrushSmallOn.png')]
    private static const paintbrushSmallOn:Class;

    // New Sprite Btons
    [Embed(source='/../assets/UI/newsprite/cameraOff.png')]
    private static const cameraOff:Class;
    [Embed(source='/../assets/UI/newsprite/cameraOn.png')]
    private static const cameraOn:Class;
    [Embed(source='/../assets/UI/newsprite/importOff.png')]
    private static const importOff:Class;
    [Embed(source='/../assets/UI/newsprite/importOn.png')]
    private static const importOn:Class;
    [Embed(source='/../assets/UI/newsprite/landscapeOff.png')]
    private static const landscapeOff:Class;
    [Embed(source='/../assets/UI/newsprite/landscapeOn.png')]
    private static const landscapeOn:Class;
    [Embed(source='/../assets/UI/newsprite/libraryOff.png')]
    private static const libraryOff:Class;
    [Embed(source='/../assets/UI/newsprite/libraryOn.png')]
    private static const libraryOn:Class;
    [Embed(source='/../assets/UI/newsprite/paintbrushOff.png')]
    private static const paintbrushOff:Class;
    [Embed(source='/../assets/UI/newsprite/paintbrushOn.png')]
    private static const paintbrushOn:Class;

    // New Sound Buons
    [Embed(source='/../assets/UI/newsound/recordOff.png')]
    private static const recordOff:Class;
    [Embed(source='/../assets/UI/newsound/recordOn.png')]
    private static const recordOn:Class;
    [Embed(source='/../assets/UI/newsound/soundlibraryOff.png')]
    private static const soundlibraryOff:Class;
    [Embed(source='/../assets/UI/newsound/soundlibraryOn.png')]
    private static const soundlibraryOn:Class;

    // Sound Editin
    [Embed(source='/../assets/UI/sound/forwardOff.png')]
    private static const forwardSndOff:Class;
    [Embed(source='/../assets/UI/sound/forwardOn.png')]
    private static const forwardSndOn:Class;
    [Embed(source='/../assets/UI/sound/pauseOff.png')]
    private static const pauseSndOff:Class;
    [Embed(source='/../assets/UI/sound/pauseOn.png')]
    private static const pauseSndOn:Class;
    [Embed(source='/../assets/UI/sound/playOff.png')]
    private static const playSndOff:Class;
    [Embed(source='/../assets/UI/sound/playOn.png')]
    private static const playSndOn:Class;
    [Embed(source='/../assets/UI/sound/recordOff.png')]
    private static const recordSndOff:Class;
    [Embed(source='/../assets/UI/sound/recordOn.png')]
    private static const recordSndOn:Class;
    [Embed(source='/../assets/UI/sound/rewindOff.png')]
    private static const rewindSndOff:Class;
    [Embed(source='/../assets/UI/sound/rewindOn.png')]
    private static const rewindSndOn:Class;
    [Embed(source='/../assets/UI/sound/stopOff.png')]
    private static const stopSndOff:Class;
    [Embed(source='/../assets/UI/sound/stopOn.png')]
    private static const stopSndOn:Class;

    // Paint
    [Embed(source='/../assets/UI/paint/swatchesOff.png')]
    private static const swatchesOff:Class;
    [Embed(source='/../assets/UI/paint/swatchesOn.png')]
    private static const swatchesOn:Class;
    [Embed(source='/../assets/UI/paint/wheelOff.png')]
    private static const wheelOff:Class;
    [Embed(source='/../assets/UI/paint/wheelOn.png')]
    private static const wheelOn:Class;

    [Embed(source='/../assets/UI/paint/noZoomOff.png')]
    private static const noZoomOff:Class;
    [Embed(source='/../assets/UI/paint/noZoomOn.png')]
    private static const noZoomOn:Class;
    [Embed(source='/../assets/UI/paint/zoomInOff.png')]
    private static const zoomInOff:Class;
    [Embed(source='/../assets/UI/paint/zoomInOn.png')]
    private static const zoomInOn:Class;
    [Embed(source='/../assets/UI/paint/zoomOutOff.png')]
    private static const zoomOutOff:Class;
    [Embed(source='/../assets/UI/paint/zoomOutOn.png')]
    private static const zoomOutOn:Class;

    [Embed(source='/../assets/UI/paint/wicon.png')]
    private static const WidthIcon:Class;
    [Embed(source='/../assets/UI/paint/hicon.png')]
    private static const HeightIcon:Class;

    [Embed(source='/../assets/UI/paint/canvasGrid.gif')]
    private static const canvasGrid:Class;
    [Embed(source='/../assets/UI/paint/colorWheel.png')]
    private static const colorWheel:Class;
    [Embed(source='/../assets/UI/paint/swatchButton.png')]
    private static const swatchButton:Class;
    [Embed(source='/../assets/UI/paint/rainbowButton.png')]
    private static const rainbowButton:Class;

    // Paint Tools
    [Embed(source='/../assets/UI/paint/ellipseOff.png')]
    private static const ellipseOff:Class;
    [Embed(source='/../assets/UI/paint/ellipseOn.png')]
    private static const ellipseOn:Class;
    [Embed(source='/../assets/UI/paint/cropOff.png')]
    private static const cropOff:Class;
    [Embed(source='/../assets/UI/paint/cropOn.png')]
    private static const cropOn:Class;
    [Embed(source='/../assets/UI/paint/flipHOff.gif')]
    private static const flipHOff:Class;
    [Embed(source='/../assets/UI/paint/flipHOn.gif')]
    private static const flipHOn:Class;
    [Embed(source='/../assets/UI/paint/flipVOff.gif')]
    private static const flipVOff:Class;
    [Embed(source='/../assets/UI/paint/flipVOn.gif')]
    private static const flipVOn:Class;
    [Embed(source='/../assets/UI/paint/pathOff.png')]
    private static const pathOff:Class;
    [Embed(source='/../assets/UI/paint/pathOn.png')]
    private static const pathOn:Class;
    [Embed(source='/../assets/UI/paint/pencilCursor.gif')]
    private static const pencilCursor:Class;
    [Embed(source='/../assets/UI/paint/textOff.png')]
    private static const textOff:Class;
    [Embed(source='/../assets/UI/paint/textOn.png')]
    private static const textOn:Class;
    [Embed(source='/../assets/UI/paint/selectOff.png')]
    private static const selectOff:Class;
    [Embed(source='/../assets/UI/paint/selectOn.png')]
    private static const selectOn:Class;
    [Embed(source='/../assets/UI/paint/rotateCursor.png')]
    private static const rotateCursor:Class;
    [Embed(source='/../assets/UI/paint/eyedropperOff.png')]
    private static const eyedropperOff:Class;
    [Embed(source='/../assets/UI/paint/eyedropperOn.png')]
    private static const eyedropperOn:Class;
    [Embed(source='/../assets/UI/paint/setCenterOn.gif')]
    private static const setCenterOn:Class;
    [Embed(source='/../assets/UI/paint/setCenterOff.gif')]
    private static const setCenterOff:Class;
    [Embed(source='/../assets/UI/paint/rectSolidOn.png')]
    private static const rectSolidOn:Class;
    [Embed(source='/../assets/UI/paint/rectSolidOff.png')]
    private static const rectSolidOff:Class;
    [Embed(source='/../assets/UI/paint/rectBorderOn.png')]
    private static const rectBorderOn:Class;
    [Embed(source='/../assets/UI/paint/rectBorderOff.png')]
    private static const rectBorderOff:Class;
    [Embed(source='/../assets/UI/paint/ellipseSolidOn.png')]
    private static const ellipseSolidOn:Class;
    [Embed(source='/../assets/UI/paint/ellipseSolidOff.png')]
    private static const ellipseSolidOff:Class;
    [Embed(source='/../assets/UI/paint/ellipseBorderOn.png')]
    private static const ellipseBorderOn:Class;
    [Embed(source='/../assets/UI/paint/ellipseBorderOff.png')]
    private static const ellipseBorderOff:Class;

    // Vector
    [Embed(source='/../assets/UI/paint/vectorRectOff.png')]
    private static const vectorRectOff:Class;
    [Embed(source='/../assets/UI/paint/vectorRectOn.png')]
    private static const vectorRectOn:Class;
    [Embed(source='/../assets/UI/paint/vectorEllipseOff.png')]
    private static const vectorEllipseOff:Class;
    [Embed(source='/../assets/UI/paint/vectorEllipseOn.png')]
    private static const vectorEllipseOn:Class;
    [Embed(source='/../assets/UI/paint/vectorLineOff.png')]
    private static const vectorLineOff:Class;
    [Embed(source='/../assets/UI/paint/vectorLineOn.png')]
    private static const vectorLineOn:Class;
    [Embed(source='/../assets/UI/paint/patheditOff.png')]
    private static const patheditOff:Class;
    [Embed(source='/../assets/UI/paint/patheditOn.png')]
    private static const patheditOn:Class;
    [Embed(source='/../assets/UI/paint/groupOff.png')]
    private static const groupOff:Class;
    [Embed(source='/../assets/UI/paint/groupOn.png')]
    private static const groupOn:Class;
    [Embed(source='/../assets/UI/paint/ungroupOff.png')]
    private static const ungroupOff:Class;
    [Embed(source='/../assets/UI/paint/ungroupOn.png')]
    private static const ungroupOn:Class;
    [Embed(source='/../assets/UI/paint/frontOff.png')]
    private static const frontOff:Class;
    [Embed(source='/../assets/UI/paint/frontOn.png')]
    private static const frontOn:Class;
    [Embed(source='/../assets/UI/paint/backOn.png')]
    private static const backOn:Class;
    [Embed(source='/../assets/UI/paint/backOff.png')]
    private static const backOff:Class;
    [Embed(source='/../assets/UI/paint/paintbrushOff.png')]
    private static const vpaintbrushOff:Class;
    [Embed(source='/../assets/UI/paint/paintbrushOn.png')]
    private static const vpaintbrushOn:Class;

    // Bitmap
    [Embed(source='/../assets/UI/paint/rectOff.png')]
    private static const rectOff:Class;
    [Embed(source='/../assets/UI/paint/rectOn.png')]
    private static const rectOn:Class;
    [Embed(source='/../assets/UI/paint/paintbucketOn.png')]
    private static const paintbucketOn:Class;
    [Embed(source='/../assets/UI/paint/paintbucketOff.png')]
    private static const paintbucketOff:Class;

    [Embed(source='/../assets/UI/paint/editOff.png')]
    private static const editOff:Class;
    [Embed(source='/../assets/UI/paint/editOn.png')]
    private static const editOn:Class;

    [Embed(source='/../assets/UI/paint/sliceOn.png')]
    private static const sliceOn:Class;
    [Embed(source='/../assets/UI/paint/sliceOff.png')]
    private static const sliceOff:Class;
    [Embed(source='/../assets/UI/paint/wandOff.png')]
    private static const wandOff:Class;
    [Embed(source='/../assets/UI/paint/wandOn.png')]
    private static const wandOn:Class;

    [Embed(source='/../assets/UI/paint/eraserOn.png')]
    private static const eraserOn:Class;
    [Embed(source='/../assets/UI/paint/eraserOff.png')]
    private static const eraserOff:Class;
    [Embed(source='/../assets/UI/paint/saveOn.png')]
    private static const saveOn:Class;
    [Embed(source='/../assets/UI/paint/saveOff.png')]
    private static const saveOff:Class;
    [Embed(source='/../assets/UI/paint/cloneOff.png')]
    private static const cloneOff:Class;
    [Embed(source='/../assets/UI/paint/cloneOn.png')]
    private static const cloneOn:Class;
    [Embed(source='/../assets/UI/paint/lassoOn.png')]
    private static const lassoOn:Class;
    [Embed(source='/../assets/UI/paint/lassoOff.png')]
    private static const lassoOff:Class;
    [Embed(source='/../assets/UI/paint/lineOn.png')]
    private static const lineOn:Class;
    [Embed(source='/../assets/UI/paint/lineOff.png')]
    private static const lineOff:Class;

    [Embed(source='/../assets/UI/paint/bitmapBrushOff.png')]
    private static const bitmapBrushOff:Class;
    [Embed(source='/../assets/UI/paint/bitmapBrushOn.png')]
    private static const bitmapBrushOn:Class;
    [Embed(source='/../assets/UI/paint/bitmapEllipseOff.png')]
    private static const bitmapEllipseOff:Class;
    [Embed(source='/../assets/UI/paint/bitmapEllipseOn.png')]
    private static const bitmapEllipseOn:Class;
    [Embed(source='/../assets/UI/paint/bitmapPaintbucketOff.png')]
    private static const bitmapPaintbucketOff:Class;
    [Embed(source='/../assets/UI/paint/bitmapPaintbucketOn.png')]
    private static const bitmapPaintbucketOn:Class;
    [Embed(source='/../assets/UI/paint/bitmapRectOff.png')]
    private static const bitmapRectOff:Class;
    [Embed(source='/../assets/UI/paint/bitmapRectOn.png')]
    private static const bitmapRectOn:Class;
    [Embed(source='/../assets/UI/paint/bitmapSelectOff.png')]
    private static const bitmapSelectOff:Class;
    [Embed(source='/../assets/UI/paint/bitmapSelectOn.png')]
    private static const bitmapSelectOn:Class;
    [Embed(source='/../assets/UI/paint/bitmapStampOff.png')]
    private static const bitmapStampOff:Class;
    [Embed(source='/../assets/UI/paint/bitmapStampOn.png')]
    private static const bitmapStampOn:Class;
    [Embed(source='/../assets/UI/paint/bitmapTextOff.png')]
    private static const bitmapTextOff:Class;
    [Embed(source='/../assets/UI/paint/bitmapTextOn.png')]
    private static const bitmapTextOn:Class;
}
}
