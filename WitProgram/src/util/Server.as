// Interface to the WitProgram website API's for Offline Editor.
//
// Note: All operations call the whenDone function with the result
// if the operation succeeded or null if it failed.

package util {
import by.blooddy.crypto.serialization.JSON;

import flash.display.BitmapData;
import flash.display.Loader;
import flash.display.Sprite;
import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.geom.Matrix;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;
import flash.system.Security;
import flash.utils.ByteArray;

import mx.utils.URLUtil;

import wp.editor.uiwidgets.DialogBox;

public class Server implements IServer {
    private static var _instance:Server;
    protected var URLs:Object = {};
    public var app:Sprite;

    public function Server($singletoner:Singletoner) {
        if ($singletoner == null) {
            throw Error("singleton");
        }

        setDefaultURLs();

        // Accept URL overrides from the flash variables
        try {
            var urlOverrides:String = app.loaderInfo.parameters['urlOverrides'];
            if (urlOverrides) overrideURLs(by.blooddy.crypto.serialization.JSON.decode(urlOverrides));
        }
        catch (e:*) {
        }
    }

    public static function get instance():Server {
        if (_instance == null) {
            _instance = new Server(new Singletoner());
        }
        return _instance;
    }

    // No default URLs
    protected function setDefaultURLs():void {
    }

    public function overrideURLs(overrides:Object):void {
        var forceProtocol:String;
        var swfURL:String = app.loaderInfo.url;
        if (swfURL && URLUtil.isHttpURL(swfURL)) { // "isHttpURL" is true if the protocol is either HTTP or HTTPS
            forceProtocol = URLUtil.getProtocol(swfURL);
        }
        for (var name:String in overrides) {
            if (overrides.hasOwnProperty(name)) {
                var url:String = overrides[name];

                if (forceProtocol && URLUtil.isHttpURL(url)) {
                    url = URLUtil.replaceProtocol(url, forceProtocol);
                }

                URLs[name] = url;
            }
        }
    }

    protected function getCdnStaticSiteURL():String {
        return URLs.siteCdnPrefix + URLs.staticFiles;
    }

    // -----------------------------
    // Server GET/POST
    //------------------------------

    // This will be called with the HTTP status result from any callServer() that receives one, even when successful.
    // The url and data parameters match those passed to callServer.
    protected function onCallServerHttpStatus(url:String, data:*, event:HTTPStatusEvent):void {
        if (event.status < 200 || event.status > 299) {
//			logMessage(event.toString());
        }
    }

    // This will be called if callServer encounters an error, before whenDone(null) is called.
    // The url and data parameters match those passed to callServer.
    protected function onCallServerError(url:String, data:*, event:ErrorEvent):void {
//			if(err.type != IOErrorEvent.IO_ERROR || url.indexOf('/backpack/') == -1) {
//				if(data)
//					logMessage('Failed server request for '+url+' with data ['+data+']');
//				else
//					logMessage('Failed server request for '+url);
//			}
        // We shouldn't have SecurityErrorEvents unless the crossdomain file failed to load
        // Re-trying here should help project save failures but we'll need to add more code to re-try loading projects
        if (event is SecurityErrorEvent) {
            var urlPathStart:int = url.indexOf('/', 10);
            var policyFileURL:String = url.substr(0, urlPathStart) + '/crossdomain.xml?cb=' + Math.random();
            Security.loadPolicyFile(policyFileURL);
            trace('Reloading policy file from : ' + policyFileURL);
        }
        if (data || url.indexOf('/set/') > -1) {
            // TEMPORARY HOTFIX: Don't send this message since it seems to saturate our logging backend.
            //logMessage('Failed server request for '+url+' with data ['+data+']');
            trace('Failed server request for ' + url + ' with data [' + data + ']');
        }
    }

    // This will be called if callServer encounters an exception, before whenDone(null) is called.
    // The url and data parameters match those passed to callServer.
    protected function onCallServerException(url:String, data:*, exception:*):void {
        if (exception is Error) {
            trace(exception);
        }
    }

    // TODO: Maybe should have this or onCallServerError() but not both
    public var callServerErrorInfo:Object; // only valid during a whenDone() call reporting failure.

    // Make a GET or POST request to the given URL (do a POST if the data is not null).
    // The whenDone() function is called when the request is done, either with the
    // data returned by the server or with a null argument if the request failed.
    // The request includes site and session authentication headers.
    protected function callServer(url:String, data:*, mimeType:String, whenDone:Function, queryParams:Object = null):URLLoader {
        function addListeners():void {
            loader.addEventListener(Event.COMPLETE, completeHandler);
            loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler);
            loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
            loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, statusHandler);
        }

        function removeListeners():void {
            loader.removeEventListener(Event.COMPLETE, completeHandler);
            loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler);
            loader.removeEventListener(IOErrorEvent.IO_ERROR, errorHandler);
            loader.removeEventListener(HTTPStatusEvent.HTTP_STATUS, statusHandler);
        }

        function completeHandler(event:Event):void {
            removeListeners();
            callServerErrorInfo = null;
            whenDone(loader.data);
        }

        var httpStatus:int = 0;

        function errorHandler(event:ErrorEvent):void {
            removeListeners();
            onCallServerError(url, data, event);
            callServerErrorInfo = {
                url: url,
                httpStatus: httpStatus,
                errorEvent: event
            };
            whenDone(null);
            callServerErrorInfo = null;
        }

        function exceptionHandler(exception:*):void {
            removeListeners();
            onCallServerException(url, data, exception);
            whenDone(null);
        }

        function statusHandler(e:HTTPStatusEvent):void {
            httpStatus = e.status;
            onCallServerHttpStatus(url, data, e);
        }

        var loader:URLLoader = new URLLoader();
        loader.dataFormat = URLLoaderDataFormat.BINARY;
        addListeners();

        // Add a cache breaker if we're sending data and the url has no query string.
        var nextSeparator:String = '?';
        if (data && url.indexOf('?') == -1) {
            url += '?' + '_rnd=' + Math.random();
            nextSeparator = '&';
        }
        for (var key:String in queryParams) {
            if (queryParams.hasOwnProperty(key)) {
                url += nextSeparator + encodeURIComponent(key) + '=' + encodeURIComponent(queryParams[key]);
                nextSeparator = '&';
            }
        }
        var request:URLRequest = new URLRequest(url);
        if (data) {
            request.method = URLRequestMethod.POST;
            request.data = data;

            if (mimeType) request.requestHeaders.push(new URLRequestHeader("Content-type", mimeType));

            // header for CSRF authentication when sending data
            var csrfCookie:String = getCSRF();
            if (csrfCookie && (csrfCookie.length > 0)) {
                request.requestHeaders.push(new URLRequestHeader('X-CSRFToken', csrfCookie));
            }
        }

        try {
            loader.load(request);
        }
        catch (e:*) {
            // Local sandbox exception?
            exceptionHandler(e);
        }
        return loader;
    }

    public function getCSRF():String {
        return null;
    }

    // Make a simple GET. Uses the same callbacks as callServer().
    public function serverGet(url:String, whenDone:Function):URLLoader {
        return callServer(url, null, null, whenDone);
    }

    // -----------------------------
    // Asset API
    //------------------------------
    private function onFail():void {
        var d:DialogBox = new DialogBox();
        d.addTitle('加载素材失败');
        d.addText('请将素材库media目录复制的程序目录，重试……');
        d.addButton('关闭', function():void {
            d.cancel();
        });
        d.showOnStage(TempStatic.stagePart.stage);
    }

    public function getAsset(md5:String, whenDone:Function):URLLoader {
//		if (BackpackPart.localAssets[md5] && BackpackPart.localAssets[md5].length > 0) {
//			whenDone(BackpackPart.localAssets[md5]);
//			return null;
//		}
        var url:String = 'media/' + md5;
        var ldr:URLLoader = serverGet(url, function(data:*):void{
            if (data == null) {
                onFail();
                return;
            }
            whenDone(data);
        });
        return ldr;
    }

    public function getMediaLibrary(libraryType:String, whenDone:Function):URLLoader {
        var url:String = 'media/libs/' + libraryType + 'Library.json';
        var ldr:URLLoader = serverGet(url, function(data:*):void{
            if (data == null) {
                onFail();
                return;
            }
            whenDone(data);
        });
        return ldr;
    }

    protected function downloadThumbnail(url:String, w:int, h:int, whenDone:Function):URLLoader {
        function decodeImage(data:ByteArray):void {
            if (!data || data.length == 0) return; // no data
            var decoder:Loader = new Loader();
            decoder.contentLoaderInfo.addEventListener(Event.COMPLETE, imageDecoded);
            decoder.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, imageError);
            try {
                decoder.loadBytes(data);
            } catch (e:*) {
                if (e is Error) {
                    trace(e);
                }
                else {
                    trace("Server caught exception decoding image: " + url);
                }
            }
        }

        function imageError(e:IOErrorEvent):void {
            trace('ServerOnline failed to decode image: ' + url);
        }

        function imageDecoded(e:Event):void {
            whenDone(makeThumbnail(e.target.content.bitmapData));
        }

        return serverGet(url, decodeImage);
    }

    private static function makeThumbnail(bm:BitmapData):BitmapData {
        const tnWidth:int = 120;
        const tnHeight:int = 90;
        var result:BitmapData = new BitmapData(tnWidth, tnHeight, true, 0);
        if ((bm.width == 0) || (bm.height == 0)) return result;
        var scale:Number = Math.min(tnWidth / bm.width, tnHeight / bm.height);
        var m:Matrix = new Matrix();
        m.scale(scale, scale);
        m.translate((tnWidth - (scale * bm.width)) / 2, (tnHeight - (scale * bm.height)) / 2);
        result.draw(bm, m);
        return result;
    }

    public function getThumbnail(idAndExt:String, w:int, h:int, whenDone:Function):URLLoader {
        var url:String = 'media/' + idAndExt;
        return downloadThumbnail(url, w, h, whenDone);
    }
}
}

internal class Singletoner {
}
