package util {
import com.adobe.utils.StringUtil;
import com.debokeh.anes.utils.USBUtil;

import deng.fzip.FZip;
import deng.fzip.FZipFile;

import flash.display.*;
import flash.events.*;
import flash.net.URLLoader;
import flash.utils.ByteArray;
import flash.utils.Dictionary;
import flash.utils.setTimeout;

import jijunzeng.MyEvent;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.sound.WAVFile;
import wp.editor.sound.mp3.MP3Loader;
import wp.editor.svgutils.*;
import wp.editor.uiwidgets.DialogBox;
import wp.editor.wit.*;

public class ProjectIO {
    private var images:Array = [];
    private var sounds:Array = [];

    public function ProjectIO():void {
    }

    //----------------------------
    // Encode a project or sprite as a ByteArray (a 'one-file' project)
    //----------------------------

    public function encodeProjectAsZipFile(proj:WitStage):ByteArray {
        // Encode a project into a ByteArray. The format is a ZIP file containing
        // the JSON project data and all images and sounds as files.
        delete proj.info.penTrails; // remove the penTrails bitmap saved in some old projects' info
        proj.savePenLayer();
        proj.updateInfo();
        recordImagesAndSounds(proj.allObjects(), false, proj);
        var zip:FZip = new FZip();
        addJSONData('project.json', proj, zip);
        addImagesAndSounds(zip);
        proj.clearPenLayer();
        var ba:ByteArray = new ByteArray();
        zip.serialize(ba);
        return ba;
    }

    public function encodeSpriteAsZipFile(spr:WitSprite):ByteArray {
        // Encode a sprite into a ByteArray. The format is a ZIP file containing
        // the JSON sprite data and all images and sounds as files.
        recordImagesAndSounds([spr], false);
        var zip:FZip = new FZip();
        addJSONData('sprite.json', spr, zip);
        addImagesAndSounds(zip);
        var ba:ByteArray = new ByteArray();
        zip.serialize(ba);
        return ba;
    }

    private function addJSONData(fileName:String, obj:*, zip:FZip):void {
        var str:String = JSON.stringify(obj);
        zip.addFileFromString(fileName, str);
    }

    private function addImagesAndSounds(zip:FZip):void {
        var i:int, ext:String;
        for (i = 0; i < images.length; i++) {
            var imgData:ByteArray = images[i][1];
            ext = WitCostume.fileExtension(imgData);
            zip.addFile(i+ext, imgData);
        }
        for (i = 0; i < sounds.length; i++) {
            var sndData:ByteArray = sounds[i][1];
            ext = WitSound.isWAV(sndData) ? '.wav' : '.mp3';
            zip.addFile(i+ext, sndData);
        }
    }

    //----------------------------
    // Decode a project or sprite from a ByteArray containing ZIP data
    //----------------------------

    public function decodeProjectFromZipFile(zipData:ByteArray, callback:Function){
        decodeFromZipFile(zipData, callback);
    }

    public function decodeSpriteFromZipFile(zipData:ByteArray, whenDone:Function, fail:Function = null):void {
        decodeFromZipFile(zipData, function(spr:WitSprite):void {
            function imagesDecoded():void {
                spr.showCostume(spr.currentCostumeIndex);
                whenDone(spr);
            }

            if (spr) {
                if (spr.spriteInfo.deviceID && spr.spriteInfo.deviceID.length > 0) {
                    if (!matchUSB(spr.spriteInfo.deviceID)) {
                        if (CONFIG::editor) {
                            DialogBox.notify('U盘不正确', '此文件已加密，请使用正确的U盘', TempStatic.stagePart.stage);
                        }
                        fail();
                        return;
                    }
                }
                decodeAllImages([spr], imagesDecoded, fail);
            }
            else if (fail != null) fail();
        });
    }

    private function matchUSB(id:String):Boolean {
        var arr:Array = [];
        for each (var usb:String in USBUtil.getUSBInfo().split('\n')) {
            arr = arr.concat(usb.split('\\'));
        }
        var matched:Boolean = false;
        id = StringUtil.trim(id);
        for each (var str:String in arr) {
            str = StringUtil.trim(str);
            if (str == id) {
                return true;
            }
        }
        return matched;
    }

    private function decodeFromZipFile(zipData:ByteArray, callback:Function):void {
        var jsonData:String;
        images = [];
        sounds = [];

        var zip:FZip = new FZip();
        zip.addEventListener(Event.COMPLETE, function(evt:Event):void {
            var num:uint = zip.getFileCount();
            var f:FZipFile;
            for (var i:uint=0; i<num; i++) {
                f = zip.getFileAt(i);
                if (!f) continue;
                var fName:String = f.filename;
                if (fName.indexOf('__MACOSX') > -1) continue; // skip MacOS meta info in zip file
                var fIndex:int = int(integerName(fName));
                var contents:ByteArray = f.content;
                if (fName.slice(-4) == '.gif') images[fIndex] = contents;
                if (fName.slice(-4) == '.jpg') images[fIndex] = contents;
                if (fName.slice(-4) == '.png') images[fIndex] = contents;
                if (fName.slice(-4) == '.svg') images[fIndex] = contents;
                if (fName.slice(-4) == '.wav') sounds[fIndex] = contents;
                if (fName.slice(-4) == '.mp3') sounds[fIndex] = contents;
                if (fName.slice(-5) == '.json') jsonData = contents.readUTFBytes(contents.length);
                if (fName.slice(-3) == '.kp') jsonData = contents.readUTFBytes(contents.length);
                if (fName.slice(-4) == '.wit') jsonData = contents.readUTFBytes(contents.length);
            }

            if (jsonData == null) {
                callback(null);
                return;
            }
            var jsonObj:Object = JSON.parse(jsonData);
            if (jsonObj.hasOwnProperty("info") && jsonObj.info.hasOwnProperty("deviceID")) {
                if (!matchUSB(jsonObj.info.deviceID)) {
                    if (CONFIG::editor) {
                        DialogBox.notify('U盘不正确', '此文件已加密，请使用正确的U盘', TempStatic.stagePart.stage);
                    }
                    callback(null);
                    return;
                }
            }
            if (jsonObj['kpchildren']) {
                jsonObj['children'] = jsonObj['kpchildren'];
                delete jsonObj['kpchildren'];
            }
            if (jsonObj['apchildren']) {
                jsonObj['children'] = jsonObj['apchildren'];
                delete jsonObj['apchildren'];
            }
            if (jsonObj['wpchildren']) {
                jsonObj['children'] = jsonObj['wpchildren'];
                delete jsonObj['wpchildren'];
            }
            if (jsonObj['children']) { // project JSON
                var proj:WitStage = new WitStage();
                proj.readJSON(jsonObj);
                if (proj.penLayerID >= 0) proj.penLayerPNG = images[proj.penLayerID];
                else if (proj.penLayerMD5) proj.penLayerPNG = images[0];
                installImagesAndSounds(proj.allObjects());
                callback(proj);
                return;
            }
            if (jsonObj['direction'] != null) { // sprite JSON
                var sprite:WitSprite = new WitSprite();
                sprite.readJSON(jsonObj);
                sprite.instantiateFromJSON(TempStatic.witStage);
                installImagesAndSounds([sprite]);
                callback(sprite);
                return;
            }
            callback(null);
        });
        zip.loadBytes(zipData);
    }

    private function integerName(s:String):String {
        // Return the substring of digits preceding the last '.' in the given string.
        // For example integerName('123.jpg') -> '123'.
        const digits:String = '1234567890';
        var end:int = s.lastIndexOf('.');
        if (end < 0) end = s.length;
        var start:int = end - 1;
        if (start < 0) return s;
        while ((start >= 0) && (digits.indexOf(s.charAt(start)) >= 0)) start--;
        return s.slice(start + 1, end);
    }

    private function installImagesAndSounds(objList:Array):void {
        // Install the images and sounds for the given list of ScratchObj objects.
        for each (var obj:WitObj in objList) {
            for each (var c:WitCostume in obj.costumes) {
                if (images[c.baseLayerID] != undefined) c.baseLayerData = images[c.baseLayerID];
                if (images[c.textLayerID] != undefined) c.textLayerData = images[c.textLayerID];
            }
            for each (var snd:WitSound in obj.sounds) {
                var sndData:* = sounds[snd.soundID];
                if (sndData) {
                    snd.soundData = sndData;
                    snd.convertMP3IfNeeded();
                }
            }
        }
    }

    public function decodeAllImages(objList:Array, whenDone:Function, fail:Function = null):void {
        // Load all images in all costumes from their image data, then call whenDone.
        function imageDecoded():void {
            for each (var o:* in imageDict) {
                if (o == 'loading...') return; // not yet finished loading
            }
            allImagesLoaded();
        }

        var error:Boolean = false;

        function decodeError():void {
            if (error) return;
            error = true;
            if (fail != null) fail();
        }

        function allImagesLoaded():void {
            if (error) {
                return;
            }
            for each (c in allCostumes) {
                if ((c.baseLayerData != null) && (c.baseLayerBitmap == null)) {
                    var img:* = imageDict[c.baseLayerData];
                    if (img is BitmapData) c.baseLayerBitmap = img;
                    if (img is SVGElement) c.setSVGRoot(img, false);
                }
                if ((c.textLayerData != null) && (c.textLayerBitmap == null)) c.textLayerBitmap = imageDict[c.textLayerData];
            }
            for each (c in allCostumes) c.generateOrFindComposite(allCostumes);
            whenDone();
        }

        var c:WitCostume;
        var allCostumes:Array = [];
        for each (var o:WitObj in objList) {
            for each (c in o.costumes) {
                if (c.baseLayerData && c.baseLayerData.length == 0) continue;
                allCostumes.push(c);
            }
        }
        var imageDict:Dictionary = new Dictionary(); // maps image data to BitmapData
        for each (c in allCostumes) {
            if ((c.baseLayerData != null) && (c.baseLayerBitmap == null)) {
                if (WitCostume.isSVGData(c.baseLayerData)) decodeSVG(c.baseLayerData, imageDict, imageDecoded);
                else decodeImage(c.baseLayerData, imageDict, imageDecoded, decodeError);
            }
            if ((c.textLayerData != null) && (c.textLayerBitmap == null)) decodeImage(c.textLayerData, imageDict, imageDecoded, decodeError);
        }
        imageDecoded(); // handles case when there were no images to load
    }

    private function decodeImage(imageData:ByteArray, imageDict:Dictionary, doneFunction:Function, fail:Function):void {
        function loadDone(e:Event):void {
            imageDict[imageData] = e.target.content.bitmapData;
            doneFunction();
        }

        function loadError(e:Event):void {
            if (fail != null) fail();
        }

        if (imageDict[imageData] != null) return; // already loading or loaded
        if (!imageData || imageData.length == 0) {
            if (fail != null) fail();
            return;
        }
        imageDict[imageData] = 'loading...';
        var loader:Loader = new Loader();
        loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadDone);
        loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, loadError);
        loader.loadBytes(imageData);
    }

    private function decodeSVG(svgData:ByteArray, imageDict:Dictionary, doneFunction:Function):void {
        function loadDone(svgRoot:SVGElement):void {
            imageDict[svgData] = svgRoot;
            doneFunction();
        }

        if (imageDict[svgData] != null) return; // already loading or loaded
        var importer:SVGImporter = new SVGImporter(XML(svgData));
        if (importer.hasUnloadedImages()) {
            imageDict[svgData] = 'loading...';
            importer.loadAllImages(loadDone);
        } else {
            imageDict[svgData] = importer.root;
        }
    }

    //----------------------------
    // Fetch a costume or sound from the server
    //----------------------------

    public function fetchImage(id:String, costumeName:String, width:int, whenDone:Function, otherData:Object = null):URLLoader {
        // Fetch an image asset from the server and call whenDone with the resulting WitCostume.
        var c:WitCostume;

        function gotCostumeData(data:ByteArray):void {
            if (!data) {
                trace('Image not found on server: ' + id);
                return;
            }
            if (WitCostume.isSVGData(data)) {
                if (otherData && otherData.centerX)
                    c = new WitCostume(costumeName, data, otherData.centerX, otherData.centerY, otherData.bitmapResolution);
                else
                    c = new WitCostume(costumeName, data);
                c.baseLayerMD5 = id;
                whenDone(c);
            } else {
                var loader:Loader = new Loader();
                loader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded);
                loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, imageError);
                loader.loadBytes(data);
            }
        }

        function imageError(event:IOErrorEvent):void {
            trace('ProjectIO failed to load: ' + id);
        }

        function imageLoaded(e:Event):void {
            if (otherData && otherData.centerX)
                c = new WitCostume(costumeName, e.target.content.bitmapData, otherData.centerX, otherData.centerY, otherData.bitmapResolution);
            else
                c = new WitCostume(costumeName, e.target.content.bitmapData);
            if (width) c.bitmapResolution = c.baseLayerBitmap.width / width;
            c.baseLayerMD5 = id;
            whenDone(c);
        }

        return Server.instance.getAsset(id, gotCostumeData);
    }

    public function fetchSound(id:String, sndName:String, whenDone:Function):void {
        // Fetch a sound asset from the server and call whenDone with the resulting WitSound.
        function gotSoundData(sndData:ByteArray):void {
            if (!sndData) {
                trace('Sound not found on server: ' + id);
                return;
            }
            var snd:WitSound;
            try {
                snd = new WitSound(sndName, sndData); // try reading data as WAV file
            } catch (e:*) {
            }
            if (snd && (snd.sampleCount > 0)) { // WAV data
                snd.md5 = id;
                whenDone(snd);
            } else { // try to read data as an MP3 file
                MP3Loader.convertToScratchSound(sndName, sndData, whenDone);
            }
        }

        Server.instance.getAsset(id, gotSoundData);
    }

    //----------------------------
    // Download a sprite from the server
    //----------------------------

    public function fetchSprite(md5AndExt:String, whenDone:Function):void {
        // Fetch a sprite with the md5 hash.
        function jsonReceived(data:ByteArray):void {
            if (!data) return;
            spr.readJSON(JSON.parse(data.readUTFBytes(data.length)));
            spr.instantiateFromJSON(TempStatic.witStage);
            fetchSpriteAssets([spr], assetsReceived);
        }

        function assetsReceived(assetDict:Object):void {
            installAssets([spr], assetDict);
            decodeAllImages([spr], done);
        }

        function done():void {
            spr.showCostume(spr.currentCostumeIndex);
            spr.setDirection(spr.direction);
            whenDone(spr);
        }

        var spr:WitSprite = new WitSprite();
        Server.instance.getAsset(md5AndExt, jsonReceived);
    }

    private function fetchSpriteAssets(objList:Array, whenDone:Function):void {
        // Download all media for the given list of ScratchObj objects.
        function assetReceived(md5:String, data:ByteArray):void {
            if (!data) {
                trace('missing sprite asset: ' + md5);
            }
            assetDict[md5] = data;
            assetCount++;
            if (assetCount == assetsToFetch.length) whenDone(assetDict);
        }

        var assetDict:Object = {};
        var assetCount:int = 0;
        var assetsToFetch:Array = collectAssetsToFetch(objList);
        for each (var md5:String in assetsToFetch) fetchAsset(md5, assetReceived);
    }

    private function collectAssetsToFetch(objList:Array):Array {
        // Return list of MD5's for all project assets.
        var list:Array = [];
        for each (var obj:WitObj in objList) {
            for each (var c:WitCostume in obj.costumes) {
                if (list.indexOf(c.baseLayerMD5) < 0) list.push(c.baseLayerMD5);
                if (c.textLayerMD5) {
                    if (list.indexOf(c.textLayerMD5) < 0) list.push(c.textLayerMD5);
                }
            }
            for each (var snd:WitSound in obj.sounds) {
                if (list.indexOf(snd.md5) < 0) list.push(snd.md5);
            }
        }
        return list;
    }

    private function installAssets(objList:Array, assetDict:Object):void {
        var data:ByteArray;
        for each (var obj:WitObj in objList) {
            for each (var c:WitCostume in obj.costumes) {
                data = assetDict[c.baseLayerMD5];
                if (data) c.baseLayerData = data;
                else {
                    // Asset failed to load so use an empty costume
                    // BUT retain the original MD5 and don't break the reference to the costume that failed to load.
                    var origMD5:String = c.baseLayerMD5;
                    c.baseLayerData = WitCostume.emptySVG();
                    c.baseLayerMD5 = origMD5;
                }
                if (c.textLayerMD5) c.textLayerData = assetDict[c.textLayerMD5];
            }
            for each (var snd:WitSound in obj.sounds) {
                data = assetDict[snd.md5];
                if (data) {
                    snd.soundData = data;
                    snd.convertMP3IfNeeded();
                } else {
                    snd.soundData = WAVFile.empty();
                }
            }
        }
    }

    public function fetchAsset(md5:String, whenDone:Function):URLLoader {
        return Server.instance.getAsset(md5, function (data:*):void {
            whenDone(md5, data);
        });
    }

    //----------------------------
    // Record unique images and sounds
    //----------------------------

    private function recordImagesAndSounds(objList:Array, uploading:Boolean, proj:WitStage = null):void {
        var recordedAssets:Object = {};
        images = [];
        sounds = [];

        if (CONFIG::editor) {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_CACHED_BITMAPS, null, true));
        }
        if (!uploading && proj) proj.penLayerID = recordImage(proj.penLayerPNG, proj.penLayerMD5, recordedAssets, uploading);

        for each (var obj:WitObj in objList) {
            if (obj is WitSprite) {
                if (TempStatic.usbID.length > 0) {
                    (obj as WitSprite).spriteInfo.deviceID = TempStatic.usbID;
                }
            }
            for each (var c:WitCostume in obj.costumes) {
                c.prepareToSave(); // encodes image and computes md5 if necessary
                c.baseLayerID = recordImage(c.baseLayerData, c.baseLayerMD5, recordedAssets, uploading);
                if (c.textLayerBitmap) {
                    c.textLayerID = recordImage(c.textLayerData, c.textLayerMD5, recordedAssets, uploading);
                }
            }
            for each (var snd:WitSound in obj.sounds) {
                snd.prepareToSave(); // compute md5 if necessary
                snd.soundID = recordSound(snd, snd.md5, recordedAssets, uploading);
            }
        }
    }

    public function convertSqueakSounds(scratchObj:WitObj, done:Function):void {
        // Pre-convert any Squeak sounds (asynch, with a progress bar) before saving a project.
        // Note: If this is not called before recordImagesAndSounds(), sounds will
        // be converted synchronously, but there may be a long delay without any feedback.
        function convertASound():void {
            if (i < soundsToConvert.length) {
                var sndToConvert:WitSound = soundsToConvert[i++] as WitSound;
                sndToConvert.prepareToSave();
                setTimeout(convertASound, 50);
            } else {
                // Note: Must get user click in order to proceed with saving...
                DialogBox.notify('', 'Sounds converted', TempStatic.stagePart.stage, false, soundsConverted);
            }
        }

        function soundsConverted(ignore:*):void {
            done()
        }

        var soundsToConvert:Array = [];
        for each (var obj:WitObj in scratchObj.allObjects()) {
            for each (var snd:WitSound in obj.sounds) {
                if ('squeak' == snd.format) soundsToConvert.push(snd);
            }
        }
        var i:int;
        if (soundsToConvert.length > 0) {
            setTimeout(convertASound, 50);
        } else done();
    }

    private function recordImage(img:*, md5:String, recordedAssets:Object, uploading:Boolean):int {
        var id:int = recordedAssetID(md5, recordedAssets, uploading);
        if (id > -2) return id; // image was already added
        images.push([md5, img]);
        id = images.length - 1;
        recordedAssets[md5] = id;
        return id;
    }

    private function recordedAssetID(md5:String, recordedAssets:Object, uploading:Boolean):int {
        var id:* = recordedAssets[md5];
        return id != undefined ? id : -2;
    }

    private function recordSound(snd:WitSound, md5:String, recordedAssets:Object, uploading:Boolean):int {
        var id:int = recordedAssetID(md5, recordedAssets, uploading);
        if (id > -2) return id; // image was already added
        sounds.push([md5, snd.soundData]);
        id = sounds.length - 1;
        recordedAssets[md5] = id;
        return id;
    }
}
}
