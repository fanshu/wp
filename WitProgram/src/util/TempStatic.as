package util {
import flash.display.Stage;
import flash.utils.ByteArray;

import wp.editor.extensions.ExtensionManager;
import wp.editor.interpreter.Interpreter;
import wp.editor.ui.parts.StagePart;
import wp.editor.wit.WitRuntime;
import wp.editor.wit.WitStage;

public class TempStatic {
    public static var stage:Stage;
    public static var usesUserNameBlock:Boolean = false;
    public static var loadInProgress:Boolean;
    public static var extensionManager:ExtensionManager;
    public static var gh:GestureHandler;
    public static var interp:Interpreter;
    public static var runtime:WitRuntime;
    public static var projectName:String = "";
    public static var editMode:Boolean; // true when project editor showing, false when only the player is showing
    public static var usbID:String = "";

    public static var stagePart:StagePart;
    public static var witStage:WitStage;

    // -----------------------------
    // Project Reverting
    //------------------------------

    public static var originalProj:ByteArray;
    public static var revertUndo:ByteArray;

    public static function windowTitle(fileName:String = ""):void {
        if (CONFIG::editor) {
            var title:String = "维度积木软件 V2.4";
            if (fileName.length > 0) {
                title += ' - ' + fileName;
            }
            stage['nativeWindow'].title = title;
        }
    }

    public static function fixFileName(s:String):String {
        // Replace illegal characters in the given string with dashes.
        const illegal:String = '\\/:*?"<>|%';
        var result:String = '';
        for (var i:int = 0; i < s.length; i++) {
            var ch:String = s.charAt(i);
            if ((i == 0) && ('.' == ch)) ch = '-'; // don't allow leading period
            result += (illegal.indexOf(ch) > -1) ? '-' : ch;
        }
        return result;
    }
}
}
