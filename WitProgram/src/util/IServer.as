// Interface to the WitProgram website API's
//
// Note: All operations call the callback function with the result
// if the operation succeeded or null if it failed.

package util {
import flash.net.URLLoader;

public interface IServer {
    // -----------------------------
    // Asset API
    //------------------------------
    function getAsset(md5:String, callback:Function):URLLoader;

    function getMediaLibrary(type:String, callback:Function):URLLoader;

    function getThumbnail(md5:String, w:int, h:int, callback:Function):URLLoader;
}
}
