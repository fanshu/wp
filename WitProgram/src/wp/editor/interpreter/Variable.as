// A variable is a name-value pair.

package wp.editor.interpreter {
public class Variable {
    public var name:String;
    public var value:*;
    public var watcher:*;
    public var isPersistent:Boolean;

    public function Variable(vName:String, initialValue:*) {
        name = vName;
        value = initialValue;
    }

    public function toJSON(k:String):Object {
        return {
            name: name,
            value: value,
            isPersistent: isPersistent
        };
    }
}
}
