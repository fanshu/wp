package wp.editor.svgeditor.objs {
import wp.editor.svgutils.SVGElement;

public interface ISVGEditable {
    // Returns the SVGElement for this object
    function getElement():SVGElement;

    // Redraws the element
    function redraw(forHitTest:Boolean = false):void;

    // Returns a copy of the current element
    function clone():ISVGEditable;

    // Fixes up the transform and element specific position data
    //function normalize():void;
}
}
