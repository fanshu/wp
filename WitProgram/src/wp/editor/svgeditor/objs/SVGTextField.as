package wp.editor.svgeditor.objs {
import flash.text.AntiAliasType;
import flash.text.TextField;
import flash.text.TextFieldType;

import wp.editor.svgutils.SVGElement;

public class SVGTextField extends TextField implements ISVGEditable {
    private var element:SVGElement;
    private var _editable:Boolean;

    public function SVGTextField(elem:SVGElement) {
        element = elem;
        if (element.text == null) element.text = '';
        _editable = false;
        antiAliasType = AntiAliasType.ADVANCED;
        cacheAsBitmap = true;
        embedFonts = true;
        backgroundColor = 0xFFFFFF;
        multiline = true;
    }

    public function getElement():SVGElement {
        element.transform = transform.matrix.clone();
        return element;
    }

    public function redraw(forHitTest:Boolean = false):void {
        var fixup:Boolean = (type == TextFieldType.INPUT && element.text.length < 4);
        var origText:String = element.text;
        if (element.text == "") element.text = " ";
        element.renderTextOn(this);
        element.text = origText;
        if (fixup) width += 25;
    }

    public function clone():ISVGEditable {
        var copy:SVGTextField = new SVGTextField(element.clone());
        copy.transform.matrix = transform.matrix.clone();
        copy.selectable = false;
        copy.redraw();
        return copy as ISVGEditable;
    }
}
}
