package wp.editor.svgeditor.objs {
import flash.geom.Point;

public class PathDrawContext {
    public var cmds:Array;
    public var acurve:Boolean;
    public var lastcxy:Point;
    public var adjust:Boolean;

    function PathDrawContext() {
        cmds = [];
        acurve = false;
        adjust = false;
    }
}
}
