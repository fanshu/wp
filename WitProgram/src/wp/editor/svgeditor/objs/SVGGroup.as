package wp.editor.svgeditor.objs {
import flash.display.DisplayObject;
import flash.display.Sprite;

import wp.editor.svgutils.SVGElement;

public class SVGGroup extends Sprite implements ISVGEditable {
    private var element:SVGElement;

    public function SVGGroup(elem:SVGElement) {
        super();
        element = elem;
    }

    public function getElement():SVGElement {
        element.subElements = getSubElements();
        element.transform = transform.matrix;
        return element;
    }

    public function redraw(forHitTest:Boolean = false):void {
        if (element.transform) transform.matrix = element.transform;

        // Redraw the sub elements
        for (var i:uint = 0; i < numChildren; ++i) {
            var child:DisplayObject = getChildAt(i);
            if (child is ISVGEditable) {
                (child as ISVGEditable).redraw();
            }
        }
    }

    private function getSubElements():Array {
        var elements:Array = [];
        for (var i:uint = 0; i < numChildren; ++i) {
            var child:DisplayObject = getChildAt(i);
            if (child is ISVGEditable) {
                elements.push((child as ISVGEditable).getElement());
            }
        }
        return elements;
    }

    public function clone():ISVGEditable {
        var copy:SVGGroup = new SVGGroup(element.clone());
        (copy as DisplayObject).transform.matrix = transform.matrix.clone();

        var elements:Array = [];
        for (var i:uint = 0; i < numChildren; ++i) {
            var child:DisplayObject = getChildAt(i);
            if (child is ISVGEditable) {
                copy.addChild((child as ISVGEditable).clone() as DisplayObject);
            }
        }

        copy.redraw();
        return copy;
    }
}
}
