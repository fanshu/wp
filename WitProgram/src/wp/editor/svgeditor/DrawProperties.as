package wp.editor.svgeditor {
public class DrawProperties {
    // colors
    public var rawColor:uint = 0xFF000000;
    public var rawSecondColor:uint = 0xFFFFFFFF;

    public function set color(c:uint):void {
        rawColor = c
    }

    public function get color():uint {
        return rawColor & 0xFFFFFF
    }

    public function get alpha():Number {
        return ((rawColor >> 24) & 0xFF) / 0xFF
    }

    public function set secondColor(c:uint):void {
        rawSecondColor = c
    }

    public function get secondColor():uint {
        return rawSecondColor & 0xFFFFFF
    }

    public function get secondAlpha():Number {
        return ((rawSecondColor >> 24) & 0xFF) / 0xFF
    }

    // stroke
    public var smoothness:Number = 1;
    private var rawStrokeWidth:Number = 1;
    private var rawEraserWidth:Number = 4;

    public function set strokeWidth(w:int):void {
        rawStrokeWidth = w
    }

    public function set eraserWidth(w:int):void {
        rawEraserWidth = w
    }

    public function get strokeWidth():int {
        return adjustWidth(rawStrokeWidth);
    }

    public function get eraserWidth():int {
        return adjustWidth(rawEraserWidth);
    }

    private static function adjustWidth(raw:int):int {
        if (WitProgram.editorPart && WitProgram.editorPart.imagesPart && (WitProgram.editorPart.imagesPart.editor is SVGEdit)) return raw;

        // above 10, use Squeak brush sizes
        const n:Number = Math.max(1, Math.round(raw));
        switch (n) {
            case 11:
                return 13;
            case 12:
                return 19;
            case 13:
                return 29;
            case 14:
                return 47;
            case 15:
                return 75;
            default:
                return n;
        }
    }

    // fill
    public var fillType:String = 'solid'; // solid, linearHorizontal, linearVertical, radial
    public var filledShape:Boolean = false;

    // font
    public var fontName:String = 'Helvetica';
}
}
