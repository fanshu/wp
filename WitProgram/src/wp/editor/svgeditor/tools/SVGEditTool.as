package wp.editor.svgeditor.tools {
import flash.events.Event;
import flash.events.MouseEvent;

import wp.editor.svgeditor.ImageEdit;
import wp.editor.svgeditor.Selection;
import wp.editor.svgeditor.objs.ISVGEditable;

public class SVGEditTool extends SVGTool {
    protected var object:ISVGEditable;
    private var editTag:Array;

    public function SVGEditTool(ed:ImageEdit, tag:* = null) {
        super(ed);
        touchesContent = true;
        object = null;
        editTag = (tag is String) ? [tag] : tag;
    }

    public function editSelection(s:Selection):void {
        if (s && s.getObjs().length == 1)
            setObject(s.getObjs()[0] as ISVGEditable);
    }

    public function setObject(obj:ISVGEditable):void {
        edit(obj, null);
    }

    public function getObject():ISVGEditable {
        return object;
    }

    // When overriding this method, usually an event handler will be added with a higher priority
    // so that the mouseDown method below is overridden
    protected function edit(obj:ISVGEditable, event:MouseEvent):void {
        if (obj == object) return;

        if (object) {
            //(object as DisplayObject).filters = [];
        }

        if (obj && (!editTag || editTag.indexOf(obj.getElement().tag) > -1)) {
            object = obj;

            if (object) {
                //(object as DisplayObject).filters = [new GlowFilter(0x28A5DA)];
            }
        } else {
            object = null;
        }
        dispatchEvent(new Event('select'));
    }

    override protected function init():void {
        super.init();
        editor.getContentLayer().addEventListener(MouseEvent.MOUSE_DOWN, mouseDown, false, 0, true);
    }

    override protected function shutdown():void {
        editor.getContentLayer().removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
        super.shutdown();

        if (object) {
            setObject(null);
        }
    }

    public function mouseDown(event:MouseEvent):void {
        var obj:ISVGEditable = getEditableUnderMouse(!(this is PathEditTool));
        currentEvent = event;
        edit(obj, event);
        currentEvent = null;

        event.stopPropagation();
    }
}
}
