package wp.editor.svgeditor.tools {
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Point;

import wp.editor.svgeditor.ImageEdit;
import wp.editor.svgeditor.objs.SVGShape;

public class PathEndPoint extends Sprite {
    private var editor:ImageEdit;
    private var shape:SVGShape;
    private var strokeWidth:Number;

    public function PathEndPoint(ed:ImageEdit, a:SVGShape, p:Point) {
        editor = ed;
        shape = a;
        x = p.x;
        y = p.y;

        addEventListener(MouseEvent.MOUSE_OVER, toggleHighlight, false, 0, true);
        addEventListener(MouseEvent.MOUSE_OUT, toggleHighlight, false, 0, true);
        addEventListener(MouseEvent.MOUSE_DOWN, proxyEvent, false, 0, true);
        addEventListener(MouseEvent.MOUSE_MOVE, showOrb, false, 0, true);

        graphics.clear();
        graphics.beginFill(0, 0);
        graphics.drawCircle(0, 0, 10);
        graphics.endFill();
    }

    private function toggleHighlight(e:MouseEvent):void {
        PathEndPointManager.toggleEndPoint(e.type == MouseEvent.MOUSE_OVER, new Point(x, y));
        editor.getWorkArea().dispatchEvent(e);

        if (e.type == MouseEvent.MOUSE_OVER) {
            strokeWidth = editor.getShapeProps().strokeWidth;
        }
    }

    private function proxyEvent(e:MouseEvent):void {
        editor.getCanvasLayer().dispatchEvent(e);
        e.stopImmediatePropagation();
    }

    private function showOrb(e:MouseEvent):void {
        var w:Number = (strokeWidth + shape.getElement().getAttribute('stroke-width', 1)) / 4;
        PathEndPointManager.updateOrb((new Point(mouseX, mouseY)).length < w);
    }
}
}
