package wp.editor.svgeditor.tools {
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Point;

import wp.editor.svgeditor.ImageCanvas;
import wp.editor.svgeditor.ImageEdit;
import wp.editor.svgeditor.objs.ISVGEditable;
import wp.editor.wit.WitObj;

public class SVGCreateTool extends SVGTool {
    protected var newObject:ISVGEditable;
    protected var contentLayer:Sprite;
    protected var isQuick:Boolean;
    private var lastPos:Point;

    public function SVGCreateTool(svgEditor:ImageEdit, quick:Boolean = true) {
        super(svgEditor);
        contentLayer = editor.getContentLayer();
        isQuick = quick;
    }

    // Pretend these are abstract ;)
    // Mouse down
    protected function mouseDown(p:Point):void {
    }

    // Mouse move
    protected function mouseMove(p:Point):void {
    }

    // Mouse up
    protected function mouseUp(p:Point):void {
    }

    public function getObject():ISVGEditable {
        return newObject;
    }

    override protected function init():void {
        super.init();
        addEventHandlers();
    }

    override protected function shutdown():void {
        //editor.toggleZoomUI(true);
        removeEventHandlers();
        super.shutdown();
        newObject = null;
        contentLayer = null;
    }

    override public function cancel():void {
        // Remove the object if it was added to the display list
        if (newObject && newObject is DisplayObject) {
            var dObj:DisplayObject = newObject as DisplayObject;
            if (dObj.parent) {
                dObj.parent.removeChild(dObj);
            }
            newObject = null;
        }

        super.cancel();
    }

    public function eventHandler(e:MouseEvent = null):void {
        if (!contentLayer) return;
        var p:Point = new Point(contentLayer.mouseX, contentLayer.mouseY);
        p.x = Math.min(WitObj.STAGEW, Math.max(0, p.x));
        p.y = Math.min(WitObj.STAGEH, Math.max(0, p.y));
        currentEvent = e;

        if (e.type == MouseEvent.MOUSE_DOWN) {
            //editor.toggleZoomUI(false);
            mouseDown(p);
            if (isQuick && !isShuttingDown) {
                // Add the mouse event handlers
                STAGE.addEventListener(MouseEvent.MOUSE_MOVE, eventHandler, false, 0, true);
                STAGE.addEventListener(MouseEvent.MOUSE_UP, eventHandler, false, 0, true);
            }
            lastPos = p;
        } else if (e.type == MouseEvent.MOUSE_MOVE) {
            mouseMove(p);
            lastPos = p;
        } else if (e.type == MouseEvent.MOUSE_UP) {
            //editor.toggleZoomUI(true);
            if (!stage) return;

            // If the mouse came up outside of the canvas, use the last mouse position within the canvas
            if (!editor.getCanvasLayer().hitTestPoint(STAGE.mouseX, STAGE.mouseY, true))
                p = lastPos;

            mouseUp(p);
            if (isQuick) editor.endCurrentTool(newObject);
        }
    }

    private function addEventHandlers():void {
        editor.getCanvasLayer().addEventListener(MouseEvent.MOUSE_DOWN, eventHandler, false, 0, true);
        if (!isQuick) {
            STAGE.addEventListener(MouseEvent.MOUSE_MOVE, eventHandler, false, 0, true);
            STAGE.addEventListener(MouseEvent.MOUSE_UP, eventHandler, false, 0, true);
        }
    }

    private function removeEventHandlers():void {
        editor.getCanvasLayer().removeEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
        STAGE.removeEventListener(MouseEvent.MOUSE_MOVE, eventHandler);
        STAGE.removeEventListener(MouseEvent.MOUSE_UP, eventHandler);
    }
}
}
