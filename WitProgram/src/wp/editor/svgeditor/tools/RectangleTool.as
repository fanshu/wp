package wp.editor.svgeditor.tools {
import flash.display.DisplayObject;
import flash.geom.Point;

import wp.editor.svgeditor.DrawProperties;
import wp.editor.svgeditor.ImageEdit;
import wp.editor.svgeditor.objs.SVGShape;
import wp.editor.svgutils.SVGElement;

public final class RectangleTool extends SVGCreateTool {
    private var createOrigin:Point;
    private var newElement:SVGElement;

    public function RectangleTool(svgEditor:ImageEdit) {
        super(svgEditor);
    }

    override protected function mouseDown(p:Point):void {
        // If we're trying to draw with invisible settings then bail
        var props:DrawProperties = editor.getShapeProps();
        if (props.alpha == 0)
            return;

        createOrigin = p;

        newElement = new SVGElement('rect', null);
        if (props.filledShape) {
            newElement.setShapeFill(props);
            newElement.setAttribute('stroke', 'none');
        }
        else {
            newElement.setShapeStroke(props);
            newElement.setAttribute('fill', 'none');
        }

        newObject = new SVGShape(newElement);
        contentLayer.addChild(newObject as DisplayObject);
    }

    override protected function mouseMove(p:Point):void {
        if (!createOrigin) return;

        var ofs:Point = createOrigin.subtract(p);
        var w:Number = Math.abs(ofs.x);
        var h:Number = Math.abs(ofs.y);

        // Shift key makes a square
        if (currentEvent.shiftKey) {
            w = h = Math.max(w, h);
            p.x = createOrigin.x + (ofs.x < 0 ? w : -w);
            p.y = createOrigin.y + (ofs.y < 0 ? h : -h);
        }

        newElement.setAttribute('x', Math.min(p.x, createOrigin.x));
        newElement.setAttribute('y', Math.min(p.y, createOrigin.y));
        newElement.setAttribute('width', w);
        newElement.setAttribute('height', h);
//        newElement.setAttribute('wit-type', 'backdrop-fill');
        newElement.updatePath();
        newObject.redraw();
    }
}
}
