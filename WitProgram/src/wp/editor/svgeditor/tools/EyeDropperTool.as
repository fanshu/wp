package wp.editor.svgeditor.tools {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.events.MouseEvent;
import flash.geom.Matrix;
import flash.geom.Point;

import wp.editor.svgeditor.BitmapEdit;
import wp.editor.svgeditor.ImageEdit;
import wp.editor.svgeditor.objs.ISVGEditable;

public final class EyeDropperTool extends SVGTool {
    public function EyeDropperTool(svgEditor:ImageEdit) {
        super(svgEditor);
        touchesContent = true;
        cursorBMName = 'eyedropperOff';
        cursorHotSpot = new Point(14, 20);
    }

    override protected function init():void {
        super.init();
        editor.getWorkArea().addEventListener(MouseEvent.MOUSE_DOWN, mouseDown, false, 0, true);
    }

    override protected function shutdown():void {
        editor.getWorkArea().removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
        mouseUp();
        super.shutdown();
    }

    private function mouseDown(event:MouseEvent):void {
        currentEvent = event;
        grabColor();

        STAGE.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove, false, 0, true);
        STAGE.addEventListener(MouseEvent.MOUSE_UP, mouseUp, false, 0, true);
        event.stopPropagation();
    }

    private function mouseMove(event:MouseEvent):void {
        currentEvent = event;
        grabColor();
    }

    private function mouseUp(event:MouseEvent = null):void {
        STAGE.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
        STAGE.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
    }

    private function grabColor():void {
        var c:uint;
        var obj:ISVGEditable;
        if (editor is BitmapEdit) {
            var bmap:Bitmap = editor.getWorkArea().bitmapLayer;
            var p:Point = editor.getWorkArea().bitmapMousePoint();
            c = bmap.bitmapData.getPixel32(p.x, p.y);
        }
        else if ((obj = getEditableUnderMouse(false)) != null) {
            var dObj:DisplayObject = obj as DisplayObject;
            var b:BitmapData = new BitmapData(1, 1, true, 0);
            var m:Matrix = new Matrix();
            m.translate(-dObj.mouseX, -dObj.mouseY);
            b.draw(dObj, m);
            c = b.getPixel32(0, 0);
        }

        // only grab the color if it's not transparent
        // TODO: Should we ever handle colors with partial alpha?
        if (c & 0xFF000000) {
            editor.setCurrentColor(c & 0xFFFFFF, 1);
        }
    }
}
}
