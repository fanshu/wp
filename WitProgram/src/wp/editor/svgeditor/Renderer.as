package wp.editor.svgeditor {
import flash.display.*;

import wp.editor.svgeditor.objs.SVGBitmap;
import wp.editor.svgeditor.objs.SVGGroup;
import wp.editor.svgeditor.objs.SVGShape;
import wp.editor.svgeditor.objs.SVGTextField;
import wp.editor.svgutils.*;

public class Renderer {

    static public function renderToSprite(spr:Sprite, rootSVG:SVGElement):void {
        // Populate the given sprite with DisplayObjects (e.g. SVGBitmap) for the subelements of rootSVG.
        if (!rootSVG) return;
        for each (var el:SVGElement in rootSVG.subElements) {
            appendElementToSprite(el, spr);
        }
    }

    static private function appendElementToSprite(el:SVGElement, spr:Sprite):void {
        // Append a DisplayObject for the given element to the given sprite.
        if ('g' == el.tag) {
            var groupSprite:SVGGroup = new SVGGroup(el);
            renderToSprite(groupSprite, el);
            if (el.transform) groupSprite.transform.matrix = el.transform;
            spr.addChild(groupSprite);
        } else if ('image' == el.tag) {
            var bmp:SVGBitmap = new SVGBitmap(el);
            bmp.redraw();
            if (el.transform) bmp.transform.matrix = el.transform;
            spr.addChild(bmp);
        } else if ('text' == el.tag) {
            var tf:SVGTextField = new SVGTextField(el);
            tf.selectable = false;
            el.renderTextOn(tf);
            if (el.transform) tf.transform.matrix = el.transform;
            spr.addChild(tf);
        } else if (el.path) {
            var shape:SVGShape = new SVGShape(el);
            shape.redraw();
            if (el.transform) shape.transform.matrix = el.transform;
            spr.addChild(shape);
        }
    }
}
}
