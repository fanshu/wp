package wp.editor.uiwidgets {
import flash.display.*;
import flash.text.*;

import gnu.as3.gettext.gettext;

import util.Resources;

public class VariableSettings extends Sprite {

    public var isLocal:Boolean;

    public var isList:Boolean;
    private var isStage:Boolean;

    protected var globalButton:IconButton;
    private var globalLabel:TextField;
    protected var localButton:IconButton;
    protected var localLabel:TextField;

    public function VariableSettings(isList:Boolean, isStage:Boolean) {
        this.isList = isList;
        this.isStage = isStage;
        addLabels();
        addButtons();
        fixLayout();
        updateButtons();
    }

    protected function addLabels():void {
        const _:Function = gettext;
        addChild(localLabel = Resources.makeLabel(
                _('For this sprite only'), CSS.normalTextFormat));

        addChild(globalLabel = Resources.makeLabel(
                _('For all sprites'), CSS.normalTextFormat));
    }

    protected function addButtons():void {
        function setLocal(b:IconButton):void {
            isLocal = true;
            updateButtons()
        }

        function setGlobal(b:IconButton):void {
            isLocal = false;
            updateButtons()
        }

        addChild(localButton = new IconButton(setLocal, null));
        addChild(globalButton = new IconButton(setGlobal, null));
    }

    protected function updateButtons():void {
        localButton.setOn(isLocal);
        localButton.setDisabled(false, 0.2);
        localLabel.alpha = 1;
        globalButton.setOn(!isLocal);
    }

    protected function fixLayout():void {
        var nextX:int = 0;
        var baseY:int = 10;

        globalButton.x = nextX;
        globalButton.y = baseY + 3;
        globalLabel.x = (nextX += 16);
        globalLabel.y = baseY;

        nextX += globalLabel.textWidth + 20;

        localButton.x = nextX;
        localButton.y = baseY + 3;
        localLabel.x = (nextX += 16);
        localLabel.y = baseY;

        nextX = 15;
        if (isStage) {
            localButton.visible = false;
            localLabel.visible = false;
            globalButton.x = nextX;
            globalLabel.x = nextX + 16;
        }
    }

    protected function drawLine():void {
        var lineY:int = 36;
        var w:int = getRect(this).width;
        if (isStage) w += 10;
        var g:Graphics = graphics;
        g.clear();
        g.beginFill(0xD0D0D0);
        g.drawRect(0, lineY, w, 1);
        g.beginFill(0x909090);
        g.drawRect(0, lineY + 1, w, 1);
        g.endFill();
    }
}
}
