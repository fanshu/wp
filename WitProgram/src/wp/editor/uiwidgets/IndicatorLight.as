// A simple indicator light with a color and a tooltip. Used to show extension state.
package wp.editor.uiwidgets {
import flash.display.*;

public class IndicatorLight extends Sprite {
    public var target:*;

    private var color:int;
    private var msg:String = '';

    public function IndicatorLight(target:* = null) {
        this.target = target;
        redraw();
    }

    public function setColorAndMsg(color:int, msg:String):void {
        if ((color == this.color) && (msg == this.msg)) return; // no change
        this.color = color;
        this.msg = msg;
        SimpleTooltips.add(this, {text: msg, direction: 'bottom'});
        redraw();
    }

    private function redraw():void {
        const borderColor:int = 0x505050;
        var g:Graphics = graphics;
        g.clear();
        g.lineStyle(1, borderColor);
        g.beginFill(color);
        g.drawCircle(7, 7, 6);
        g.endFill();
    }
}
}
