package wp.editor.uiwidgets {
import flash.text.TextField;
import flash.text.TextFormat;
import flash.utils.Dictionary;

import util.StringUtils;

public class VariableTextField extends TextField {
    private var originalText:String;

    override public function set text(value:String):void {
        throw Error('Call setText() instead');
    }

    public function setText(t:String, context:Dictionary = null):void {
        originalText = t;
        applyContext(context);
    }

    // Re-substitutes values from this new context into the original text.
    // This context must be a complete context, not just the fields that have changed.
    public function applyContext(context:Dictionary):void {
        // Assume that the whole text field uses the same format since there's no guarantee how indices will map.
        var oldFormat:TextFormat = this.getTextFormat();
        super.text = context ? StringUtils.substitute(originalText, context) : originalText;
        setTextFormat(oldFormat);
    }
}
}
