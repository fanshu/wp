package wp.editor.soundedit {
import flash.display.*;

public class SoundLevelMeter extends Sprite {

    private var w:int, h:int;
    private var bar:Shape;
    private var recentMax:Number = 0;

    public function SoundLevelMeter(barWidth:int, barHeight:int) {
        w = barWidth;
        h = barHeight;

        // frame
        graphics.lineStyle(1, CSS.borderColor, 1, true);
        graphics.drawRoundRect(0, 0, w, h, 7, 7);

        // meter bar
        addChild(bar = new Shape());
    }

    public function clear():void {
        recentMax = 0;
        setLevel(0);
    }

    public function setLevel(percent:Number):void {
        recentMax *= 0.85;
        recentMax = Math.max(percent, recentMax);
        drawBar(recentMax);
    }

    private function drawBar(percent:Number):void {
        const red:int = 0xFF0000;
        const yellow:int = 0xFFFF00;
        const green:int = 0xFF00;
        const r:int = 3;

        var g:Graphics = bar.graphics;
        g.clear();

        g.beginFill(red);
        var barH:int = (h - 1) * Math.min(percent, 100) / 100;
        g.drawRoundRect(1, h - barH, w - 1, barH, r, r);

        g.beginFill(yellow);
        barH = h * Math.min(percent, 95) / 100;
        g.drawRoundRect(1, h - barH, w - 1, barH, r, r);

        g.beginFill(green);
        barH = h * Math.min(percent, 70) / 100;
        g.drawRoundRect(1, h - barH, w - 1, barH, r, r);
    }
}
}
