// This is the superclass for both ScratchStage and ScratchSprite,
// containing the variables and methods common to both.
package wp.editor.wit {
import flash.display.Bitmap;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.ColorTransform;
import flash.utils.getDefinitionByName;
import flash.utils.getQualifiedClassName;
import flash.utils.getTimer;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import wp.editor.blocks.Block;

import wp.editor.blocks.BlockIO;
import wp.editor.controllers.ContextEvtConst;
import wp.editor.filters.FilterPack;
import wp.editor.interpreter.Variable;

public class WitObj extends Sprite {
    [Embed(source='/../assets/pop.wav', mimeType='application/octet-stream')]
    protected static var Pop:Class;

    public static const STAGEW:int = 480;
    public static const STAGEH:int = 360;

    private static const _:Function = gettext;

    public var objName:String = 'no name';
    public var variables:Array = [];
    public var lists:Array = [];
    public var scripts:Array = [];
    public var scriptComments:Array = [];
    public var sounds:Array = [];
    public var costumes:Array = [];
    public var currentCostumeIndex:Number;
    public var volume:Number = 100;
    public var instrument:int = 0;
    public var filterPack:FilterPack;
    public var isClone:Boolean;

    public var img:Sprite; // holds a bitmap or svg object, after applying image filters, scale, and rotation
    private var lastCostume:WitCostume;

    // Caches used by the interpreter:
    public var listCache:Object = {};
    public var procCache:Object = {};
    public var varCache:Object = {};

    public function clearCaches():void {
        // Clear the list, procedure, and variable caches for this object.
        listCache = {};
        procCache = {};
        varCache = {};
    }

    public function allObjects():Array {
        return [this]
    }

    public function deleteCostume(c:WitCostume):void {
        if (costumes.length < 2) return; // a sprite must have at least one costume
        var i:int = costumes.indexOf(c);
        if (i < 0) return;
        costumes.splice(i, 1);
        if (currentCostumeIndex >= i) showCostume(currentCostumeIndex - 1);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
    }

    public function deleteSound(snd:WitSound):void {
        var i:int = sounds.indexOf(snd);
        if (i < 0) return;
        sounds.splice(i, 1);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
    }

    public function showCostumeNamed(n:String):void {
        var i:int = indexOfCostumeNamed(n);
        if (i >= 0) showCostume(i);
    }

    public function indexOfCostumeNamed(n:String):int {
        for (var i:int = 0; i < costumes.length; i++) {
            if (WitCostume(costumes[i]).costumeName == n) return i;
        }
        return -1;
    }

    public function showCostume(costumeIndex:Number):void {
        if (isNaNOrInfinity(costumeIndex)) costumeIndex = 0;
        currentCostumeIndex = costumeIndex % costumes.length;
        if (currentCostumeIndex < 0) currentCostumeIndex += costumes.length;
        var c:WitCostume = currentCostume();
        if (c == lastCostume) return; // optimization: already showing that costume
        lastCostume = c.isBitmap() ? c : null; // cache only bitmap costumes for now

        updateImage();
    }

    public function updateCostume():void {
        updateImage();
    }

    public function currentCostume():WitCostume {
        return costumes[Math.round(currentCostumeIndex) % costumes.length];
    }

    public function costumeNumber():int {
        // One-based costume number as seen by user (currentCostumeIndex is 0-based)
        return currentCostumeIndex + 1;
    }

    public function unusedCostumeName(baseName:String = ''):String {
        // Create a unique costume name by appending a number if necessary.
        if (baseName == '') baseName = _((this is WitStage) ? 'backdrop1' : 'costume1');
        var existingNames:Array = [];
        for each (var c:WitCostume in costumes) {
            existingNames.push(c.costumeName.toLowerCase());
        }
        var lcBaseName:String = baseName.toLowerCase();
        if (existingNames.indexOf(lcBaseName) < 0) return baseName; // basename is not already used
        lcBaseName = withoutTrailingDigits(lcBaseName);
        var i:int = 2;
        while (existingNames.indexOf(lcBaseName + i) >= 0) {
            i++
        } // find an unused name
        return withoutTrailingDigits(baseName) + i;
    }

    public function unusedSoundName(baseName:String = ''):String {
        // Create a unique sound name by appending a number if necessary.
        if (baseName == '') baseName = 'sound';
        var existingNames:Array = [];
        for each (var snd:WitSound in sounds) {
            existingNames.push(snd.soundName.toLowerCase());
        }
        var lcBaseName:String = baseName.toLowerCase();
        if (existingNames.indexOf(lcBaseName) < 0) return baseName; // basename is not already used
        lcBaseName = withoutTrailingDigits(lcBaseName);
        var i:int = 2;
        while (existingNames.indexOf(lcBaseName + i) >= 0) {
            i++
        } // find an unused name
        return withoutTrailingDigits(baseName) + i;
    }

    protected function withoutTrailingDigits(s:String):String {
        var i:int = s.length - 1;
        while ((i >= 0) && ('0123456789'.indexOf(s.charAt(i)) > -1)) i--;
        return s.slice(0, i + 1);
    }

    protected function updateImage():void {
        var currChild:DisplayObject = (img.numChildren == 1 ? img.getChildAt(0) : null);
        var currDispObj:DisplayObject = currentCostume().displayObj();
        var change:Boolean = (currChild != currDispObj);
        if (change) {
            while (img.numChildren > 0) img.removeChildAt(0);
            img.addChild(currDispObj);
        }
        clearCachedBitmap();
        adjustForRotationCenter();
    }

    protected function adjustForRotationCenter():void {
        // Adjust the offset of img relative to it's parent. If this object is a
        // WitSprite, then img is adjusted based on the costume's rotation center.
        // If it is a ScratchStage, img is centered on the stage.
        var costumeObj:DisplayObject = img.getChildAt(0);
        if (this is WitStage) {
            if (costumeObj is Bitmap) {
                img.x = (STAGEW - costumeObj.width) / 2;
                img.y = (STAGEH - costumeObj.height) / 2;
            } else {
                // SVG costume; don't center for now
                img.x = img.y = 0;
            }
        } else {
            var c:WitCostume = currentCostume();
            costumeObj.scaleX = 1 / c.bitmapResolution; // don't flip
            img.x = -c.rotationCenterX / c.bitmapResolution;
            img.y = -c.rotationCenterY / c.bitmapResolution;
            if ((this as WitSprite).isCostumeFlipped()) {
                costumeObj.scaleX = -1 / c.bitmapResolution; // flip
                img.x = -img.x;
            }
        }
    }

    public function clearCachedBitmap():void {
        // Does nothing here, but overridden in WitSprite
    }

    static private var cTrans:ColorTransform = new ColorTransform();

    public function applyFilters(forDragging:Boolean = false):void {
        img.filters = filterPack.buildFilters(forDragging);
        clearCachedBitmap();
        if (!forDragging) {
            var n:Number = Math.max(0, Math.min(filterPack.getFilterSetting('ghost'), 100));
            cTrans.alphaMultiplier = 1.0 - (n / 100.0);
            n = 255 * Math.max(-100, Math.min(filterPack.getFilterSetting('brightness'), 100)) / 100;
            cTrans.redOffset = cTrans.greenOffset = cTrans.blueOffset = n;
            img.transform.colorTransform = cTrans;
        }
        else {
            updateEffectsFor3D();
        }
    }

    public function updateEffectsFor3D():void {
    }

    protected function shapeChangedByFilter():Boolean {
        var filters:Object = filterPack.getAllSettings();
        return (filters['fisheye'] !== 0 || filters['whirl'] !== 0 || filters['mosaic'] !== 0);
    }

    static public const clearColorTrans:ColorTransform = new ColorTransform();

    public function clearFilters():void {
        filterPack.resetAllFilters();
        img.filters = [];
        img.transform.colorTransform = clearColorTrans;
        clearCachedBitmap();
    }

    public function setMedia(media:Array, currentCostume:WitCostume):void {
        var newCostumes:Array = [];
        sounds = [];
        for each (var m:* in media) {
            if (m is WitSound) sounds.push(m);
            if (m is WitCostume) newCostumes.push(m);
        }
        if (newCostumes.length > 0) costumes = newCostumes;
        var i:int = costumes.indexOf(currentCostume);
        currentCostumeIndex = (i < 0) ? 0 : i;
        showCostume(i);
    }

    public function defaultArgsFor(op:String, specDefaults:Array):Array {
        // Return an array of default parameter values for the given operation (primitive name).
        // For most ops, this will simply return the array of default arg values from the command spec.
        var sprites:Array;

        if ((['broadcast:', 'doBroadcastAndWait', 'whenIReceive'].indexOf(op)) > -1) {
            var msgs:Array = TempStatic.runtime.collectBroadcasts();
            return (msgs.length > 0) ? [msgs[0]] : ['message1'];
        }
        if ((['lookLike:', 'startScene', 'startSceneAndWait', 'whenSceneStarts'].indexOf(op)) > -1) {
            return [costumes[costumes.length - 1].costumeName];
        }
        if ((['playSound:', 'doPlaySoundAndWait'].indexOf(op)) > -1) {
            return (sounds.length > 0) ? [sounds[sounds.length - 1].soundName] : [''];
        }
        if ('createCloneOf' == op) {
            if (!(this is WitStage)) return ['_myself_'];
            sprites = TempStatic.witStage.sprites();
            return (sprites.length > 0) ? [sprites[sprites.length - 1].objName] : [''];
        }
        if ('getAttribute:of:' == op) {
            sprites = TempStatic.witStage.sprites();
            return (sprites.length > 0) ? ['x position', sprites[sprites.length - 1].objName] : ['volume', '_stage_'];
        }

        if ('setVar:to:' == op) return [defaultVarName(), 0];
        if ('changeVar:by:' == op) return [defaultVarName(), 1];
        if ('showVariable:' == op) return [defaultVarName()];
        if ('hideVariable:' == op) return [defaultVarName()];

        if ('append:toList:' == op) return ['thing', defaultListName()];
        if ('deleteLine:ofList:' == op) return [1, defaultListName()];
        if ('insert:at:ofList:' == op) return ['thing', 1, defaultListName()];
        if ('setLine:ofList:to:' == op) return [1, defaultListName(), 'thing'];
        if ('getLine:ofList:' == op) return [1, defaultListName()];
        if ('lineCountOfList:' == op) return [defaultListName()];
        if ('list:contains:' == op) return [defaultListName(), 'thing'];
        if ('showList:' == op) return [defaultListName()];
        if ('hideList:' == op) return [defaultListName()];

        return specDefaults;
    }

    public function defaultVarName():String {
        if (variables.length > 0) return variables[variables.length - 1].name; // local var
        return (this is WitStage) ? '' : TempStatic.witStage.defaultVarName(); // global var, if any
    }

    public function defaultListName():String {
        if (lists.length > 0) return lists[lists.length - 1].listName; // local list
        return (this is WitStage) ? '' : TempStatic.witStage.defaultListName(); // global list, if any
    }

    /* Scripts */

    public function allBlocks():Array {
        var result:Array = [];
        for each (var script:* in scripts) {
            if (getQualifiedClassName(script) != "wp.editor.blocks::Block") continue;
            script.allBlocksDo(function (b:*):void {
                result.push(b)
            });
        }
        return result;
    }

    /* Sounds */

    public function findSound(arg:*):WitSound {
        // Return a sound describe by arg, which can be a string (sound name),
        // a number (sound index), or a string representing a number (sound index).
        if (sounds.length == 0) return null;
        if (typeof(arg) == 'number') {
            var i:int = Math.round(arg - 1) % sounds.length;
            if (i < 0) i += sounds.length; // ensure positive
            return sounds[i];
        } else if (typeof(arg) == 'string') {
            for each (var snd:WitSound in sounds) {
                if (snd.soundName == arg) return snd; // arg matches a sound name
            }
            // try converting string arg to a number
            var n:Number = Number(arg);
            if (isNaN(n)) return null;
            return findSound(n);
        }
        return null;
    }

    public function setVolume(vol:Number):void {
        volume = Math.max(0, Math.min(vol, 100));
    }

    public function setInstrument(instr:Number):void {
        instrument = Math.max(1, Math.min(Math.round(instr), 128));
    }

    /* Procedures */

    public function procedureDefinitions():Array {
        var result:Array = [];
        var b:*;
        for (var i:int = 0; i < scripts.length; i++) {
            b = scripts[i];
            if (getQualifiedClassName(b) != "wp.editor.blocks::Block") continue;
            if (b.op != Specs.PROCEDURE_DEF) continue;
            result.push(b);
        }
        return result;
    }

    public function lookupProcedure(procName:String):* {
        var b:*;
        for (var i:int = 0; i < scripts.length; i++) {
            b = scripts[i];
            if (getQualifiedClassName(b) != "wp.editor.blocks::Block") continue;
            if (b.op != Specs.PROCEDURE_DEF) continue;
            if (b.spec != procName) continue;
            return b;
        }
        return null;
    }

    /* Variables */

    public function varNames():Array {
        var varList:Array = [];
        for each (var v:Variable in variables) varList.push(v.name);
        return varList;
    }

    public function setVarTo(varName:String, value:*):void {
        var v:Variable = lookupOrCreateVar(varName);
        v.value = value;
    }

    public function ownsVar(varName:String):Boolean {
        // Return true if this object owns a variable of the given name.
        for each (var v:Variable in variables) {
            if (v.name == varName) return true;
        }
        return false;
    }

    public function hasName(varName:String):Boolean {
        var p:WitObj = parent as WitObj;
        return ownsVar(varName) || ownsList(varName) || p && (p.ownsVar(varName) || p.ownsList(varName));
    }

    public function lookupOrCreateVar(varName:String):Variable {
        // Lookup and return a variable. If lookup fails, create the variable in this object.
        var v:Variable = lookupVar(varName);
        if (v == null) { // not found; create it
            v = new Variable(varName, 0);
            variables.push(v);
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
        }
        return v;
    }

    public function lookupVar(varName:String):Variable {
        // Look for variable first in sprite (local), then stage (global).
        // Return null if not found.
        var v:Variable;
        for each (v in variables) {
            if (v.name == varName) return v;
        }
        for each (v in TempStatic.witStage.variables) {
            if (v.name == varName) return v;
        }
        return null;
    }

    public function deleteVar(varToDelete:String):void {
        var newVars:Array = [];
        for each (var v:Variable in variables) {
            if (v.name == varToDelete) {
                if ((v.watcher != null) && (v.watcher.parent != null)) {
                    v.watcher.parent.removeChild(v.watcher);
                }
                v.watcher = v.value = null;
            }
            else newVars.push(v);
        }
        variables = newVars;
    }

    /* Lists */

    public function listNames():Array {
        var result:Array = [];
        for each (var list:* in lists) result.push(list.listName);
        return result;
    }

    public function ownsList(listName:String):Boolean {
        // Return true if this object owns a list of the given name.
        for each (var w:* in lists) {
            if (w.listName == listName) return true;
        }
        return false;
    }

    public function lookupOrCreateList(listName:String):* {
        // Look and return a list. If lookup fails, create the list in this object.
        var list:* = lookupList(listName);
        var LWClass:Class = getDefinitionByName("wp.editor.watchers::ListWatcher") as Class;
        if (list == null) { // not found; create it
            list = new LWClass(listName, [], this);
            lists.push(list);
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
        }
        return list;
    }

    public function lookupList(listName:String):* {
        // Look for list first in this sprite (local), then stage (global).
        // Return null if not found.
        var list:*;
        for each (list in lists) {
            if (list.listName == listName) return list;
        }
        for each (list in TempStatic.witStage.lists) {
            if (list.listName == listName) return list;
        }
        return null;
    }

    public function deleteList(listName:String):void {
        var newLists:Array = [];
        for each (var w:* in lists) {
            if (w.listName == listName) {
                if (w.parent) w.parent.removeChild(w);
            } else {
                newLists.push(w);
            }
        }
        lists = newLists;
    }

    /* Events */

    private const DOUBLE_CLICK_MSECS:int = 300;
    private var lastClickTime:uint;

    public function click(evt:MouseEvent):void {
        var now:uint = getTimer();
        for each (var stack:Block in scripts) {
            if (stack.op == 'whenClicked') {
                TempStatic.interp.restartThread(stack, this);
            }
        }
        if ((now - lastClickTime) < DOUBLE_CLICK_MSECS) {
            if ((this is WitStage) || WitSprite(this).isClone) return;
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, this, true));
            lastClickTime = 0;
        } else {
            lastClickTime = now;
        }
    }

    /* Translation */

    public function updateScriptsAfterTranslation():void {
        // Update the scripts of this object after switching languages.
        var newScripts:Array = [];
        for each (var b:* in scripts) {
            if (getQualifiedClassName(b) != "wp.editor.blocks::Block") continue;
            var newStack:* = BlockIO.arrayToStack(BlockIO.stackToArray(b), (this is WitStage));
            newStack.x = b.x;
            newStack.y = b.y;
            newScripts.push(newStack);
            if (b.parent) { // stack in the scripts pane; replace it
                b.parent.addChild(newStack);
                b.parent.removeChild(b);
            }
        }
        scripts = newScripts;
        var blockList:Array = allBlocks();
        for each (var c:WitComment in scriptComments) {
            c.updateBlockRef(blockList);
        }
    }

    /* Saving */

    public function toJSON(k:String):Object {
        var allScripts:Array = [];
        for each (var b:* in scripts) {
            allScripts.push([b.x, b.y, BlockIO.stackToArray(b)]);
        }
        var allComments:Array = [];
        for each (var c:WitComment in scriptComments) {
            allComments.push(c.toArray());
        }
        var obj:Object = {};
        obj['objName'] = objName;
        if (variables.length > 0) obj['variables'] = variables;
        if (lists.length > 0) obj['lists'] = lists;
        if (scripts.length > 0) obj['scripts'] = allScripts;
        if (scriptComments.length > 0) obj['scriptComments'] = allComments;
        if (sounds.length > 0) obj['sounds'] = sounds;
        obj['costumes'] = costumes;
        obj['currentCostumeIndex'] = currentCostumeIndex;

        return obj;
    }

    public function readJSON(jsonObj:Object):void {
        objName = jsonObj.objName;
        variables = jsonObj.variables || [];
        for (var i:int = 0; i < variables.length; i++) {
            var varObj:Object = variables[i];
            variables[i] = TempStatic.runtime.makeVariable(varObj);
        }
        lists = jsonObj.lists || [];
        scripts = jsonObj.scripts || [];
        scriptComments = jsonObj.scriptComments || [];
        sounds = jsonObj.sounds || [];
        costumes = jsonObj.costumes || [];
        currentCostumeIndex = jsonObj.currentCostumeIndex;
        if (isNaNOrInfinity(currentCostumeIndex)) currentCostumeIndex = 0;
    }

    private function isNaNOrInfinity(n:Number):Boolean {
        if (n != n) return true; // NaN
        if (n == Number.POSITIVE_INFINITY) return true;
        if (n == Number.NEGATIVE_INFINITY) return true;
        return false;
    }

    public function instantiateFromJSON(newStage:WitStage):void {
        var i:int, jsonObj:Object;

        // lists
        var LWClass:Class = getDefinitionByName("wp.editor.watchers::ListWatcher") as Class;
        for (i = 0; i < lists.length; i++) {
            jsonObj = lists[i];
            var newList:* = new LWClass();
            newList.readJSON(jsonObj);
            newList.target = this;
            newStage.addChild(newList);
            newList.updateTitleAndContents();
            lists[i] = newList;
        }

        // scripts
        for (i = 0; i < scripts.length; i++) {
            // entries are of the form: [x y stack]
            var entry:Array = scripts[i];
            var b:* = BlockIO.arrayToStack(entry[2], (this is WitStage));
            b.x = entry[0];
            b.y = entry[1];
            scripts[i] = b;
        }

        // script comments
        for (i = 0; i < scriptComments.length; i++) {
            scriptComments[i] = WitComment.fromArray(scriptComments[i]);
        }

        // sounds
        for (i = 0; i < sounds.length; i++) {
            jsonObj = sounds[i];
            sounds[i] = new WitSound('json temp', null);
            sounds[i].readJSON(jsonObj);
        }

        // costumes
        for (i = 0; i < costumes.length; i++) {
            jsonObj = costumes[i];
            costumes[i] = new WitCostume('json temp', null);
            costumes[i].readJSON(jsonObj);
        }
    }
}
}
