// A WitProgram sprite object. State specific to sprites includes: position, direction,
// rotation style, size, draggability, and pen state.
package wp.editor.wit {
import flash.display.*;
import flash.events.*;
import flash.geom.*;
import flash.net.FileReference;
import flash.utils.*;

import gnu.as3.gettext.gettext;

import jijunzeng.Color;

import jijunzeng.MyEvent;

import util.*;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.filters.FilterPack;
import wp.editor.interpreter.Variable;
import wp.editor.uiwidgets.Menu;
import wp.editor.watchers.ListWatcher;

public class WitSprite extends WitObj {
    public var scratchX:Number;
    public var scratchY:Number;
    public var direction:Number = 90;
    public var rotationStyle:String = 'normal'; // 'normal', 'leftRight', 'none'

    public var isDraggable:Boolean = false;
    public var indexInLibrary:int;
    public var bubble:TalkBubble;

    public var penIsDown:Boolean;
    public var penWidth:Number = 1;
    public var penHue:Number = 120; // blue
    public var penShade:Number = 50; // full brightness and saturation
    public var penColorCache:Number = 0xFF;

    private var cachedBitmap:BitmapData;	// current costume, rotated & scaled
    private var cachedBounds:Rectangle;		// bounds of non-transparent cachedBitmap in stage coords

    public var localMotionAmount:int = -2;
    public var localMotionDirection:int = -2;
    public var localFrameNum:int;

    public var spriteInfo:Object = {};
    private var geomShape:Shape;

    public function WitSprite(name:String = 'Sprite1') {
        objName = name;
        filterPack = new FilterPack(this);
        initMedia();
        img = new Sprite();
        img.cacheAsBitmap = true;
        addChild(img);
        geomShape = new Shape();
        geomShape.visible = false;
        img.addChild(geomShape);
        showCostume(0);
        setScratchXY(0, 0);
    }

    private function initMedia():void {
        const _:Function = gettext;
        var graySquare:BitmapData = new BitmapData(4, 4, true, 0x808080);
        costumes.push(new WitCostume(_('costume1'), graySquare));
        sounds.push(new WitSound(_('pop'), new Pop()));
        sounds[0].prepareToSave();
    }

    public function setInitialCostume(c:WitCostume):void {
        costumes = [c];
        showCostume(0);
    }

    public function setRotationStyle(newRotationStyle:String):void {
        var oldDir:Number = direction;
        setDirection(90);
        if ('all around' == newRotationStyle) rotationStyle = 'normal';
        if ('left-right' == newRotationStyle) rotationStyle = 'leftRight';
        if ("don't rotate" == newRotationStyle) rotationStyle = "none";
        setDirection(oldDir);
    }

    public function duplicate():WitSprite {
        var dup:WitSprite = new WitSprite();
        dup.initFrom(this, false);
        return dup;
    }

    public function initFrom(spr:WitSprite, forClone:Boolean):void {
        // Copy all the state from the given sprite. Used by both
        // the clone block and duplicate().
        var i:int;

        // Copy variables and lists.
        for (i = 0; i < spr.variables.length; i++) {
            var v:Variable = spr.variables[i];
            variables.push(new Variable(v.name, v.value));
        }
        for (i = 0; i < spr.lists.length; i++) {
            var lw:ListWatcher = spr.lists[i];
            var lwDup:ListWatcher;
            lists.push(lwDup = new ListWatcher(lw.listName, lw.contents.concat(), spr));
            lwDup.visible = false;
        }

        if (forClone) {
            // Clones share scripts and sounds with the original sprite.
            scripts = spr.scripts;
            sounds = spr.sounds;
        } else {
            for (i = 0; i < spr.scripts.length; i++) scripts.push(spr.scripts[i].duplicate(forClone));
            sounds = spr.sounds.concat();
        }

        // To support vector costumes, every sprite must have its own costume copies, even clones.
        costumes = [];
        for each (var c:WitCostume in spr.costumes) costumes.push(c.duplicate());
        currentCostumeIndex = spr.currentCostumeIndex;

        objName = spr.objName;
        volume = spr.volume;
        instrument = spr.instrument;
        filterPack = spr.filterPack.duplicateFor(this);

        visible = spr.visible;
        scratchX = spr.scratchX;
        scratchY = spr.scratchY;
        direction = spr.direction;
        rotationStyle = spr.rotationStyle;
        isClone = forClone;
        isDraggable = spr.isDraggable;
        indexInLibrary = 100000;

        penIsDown = spr.penIsDown;
        penWidth = spr.penWidth;
        penHue = spr.penHue;
        penShade = spr.penShade;
        penColorCache = spr.penColorCache;

        showCostume(spr.currentCostumeIndex);
        setDirection(spr.direction);
        setScratchXY(spr.scratchX, spr.scratchY);
        setSize(spr.getSize());
        applyFilters();
    }

    override protected function updateImage():void {
        // Make sure to update the shape
        if (geomShape.parent) img.removeChild(geomShape);
        super.updateImage();
        if (bubble) updateBubble();
    }

    public function setScratchXY(newX:Number, newY:Number):void {
        scratchX = isFinite(newX) ? newX : newX > 0 ? 1e6 : -1e6;
        scratchY = isFinite(newY) ? newY : newY > 0 ? 1e6 : -1e6;
        x = 240 + Math.round(scratchX);
        y = 180 - Math.round(scratchY);
        updateBubble();
    }

    static private var stageRect:Rectangle = new Rectangle(0, 0, WitObj.STAGEW, WitObj.STAGEH);
    static private var emptyRect:Rectangle = new Rectangle(0, 0, 0, 0);
    static private var edgeBox:Rectangle = new Rectangle(0, 0, WitObj.STAGEW, WitObj.STAGEH);

    public function keepOnStage():void {
        var myBox:Rectangle;
        if (width == 0 && height == 0) {
            emptyRect.x = x;
            emptyRect.y = y;
            myBox = emptyRect;
        }
        else {
            myBox = geomShape.getRect(parent);
            if (myBox.width == 0 || myBox.height == 0) {
                myBox.x = x;
                myBox.y = y;
            }
            myBox.inflate(3, 3);
        }

        if (stageRect.containsRect(myBox)) return;

        var inset:int = Math.min(18, Math.min(myBox.width, myBox.height) / 2);
        edgeBox.x = edgeBox.y = inset;
        inset += inset;
        edgeBox.width = WitObj.STAGEW - inset;
        edgeBox.height = WitObj.STAGEH - inset;
        if (myBox.intersects(edgeBox)) return; // sprite is sufficiently on stage
        if (myBox.right < edgeBox.left)
            scratchX = Math.ceil(scratchX + (edgeBox.left - myBox.right));
        if (myBox.left > edgeBox.right)
            scratchX = Math.floor(scratchX + (edgeBox.right - myBox.left));
        if (myBox.bottom < edgeBox.top)
            scratchY = Math.floor(scratchY + (myBox.bottom - edgeBox.top));
        if (myBox.top > edgeBox.bottom)
            scratchY = Math.ceil(scratchY + (myBox.top - edgeBox.bottom));
        setScratchXY(scratchX, scratchY);
    }

    public function setDirection(d:Number):void {
        if ((d * 0) != 0) return; // d is +/-Infinity or NaN
        var wasFlipped:Boolean = isCostumeFlipped();
        d = d % WitObj.STAGEH;
        if (d < 0) d += WitObj.STAGEH;
        direction = (d > 180) ? d - WitObj.STAGEH : d;
        if ('normal' == rotationStyle) {
            rotation = (direction - 90) % WitObj.STAGEH;
        } else {
            rotation = 0;
            if ('none' == rotationStyle && !wasFlipped) return;
            if (('leftRight' == rotationStyle) && (isCostumeFlipped() == wasFlipped)) return;
        }

        updateImage();
        adjustForRotationCenter();
    }

    protected override function adjustForRotationCenter():void {
        super.adjustForRotationCenter();
        geomShape.scaleX = img.getChildAt(0).scaleX;
    }

    public function getSize():Number {
        return 100 * scaleX;
    }

    public function setSize(percent:Number):void {
        var origW:int = img.width;
        var origH:int = img.height;
        var minScale:Number = Math.min(1, Math.max(5 / origW, 5 / origH));
        var maxScale:Number = Math.min((1.5 * WitObj.STAGEW) / origW, (1.5 * WitObj.STAGEH) / origH);
        scaleX = scaleY = Math.max(minScale, Math.min(percent / 100.0, maxScale));
        clearCachedBitmap();
        updateBubble();
    }

    public function setPenSize(n:Number):void {
        penWidth = Math.max(1, Math.min(Math.round(n), 255)); // 255 is the maximum line with supported by Flash
    }

    public function setPenColor(c:Number):void {
        var hsv:Array = Color.rgb2hsv(c);
        penHue = (200 * hsv[0]) / WitObj.STAGEH;
        penShade = 50 * hsv[2];  // not quite right; doesn't account for saturation
        penColorCache = c;
    }

    public function setPenHue(n:Number):void {
        penHue = n % 200;
        if (penHue < 0) penHue += 200;
        updateCachedPenColor();
    }

    public function setPenShade(n:Number):void {
        penShade = n % 200;
        if (penShade < 0) penShade += 200;
        updateCachedPenColor();
    }

    private function updateCachedPenColor():void {
        var c:int = Color.fromHSV((penHue * 180) / 100, 1, 1);
        var shade:Number = (penShade > 100) ? 200 - penShade : penShade; // range 0..100
        if (shade < 50) {
            penColorCache = Color.mixRGB(0, c, (10 + shade) / 60);
        } else {
            penColorCache = Color.mixRGB(c, 0xFFFFFF, (shade - 50) / 60);
        }
    }

    public function isCostumeFlipped():Boolean {
        return (rotationStyle == 'leftRight') && (direction < 0);
    }

    public override function clearCachedBitmap():void {
        super.clearCachedBitmap();
        cachedBitmap = null;
        cachedBounds = null;

        if (!geomShape.parent) {
            geomShape.graphics.copyFrom(currentCostume().getShape().graphics);
            var currDO:DisplayObject = img.getChildAt(0);
            geomShape.scaleX = currDO.scaleX;
            geomShape.scaleY = currDO.scaleY;
            geomShape.x = currDO.x;
            geomShape.y = currDO.y;
            geomShape.rotation = currDO.rotation;
            img.addChild(geomShape);
        }
    }

    public override function hitTestPoint(globalX:Number, globalY:Number, shapeFlag:Boolean = true):Boolean {
        if ((!visible) || (img.transform.colorTransform.alphaMultiplier == 0)) return false;
        var p:Point = parent.globalToLocal(new Point(globalX, globalY));
        var myRect:Rectangle = bounds();
        if (!myRect.containsPoint(p)) return false;
        return shapeFlag ? bitmap(true).hitTest(myRect.topLeft, 1, p) : true;
    }

    public override function getBounds(space:DisplayObject):Rectangle {
        //if(space == this && geomShape.parent) img.removeChild(geomShape);
        var b:Rectangle = getChildAt(0).getBounds(space);
        //img.addChild(geomShape);
        return b;
    }

    public function bounds():Rectangle {
        // return the bounding rectangle of my visible pixels (scaled and rotated)
        // in the coordinate system of my parent (i.e. the stage)
        if (cachedBounds == null) bitmap(); // computes cached bounds
        var result:Rectangle = cachedBounds.clone();
        result.offset(x, y);
//		trace('old code bounds: '+result+'     new code bounds: '+geomShape.getBounds(parent));
//		return geomShape.getBounds(parent);
        return result;
    }

//	private var testBM:Bitmap = new Bitmap();
//	private var testSpr:Sprite = new Sprite();
    public function bitmap(forTest:Boolean = false):BitmapData {
        if (cachedBitmap != null && !forTest)
            return cachedBitmap;

        // compute cachedBitmap
        // Note: cachedBitmap must be drawn with alpha=1 to allow the sprite/color touching tests to work
        var m:Matrix = new Matrix();
        m.rotate((Math.PI * rotation) / 180);
        m.scale(scaleX, scaleY);
        var b:Rectangle = (currentCostume().bitmap) ? img.getChildAt(0).getBounds(this) : getVisibleBounds(this);
        var r:Rectangle = transformedBounds(b, m);

        // returns true if caller should immediately return cachedBitmap
        var self:WitSprite = this;

        function bitmap2d():Boolean {
            if ((r.width == 0) || (r.height == 0)) { // empty costume: use an invisible 1x1 bitmap
                cachedBitmap = new BitmapData(1, 1, true, 0);
                cachedBounds = cachedBitmap.rect;
                return true;
            }

            var oldTrans:ColorTransform = img.transform.colorTransform;
            img.transform.colorTransform = new ColorTransform(1, 1, 1, 1, oldTrans.redOffset, oldTrans.greenOffset, oldTrans.blueOffset, 0);
            cachedBitmap = new BitmapData(Math.max(int(r.width), 1), Math.max(int(r.height), 1), true, 0);
            m.translate(-r.left, -r.top);
            cachedBitmap.draw(self, m);
            img.transform.colorTransform = oldTrans;
            return false;
        }

        if (bitmap2d()) return cachedBitmap;

        cachedBounds = cachedBitmap.rect;

        // crop cachedBitmap and record cachedBounds
        // Note: handles the case where cropR is empty
        var cropR:Rectangle = cachedBitmap.getColorBoundsRect(0xFF000000, 0, false);
        if ((cropR.width > 0) && (cropR.height > 0)) {
            var cropped:BitmapData = new BitmapData(Math.max(int(cropR.width), 1), Math.max(int(cropR.height), 1), true, 0);
            cropped.copyPixels(cachedBitmap, cropR, new Point(0, 0));
            cachedBitmap = cropped;
            cachedBounds = cropR;
        }

        cachedBounds.offset(r.x, r.y);
        return cachedBitmap;
    }

    private function transformedBounds(r:Rectangle, m:Matrix):Rectangle {
        // Return the rectangle that encloses the corners of r when transformed by m.
        var p1:Point = m.transformPoint(r.topLeft);
        var p2:Point = m.transformPoint(new Point(r.right, r.top));
        var p3:Point = m.transformPoint(new Point(r.left, r.bottom));
        var p4:Point = m.transformPoint(r.bottomRight);
        var xMin:Number, xMax:Number, yMin:Number, yMax:Number;
        xMin = Math.min(p1.x, p2.x, p3.x, p4.x);
        yMin = Math.min(p1.y, p2.y, p3.y, p4.y);
        xMax = Math.max(p1.x, p2.x, p3.x, p4.x);
        yMax = Math.max(p1.y, p2.y, p3.y, p4.y);
        var newR:Rectangle = new Rectangle(xMin, yMin, xMax - xMin, yMax - yMin);
        return newR;
    }

    public override function defaultArgsFor(op:String, specDefaults:Array):Array {
        if ('gotoSpriteOrMouse:' == op) return ['_mouse_'];
        if ('gotoX:y:' == op) return [Math.round(scratchX), Math.round(scratchY)];
        if ('glideSecs:toX:y:elapsed:from:' == op) return [1, Math.round(scratchX), Math.round(scratchY)];
        if ('setSizeTo:' == op) return [Math.round(getSize() * 10) / 10];
        if ((['startScene', 'startSceneAndWait', 'whenSceneStarts'].indexOf(op)) > -1) {
            var stg:WitStage = parent as WitStage;
            if (stg) return [stg.costumes[stg.costumes.length - 1].costumeName];
        }
        if ('senseVideoMotion' == op) return ['motion', 'this sprite'];
        return super.defaultArgsFor(op, specDefaults);
    }

    /* Dragging */

    public function objToGrab(evt:MouseEvent):WitSprite {
        return this
    } // allow dragging

    /* Menu */

    public function menu(evt:MouseEvent):Menu {
        var m:Menu = new Menu();
        m.addItem('info', showDetails);
        m.addLine();
        m.addItem('duplicate', duplicateSprite);
        m.addItem('delete', deleteSprite);
        m.addLine();
        m.addItem('save to local file', saveToLocalFile);
        return m;
    }

    public function handleTool(tool:String, evt:MouseEvent):void {
        if (tool == 'copy') duplicateSprite(true);
        if (tool == 'cut') deleteSprite();
        if (tool == 'grow') growSprite();
        if (tool == 'shrink') shrinkSprite();
        if (tool == 'help') trace('scratchUI');
    }

    private function growSprite():void {
        setSize(getSize() + 5);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null, true));
    }

    private function shrinkSprite():void {
        setSize(getSize() - 5);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null, true));
    }

    public function duplicateSprite(grab:Boolean = false):void {
        var dup:WitSprite = duplicate();
        dup.objName = unusedSpriteName(objName);
        if (!grab) {
            dup.setScratchXY(
                    int(Math.random() * 400) - 200,
                    int(Math.random() * 300) - 150);
        }
        if (parent != null) {
            parent.addChild(dup);
            var app:WitProgram = root as WitProgram;
            if (app) {
                TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
                WitProgram.libraryPart.refresh();
                if (grab) TempStatic.gh.grabOnMouseUp(dup);
            }
        }
    }

    public function showDetails():void {
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, this, true));
        WitProgram.libraryPart.showSpriteDetails(true);
    }

    public function unusedSpriteName(baseName:String):String {
        var stg:WitStage = parent as WitStage;
        return stg ? stg.unusedSpriteName(baseName) : baseName;
    }

    public function deleteSprite():void {
        if (parent != null) {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.RECORD_FOR_UNDELETE,
                    {
                        obj: this,
                        x: scratchX,
                        y: scratchY,
                        index: 0,
                        owner: TempStatic.witStage
                    }, true
            ));
            hideBubble();

            // Force redisplay (workaround for flash display update bug)
            parent.visible = false;
            parent.visible = true;

            parent.removeChild(this);
            TempStatic.witStage.removeObsoleteWatchers();
            var sprites:Array = TempStatic.witStage.sprites();
            if (sprites.length > 0) {
                // Pick the sprite just before the deleted sprite in the sprite library to select next.
                sprites.sortOn('indexInLibrary');
                var nextSelection:WitSprite = sprites[0];
                for each (var spr:WitSprite in sprites) {
                    if (spr.indexInLibrary > this.indexInLibrary) break;
                    else nextSelection = spr;
                }
                TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, nextSelection, true));
            } else {
                // If there are no sprites, select the stage.
                TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, TempStatic.witStage, true));
            }
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
            WitProgram.libraryPart.refresh();
        }
    }

    private function saveToLocalFile():void {
        function success():void {
            trace('sprite saved to file: ' + file.name);
        }

        var zipData:ByteArray = new ProjectIO().encodeSpriteAsZipFile(copyToShare());
        var defaultName:String = objName + '.spr';
        var file:FileReference = new FileReference();
        file.addEventListener(Event.COMPLETE, success);
        file.save(zipData, defaultName);
    }

    public function copyToShare():WitSprite {
        // Return a copy of the current sprite set up to be shared.
        var dup:WitSprite = new WitSprite();
        dup.initFrom(this, false);
        dup.setScratchXY(0, 0);
        dup.visible = true;
        if (TempStatic.usbID.length > 0) {
            dup.spriteInfo.deviceID = TempStatic.usbID;
        }
        return dup;
    }

    /* talk/think bubble support */

    public function showBubble(s:*, type:String, source:Object, isAsk:Boolean = false):void {
        hideBubble();
        if (s == null) s = 'NULL';
        if (s is Number) {
            if ((Math.abs(s) >= 0.01) && (int(s) != s)) {
                s = s.toFixed(2); // 2 digits after decimal point
            } else {
                s = s.toString();
            }
        }
        if (!(s is String)) s = s.toString();
        if (s.length == 0) return;
        bubble = new TalkBubble(s, type, isAsk ? 'ask' : 'say', source);
        parent.addChild(bubble);
        updateBubble();
    }

    public function hideBubble():void {
        if (bubble == null) return;
        bubble.parent.removeChild(bubble);
        bubble = null;
    }

    public function updateBubble():void {
        if (bubble == null) return;
        if (bubble.visible != visible) bubble.visible = visible;
        if (!visible) return;
        var pad:int = 3;
        var stageL:int = pad;
        var stageR:int = STAGEW - pad;
        var stageH:int = STAGEH;
        var r:Rectangle = bubbleRect();

        // decide which side of the sprite the bubble should be on
        var bubbleOnRight:Boolean = bubble.pointsLeft;
        if (bubbleOnRight && ((r.x + r.width + bubble.width) > stageR)) bubbleOnRight = false;
        if (!bubbleOnRight && ((r.x - bubble.width) < 0)) bubbleOnRight = true;

        if (bubbleOnRight) {
            bubble.setDirection('left');
            bubble.x = r.x + r.width;
        } else {
            bubble.setDirection('right');
            bubble.x = r.x - bubble.width;
        }

        // make sure bubble stays on screen
        if ((bubble.x + bubble.width) > stageR) bubble.x = stageR - bubble.width;
        if (bubble.x < stageL) bubble.x = stageL;
        bubble.y = Math.max(r.y - bubble.height, pad);
        if ((bubble.y + bubble.height) > stageH) {
            bubble.y = stageH - bubble.height;
        }
    }

    private function bubbleRect():Rectangle {
        // Answer a rectangle to be used for position a talk/think bubble, based on
        // the bounds of the non-transparent pixels along the top edge of this sprite.
        var myBM:BitmapData = bitmap();
        var h:int = 8; // strip height

        // compute bounds
        var p:Point = TempStatic.witStage.globalToLocal(localToGlobal(new Point(0, 0)));
        if (cachedBounds == null) bitmap(); // computes cached bounds
        var myBounds:Rectangle = cachedBounds.clone();
        myBounds.offset(p.x, p.y);

        var topStrip:BitmapData = new BitmapData(myBM.width, h, true, 0);
        topStrip.copyPixels(myBM, myBM.rect, new Point(0, 0));
        var r:Rectangle = topStrip.getColorBoundsRect(0xFF000000, 0, false);
        if ((r.width == 0) || (r.height == 0)) return myBounds;
        return new Rectangle(myBounds.x + r.x, myBounds.y, r.width, 10);
    }

    /* Saving */
    override public function toJSON(k:String):Object {
        var obj:Object = super.toJSON(k);
        obj['scratchX'] = scratchX;
        obj['scratchY'] = scratchY;
        obj['scale'] = scaleX;
        obj['direction'] = direction;
        obj['rotationStyle'] = rotationStyle;
        obj['isDraggable'] = isDraggable;
        obj['indexInLibrary'] = indexInLibrary;
        obj['visible'] = visible;
        obj['spriteInfo'] = spriteInfo;

        return obj;
    }

    public override function readJSON(jsonObj:Object):void {
        super.readJSON(jsonObj);
        scratchX = jsonObj.scratchX;
        scratchY = jsonObj.scratchY;
        scaleX = scaleY = jsonObj.scale;
        direction = jsonObj.direction;
        rotationStyle = jsonObj.rotationStyle;
        isDraggable = jsonObj.isDraggable;
        indexInLibrary = jsonObj.indexInLibrary;
        visible = jsonObj.visible;
        spriteInfo = jsonObj.spriteInfo ? jsonObj.spriteInfo : {};
        setScratchXY(scratchX, scratchY);
    }

    public function getVisibleBounds(space:DisplayObject):Rectangle {
        if (space == this) {
            var rot:Number = rotation;
            rotation = 0;
        }

        if (!geomShape.parent) {
            img.addChild(geomShape);
            geomShape.x = img.getChildAt(0).x;
            geomShape.scaleX = img.getChildAt(0).scaleX;
        }

        var b:Rectangle = geomShape.getRect(space);

        if (space == this) {
            rotation = rot;
            b.inflate(2, 2);
            b.offset(-1, -1);
        }

        return b;
    }

    public function prepareToDrag():void {
        // Force rendering with PixelBender for a dragged sprite
        applyFilters(true);
    }

    public override function stopDrag():void {
        super.stopDrag();
        applyFilters();
    }
}
}
