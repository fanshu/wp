package wp.editor.wit {
import by.blooddy.crypto.MD5;
import by.blooddy.crypto.image.PNG24Encoder;
import by.blooddy.crypto.image.PNGFilter;

import flash.display.*;
import flash.geom.*;
import flash.media.*;
import flash.net.LocalConnection;
import flash.system.Capabilities;
import flash.utils.ByteArray;
import flash.utils.getDefinitionByName;
import flash.utils.getQualifiedClassName;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.*;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.filters.FilterPack;

public class WitStage extends WitObj {
    public var istoswf:Boolean = false;

    public var info:Object = {};
    public var tempoBPM:Number = 60;

    public var penActivity:Boolean;
    public var newPenStrokes:Shape;
    public var penLayer:Bitmap;

    public var penLayerPNG:ByteArray;
    public var penLayerID:int = -1;
    public var penLayerMD5:String;

    // camera support
    public var videoImage:Bitmap;
    static private var camera:Camera;
    private var video:Video;
    private var videoAlpha:Number = 0.5;
    public var flipVideo:Boolean = false;

    public function WitStage() {
        objName = 'Stage';
        scrollRect = new Rectangle(0, 0, STAGEW, STAGEH); // clip drawing to my bounds
        cacheAsBitmap = true; // clip damage reports to my bounds
        filterPack = new FilterPack(this);

        addWhiteBG();
        img = new Sprite();
        img.addChild(new Bitmap(new BitmapData(1, 1)));
        img.cacheAsBitmap = true;
        addChild(img);
        addPenLayer();
        initMedia();
        showCostume(0);
    }

    public function clearCachedBitmaps():void {
        for (var i:int = 0; i < numChildren; ++i) {
            var spr:WitSprite = (getChildAt(i) as WitSprite);
            if (spr) spr.clearCachedBitmap();
        }
        clearCachedBitmap();

        // unsupported technique that seems to force garbage collection
        try {
            new LocalConnection().connect('foo');
            new LocalConnection().connect('foo');
        } catch (e:Error) {
        }
    }

    public function clearAllCaches():void {
        for each (var obj:WitObj in allObjects()) obj.clearCaches();
    }

    public function setTempo(bpm:Number):void {
        tempoBPM = Math.max(20, Math.min(bpm, 500));
    }

    public function objNamed(s:String):WitObj {
        // Return the object with the given name, or null if not found.
        if (('_stage_' == s) || (objName == s)) return this;
        return spriteNamed(s);
    }

    public function spriteNamed(spriteName:String):WitSprite {
        // Return the sprite (but not a clone) with the given name, or null if not found.
        for each (var spr:WitSprite in sprites()) {
            if ((spr.objName == spriteName) && !spr.isClone) return spr;
        }
        if ((TempStatic.gh.carriedObj is WitSprite)) {
            spr = WitSprite(TempStatic.gh.carriedObj);
            if ((spr.objName == spriteName) && !spr.isClone) return spr;
        }
        return null;
    }

    public function spritesAndClonesNamed(spriteName:String):Array {
        // Return all sprites and clones with the given name.
        var result:Array = [];
        for (var i:int = 0; i < numChildren; i++) {
            var c:* = getChildAt(i);
            if ((c is WitSprite) && (c.objName == spriteName)) result.push(c);
        }
        var spr:WitSprite = TempStatic.gh.carriedObj as WitSprite;
        if (spr && (spr.objName == spriteName)) result.push(spr);
        return result;
    }

    public function unusedSpriteName(baseName:String):String {
        var existingNames:Array = ['_mouse_', '_stage_', '_edge_', '_myself_'];
        for each (var s:WitSprite in sprites()) {
            existingNames.push(s.objName.toLowerCase());
        }
        var lcBaseName:String = baseName.toLowerCase();
        if (existingNames.indexOf(lcBaseName) < 0) return baseName; // basename is not already used
        lcBaseName = withoutTrailingDigits(lcBaseName);
        var i:int = 2;
        while (existingNames.indexOf(lcBaseName + i) >= 0) {
            i++
        } // find an unused name
        return withoutTrailingDigits(baseName) + i;
    }

    override public function hasName(varName:String):Boolean {
        // Return true if this object owns a variable of the given name.
        for each (var s:WitSprite in sprites()) {
            if (s.ownsVar(varName) || s.ownsList(varName)) return true;
        }
        return ownsVar(varName) || ownsList(varName);
    }

    private function initMedia():void {
        const _:Function = gettext;
        costumes.push(WitCostume.emptyBitmapCostume(_('backdrop1'), true));
        sounds.push(new WitSound(_('pop'), new Pop()));
        sounds[0].prepareToSave();
    }

    private function addWhiteBG():void {
        var bg:Shape = new Shape();
        bg.graphics.beginFill(0xFFFFFF);
        bg.graphics.drawRect(0, 0, STAGEW, STAGEH);
        addChild(bg);
    }

    private function addPenLayer():void {
        newPenStrokes = new Shape();
        var bm:BitmapData = new BitmapData(STAGEW, STAGEH, true, 0);
        penLayer = new Bitmap(bm);
        addChild(penLayer);
    }

    public function scratchMouseX():int {
        return Math.max(-240, Math.min(mouseX - (STAGEW / 2), 240))
    }

    public function scratchMouseY():int {
        return -Math.max(-180, Math.min(mouseY - (STAGEH / 2), 180))
    }

    public override function allObjects():Array {
        // Return an array of all sprites in this project plus the stage.
        return sprites().concat(this);
    }

    public function updateTranslation():void {
        for each (var o:WitObj in allObjects()) {
            o.updateScriptsAfterTranslation();
        }
        var uiLayer:Sprite = getUILayer();
        var lw:*;
        for (var i:int = 0; i < uiLayer.numChildren; ++i) {
            lw = uiLayer.getChildAt(i);
            if (getQualifiedClassName(lw) == "wp.editor.watchers::ListWatcher") {
                lw.updateTranslation();
            }
        }
    }

    public function sprites():Array {
        // Return an array of all sprites in this project.
        var result:Array = [];
        for (var i:int = 0; i < numChildren; i++) {
            var o:* = getChildAt(i);
            if ((o is WitSprite) && !o.isClone) result.push(o);
        }
        return result;
    }

    public function deleteClones():void {
        var clones:Array = [];
        for (var i:int = 0; i < numChildren; i++) {
            var o:* = getChildAt(i);
            if ((o is WitSprite) && o.isClone) {
                if (o.bubble && o.bubble.parent) o.bubble.parent.removeChild(o.bubble);
                clones.push(o);
            }
        }
        for each (var c:WitSprite in clones) removeChild(c);
    }

    public function watchers():Array {
        // Return an array of all variable and lists on the stage, visible or not.
        var result:Array = [];
        var uiLayer:Sprite = getUILayer();
        for (var i:int = 0; i < uiLayer.numChildren; i++) {
            var o:* = uiLayer.getChildAt(i);
            switch (getQualifiedClassName(o)) {
                case "wp.editor.watchers::Watcher":
                case "wp.editor.watchers::ListWatcher":
                    result.push(o);
            }
        }
        return result;
    }

    public function removeObsoleteWatchers():void {
        // Called after deleting a sprite.
        var toDelete:Array = [];
        var uiLayer:Sprite = getUILayer();
        var w:*;
        for (var i:int = 0; i < uiLayer.numChildren; i++) {
            w = uiLayer.getChildAt(i);
            switch (getQualifiedClassName(w)) {
                case "wp.editor.watchers::Watcher":
                case "wp.editor.watchers::ListWatcher":
                    if (w.target is WitStage) continue;
                    if (w.target.parent == this) continue;
                    toDelete.push(w);
            }
        }
        for each (var c:DisplayObject in toDelete) uiLayer.removeChild(c);
    }

    /* Scrolling support */

    public var xScroll:Number = 0;
    public var yScroll:Number = 0;

    public function scrollAlign(s:String):void {
        var c:DisplayObject = currentCostume().displayObj();
        var sceneW:int = Math.max(c.width, STAGEW);
        var sceneH:int = Math.max(c.height, STAGEH);
        switch (s) {
            case 'top-left':
                xScroll = 0;
                yScroll = sceneH - STAGEH;
                break;
            case 'top-right':
                xScroll = sceneW - STAGEW;
                yScroll = sceneH - STAGEH;
                break;
            case 'middle':
                xScroll = Math.floor((sceneW - STAGEW) / 2);
                yScroll = Math.floor((sceneH - STAGEH) / 2);
                break;
            case 'bottom-left':
                xScroll = 0;
                yScroll = 0;
                break;
            case 'bottom-right':
                xScroll = sceneW - STAGEW;
                yScroll = 0;
                break;
        }
        updateImage();
    }

    public function scrollRight(n:Number):void {
        xScroll += n;
        updateImage()
    }

    public function scrollUp(n:Number):void {
        yScroll += n;
        updateImage()
    }

    public function getUILayer():Sprite {
        return this;
    }

    /* Camera support */

    public function step(runtime:WitRuntime):void {
        if (videoImage != null) {
            if (flipVideo) {
                // flip the image like a mirror
                var m:Matrix = new Matrix();
                m.scale(-1, 1);
                m.translate(video.width, 0);
                videoImage.bitmapData.draw(video, m);
            }
            else {
                videoImage.bitmapData.draw(video);
            }
        }
        cachedBitmapIsCurrent = false;

        // Step the watchers
        var uiContainer:Sprite = getUILayer();
        for (var i:int = 0; i < uiContainer.numChildren; i++) {
            var c:* = uiContainer.getChildAt(i);
            if (c.visible == true) {
                switch (getQualifiedClassName(c)) {
                    case "wp.editor.watchers::Watcher":
                        c.step(runtime);
                        break;
                    case "wp.editor.watchers::ListWatcher":
                        c.step();
                        break;
                }
            }
        }
    }

    public function stampSprite(s:WitSprite, stampAlpha:Number):void {
        if (s == null) return;

        var penBM:BitmapData = penLayer.bitmapData;
        var m:Matrix = new Matrix();

        function stamp2d():void {
            var wasVisible:Boolean = s.visible;
            s.visible = true;  // if this is done after commitPenStrokes, it doesn't work...
            commitPenStrokes();
            m.rotate((Math.PI * s.rotation) / 180);
            m.scale(s.scaleX, s.scaleY);
            m.translate(s.x, s.y);
            var oldGhost:Number = s.filterPack.getFilterSetting('ghost');
            s.filterPack.setFilter('ghost', 0);
            s.applyFilters();
            penBM.draw(s, m, new ColorTransform(1, 1, 1, stampAlpha));
            s.filterPack.setFilter('ghost', oldGhost);
            s.applyFilters();
            s.visible = wasVisible;
        }

        stamp2d();
    }

    public function setVideoState(newState:String):void {
        if ('off' == newState) {
            if (video) video.attachCamera(null); // turn off camera
            if (videoImage && videoImage.parent) videoImage.parent.removeChild(videoImage);
            video = null;
            videoImage = null;
            return;
        }
        flipVideo = ('on' == newState); // 'on' means mirrored; 'on-flip' means unmirrored
        if (camera == null) {
            // Set up the camera only the first time it is used.
            camera = Camera.getCamera();
            if (!camera) return; // no camera available or access denied
            camera.setMode(WitObj.STAGEW, WitObj.STAGEH, 30);
        }
        if (video == null) {
            video = new Video(WitObj.STAGEW, WitObj.STAGEH);
            video.attachCamera(camera);
            videoImage = new Bitmap(new BitmapData(video.width, video.height, false));
            videoImage.alpha = videoAlpha;
            addChildAt(videoImage, getChildIndex(penLayer) + 1);
        }
    }

    public function setVideoTransparency(transparency:Number):void {
        videoAlpha = 1 - Math.max(0, Math.min(transparency / 100, 1));
        if (videoImage) {
            videoImage.alpha = videoAlpha;
        }
    }

    public function isVideoOn():Boolean {
        return videoImage != null
    }

    /* Pen support */

    public function clearPenStrokes():void {
        var bm:BitmapData = penLayer.bitmapData;
        bm.fillRect(bm.rect, 0);
        newPenStrokes.graphics.clear();
        penActivity = false;
    }

    public function commitPenStrokes():void {
        if (!penActivity) return;
        penLayer.bitmapData.draw(newPenStrokes);
        newPenStrokes.graphics.clear();
        penActivity = false;
    }

    private var cachedBM:BitmapData;
    private var cachedBitmapIsCurrent:Boolean;

    private function updateCachedBitmap():void {
        if (cachedBitmapIsCurrent) return;
        if (!cachedBM) cachedBM = new BitmapData(STAGEW, STAGEH, false);
        cachedBM.fillRect(cachedBM.rect, 0xF0F080);
        cachedBM.draw(img);
        if (penLayer) cachedBM.draw(penLayer);
        if (videoImage) cachedBM.draw(videoImage);
        cachedBitmapIsCurrent = true;
    }

    public function bitmapWithoutSprite(s:WitSprite):BitmapData {
        // Used by the 'touching color' primitives. Draw the background layers
        // and all sprites (but not watchers or talk bubbles) except the given
        // sprite within the bounding rectangle of the given sprite into
        // a bitmap and return it.

        var r:Rectangle = s.bounds();
        var bm:BitmapData = new BitmapData(r.width, r.height, false);

        if (!cachedBitmapIsCurrent) updateCachedBitmap();

        var m:Matrix = new Matrix();
        m.translate(-Math.floor(r.x), -Math.floor(r.y));
        bm.draw(cachedBM, m);

        for (var i:int = 0; i < this.numChildren; i++) {
            var o:WitSprite = this.getChildAt(i) as WitSprite;
            if (o && (o != s) && o.visible && o.bounds().intersects(r)) {
                m.identity();
                m.translate(o.img.x, o.img.y);
                m.rotate((Math.PI * o.rotation) / 180);
                m.scale(o.scaleX, o.scaleY);
                m.translate(o.x - r.x, o.y - r.y);
                m.tx = Math.floor(m.tx);
                m.ty = Math.floor(m.ty);
                var colorTransform:ColorTransform = (o.img.alpha == 1) ? null :
                        new ColorTransform(1, 1, 1, o.img.alpha);
                bm.draw(o.img, m, colorTransform);
            }
        }

        return bm;
    }

    public function getBitmapWithoutSpriteFilteredByColor(s:WitSprite, c:int):BitmapData {
        commitPenStrokes(); // force any pen strokes to be rendered so they can be sensed

        var bm1:BitmapData;
        var mask:uint = 0x00F8F8F0;
        // OLD code here
        bm1 = bitmapWithoutSprite(s);

        var bm2:BitmapData = new BitmapData(bm1.width, bm1.height, true, 0);
        bm2.threshold(bm1, bm1.rect, bm1.rect.topLeft, '==', c, 0xFF000000, mask); // match only top five bits of each

        return bm2;
    }

    public function savePenLayer():void {
        penLayerID = -1;
        penLayerPNG = PNG24Encoder.encode(penLayer.bitmapData, PNGFilter.PAETH);
        penLayerMD5 = MD5.hashBytes(penLayerPNG) + '.png';
    }

    public function clearPenLayer():void {
        penLayerPNG = null;
        penLayerMD5 = null;
    }

    public function isEmpty():Boolean {
        // Return true if this project has no scripts, no variables, no lists,
        // at most one sprite, and only the default costumes and sound media.
        var defaultMedia:Array = [
            '510da64cf172d53750dffd23fbf73563.png', 'b82f959ab7fa28a70b06c8162b7fef83.svg',
            'df0e59dcdea889efae55eb77902edc1c.svg', '83a9787d4cb6f3b7632b4ddfebf74367.wav',
            'f9a1c175dbe2e5dee472858dd30d16bb.svg', '6e8bd9ae68fdb02b7e1e3df656a75635.svg',
            '0aa976d536ad6667ce05f9f2174ceb3d.svg',	// new empty backdrop
            '790f7842ea100f71b34e5b9a5bfbcaa1.svg', // even newer empty backdrop
            'c969115cb6a3b75470f8897fbda5c9c9.svg'	// new empty costume
        ];
        if (sprites().length > 1) return false;
        if (scriptCount() > 0) return false;
        for each (var obj:WitObj in allObjects()) {
            if (obj.variables.length > 0) return false;
            if (obj.lists.length > 0) return false;
            for each (var c:WitCostume in obj.costumes) {
                if (defaultMedia.indexOf(c.baseLayerMD5) < 0) return false;
            }
            for each (var snd:WitSound in obj.sounds) {
                if (defaultMedia.indexOf(snd.md5) < 0) return false;
            }
        }
        return true;
    }

    public function updateInfo():void {
        info.scriptCount = scriptCount();
        info.flashVersion = Capabilities.version;
        info.videoOn = isVideoOn();
        if (TempStatic.usbID.length > 0) {
            info.deviceID = TempStatic.usbID;
        }

        delete info.loadInProgress;
        if (TempStatic.loadInProgress) info.loadInProgress = true; // log flag for debugging

        if (this == TempStatic.witStage) {
            // If this is the active stage pane, record the current extensions.
            var extensionsToSave:Array = TempStatic.extensionManager.extensionsToSave();
            if (extensionsToSave.length == 0) delete info.savedExtensions;
            else info.savedExtensions = extensionsToSave;
        }

        delete info.userAgent;
    }

    public function setPresentationMode(enterPresentation:Boolean):void {
        TempStatic.editMode = !enterPresentation;
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_EDIT_MODE, TempStatic.editMode, true));
        for each (var o:WitObj in allObjects()) o.applyFilters();

        updateCostume();
    }

    public function updateListWatchers():void {
        for (var i:int = 0; i < numChildren; i++) {
            var c:* = getChildAt(i);
            if (getQualifiedClassName(c) == "wp.editor.watchers::ListWatcher") {
                c.updateContents();
            }
        }
    }

    public function scriptCount():int {
        var scriptCount:int = 0;
        for each (var obj:WitObj in allObjects()) {
            for each (var b:* in obj.scripts) {
                if (getQualifiedClassName(b) != 'wp.editor.blocks::Block') continue;
                if (b.isHat) continue;
                scriptCount++;
            }
        }
        return scriptCount;
    }

    /* Dropping */
    public function handleDrop(obj:*):Boolean {
        switch (getQualifiedClassName(obj)) {
            case "wp.editor.wit::WitSprite":
            case "wp.editor.watchers::Watcher":
            case "wp.editor.watchers::ListWatcher":
                if (scaleX != 1) {
                    obj.scaleX = obj.scaleY = obj.scaleX / scaleX; // revert to original scale
                }
                var p:Point = globalToLocal(new Point(obj.x, obj.y));
                obj.x = p.x;
                obj.y = p.y;
                if (obj.parent) obj.parent.removeChild(obj); // force redisplay
                addChild(obj);
                if (obj is WitSprite) {
                    (obj as WitSprite).updateCostume();
                    obj.setScratchXY(p.x - 240, 180 - p.y);
                    TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, obj, true));
                    obj.setScratchXY(p.x - 240, 180 - p.y); // needed because selectSprite() moves sprite back if costumes
                                                            // tab is open
                    (obj as WitObj).applyFilters();
                }
                if (!(obj is WitSprite) || TempStatic.editMode) {
                    TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
                }
                return true;
            default:
                TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
                return false;
        }
    }

    /* Saving */
    override public function toJSON(k:String):Object {
        var obj:Object = super.toJSON(k);

        var children:Array = [];
        var c:*;
        for (var i:int = 0; i < numChildren; i++) {
            c = getChildAt(i);
            switch (getQualifiedClassName(c)) {
                case "wp.editor.wit::WitSprite":
                    if (c.isClone) break;
                case "wp.editor.watchers::Watcher":
                case "wp.editor.watchers::ListWatcher":
                    children.push(c);
                    break;
            }
        }

        // If UI elements are on another layer (during 3d rendering), process them from there
        var uiLayer:Sprite = getUILayer();
        if (uiLayer != this) {
            for (i = 0; i < uiLayer.numChildren; i++) {
                c = uiLayer.getChildAt(i);
                switch (getQualifiedClassName(c)) {
                    case "wp.editor.wit::WitSprite":
                        if (c.isClone) break;
                    case "wp.editor.watchers::Watcher":
                    case "wp.editor.watchers::ListWatcher":
                        children.push(c);
                        break;
                }
            }
        }
        obj['penLayerMD5'] = penLayerMD5;
        obj['penLayerID'] = penLayerID;
        obj['tempoBPM'] = tempoBPM;
        obj['videoAlpha'] = videoAlpha;
        obj['info'] = info;
        obj['children'] = children;

        return obj;
    }

    public override function readJSON(jsonObj:Object):void {
        var children:Array, i:int, o:Object;

        // read stage fields
        super.readJSON(jsonObj);
        penLayerMD5 = jsonObj.penLayerMD5;
        tempoBPM = jsonObj.tempoBPM;
        if (jsonObj.videoAlpha) videoAlpha = jsonObj.videoAlpha;
        if (jsonObj.children) children = jsonObj.children;
        info = jsonObj.info;

        // instantiate sprites and record their names
        var spriteNameMap:Object = {};
        spriteNameMap[objName] = this; // record stage's name
        for (i = 0; i < children.length; i++) {
            o = children[i];
            if (o.objName != undefined) { // o is a sprite record
                var s:WitSprite = new WitSprite();
                s.readJSON(o);
                spriteNameMap[s.objName] = s;
                children[i] = s;
            }
        }

        // instantiate Watchers and add all children (sprites and watchers)
        for (i = 0; i < children.length; i++) {
            o = children[i];
            if (o is WitSprite) {
                addChild(WitSprite(o));
            }
            else if (o.sliderMin != undefined) { // o is a watcher record
                o.target = spriteNameMap[o.target]; // update target before instantiating
                if (o.target) {
                    if (o.cmd == "senseVideoMotion" && o.param && o.param.indexOf(',')) {
                        // fix old video motion/direction watchers
                        var args:Array = o.param.split(',');
                        if (args[1] == 'this sprite') continue;
                        o.param = args[0];
                    }
                    var WClass:Class = getDefinitionByName("wp.editor.watchers::Watcher") as Class;
                    if (WClass) {
                        var w:* = new WClass();
                        w.readJSON(o);
                        addChild(w);
                    }
                }
            }
        }

        // instantiate lists, variables, scripts, costumes, and sounds
        for each (var scratchObj:WitObj in allObjects()) {
            scratchObj.instantiateFromJSON(this);
        }
    }
}
}
