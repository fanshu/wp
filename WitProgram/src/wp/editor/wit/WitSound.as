// Represents a WitProgram sampled sound.
// Possible formats:
//	''			WAVE format 1, 16-bit uncompressed
//	'adpcm'		WAVE format 17, 4-bit ADPCM
//	'squeak'	Squeak ADPCM format, 2-bits to 5-bits per sample
//
// Note: 'mp3' format was removed during alpha test to avoid the need to support MP3 in the future.
package wp.editor.wit {
import by.blooddy.crypto.MD5;

import flash.utils.*;

import wp.editor.sound.*;
import wp.editor.sound.mp3.MP3Loader;

public class WitSound {
    public var soundName:String = '';
    public var soundID:int;
    public var md5:String;
    private var __soundData:ByteArray = new ByteArray();
    public var format:String = '';
    public var rate:int = 44100;
    public var sampleCount:int;
    public var sampleDataStart:int;
    public var bitsPerSample:int; // primarily used for compressed Squeak sounds; not saved

    public var editorData:Object; // cache of data used by sound editor; not saved
    public var channels:uint = 1;
    private const WasEdited:int = -10; // special soundID used to indicate sounds that have been edited

    public function WitSound(name:String, sndData:ByteArray) {
        this.soundName = name;
        if (sndData != null) {
            try {
                var info:Object = WAVFile.decode(sndData);
                if ([1, 3, 17].indexOf(info.encoding) == -1) throw Error('Unsupported WAV format');
                soundData = sndData;
                if (info.encoding == 17)
                    format = 'adpcm';
                else if (info.encoding == 3)
                    format = 'float';
                rate = info.samplesPerSecond;
                sampleCount = info.sampleCount;
                bitsPerSample = info.bitsPerSample;
                channels = info.channels;
                sampleDataStart = info.sampleDataStart;
                reduceSizeIfNeeded(info.channels);
            } catch (e:*) {
                setSamples(new Vector.<int>(0), 22050);
            }
        }
    }

    public function get soundData():ByteArray {
        return __soundData;
    }

    public function set soundData(data:ByteArray):void {
        __soundData = data;
        md5 = null;
    }

    private function reduceSizeIfNeeded(channels:int):void {
        // Convert stereo to mono, downsample if rate > 32000, or both.
        // Compress if data is over threshold and not already compressed.
        const compressionThreshold:int = 30 * 44100; // about 30 seconds
        if (rate > 32000 || channels == 2 || format == 'float') {
            var newRate:int = (rate > 32000) ? rate / 2 : rate;
            var samples:Vector.<int> = WAVFile.extractSamples(soundData);
            if (rate > 32000 || channels == 2)
                samples = (channels == 2) ?
                        stereoToMono(samples, (newRate < rate)) :
                        downsample(samples);
            setSamples(samples, newRate, true);
            soundID = 0;
        } else if ((soundData.length > compressionThreshold) && ('' == format)) {
            // Compress large, uncompressed sounds
            setSamples(WAVFile.extractSamples(soundData), rate, true);
        }
    }

    private function stereoToMono(stereo:Vector.<int>, downsample:Boolean):Vector.<int> {
        var mono:Vector.<int> = new Vector.<int>();
        var skip:int = downsample ? 4 : 2;
        var i:int = 0, end:int = stereo.length - 1;
        while (i < end) {
            mono.push((stereo[i] + stereo[i + 1]) / 2);
            i += skip;
        }
        return mono;
    }

    private function downsample(samples:Vector.<int>):Vector.<int> {
        var result:Vector.<int> = new Vector.<int>();
        for (var i:int = 0; i < samples.length; i += 2) result.push(samples[i]);
        return result;
    }

    public function setSamples(samples:Vector.<int>, samplingRate:int, compress:Boolean = false):void {
        var data:ByteArray = new ByteArray();
        data.endian = Endian.LITTLE_ENDIAN;
        for (var i:int = 0; i < samples.length; i++) data.writeShort(samples[i]);
        if (samples.length == 0) data.writeShort(0); // a WAV file must have at least one sample

        soundID = WasEdited;
        soundData = WAVFile.encode(data, samples.length, samplingRate, compress);
        format = compress ? 'adpcm' : '';
        rate = samplingRate;
        sampleCount = samples.length;
    }

    public function convertMP3IfNeeded():void {
        // Support for converting MP3 format sounds in WitProgram projects was removed during alpha test.
        // If this is on old, MP3 formatted sound, convert it to WAV format. Otherwise, do nothing.
        function whenDone(snd:WitSound):void {
            trace('Converting MP3 to WAV: ' + soundName);
            md5 = null;
            soundData = snd.soundData;
            format = snd.format;
            rate = snd.rate;
            sampleCount = snd.sampleCount;
        }

        if (format == 'mp3') {
            if (soundData) MP3Loader.convertToScratchSound('', soundData, whenDone);
            else setSamples(new Vector.<int>, 22050);
        }
    }

    public function sndplayer():WitSoundPlayer {
        var player:WitSoundPlayer;
        if (format == 'squeak') player = new SqueakSoundPlayer(soundData, bitsPerSample, rate);
        else if (format == '' || format == 'adpcm' || format == 'float') player = new WitSoundPlayer(soundData);
        else player = new WitSoundPlayer(WAVFile.empty()); // player on empty sound
        player.scratchSound = this;
        return player;
    }

    public function duplicate():WitSound {
        var dup:WitSound = new WitSound(soundName, null);
        dup.setSamples(getSamples(), rate, (format == 'adpcm'));
        return dup;
    }

    public function getSamples():Vector.<int> {
        if (format == 'squeak') prepareToSave(); // convert to WAV
        if ((format == '') || (format == 'adpcm')) return WAVFile.extractSamples(soundData);
        return new Vector.<int>(0); // dummy data
    }

    public function getLengthInMsec():Number {
        return (1000.0 * sampleCount) / rate
    }

    public function toString():String {
        var secs:Number = Math.ceil(getLengthInMsec() / 1000);
        var result:String = 'WitSound(' + secs + ' secs, ' + rate;
        if (format != '') result += ' ' + format;
        result += ')';
        return result;
    }

    public function prepareToSave():void {
        if (format == 'squeak') { // convert Squeak ADPCM to WAV ADPCM
            var uncompressedData:ByteArray = new SqueakSoundDecoder(bitsPerSample).decode(soundData);
            if (uncompressedData.length == 0) uncompressedData.writeShort(0); // a WAV file must have at least one sample
            trace('Converting squeak sound to WAV ADPCM; sampleCount old: ' + sampleCount + ' new: ' + (uncompressedData.length / 2));
            sampleCount = uncompressedData.length / 2;
            soundData = WAVFile.encode(uncompressedData, sampleCount, rate, true);
            format = 'adpcm';
            bitsPerSample = 4;
            md5 = null;
        }
        reduceSizeIfNeeded(1); // downsample or compress to reduce size before saving
        if (soundID == WasEdited) {
            md5 = null;
            soundID = -1
        } // sound was edited; force md5 to be recomputed
        if (!md5) md5 = MD5.hashBytes(soundData) + '.wav';
    }

    public static function isWAV(data:ByteArray):Boolean {
        if (data.length < 12) return false;
        data.position = 0;
        if (data.readUTFBytes(4) != 'RIFF') return false;
        data.readInt();
        return (data.readUTFBytes(4) == 'WAVE');
    }

    public function toJSON(k:String):Object {
        return {
            soundName: soundName,
            soundID: soundID,
            md5: md5,
            sampleCount: sampleCount,
            rate: rate,
            format: format
        };
    }

    public function readJSON(jsonObj:Object):void {
        soundName = jsonObj.soundName;
        soundID = jsonObj.soundID;
        md5 = jsonObj.md5;
        sampleCount = jsonObj.sampleCount;
        rate = jsonObj.rate;
        format = jsonObj.format;
    }
}
}
