package wp.editor.wit {
import flash.display.*;
import flash.events.*;
import flash.media.*;
import flash.text.TextField;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.*;

import wp.editor.blocks.Block;
import wp.editor.blocks.BlockArg;
import wp.editor.controllers.ContextEvtConst;
import wp.editor.interpreter.*;
import wp.editor.primitives.VideoMotionPrims;
import wp.editor.ui.BlockPalette;
import wp.editor.uiwidgets.DialogBox;
import wp.editor.watchers.*;

public class WitRuntime {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    private static const _:Function = gettext;

    public var motionDetector:VideoMotionPrims;
    public var keyIsDown:Array = new Array(128); // records key up/down state
    public var shiftIsDown:Boolean;
    public var lastAnswer:String = '';
    public var cloneCount:int;
    public var edgeTriggersEnabled:Boolean = false; // initially false, becomes true when project first run

    private var microphone:Microphone;
    private var timerBase:uint;

    public function WitRuntime() {
        TempStatic.runtime = this;
        timerBase = TempStatic.interp.currentMSecs;
        clearKeyDownArray();
    }

    // -----------------------------
    // Running and stopping
    //------------------------------

    public function stepRuntime():void {
        TempStatic.extensionManager.step();
        if (motionDetector) motionDetector.step(); // Video motion detection

        // Step the stage, sprites, and watchers
        TempStatic.witStage.step(this);

        // run scripts and commit any pen strokes
        processEdgeTriggeredHats();
        TempStatic.interp.stepThreads();
        TempStatic.witStage.commitPenStrokes();
    }

    // -----------------------------
    // Hat Blocks
    //------------------------------

    public function collectBroadcasts():Array {
        function addBlock(b:Block):void {
            if ((b.op == 'broadcast:') ||
                    (b.op == 'doBroadcastAndWait') ||
                    (b.op == 'whenIReceive')) {
                if (b.args[0] is BlockArg) {
                    var msg:String = b.args[0].argValue;
                    if (result.indexOf(msg) < 0) result.push(msg);
                }
            }
        }

        var result:Array = [];
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ALL_STACKS_AND_OWNERS_DO,
                function (stack:Block, target:WitObj):void {
                    stack.allBlocksDo(addBlock);
                }));
        var palette:BlockPalette = WitProgram.palette;
        for (var i:int = 0; i < palette.numChildren; i++) {
            var b:Block = palette.getChildAt(i) as Block;
            if (b) addBlock(b);
        }
        result.sort();
        return result;
    }

    // -----------------------------
    // Edge-trigger sensor hats
    //------------------------------

    public var triggeredHats:Array = [];

    // hats whose triggering condition is currently true
    private var activeHats:Array = [];


    private function processEdgeTriggeredHats():void {
        if (!edgeTriggersEnabled) return;
        activeHats = [];
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ALL_STACKS_AND_OWNERS_DO,
                function (hat:Block, target:WitObj):void {
                    if (!hat.isHat || !hat.nextBlock) return; // skip disconnected hats

                    if ('whenSensorGreaterThan' == hat.op) {
                        var sensorName:String = TempStatic.interp.arg(hat, 0);
                        var threshold:Number = TempStatic.interp.numarg(hat, 1);
                        if (('loudness' == sensorName && soundLevel() > threshold) ||
                                ('timer' == sensorName && timer() > threshold) ||
                                ('video motion' == sensorName && target.visible && VideoMotionPrims.readMotionSensor('motion', target) > threshold)) {
                            if (triggeredHats.indexOf(hat) == -1) { // not already trigged
                                // only start the stack if it is not already running
                                if (!TempStatic.interp.isRunning(hat, target)) TempStatic.interp.toggleThread(hat, target);
                            }
                            activeHats.push(hat);
                        }
                    } else if ('whenSensorConnected' == hat.op) {
                        if (getBooleanSensor(TempStatic.interp.arg(hat, 0))) {
                            if (triggeredHats.indexOf(hat) == -1) { // not already trigged
                                // only start the stack if it is not already running
                                if (!TempStatic.interp.isRunning(hat, target)) TempStatic.interp.toggleThread(hat, target);
                            }
                            activeHats.push(hat);
                        }
                    }
                }));
        triggeredHats = activeHats;
    }

    // -----------------------------
    // Ask prompter
    //------------------------------

    public function askPromptShowing():Boolean {
        var uiLayer:Sprite = TempStatic.witStage.getUILayer();
        for (var i:int = 0; i < uiLayer.numChildren; i++) {
            if (uiLayer.getChildAt(i) is AskPrompter)
                return true;
        }
        return false;
    }

    // -----------------------------
    // Keyboard input handling
    //------------------------------

    public function keyDown(evt:KeyboardEvent):void {
        shiftIsDown = evt.shiftKey;
        var ch:int = evt.charCode;
        if (evt.charCode == 0) ch = mapArrowKey(evt.keyCode);
        if ((65 <= ch) && (ch <= 90)) ch += 32; // map A-Z to a-z
        if (!(evt.target is TextField)) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.START_KEY_HATS, ch));
        }
        if (ch < 128) keyIsDown[ch] = true;
    }

    public function keyUp(evt:KeyboardEvent):void {
        shiftIsDown = evt.shiftKey;
        var ch:int = evt.charCode;
        if (evt.charCode == 0) ch = mapArrowKey(evt.keyCode);
        if ((65 <= ch) && (ch <= 90)) ch += 32; // map A-Z to a-z
        if (ch < 128) keyIsDown[ch] = false;
    }

    public function clearKeyDownArray():void {
        for (var i:int = 0; i < 128; i++) keyIsDown[i] = false;
    }

    private function mapArrowKey(keyCode:int):int {
        // map key codes for arrow keys to ASCII, other key codes to zero
        if (keyCode == 37) return 28;
        if (keyCode == 38) return 30;
        if (keyCode == 39) return 29;
        if (keyCode == 40) return 31;
        return 0;
    }

    // -----------------------------
    // Sensors
    //------------------------------

    public function getSensor(sensorName:String):Number {
        return TempStatic.extensionManager.getStateVar('PicoBoard', sensorName, 0);
    }

    public function getBooleanSensor(sensorName:String):Boolean {
        if (sensorName == 'button pressed') return TempStatic.extensionManager.getStateVar('PicoBoard', 'button', 1023) < 10;
        if (sensorName.indexOf('connected') > -1) { // 'A connected' etc.
            sensorName = 'resistance-' + sensorName.charAt(0);
            return TempStatic.extensionManager.getStateVar('PicoBoard', sensorName, 1023) < 10;
        }
        return false;
    }

    public function getTimeString(which:String):* {
        // Return local time properties.
        var now:Date = new Date();
        switch (which) {
            case 'hour':
                return now.hours;
            case 'minute':
                return now.minutes;
            case 'second':
                return now.seconds;
            case 'year':
                return now.fullYear; // four digit year (e.g. 2012)
            case 'month':
                return now.month + 1; // 1-12
            case 'date':
                return now.date; // 1-31
            case 'day of week':
                return now.day + 1; // 1-7, where 1 is Sunday
        }
        return ''; // shouldn't happen
    }

    // -----------------------------
    // Variables
    //------------------------------

    public function allVarNames():Array {
        var result:Array = [], v:Variable;
        for each (v in TempStatic.witStage.variables) result.push(v.name);
        if (!(WitProgram.viewedObj is WitStage)) {
            for each (v in WitProgram.viewedObj.variables) result.push(v.name);
        }
        return result;
    }

    public function makeVariable(varObj:Object):Variable {
        return new Variable(varObj.name, varObj.value);
    }

    // -----------------------------
    // Lists
    //------------------------------

    public function allListNames():Array {
        var result:Array = TempStatic.witStage.listNames();
        if (!(WitProgram.viewedObj is WitStage)) {
            result = result.concat(WitProgram.viewedObj.listNames());
        }
        return result;
    }

    // -----------------------------
    // Sensing
    //------------------------------

    public function timer():Number {
        return (TempStatic.interp.currentMSecs - timerBase) / 1000
    }

    public function timerReset():void {
        timerBase = TempStatic.interp.currentMSecs
    }

    public function isLoud():Boolean {
        return soundLevel() > 10
    }

    public function soundLevel():int {
        if (microphone == null) {
            microphone = Microphone.getMicrophone();
            if (microphone) {
                microphone.setLoopBack(true);
                microphone.soundTransform = new SoundTransform(0, 0);
            }
        }
        return microphone ? microphone.activityLevel : 0;
    }

    // -----------------------------
    // Script utilities
    //------------------------------
    public function allSendersOfBroadcast(msg:String):Array {
        // Return an array of all WitProgram objects that broadcast the given message.
        var result:Array = [];
        for each (var o:WitObj in TempStatic.witStage.allObjects()) {
            if (sendsBroadcast(o, msg)) result.push(o);
        }
        return result;
    }

    public function allReceiversOfBroadcast(msg:String):Array {
        // Return an array of all WitProgram objects that receive the given message.
        var result:Array = [];
        for each (var o:WitObj in TempStatic.witStage.allObjects()) {
            if (receivesBroadcast(o, msg)) result.push(o);
        }
        return result;
    }

    private function sendsBroadcast(obj:WitObj, msg:String):Boolean {
        for each (var stack:Block in obj.scripts) {
            var found:Boolean;
            stack.allBlocksDo(function (b:Block):void {
                if (b.op == 'broadcast:' || b.op == 'doBroadcastAndWait') {
                    if (b.broadcastMsg == msg) found = true;
                }
            });
            if (found) return true;
        }
        return false;
    }

    private function receivesBroadcast(obj:WitObj, msg:String):Boolean {
        msg = msg.toLowerCase();
        for each (var stack:Block in obj.scripts) {
            var found:Boolean;
            stack.allBlocksDo(function (b:Block):void {
                if (b.op == 'whenIReceive') {
                    if (b.broadcastMsg.toLowerCase() == msg) found = true;
                }
            });
            if (found) return true;
        }
        return false;
    }

    public function allUsesOfSprite(spriteName:String):Array {
        var spriteMenus:Array = ["spriteOnly", "spriteOrMouse", "spriteOrStage", "touching"];
        var result:Array = [];
        for each (var stack:Block in allStacks()) {
            // for each block in stack
            stack.allBlocksDo(function (b:Block):void {
                for each (var a:* in b.args) {
                    if (a is BlockArg && spriteMenus.indexOf(a.menuName) != -1 && a.argValue == spriteName) result.push(a);
                }
            });
        }
        return result;
    }

    public function allCallsOf(callee:String, owner:WitObj, includeRecursive:Boolean = true):Array {
        var result:Array = [];
        for each (var stack:Block in owner.scripts) {
            if (!includeRecursive && stack.op == Specs.PROCEDURE_DEF && stack.spec == callee) continue;
            // for each block in stack
            stack.allBlocksDo(function (b:Block):void {
                if (b.op == Specs.CALL && b.spec == callee) result.push(b);
            });
        }
        return result;
    }

    public function allStacks():Array {
        // return an array containing all stacks in all objects
        var result:Array = [];
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ALL_STACKS_AND_OWNERS_DO,
                function (stack:Block, target:WitObj):void {
                    result.push(stack)
                }));
        return result;
    }

    // -----------------------------
    // Variable, List, and Reporter Watchers
    //------------------------------
    public function watcherShowing(data:Object):Boolean {
        if ('variable' == data.type) {
            var targetObj:WitObj = data.targetObj;
            var varName:String = data.varName;
            var uiLayer:Sprite = TempStatic.witStage.getUILayer();
            var i:int;
            if (data.isList)
                for (i = 0; i < uiLayer.numChildren; i++) {
                    var listW:ListWatcher = uiLayer.getChildAt(i) as ListWatcher;
                    if (listW && (listW.listName == varName) && listW.visible) return true;
                }
            else
                for (i = 0; i < uiLayer.numChildren; i++) {
                    var varW:Watcher = uiLayer.getChildAt(i) as Watcher;
                    if (varW && varW.isVarWatcherFor(targetObj, varName) && varW.visible) return true;
                }
        }
        if ('reporter' == data.type) {
            var w:Watcher = findReporterWatcher(data);
            return w && w.visible;
        }
        return false;
    }

    public function findReporterWatcher(data:Object):Watcher {
        var uiLayer:Sprite = TempStatic.witStage.getUILayer();
        for (var i:int = 0; i < uiLayer.numChildren; i++) {
            var w:Watcher = uiLayer.getChildAt(i) as Watcher;
            if (w && w.isReporterWatcher(data.targetObj, data.cmd, data.param)) return w;
        }
        return null;
    }

    // -----------------------------
    // Undelete support
    //------------------------------

    public var lastDelete:Array; // object, x, y, owner (for blocks/stacks/costumes/sounds)

    public function canUndelete():Boolean {
        return lastDelete != null
    }
}
}
