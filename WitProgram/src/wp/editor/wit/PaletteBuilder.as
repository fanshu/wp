// PaletteBuilder generates the contents of the blocks palette for a given
// category, including the blocks, buttons, and watcher toggle boxes.
package wp.editor.wit {
import flash.display.*;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.ColorTransform;
import flash.net.*;
import flash.text.*;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.TempStatic;
import util.UIPartUtil;

import wp.editor.blocks.*;
import wp.editor.controllers.ContextEvtConst;
import wp.editor.extensions.*;
import wp.editor.ui.ProcedureSpecEditor;
import wp.editor.ui.media.MediaLibrary;
import wp.editor.uiwidgets.*;

public class PaletteBuilder {
    private static const _:Function = gettext;

    protected var nextY:int;

    public function PaletteBuilder() {
    }

    public function showBlocksForCategory(selectedCategory:int, scrollToOrigin:Boolean, shiftKey:Boolean = false):void {
        if (WitProgram.palette == null) return;
        WitProgram.palette.clear(scrollToOrigin);
        nextY = 7;

        if (selectedCategory == Specs.dataCategory) {
            showDataCategory();
            return;
        }
        if (selectedCategory == Specs.myBlocksCategory) {
            showMyBlocksPalette(shiftKey);
            return;
        }

        var catName:String = Specs.categories[selectedCategory][1];
        var catColor:int = Specs.blockColor(selectedCategory);
        if (WitProgram.viewedObj && (WitProgram.viewedObj is WitStage)) {
            // The stage has different blocks for some categories:
            var stageSpecific:Array = ['Control', 'Looks', 'Motion', 'Pen', 'Sensing'];
            if (stageSpecific.indexOf(catName) != -1) selectedCategory += 100;
            if (catName == 'Motion') {
                addItem(makeLabel(_('Stage selected:')));
                nextY -= 6;
                addItem(makeLabel(_('No motion blocks')));
                return;
            }
        }
        addBlocksForCategory(selectedCategory, catColor);
        updateCheckboxes();
    }

    private function addBlocksForCategory(category:int, catColor:int):void {
        if (WitProgram.viewedObj == null) return;
        var cmdCount:int;
        var targetObj:WitObj = WitProgram.viewedObj;
        for each (var spec:Array in Specs.commands) {
            if ((spec.length > 3) && (spec[2] == category)) {
                var blockColor:int = (TempStatic.interp.isImplemented(spec[3])) ? catColor : 0x505050;
                var defaultArgs:Array = targetObj.defaultArgsFor(spec[3], spec.slice(4));
                var label:String = spec[0];
                if ((targetObj is WitStage) && spec[3] == 'whenClicked') label = 'when Stage clicked';
                var block:Block = new Block(label, spec[1], blockColor, spec[3], defaultArgs);
                var showCheckbox:Boolean = isCheckboxReporter(spec[3]);
                if (showCheckbox) addReporterCheckbox(block);
                addItem(block, showCheckbox);
                cmdCount++;
            } else {
                if ((spec.length == 1) && (cmdCount > 0)) nextY += 10 * spec[0].length; // add some space
                cmdCount = 0;
            }
        }
    }

    protected function addItem(o:DisplayObject, hasCheckbox:Boolean = false):void {
        o.x = hasCheckbox ? 23 : 6;
        o.y = nextY;
        WitProgram.palette.addChild(o);
        WitProgram.palette.updateSize();
        nextY += o.height + 5;
    }

    private function makeLabel(label:String):TextField {
        var t:TextField = new TextField();
        t.autoSize = TextFieldAutoSize.LEFT;
        t.selectable = false;
        t.background = false;
        t.text = label;
        t.setTextFormat(CSS.normalTextFormat);
        return t;
    }

    private function showMyBlocksPalette(shiftKey:Boolean):void {
        // show creation button, hat, and call blocks
        var catColor:int = Specs.blockColor(Specs.procedureColor);
        addItem(new Button(_('Make a Block'), makeNewBlock, false, '/help/studio/tips/blocks/make-a-block/'));
        var definitions:Array = WitProgram.viewedObj.procedureDefinitions();
        if (definitions.length > 0) {
            nextY += 5;
            for each (var proc:Block in definitions) {
                var b:Block = new Block(proc.spec, ' ', Specs.procedureColor, Specs.CALL, proc.defaultArgValues);
                addItem(b);
            }
            nextY += 5;
        }

        addExtensionButtons();
        for each (var ext:* in TempStatic.extensionManager.enabledExtensions()) {
            addExtensionSeparator(ext);
            addBlocksForExtension(ext);
        }

        updateCheckboxes();
    }

    protected function addExtensionButtons():void {
        addAddExtensionButton();
    }

    protected function addAddExtensionButton():void {
        addItem(new Button(_('Add an Extension'), showAnExtension, false, '/help/studio/tips/blocks/add-an-extension/'));
    }

    private function showDataCategory():void {
        var catColor:int = Specs.variableColor;

        // variable buttons, reporters, and set/change blocks
        addItem(new Button(_('Make a Variable'), makeVariable));
        var varNames:Array = TempStatic.runtime.allVarNames().sort();
        if (varNames.length > 0) {
            for each (var n:String in varNames) {
                addVariableCheckbox(n, false);
                addItem(new Block(n, 'r', catColor, Specs.GET_VAR), true);
            }
            nextY += 10;
            addBlocksForCategory(Specs.dataCategory, catColor);
            nextY += 15;
        }

        // lists
        catColor = Specs.listColor;
        addItem(new Button(_('Make a List'), makeList));

        var listNames:Array = TempStatic.runtime.allListNames().sort();
        if (listNames.length > 0) {
            for each (n in listNames) {
                addVariableCheckbox(n, true);
                addItem(new Block(n, 'r', catColor, Specs.GET_LIST), true);
            }
            nextY += 10;
            addBlocksForCategory(Specs.listCategory, catColor);
        }
        updateCheckboxes();
    }

    protected function createVar(name:String, varSettings:VariableSettings):* {
        var obj:WitObj = (varSettings.isLocal) ? WitProgram.viewedObj : TempStatic.witStage;
        if (obj.hasName(name)) {
            DialogBox.notify("Cannot Add", "That name is already in use.");
            return;
        }
        var variable:* = (varSettings.isList ? obj.lookupOrCreateList(name) : obj.lookupOrCreateVar(name));

        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.TOGGLE_VAR_OR_LIST_FOR,
                {
                    varName: name,
                    isList: varSettings.isList,
                    targetObj: obj,
                    visible: true
                }
        ));
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));

        return variable;
    }

    private function makeVariable():void {
        function makeVar2():void {
            var n:String = d.getField('Variable name').replace(/^\s+|\s+$/g, '');
            if (n.length == 0) return;

            createVar(n, varSettings);
        }

        var d:DialogBox = new DialogBox(makeVar2);
        var varSettings:VariableSettings = makeVarSettings(false, (WitProgram.viewedObj is WitStage));
        d.addTitle('New Variable');
        d.addField('Variable name', 150);
        d.addWidget(varSettings);
        d.addAcceptCancelButtons('OK');
        d.showOnStage(TempStatic.stagePart.stage);
    }

    private function makeList():void {
        function makeList2(d:DialogBox):void {
            var n:String = d.getField('List name').replace(/^\s+|\s+$/g, '');
            if (n.length == 0) return;

            createVar(n, varSettings);
        }

        var d:DialogBox = new DialogBox(makeList2);
        var varSettings:VariableSettings = makeVarSettings(true, (WitProgram.viewedObj is WitStage));
        d.addTitle('New List');
        d.addField('List name', 150);
        d.addWidget(varSettings);
        d.addAcceptCancelButtons('OK');
        d.showOnStage(TempStatic.stagePart.stage);
    }

    protected function makeVarSettings(isList:Boolean, isStage:Boolean):VariableSettings {
        return new VariableSettings(isList, isStage);
    }

    private function makeNewBlock():void {
        function addBlockHat(dialog:DialogBox):void {
            var spec:String = specEditor.spec().replace(/^\s+|\s+$/g, '');
            if (spec.length == 0) return;
            var newHat:Block = new Block(spec, 'p', Specs.procedureColor, Specs.PROCEDURE_DEF);
            newHat.parameterNames = specEditor.inputNames();
            newHat.defaultArgValues = specEditor.defaultArgValues();
            newHat.warpProcFlag = specEditor.warpFlag();
            newHat.setSpec(spec);
            newHat.x = 10 - WitProgram.scriptsPane.x + Math.random() * 100;
            newHat.y = 10 - WitProgram.scriptsPane.y + Math.random() * 100;
            WitProgram.scriptsPane.addChild(newHat);
            WitProgram.scriptsPane.saveScripts();
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_CALLS, null, true));
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null, true));
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
        }

        var specEditor:ProcedureSpecEditor = new ProcedureSpecEditor('', [], false);
        var d:DialogBox = new DialogBox(addBlockHat);
        d.addTitle('New Block');
        d.addWidget(specEditor);
        d.addAcceptCancelButtons('OK');
        d.showOnStage(TempStatic.stagePart.stage, true);
        specEditor.setInitialFocus();
    }

    private function showAnExtension():void {
        function addExt(ext:WitExtension):void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_EXT, ext));
        }

        var lib:MediaLibrary = new MediaLibrary('extension', addExt);
        lib.open();
    }

    protected function addReporterCheckbox(block:Block):void {
        var b:IconButton = new IconButton(toggleWatcher, 'checkbox');
        b.disableMouseover();
        var targetObj:WitObj = isSpriteSpecific(block.op) ? WitProgram.viewedObj : TempStatic.witStage;
        b.clientData = {
            type: 'reporter',
            targetObj: targetObj,
            cmd: block.op,
            block: block,
            color: block.base.color
        };
        b.x = 6;
        b.y = nextY + 5;
        WitProgram.palette.addChild(b);
    }

    protected function isCheckboxReporter(op:String):Boolean {
        const checkboxReporters:Array = [
            'xpos', 'ypos', 'heading', 'costumeIndex', 'scale', 'volume', 'timeAndDate',
            'backgroundIndex', 'sceneName', 'tempo', 'answer', 'timer', 'soundLevel', 'isLoud',
            'sensor:', 'sensorPressed:', 'senseVideoMotion', 'xScroll', 'yScroll',
            'getDistance', 'getTilt'];
        return checkboxReporters.indexOf(op) > -1;
    }

    private function isSpriteSpecific(op:String):Boolean {
        const spriteSpecific:Array = ['costumeIndex', 'xpos', 'ypos', 'heading', 'scale', 'volume'];
        return spriteSpecific.indexOf(op) > -1;
    }

    private function getBlockArg(b:Block, i:int):String {
        var arg:BlockArg = b.args[i] as BlockArg;
        if (arg) return arg.argValue;
        return '';
    }

    private function addVariableCheckbox(varName:String, isList:Boolean):void {
        var b:IconButton = new IconButton(toggleWatcher, 'checkbox');
        b.disableMouseover();
        var targetObj:WitObj = WitProgram.viewedObj;
        if (isList) {
            if (targetObj.listNames().indexOf(varName) < 0) targetObj = TempStatic.witStage;
        } else {
            if (targetObj.varNames().indexOf(varName) < 0) targetObj = TempStatic.witStage;
        }
        b.clientData = {
            type: 'variable',
            isList: isList,
            targetObj: targetObj,
            varName: varName
        };
        b.x = 6;
        b.y = nextY + 5;
        WitProgram.palette.addChild(b);
    }

    private function toggleWatcher(b:IconButton):void {
        var data:Object = b.clientData;
        if (data.block) {
            switch (data.block.op) {
                case 'senseVideoMotion':
                    data.targetObj = getBlockArg(data.block, 1) == 'Stage' ? TempStatic.witStage : WitProgram.viewedObj;
                case 'sensor:':
                case 'sensorPressed:':
                case 'timeAndDate':
                    data.param = getBlockArg(data.block, 0);
                    break;
            }
        }
        var showFlag:Boolean = !TempStatic.runtime.watcherShowing(data);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.TOGGLE_WATCHER,
                {
                    data: data,
                    showFlag: showFlag
                }
        ));
        b.setOn(showFlag);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
    }

    private function updateCheckboxes():void {
        for (var i:int = 0; i < WitProgram.palette.numChildren; i++) {
            var b:IconButton = WitProgram.palette.getChildAt(i) as IconButton;
            if (b && b.clientData) {
                b.setOn(TempStatic.runtime.watcherShowing(b.clientData));
            }
        }
    }

    protected function getExtensionMenu(ext:WitExtension):Menu {
        function showAbout():void {
            // Open in the tips window if the URL starts with /info/ and another tab otherwise
            if (ext.url) {
                if (ext.url.indexOf('/info/') === 0) {
                    trace(ext.url);
                } else if (ext.url.indexOf('http') === 0) {
                    navigateToURL(new URLRequest(ext.url));
                } else {
                    DialogBox.notify('Extensions', 'Unable to load about page: the URL given for extension "' + ext.name + '" is not formatted correctly.');
                }
            }
        }

        function hideExtension():void {
            TempStatic.extensionManager.setEnabled(ext.name, false);
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null));
        }

        var m:Menu = new Menu();
        m.addItem(_('About') + ' ' + ext.name + ' ' + _('extension') + '...', showAbout, !!ext.url);
        m.addItem('Remove extension blocks', hideExtension);

        var extensionDevManager:ExtensionDevManager = TempStatic.extensionManager as ExtensionDevManager;

        if (!ext.isInternal && extensionDevManager) {
            m.addLine();
            var localFileName:String = extensionDevManager.getLocalFileName(ext);
            if (localFileName) {
                if (extensionDevManager.isLocalExtensionDirty()) {
                    m.addItem('Load changes from ' + localFileName, function ():void {
                        extensionDevManager.loadLocalCode();
                    });
                }
                m.addItem('Disconnect from ' + localFileName, function ():void {
                    extensionDevManager.stopWatchingExtensionFile();
                });
            }
        }

        return m;
    }

    protected const pwidth:int = 215;

    protected function addExtensionSeparator(ext:WitExtension):void {
        function extensionMenu(ignore:*):void {
            var m:Menu = getExtensionMenu(ext);
            m.showOnStage(TempStatic.stagePart.stage);
        }

        nextY += 7;

        var titleButton:IconButton = UIPartUtil.makeMenuButton(ext.name, extensionMenu, true, CSS.textColor);
        titleButton.x = 5;
        titleButton.y = nextY;
        WitProgram.palette.addChild(titleButton);

        addLineForExtensionTitle(titleButton, ext);

        var indicator:IndicatorLight = new IndicatorLight(ext);
        indicator.addEventListener(MouseEvent.CLICK, function (e:Event):void {
            trace('ap.editor.extensions');
        }, false, 0, true);
        TempStatic.extensionManager.updateIndicator(indicator, ext);
        indicator.x = pwidth - 40;
        indicator.y = nextY + 2;
        WitProgram.palette.addChild(indicator);

        nextY += titleButton.height + 10;

        var extensionDevManager:ExtensionDevManager = TempStatic.extensionManager as ExtensionDevManager;
        if (extensionDevManager) {
            // Show if this extension is being updated by a file
            var fileName:String = extensionDevManager.getLocalFileName(ext);
            if (fileName) {
                var extensionEditStatus:TextField = UIPartUtil.makeLabel('Connected to ' + fileName, CSS.normalTextFormat, 8, nextY - 5);
                WitProgram.palette.addChild(extensionEditStatus);

                nextY += extensionEditStatus.textHeight + 3;
            }
        }
    }

    [Embed(source='/../assets/reload.png')]
    private static const reloadIcon:Class;

    protected function addLineForExtensionTitle(titleButton:IconButton, ext:WitExtension):void {
        var x:int = titleButton.width + 12;
        var w:int = pwidth - x - 48;
        var extensionDevManager:ExtensionDevManager = TempStatic.extensionManager as ExtensionDevManager;
        var dirty:Boolean = extensionDevManager && extensionDevManager.isLocalExtensionDirty(ext);
        if (dirty)
            w -= 15;
        addLine(x, nextY + 9, w);

        if (dirty) {
            var reload:Bitmap = new reloadIcon();
            reload.scaleX = 0.75;
            reload.scaleY = 0.75;
            var reloadBtn:Sprite = new Sprite();
            reloadBtn.addChild(reload);
            reloadBtn.x = x + w + 6;
            reloadBtn.y = nextY + 2;
            WitProgram.palette.addChild(reloadBtn);
            SimpleTooltips.add(reloadBtn, {
                text: 'Click to load changes (running old code from ' + extensionDevManager.getLocalCodeDate() + ')',
                direction: 'top'
            });

            reloadBtn.addEventListener(MouseEvent.MOUSE_DOWN, function (e:MouseEvent):void {
                SimpleTooltips.hideAll();
                extensionDevManager.loadLocalCode();
            });

            reloadBtn.addEventListener(MouseEvent.ROLL_OVER, function (e:MouseEvent):void {
                reloadBtn.transform.colorTransform = new ColorTransform(2, 2, 2);
            });

            reloadBtn.addEventListener(MouseEvent.ROLL_OUT, function (e:MouseEvent):void {
                reloadBtn.transform.colorTransform = new ColorTransform();
            });
        }
    }

    private function addBlocksForExtension(ext:WitExtension):void {
        var blockColor:int = Specs.extensionsColor;
        var opPrefix:String = ext.useScratchPrimitives ? '' : ext.name + '.';
        for each (var spec:Array in ext.blockSpecs) {
            if (spec.length >= 3) {
                var op:String = opPrefix + spec[2];
                var defaultArgs:Array = spec.slice(3);
                var block:Block = new Block(spec[1], spec[0], blockColor, op, defaultArgs);
                var showCheckbox:Boolean = (spec[0] == 'r' && defaultArgs.length == 0);
                if (showCheckbox) addReporterCheckbox(block);
                addItem(block, showCheckbox);
            } else {
                if (spec.length == 1) nextY += 10 * spec[0].length; // add some space
            }
        }
    }

    protected function addLine(x:int, y:int, w:int):void {
        const light:int = 0xF2F2F2;
        const dark:int = CSS.borderColor - 0x141414;
        var line:Shape = new Shape();
        var g:Graphics = line.graphics;

        g.lineStyle(1, dark, 1, true);
        g.moveTo(0, 0);
        g.lineTo(w, 0);

        g.lineStyle(1, light, 1, true);
        g.moveTo(0, 1);
        g.lineTo(w, 1);
        line.x = x;
        line.y = y;
        WitProgram.palette.addChild(line);
    }

}
}
