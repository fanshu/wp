package wp.editor.primitives {
import flash.utils.Dictionary;

import util.TempStatic;

import wp.editor.blocks.*;
import wp.editor.interpreter.*;
import wp.editor.wit.*;

public class LooksPrims {
    private var interp:Interpreter;

    public function LooksPrims(interpreter:Interpreter) {
        this.interp = interpreter;
    }

    public function addPrimsTo(primTable:Dictionary):void {
        primTable['lookLike:'] = primShowCostume;
        primTable['nextCostume'] = primNextCostume;
        primTable['costumeIndex'] = primCostumeIndex;
        primTable['costumeName'] = primCostumeName;

        primTable['showBackground:'] = primShowCostume; // used by WitProgram 1.4 and earlier (doesn't start scene hats)
        primTable['nextBackground'] = primNextCostume; // used by WitProgram 1.4 and earlier (doesn't start scene hats)
        primTable['backgroundIndex'] = primSceneIndex;
        primTable['sceneName'] = primSceneName;
        primTable['nextScene'] = function (b:*):* {
            startScene('next backdrop', false)
        };
        primTable['startScene'] = function (b:*):* {
            startScene(interp.arg(b, 0), false)
        };
        primTable['startSceneAndWait'] = function (b:*):* {
            startScene(interp.arg(b, 0), true)
        };

        primTable['say:duration:elapsed:from:'] = function (b:*):* {
            showBubbleAndWait(b, 'talk')
        };
        primTable['say:'] = function (b:*):* {
            showBubble(b, 'talk')
        };
        primTable['think:duration:elapsed:from:'] = function (b:*):* {
            showBubbleAndWait(b, 'think')
        };
        primTable['think:'] = function (b:*):* {
            showBubble(b, 'think')
        };

        primTable['changeGraphicEffect:by:'] = primChangeEffect;
        primTable['setGraphicEffect:to:'] = primSetEffect;
        primTable['filterReset'] = primClearEffects;

        primTable['changeSizeBy:'] = primChangeSize;
        primTable['setSizeTo:'] = primSetSize;
        primTable['scale'] = primSize;

        primTable['show'] = primShow;
        primTable['hide'] = primHide;
//		primTable['hideAll']				= primHideAll;

        primTable['comeToFront'] = primGoFront;
        primTable['goBackByLayers:'] = primGoBack;

        primTable['setVideoState'] = primSetVideoState;
        primTable['setVideoTransparency'] = primSetVideoTransparency;

//		primTable['scrollAlign']			= primScrollAlign;
//		primTable['scrollRight']			= primScrollRight;
//		primTable['scrollUp']				= primScrollUp;
//		primTable['xScroll']				= function(b:*):* { return app.stagePane.xScroll };
//		primTable['yScroll']				= function(b:*):* { return app.stagePane.yScroll };

        primTable['setRotationStyle'] = primSetRotationStyle;
    }

    private function primNextCostume(b:Block):void {
        var s:WitObj = interp.targetObj();
        if (s != null) s.showCostume(s.currentCostumeIndex + 1);
        if (s.visible) interp.redraw();
    }

    private function primShowCostume(b:Block):void {
        var s:WitObj = interp.targetObj();
        if (s == null) return;
        var arg:* = interp.arg(b, 0);
        if (typeof(arg) == 'number') {
            s.showCostume(arg - 1);
        } else {
            var i:int = s.indexOfCostumeNamed(arg);
            if (i >= 0) {
                s.showCostume(i);
            } else if ('previous costume' == arg) {
                s.showCostume(s.currentCostumeIndex - 1);
            } else if ('next costume' == arg) {
                s.showCostume(s.currentCostumeIndex + 1);
            } else {
                var n:Number = Interpreter.asNumber(arg);
                if (!isNaN(n)) s.showCostume(n - 1);
                else return; // arg did not match a costume name nor is it a valid number
            }
        }
        if (s.visible) interp.redraw();
    }

    private function primCostumeIndex(b:Block):Number {
        var s:WitObj = interp.targetObj();
        return (s == null) ? 1 : s.costumeNumber();
    }

    private function primCostumeName(b:Block):String {
        var s:WitObj = interp.targetObj();
        return (s == null) ? '' : s.currentCostume().costumeName;
    }

    private function primSceneIndex(b:Block):Number {
        return TempStatic.witStage.costumeNumber();
    }

    private function primSceneName(b:Block):String {
        return TempStatic.witStage.currentCostume().costumeName;
    }

    private function startScene(s:String, waitFlag:Boolean):void {
        if ('next backdrop' == s) s = backdropNameAt(TempStatic.witStage.currentCostumeIndex + 1);
        else if ('previous backdrop' == s) s = backdropNameAt(TempStatic.witStage.currentCostumeIndex - 1);
        else {
            var n:Number = Interpreter.asNumber(s);
            if (!isNaN(n)) {
                n = (Math.round(n) - 1) % TempStatic.witStage.costumes.length;
                if (n < 0) n += TempStatic.witStage.costumes.length;
                s = TempStatic.witStage.costumes[n].costumeName;
            }
        }
        interp.startScene(s, waitFlag);
    }

    private function backdropNameAt(i:int):String {
        var costumes:Array = TempStatic.witStage.costumes;
        return costumes[(i + costumes.length) % costumes.length].costumeName;
    }

    private function showBubbleAndWait(b:Block, type:String):void {
        var text:*, secs:Number;
        var s:WitSprite = interp.targetSprite();
        if (s == null) return;
        if (interp.activeThread.firstTime) {
            text = interp.arg(b, 0);
            secs = interp.numarg(b, 1);
            s.showBubble(text, type, b);
            if (s.visible) interp.redraw();
            interp.startTimer(secs);
        } else {
            if (interp.checkTimer() && s.bubble && (s.bubble.getSource() == b)) {
                s.hideBubble();
            }
        }
    }

    private function showBubble(b:Block, type:String = null):void {
        var text:*, secs:Number;
        var s:WitSprite = interp.targetSprite();
        if (s == null) return;
        if (type == null) { // combined talk/think/shout/whisper command
            type = interp.arg(b, 0);
            text = interp.arg(b, 1);
        } else { // talk or think command
            text = interp.arg(b, 0);
        }
        s.showBubble(text, type, b);
        if (s.visible) interp.redraw();
    }

    private function primChangeEffect(b:Block):void {
        var s:WitObj = interp.targetObj();
        if (s == null) return;
        var filterName:String = interp.arg(b, 0);
        var delta:Number = interp.numarg(b, 1);
        if (delta == 0) return;

        var newValue:Number = s.filterPack.getFilterSetting(filterName) + delta;
        s.filterPack.setFilter(filterName, newValue);
        s.applyFilters();
        if (s.visible || s == TempStatic.witStage) interp.redraw();
    }

    private function primSetEffect(b:Block):void {
        var s:WitObj = interp.targetObj();
        if (s == null) return;
        var filterName:String = interp.arg(b, 0);
        var newValue:Number = interp.numarg(b, 1);
        if (s.filterPack.setFilter(filterName, newValue)) {
            s.applyFilters();
        }
        if (s.visible || s == TempStatic.witStage) interp.redraw();
    }

    private function primClearEffects(b:Block):void {
        var s:WitObj = interp.targetObj();
        s.clearFilters();
        s.applyFilters();
        if (s.visible || s == TempStatic.witStage) interp.redraw();
    }

    private function primChangeSize(b:Block):void {
        var s:WitSprite = interp.targetSprite();
        if (s == null) return;
        var oldScale:Number = s.scaleX;
        s.setSize(s.getSize() + interp.numarg(b, 0));
        if (s.visible && (s.scaleX != oldScale)) interp.redraw();
    }

    private function primSetRotationStyle(b:Block):void {
        var s:WitSprite = interp.targetSprite();
        var newStyle:String = interp.arg(b, 0) as String;
        if ((s == null) || (newStyle == null)) return;
        s.setRotationStyle(newStyle);
    }

    private function primSetSize(b:Block):void {
        var s:WitSprite = interp.targetSprite();
        if (s == null) return;
        s.setSize(interp.numarg(b, 0));
        if (s.visible) interp.redraw();
    }

    private function primSize(b:Block):Number {
        var s:WitSprite = interp.targetSprite();
        if (s == null) return 100;
        return Math.round(s.getSize()); // reporter returns rounded size, as in WitProgram 1.4
    }

    private function primShow(b:Block):void {
        var s:WitSprite = interp.targetSprite();
        if (s == null) return;
        s.visible = true;
        s.applyFilters();
        s.updateBubble();
        if (s.visible) interp.redraw();
    }

    private function primHide(b:Block):void {
        var s:WitSprite = interp.targetSprite();
        if ((s == null) || !s.visible) return;
        s.visible = false;
        s.applyFilters();
        s.updateBubble();
        interp.redraw();
    }

    private function primHideAll(b:Block):void {
        // Hide all sprites and delete all clones. Only works from the stage.
        if (!(interp.targetObj() is WitStage)) return;
        TempStatic.witStage.deleteClones();
        for (var i:int = 0; i < TempStatic.witStage.numChildren; i++) {
            var o:* = TempStatic.witStage.getChildAt(i);
            if (o is WitSprite) {
                o.visible = false;
                o.updateBubble();
            }
        }
        interp.redraw();
    }

    private function primGoFront(b:Block):void {
        var s:WitSprite = interp.targetSprite();
        if ((s == null) || (s.parent == null)) return;
        s.parent.setChildIndex(s, s.parent.numChildren - 1);
        if (s.visible) interp.redraw();
    }

    private function primGoBack(b:Block):void {
        var s:WitSprite = interp.targetSprite();
        if ((s == null) || (s.parent == null)) return;
        var newIndex:int = s.parent.getChildIndex(s) - interp.numarg(b, 0);
        newIndex = Math.max(minSpriteLayer(), Math.min(newIndex, s.parent.numChildren - 1));

        if (newIndex > 0 && newIndex < s.parent.numChildren) {
            s.parent.setChildIndex(s, newIndex);
            if (s.visible) interp.redraw();
        }
    }

    private function minSpriteLayer():int {
        // Return the lowest sprite layer.
        var stg:WitStage = TempStatic.witStage;
        return stg.getChildIndex(stg.videoImage ? stg.videoImage : stg.penLayer) + 1;
    }

    private function primSetVideoState(b:Block):void {
        TempStatic.witStage.setVideoState(interp.arg(b, 0));
    }

    private function primSetVideoTransparency(b:Block):void {
        TempStatic.witStage.setVideoTransparency(interp.numarg(b, 0));
        TempStatic.witStage.setVideoState('on');
    }

    private function primScrollAlign(b:Block):void {
        if (!(interp.targetObj() is WitStage)) return;
        TempStatic.witStage.scrollAlign(interp.arg(b, 0));
    }

    private function primScrollRight(b:Block):void {
        if (!(interp.targetObj() is WitStage)) return;
        TempStatic.witStage.scrollRight(interp.numarg(b, 0));
    }

    private function primScrollUp(b:Block):void {
        if (!(interp.targetObj() is WitStage)) return;
        TempStatic.witStage.scrollUp(interp.numarg(b, 0));
    }
}
}
