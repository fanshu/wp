package wp.editor.primitives {
import flash.display.*;
import flash.geom.*;
import flash.utils.Dictionary;

import jijunzeng.MyEvent;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.controllers.ContextEvtConst;
import wp.editor.interpreter.*;
import wp.editor.wit.*;

public class SensingPrims {
    private var interp:Interpreter;

    public function SensingPrims(interpreter:Interpreter) {
        this.interp = interpreter;
    }

    public function addPrimsTo(primTable:Dictionary):void {
        // sensing
        primTable['touching:'] = primTouching;
        primTable['touchingColor:'] = primTouchingColor;
        primTable['color:sees:'] = primColorSees;

        primTable['doAsk'] = primAsk;
        primTable['answer'] = function (b:*):* {
            return TempStatic.runtime.lastAnswer
        };

        primTable['mousePressed'] = function (b:*):* {
            return TempStatic.gh.mouseIsDown
        };
        primTable['mouseX'] = function (b:*):* {
            return TempStatic.witStage.scratchMouseX()
        };
        primTable['mouseY'] = function (b:*):* {
            return TempStatic.witStage.scratchMouseY()
        };
        primTable['timer'] = function (b:*):* {
            return TempStatic.runtime.timer()
        };
        primTable['timerReset'] = function (b:*):* {
            TempStatic.runtime.timerReset()
        };
        primTable['keyPressed:'] = primKeyPressed;
        primTable['distanceTo:'] = primDistanceTo;
        primTable['getAttribute:of:'] = primGetAttribute;
        primTable['soundLevel'] = function (b:*):* {
            return TempStatic.runtime.soundLevel()
        };
        primTable['isLoud'] = function (b:*):* {
            return TempStatic.runtime.isLoud()
        };
        primTable['timestamp'] = primTimestamp;
        primTable['timeAndDate'] = function (b:*):* {
            return TempStatic.runtime.getTimeString(interp.arg(b, 0))
        };
        primTable['getUserName'] = function (b:*):* {
            return ''
        };

        // sensor
        primTable['sensor:'] = function (b:*):* {
            return TempStatic.runtime.getSensor(interp.arg(b, 0))
        };
        primTable['sensorPressed:'] = function (b:*):* {
            return TempStatic.runtime.getBooleanSensor(interp.arg(b, 0))
        };

        // variable and list watchers
        primTable['showVariable:'] = primShowWatcher;
        primTable['hideVariable:'] = primHideWatcher;
        primTable['showList:'] = primShowListWatcher;
        primTable['hideList:'] = primHideListWatcher;
    }

    // TODO: move to stage
    static private var stageRect:Rectangle = new Rectangle(0, 0, WitObj.STAGEW, WitObj.STAGEH);

    private function primTouching(b:Block):Boolean {
        var s:WitSprite = interp.targetSprite();
        if (s == null) return false;
        var arg:* = interp.arg(b, 0);
        if ('_edge_' == arg) {
            if (stageRect.containsRect(s.getBounds(s.parent))) return false;

            var r:Rectangle = s.bounds();
            return (r.left < 0) || (r.right > WitObj.STAGEW) ||
                    (r.top < 0) || (r.bottom > WitObj.STAGEH);
        }
        if ('_mouse_' == arg) {
            return mouseTouches(s);
        }
        if (!s.visible) return false;

        ;
        var sBM:BitmapData = s.bitmap(true);
        for each (var s2:WitSprite in TempStatic.witStage.spritesAndClonesNamed(arg))
            if (s2.visible && sBM.hitTest(s.bounds().topLeft, 1, s2.bitmap(true), s2.bounds().topLeft, 1))
                return true;

        return false;
    }

    public function mouseTouches(s:WitSprite):Boolean {
        // True if the mouse touches the given sprite. This test is independent
        // of whether the sprite is hidden or 100% ghosted.
        // Note: p and r are in the coordinate system of the sprite's parent (i.e. the ScratchStage).
        if (!s.parent) return false;
        if (!s.getBounds(s).contains(s.mouseX, s.mouseY)) return false;
        var r:Rectangle = s.bounds();
        if (!r.contains(s.parent.mouseX, s.parent.mouseY)) return false;
        return s.bitmap().hitTest(r.topLeft, 1, new Point(s.parent.mouseX, s.parent.mouseY));
    }

//	private var testSpr:Sprite;
//	private var myBMTest:Bitmap;
//	private var stageBMTest:Bitmap;
    private function primTouchingColor(b:Block):Boolean {
        // Note: Attempted to switch app.stage.quality to LOW to disable anti-aliasing, which
        // can create false colors. Unfortunately, that caused serious performance issues.
        var s:WitSprite = interp.targetSprite();
        if (s == null) return false;
        var c:int = interp.arg(b, 0) | 0xFF000000;
        var myBM:BitmapData = s.bitmap(true);
        var stageBM:BitmapData = stageBitmapWithoutSpriteFilteredByColor(s, c);
//		if(s.objName == 'sensor') {
//			if(!testSpr) {
//				testSpr = new Sprite();
//				app.stage.addChild(testSpr);
//				myBMTest = new Bitmap();
//				myBMTest.y = 300;
//				testSpr.addChild(myBMTest);
//				stageBMTest = new Bitmap();
//				stageBMTest.y = 300;
//				testSpr.addChild(stageBMTest);
//			}
//			myBMTest.bitmapData = myBM;
//			stageBMTest.bitmapData = stageBM;
//			testSpr.graphics.clear();
//			testSpr.graphics.lineStyle(1);
//			testSpr.graphics.drawRect(myBM.width, 300, stageBM.width, stageBM.height);
//		}
        return myBM.hitTest(new Point(0, 0), 1, stageBM, new Point(0, 0), 1);
    }

    private function primColorSees(b:Block):Boolean {
        // Note: Attempted to switch app.stage.quality to LOW to disable anti-aliasing, which
        // can create false colors. Unfortunately, that caused serious performance issues.
        var s:WitSprite = interp.targetSprite();
        if (s == null) return false;
        var c1:int = interp.arg(b, 0) | 0xFF000000;
        var c2:int = interp.arg(b, 1) | 0xFF000000;
        var myBM:BitmapData = bitmapFilteredByColor(s.bitmap(true), c1);
        var stageBM:BitmapData = stageBitmapWithoutSpriteFilteredByColor(s, c2);
//		if(!testSpr) {
//			testSpr = new Sprite();
//			testSpr.y = 300;
//			app.stage.addChild(testSpr);
//			stageBMTest = new Bitmap();
//			testSpr.addChild(stageBMTest);
//			myBMTest = new Bitmap();
//			myBMTest.filters = [new GlowFilter(0xFF00FF)];
//			testSpr.addChild(myBMTest);
//		}
//		myBMTest.bitmapData = myBM;
//		stageBMTest.bitmapData = stageBM;
//		testSpr.graphics.clear();
//		testSpr.graphics.lineStyle(1);
//		testSpr.graphics.drawRect(0, 0, stageBM.width, stageBM.height);
        return myBM.hitTest(new Point(0, 0), 1, stageBM, new Point(0, 0), 1);
    }

    // used for debugging:
    private var debugView:Bitmap;

    private function showBM(bm:BitmapData):void {
        if (debugView == null) {
            debugView = new Bitmap();
            debugView.x = 100;
            debugView.y = 600;
            TempStatic.stagePart.stage.addChild(debugView);
        }
        debugView.bitmapData = bm;
    }

//	private var testBM:Bitmap = new Bitmap();
    private function bitmapFilteredByColor(srcBM:BitmapData, c:int):BitmapData {
//		if(!testBM.parent) {
//			testBM.y = WitObj.STAGEH; testBM.x = 15;
//			app.stage.addChild(testBM);
//		}
//		testBM.bitmapData = srcBM;
        var outBM:BitmapData = new BitmapData(srcBM.width, srcBM.height, true, 0);
        outBM.threshold(srcBM, srcBM.rect, srcBM.rect.topLeft, '==', c, 0xFF000000, 0xF0F8F8F0); // match only top five bits of each component
        return outBM;
    }

    private function stageBitmapWithoutSpriteFilteredByColor(s:WitSprite, c:int):BitmapData {
        return TempStatic.witStage.getBitmapWithoutSpriteFilteredByColor(s, c);
    }

    private function primAsk(b:Block):void {
        if (TempStatic.runtime.askPromptShowing()) {
            // wait if (1) some other sprite is asking (2) this question is answered (when firstTime is false)
            interp.doYield();
            return;
        }
        var obj:WitObj = interp.targetObj();
        if (interp.activeThread.firstTime) {
            var question:String = interp.arg(b, 0);
            if ((obj is WitSprite) && (obj.visible)) {
                WitSprite(obj).showBubble(question, 'talk', true);
                TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SHOW_ASK_PROMPT, ''));
            } else {
                TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SHOW_ASK_PROMPT, question));
            }
            interp.activeThread.firstTime = false;
            interp.doYield();
        } else {
            if ((obj is WitSprite) && (obj.visible)) WitSprite(obj).hideBubble();
            interp.activeThread.firstTime = true;
        }
    }

    private function primKeyPressed(b:Block):Boolean {
        var key:String = interp.arg(b, 0);
        if (key == 'any') {
            for each (var k:Boolean in TempStatic.runtime.keyIsDown) {
                if (k) return true;
            }
            return false;
        }
        var ch:int = key.charCodeAt(0);
        if (ch > 127) return false;
        if (key == 'left arrow') ch = 28;
        if (key == 'right arrow') ch = 29;
        if (key == 'up arrow') ch = 30;
        if (key == 'down arrow') ch = 31;
        if (key == 'space') ch = 32;
        return TempStatic.runtime.keyIsDown[ch];
    }

    private function primDistanceTo(b:Block):Number {
        var s:WitSprite = interp.targetSprite();
        var p:Point = mouseOrSpritePosition(interp.arg(b, 0));
        if ((s == null) || (p == null)) return 10000;
        var dx:Number = p.x - s.scratchX;
        var dy:Number = p.y - s.scratchY;
        return Math.sqrt((dx * dx) + (dy * dy));
    }

    private function primGetAttribute(b:Block):* {
        var attribute:String = interp.arg(b, 0);
        var obj:WitObj = TempStatic.witStage.objNamed(String(interp.arg(b, 1)));
        if (!(obj is WitObj)) return 0;
        if (obj is WitSprite) {
            var s:WitSprite = WitSprite(obj);
            if ('x position' == attribute) return s.scratchX;
            if ('y position' == attribute) return s.scratchY;
            if ('direction' == attribute) return s.direction;
            if ('costume #' == attribute) return s.costumeNumber();
            if ('costume name' == attribute) return s.currentCostume().costumeName;
            if ('size' == attribute) return s.getSize();
            if ('volume' == attribute) return s.volume;
        }
        if (obj is WitStage) {
            if ('background #' == attribute) return obj.costumeNumber(); // support for old 1.4 blocks
            if ('backdrop #' == attribute) return obj.costumeNumber();
            if ('backdrop name' == attribute) return obj.currentCostume().costumeName;
            if ('volume' == attribute) return obj.volume;
        }
        if (obj.ownsVar(attribute)) return obj.lookupVar(attribute).value; // variable
        return 0;
    }

    private function mouseOrSpritePosition(arg:String):Point {
        if (arg == '_mouse_') {
            var w:WitStage = TempStatic.witStage;
            return new Point(w.scratchMouseX(), w.scratchMouseY());
        } else {
            var s:WitSprite = TempStatic.witStage.spriteNamed(arg);
            if (s == null) return null;
            return new Point(s.scratchX, s.scratchY);
        }
        return null;
    }

    private function primShowWatcher(b:Block):* {
        var obj:WitObj = interp.targetObj();
        if (obj) {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.TOGGLE_VAR_OR_LIST_FOR,
                    {
                        varName: interp.arg(b, 0),
                        isList: false,
                        targetObj: obj,
                        visible: true
                    }
            ));
        }
    }

    private function primHideWatcher(b:Block):* {
        var obj:WitObj = interp.targetObj();
        if (obj) {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.TOGGLE_VAR_OR_LIST_FOR,
                    {
                        varName: interp.arg(b, 0),
                        isList: false,
                        targetObj: obj,
                        visible: false
                    }
            ));
        }
    }

    private function primShowListWatcher(b:Block):* {
        var obj:WitObj = interp.targetObj();
        if (obj) {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.TOGGLE_VAR_OR_LIST_FOR,
                    {
                        varName: interp.arg(b, 0),
                        isList: true,
                        targetObj: obj,
                        visible: true
                    }
            ));
        }
    }

    private function primHideListWatcher(b:Block):* {
        var obj:WitObj = interp.targetObj();
        if (obj) {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.TOGGLE_VAR_OR_LIST_FOR,
                    {
                        varName: interp.arg(b, 0),
                        isList: true,
                        targetObj: obj,
                        visible: false
                    }
            ));
        }
    }

    private function primTimestamp(b:Block):* {
        const millisecondsPerDay:int = 24 * 60 * 60 * 1000;
        const epoch:Date = new Date(2000, 0, 1); // Jan 1, 2000 (Note: Months are zero-based.)
        var now:Date = new Date();
        var dstAdjust:int = now.timezoneOffset - epoch.timezoneOffset;
        var mSecsSinceEpoch:Number = now.time - epoch.time;
        mSecsSinceEpoch += ((now.timezoneOffset - dstAdjust) * 60 * 1000); // adjust to UTC (GMT)
        return mSecsSinceEpoch / millisecondsPerDay;
    }
}
}
