package wp.editor.views {
import flash.events.Event;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import util.GestureHandler;
import util.TempStatic;

import wp.editor.controllers.ContextEvtConst;

public class WitProgramMediator extends Mediator {
    [Inject]
    public var view:WitProgram;

    [Inject]
    public var gh:GestureHandler;

    override public function initialize():void {
        super.initialize();

        gh.app = view;

        /* context evts */
        addContextListener(Event.INIT, onInit, MyEvent);
        addContextListener(ContextEvtConst.INSTALL_STAGE, view.onInstallStage, MyEvent);
        addContextListener(ContextEvtConst.SET_EDIT_MODE, onSetEditMode, MyEvent);

        /* view evts */
    }

    private function onInit(evt:MyEvent):void {
        view.initialize();
    }

    private function onSetEditMode(evt:MyEvent):void {
        view.setEditMode(evt.payload);
    }
}
}
