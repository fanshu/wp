package wp.editor.views {
import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.wit.WitStage;

public class WitStageMediator extends Mediator {
    [Inject]
    public var view:WitStage;

    override public function initialize():void {
        super.initialize();

        /* context evts */
        addContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);
        addContextListener(ContextEvtConst.CLEAR_CACHED_BITMAPS, onClearCachedBitmaps, MyEvent);
        addContextListener(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, onClearWitStageAllCaches, MyEvent);

        /* view evts */
    }

    override public function destroy():void {
        super.destroy();
        removeContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);
        removeContextListener(ContextEvtConst.CLEAR_CACHED_BITMAPS, onClearCachedBitmaps, MyEvent);
        removeContextListener(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, onClearWitStageAllCaches, MyEvent);
    }

    private function onUpdateTranslation(evt:MyEvent):void {
        view.updateTranslation();
    }

    private function onClearCachedBitmaps(evt:MyEvent):void {
        view.clearCachedBitmaps();
    }

    private function onClearWitStageAllCaches(evt:MyEvent):void {
        view.clearAllCaches();
    }
}
}
