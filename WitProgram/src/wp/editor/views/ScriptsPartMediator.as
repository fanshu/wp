package wp.editor.views {
import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.parts.ScriptsPart;

public class ScriptsPartMediator extends Mediator {
    [Inject]
    public var view:ScriptsPart;

    override public function initialize():void {
        super.initialize();

        /* context evts */
        addContextListener(ContextEvtConst.UPDATE_PALETTE, onUpdatePalette, MyEvent);
        addContextListener(ContextEvtConst.SELECT_SPRITE, onSelectSprite, MyEvent);

        /* view evts */
    }

    override public function destroy():void {
        super.destroy();
        removeContextListener(ContextEvtConst.UPDATE_PALETTE, onUpdatePalette, MyEvent);
        removeContextListener(ContextEvtConst.SELECT_SPRITE, onSelectSprite, MyEvent);
    }

    private function onUpdatePalette(evt:MyEvent):void {
        view.updatePalette();
    }

    private function onSelectSprite(evt:MyEvent):void {
        WitProgram.viewedObj = evt.payload;
        view.updatePalette();
        view.updateSpriteWatermark();
        WitProgram.scriptsPane.viewScriptsFor(evt.payload);
    }
}
}
