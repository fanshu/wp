package wp.editor.views {
import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.parts.TopBarPart;

public class TopBarPartMediator extends Mediator {
    [Inject]
    public var view:TopBarPart;

    override public function initialize():void {
        super.initialize();

        /* context evts */
        addContextListener(ContextEvtConst.CLEAR_TOOL, onClearTool, MyEvent);
        addContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);
        addContextListener(ContextEvtConst.SET_SAVE_NEEDED, onSetSaveNeeded, MyEvent);

        /* view evts */
    }

    override public function destroy():void {
        super.destroy();
        removeContextListener(ContextEvtConst.CLEAR_TOOL, onClearTool, MyEvent);
        removeContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);
        removeContextListener(ContextEvtConst.SET_SAVE_NEEDED, onSetSaveNeeded, MyEvent);
    }

    private function onClearTool(evt:MyEvent):void {
        view.clearToolButtons();
    }

    private function onUpdateTranslation(evt:MyEvent):void {
        view.updateTranslation();
    }

    private function onSetSaveNeeded(evt:MyEvent):void {
        view.saveNeeded = evt.payload;
    }
}
}
