package wp.editor.views {
import flash.events.Event;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.utils.ByteArray;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import util.ProjectIO;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.wit.WitSprite;
import wp.editor.wit.WitStage;

public class WitEncryptMediator extends Mediator {
    [Inject]
    public var view:WitEncrypt;

    override public function initialize():void {
        super.initialize();

        /* context evts */
        addContextListener(Event.INIT, onInit, MyEvent);
        addContextListener(ContextEvtConst.INSTALL_PROJECT, onInstallProject, MyEvent);

        /* view evts */
        addViewListener("encrypt", onEncrypt, MyEvent);
    }

    private function onInstallProject(evt:MyEvent):void {
        var witstage:WitStage = evt.payload;
        if (!witstage) {
            view.encrypt();
            return;
        }

        // encrypt --> save
        var projIO:ProjectIO = new ProjectIO();
        projIO.convertSqueakSounds(witstage, function():void {
            var zipData:ByteArray = projIO.encodeProjectAsZipFile(witstage);
            var stream:FileStream = new FileStream();
            stream.open(view.curr_file, FileMode.WRITE);
            stream.writeBytes(zipData, 0, zipData.length);
            stream.close();

            view.encrypt();
        });
    }

    private function onInit(evt:MyEvent):void {
        view.initialize();
    }

    private function onEncrypt(evt:MyEvent):void {
        var bytes:ByteArray = new ByteArray();
        var stream:FileStream = new FileStream();
        stream.open(view.curr_file, FileMode.READ);
        stream.readBytes(bytes, 0, stream.bytesAvailable);
        stream.close();

        var ext:String = view.curr_file.nativePath.split('.').pop();
        if (ext == 'wit') {
            dispatch(new MyEvent(ContextEvtConst.INSTALL_PROJECT_FROM_DATA,
                    {
                        data: bytes,
                        saveForRevert: false
                    }
            ));
        } else if (ext == 'spr') {
            new ProjectIO().decodeSpriteFromZipFile(bytes,
                function(spr:WitSprite):void {
                    var zipData:ByteArray = new ProjectIO().encodeSpriteAsZipFile(spr.copyToShare());
                    var stream:FileStream = new FileStream();
                    stream.open(view.curr_file, FileMode.WRITE);
                    stream.writeBytes(zipData, 0, zipData.length);
                    stream.close();

                    view.encrypt();
                },
                function():void {
                    view.encrypt();
                }
            );
        }
    }
}
}
