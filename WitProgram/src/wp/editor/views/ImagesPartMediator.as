package wp.editor.views {
import flash.geom.Point;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.parts.ImagesPart;

public class ImagesPartMediator extends Mediator {
    [Inject]
    public var view:ImagesPart;

    override public function initialize():void {
        super.initialize();

        /* context evts */
        addContextListener(ContextEvtConst.SELECT_SPRITE, onSelectSprite, MyEvent);
        addContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);
        addContextListener(ContextEvtConst.ADD_COSTUME_IMAGE_EDITOR, onAddCostume, MyEvent);
        addContextListener(ContextEvtConst.ADD_AND_SELECT_COSTUME, onAddAndSelectCostume, MyEvent);

        /* view evts */

        /* fire */
    }

    override public function destroy():void {
        super.destroy();
        removeContextListener(ContextEvtConst.SELECT_SPRITE, onSelectSprite, MyEvent);
        removeContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);
        removeContextListener(ContextEvtConst.ADD_COSTUME_IMAGE_EDITOR, onAddCostume, MyEvent);
    }

    private function onSelectSprite(evt:MyEvent):void {
        WitProgram.viewedObj = evt.payload;
        view.editor.shutdown();
        view.refresh();
    }

    private function onUpdateTranslation(evt:MyEvent):void {
        view.updateTranslation();
    }

    private function onAddCostume(evt:MyEvent):void {
        var p:Point = new Point(240, 180);
        view.editor.addCostume(evt.payload, p);
    }

    private function onAddAndSelectCostume(evt:MyEvent):void {
        view.addAndSelectCostume(evt.payload);
    }
}
}
