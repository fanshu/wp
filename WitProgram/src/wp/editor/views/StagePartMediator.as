package wp.editor.views {
import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import util.TempStatic;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.parts.StagePart;

public class StagePartMediator extends Mediator {
    [Inject]
    public var view:StagePart;

    override public function initialize():void {
        super.initialize();

        /* context evts */
        addContextListener(ContextEvtConst.TOGGLE_TURBO_MODE, onToggleTurboMode, MyEvent);
        addContextListener(ContextEvtConst.SET_PROJECT_NAME, onSetProjectName, MyEvent);

        /* view evts */
    }

    override public function destroy():void {
        super.destroy();
        removeContextListener(ContextEvtConst.TOGGLE_TURBO_MODE, onToggleTurboMode, MyEvent);
        removeContextListener(ContextEvtConst.SET_PROJECT_NAME, onSetProjectName, MyEvent);
    }

    private function onToggleTurboMode(evt:MyEvent):void {
        view.toggleTurboMode();
    }

    private function onSetProjectName(evt:MyEvent):void {
        var s:String = evt.payload;
        TempStatic.projectName = s;
        TempStatic.windowTitle(s);
    }
}
}
