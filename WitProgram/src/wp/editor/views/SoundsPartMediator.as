package wp.editor.views {
import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.parts.SoundsPart;

public class SoundsPartMediator extends Mediator {
    [Inject]
    public var view:SoundsPart;

    override public function initialize():void {
        super.initialize();

        /* context evts */
        addContextListener(ContextEvtConst.SELECT_SPRITE, onSelectSprite, MyEvent);
        addContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);

        /* view evts */
    }

    override public function destroy():void {
        super.destroy();
        removeContextListener(ContextEvtConst.SELECT_SPRITE, onSelectSprite, MyEvent);
        removeContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);
    }

    private function onSelectSprite(evt:MyEvent):void {
        WitProgram.viewedObj = evt.payload;
        view.editor.shutdown();
        view.currentIndex = 0;
        view.refresh();
    }

    private function onUpdateTranslation(evt:MyEvent):void {
        view.updateTranslation();
    }
}
}
