package wp.editor.views {
import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.parts.EditorPart;

public class EditorPartMediator extends Mediator {
    [Inject]
    public var view:EditorPart;

    override public function initialize():void {
        super.initialize();

        /* context evts */
        addContextListener(ContextEvtConst.SELECT_SPRITE, onSelectSprite, MyEvent);
        addContextListener(ContextEvtConst.SET_TAB, onSetTab, MyEvent);
        addContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);

        /* view evts */
    }

    override public function destroy():void {
        super.destroy();

        /* context evts */
        removeContextListener(ContextEvtConst.SELECT_SPRITE, onSelectSprite, MyEvent);
        removeContextListener(ContextEvtConst.SET_TAB, onSetTab, MyEvent);
        removeContextListener(ContextEvtConst.UPDATE_TRANSLATION, onUpdateTranslation, MyEvent);

        /* view evts */
    }

    private function onSelectSprite(evt:MyEvent):void {
        WitProgram.viewedObj = evt.payload;
        view.tabsPart.refresh();
    }

    private function onSetTab(evt:MyEvent):void {
        view.setTab(evt.payload);
    }

    private function onUpdateTranslation(evt:MyEvent):void {
        view.tabsPart.updateTranslation();
    }
}
}
