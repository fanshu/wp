package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.sound.WitSoundPlayer;
import wp.editor.wit.WitSprite;

use namespace AsGettext;

use namespace Locale;

public class RuntimeStopAllCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();

        TempStatic.interp.stopAllThreads();
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_RUN_FEEDBACK));
        TempStatic.witStage.deleteClones();
        TempStatic.runtime.cloneCount = 0;
        TempStatic.runtime.clearKeyDownArray();
        WitSoundPlayer.stopAllSounds();
        TempStatic.extensionManager.stopButtonPressed();
        TempStatic.witStage.clearFilters();
        for each (var s:WitSprite in TempStatic.witStage.sprites()) {
            s.clearFilters();
            s.hideBubble();
        }
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_ASK_PROMPTS));
        TempStatic.runtime.motionDetector = null;
    }
}
}
