package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.wit.AskPrompter;

use namespace AsGettext;

use namespace Locale;

public class HideAskPromptCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var p:AskPrompter = evt.payload;

        TempStatic.interp.askThread = null;
        TempStatic.runtime.lastAnswer = p.answer();
        if (p.parent) {
            p.parent.removeChild(p);
        }
        TempStatic.stagePart.stage.focus = null;
    }
}
}
