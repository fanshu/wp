package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.wit.WitObj;

use namespace AsGettext;

use namespace Locale;

public class StartKeyHatsCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var ch:int = evt.payload;

        var keyName:String = null;
        if (('a'.charCodeAt(0) <= ch) && (ch <= 'z'.charCodeAt(0))) keyName = String.fromCharCode(ch);
        if (('0'.charCodeAt(0) <= ch) && (ch <= '9'.charCodeAt(0))) keyName = String.fromCharCode(ch);
        if (28 == ch) keyName = 'left arrow';
        if (29 == ch) keyName = 'right arrow';
        if (30 == ch) keyName = 'up arrow';
        if (31 == ch) keyName = 'down arrow';
        if (32 == ch) keyName = 'space';
        function startMatchingKeyHats(stack:Block, target:WitObj):void {
            if (stack.op == 'whenKeyPressed') {
                var k:String = stack.args[0].argValue;
                if (k == 'any' || k == keyName) {
                    // only start the stack if it is not already running
                    if (!TempStatic.interp.isRunning(stack, target)) TempStatic.interp.toggleThread(stack, target);
                }
            }
        }

        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ALL_STACKS_AND_OWNERS_DO, startMatchingKeyHats));
    }
}
}
