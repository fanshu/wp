package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;
import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.blocks.BlockArg;
import wp.editor.watchers.ListWatcher;

import wp.editor.wit.WitCostume;

import wp.editor.wit.WitObj;
import wp.editor.wit.WitSound;
import wp.editor.wit.WitStage;

use namespace gettext;

use namespace AsGettext;

use namespace Locale;

public class RenameSoundCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    private const _:Function = gettext;

    override public function execute():void {
        super.execute();
        var payload:Object = evt.payload;
        var s:WitSound = payload.s;
        var newName:String = payload.newName;

        var obj:WitObj = WitProgram.viewedObj;
        var oldName:String = s.soundName;
        s.soundName = '';
        newName = obj.unusedSoundName(newName || _('sound1'));
        s.soundName = newName;
        allUsesOfSoundDo(oldName, function (a:BlockArg):void {
            a.setArgValue(newName);
        });
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
    }

    private function allUsesOfSoundDo(soundName:String, f:Function):void {
        for each (var stack:Block in WitProgram.viewedObj.scripts) {
            stack.allBlocksDo(function (b:Block):void {
                for each (var a:* in b.args) {
                    if (a is BlockArg && a.menuName == 'sound' && a.argValue == soundName) f(a);
                }
            });
        }
    }
}
}
