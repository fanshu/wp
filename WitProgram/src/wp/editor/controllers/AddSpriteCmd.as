package wp.editor.controllers {
import flash.events.IEventDispatcher;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.ui.parts.TabsPart;
import wp.editor.wit.WitCostume;
import wp.editor.wit.WitSprite;

public class AddSpriteCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    [Inject]
    public var evt:MyEvent;

    override public function execute():void {
        super.execute();

        var costumeOrSprite:* = evt.payload;
        var spr:WitSprite;
        var c:WitCostume = costumeOrSprite as WitCostume;
        if (c) {
            spr = new WitSprite(c.costumeName);
            spr.setInitialCostume(c);
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_NEW_SPRITE, [spr, false, false], true));
            return;
        }
        spr = costumeOrSprite as WitSprite;
        if (spr) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_NEW_SPRITE, [spr, false, false], true));
            return;
        }
        var list:Array = costumeOrSprite as Array;
        if (list) {
            var sprName:String = list[0].costumeName;
            if (sprName.length > 3) sprName = sprName.slice(0, sprName.length - 2);
            spr = new WitSprite(sprName);
            for each (c in list) spr.costumes.push(c);
            if (spr.costumes.length > 1) spr.costumes.shift(); // remove default costume
            spr.showCostumeNamed(list[0].costumeName);
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_NEW_SPRITE, [spr, false, false], true));
        }
    }
}
}
