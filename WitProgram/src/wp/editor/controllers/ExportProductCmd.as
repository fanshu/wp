package wp.editor.controllers {
import com.codeazur.as3swf.SWF;
import com.codeazur.as3swf.data.SWFSymbol;
import com.codeazur.as3swf.tags.ITag;
import com.codeazur.as3swf.tags.TagSymbolClass;
import com.greensock.events.LoaderEvent;
import com.greensock.loading.BinaryDataLoader;

import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.net.FileReference;
import flash.utils.ByteArray;
import flash.utils.Endian;

import jijunzeng.MyEvent;

import mx.utils.StringUtil;

import robotlegs.bender.bundles.mvcs.Command;

import util.ProjectIO;
import util.Resources;
import util.TempStatic;

public class ExportProductCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    [Inject]
    public var evt:MyEvent;

    private var projIO:ProjectIO = new ProjectIO();
    private var target:String;

    override public function execute():void {
        super.execute();
        target = evt.payload;

        projIO.convertSqueakSounds(TempStatic.witStage, squeakSoundsConverted);
    }

    private function squeakSoundsConverted():void {
        TempStatic.witStage.istoswf = true;
        WitProgram.scriptsPane.saveScripts(false);

        // pack swf
        var binldr:BinaryDataLoader = new BinaryDataLoader("WitProject.swf", {onComplete: onBinLdr});
        binldr.load();
    }

    private function onBinLdr(evt:LoaderEvent):void {
        replaceWIT(evt.target.content as ByteArray);
    }

    private function replaceWIT(ba:ByteArray):void {
        var swf:SWF = new SWF(ba);
        var symbolClass:TagSymbolClass;
        var defineBinaryData:*;
        for each (var tag:ITag in swf.tags) {
            if (tag.type == TagSymbolClass.TYPE) {
                symbolClass = tag as TagSymbolClass;
                for each (var symbol:SWFSymbol in symbolClass.symbols) {
                    if (symbol.name.indexOf("project_wit") == 0) {
                        defineBinaryData = swf.getCharacter(symbol.tagId);
                        break;
                    }
                }
                if (defineBinaryData) break;
            }
        }
        if (defineBinaryData) {
            var zipData:ByteArray = projIO.encodeProjectAsZipFile(TempStatic.witStage);
            defineBinaryData.binaryData.clear();
            defineBinaryData.binaryData.writeBytes(zipData);
            var newSWF:ByteArray = new ByteArray();
            swf.publish(newSWF);
            if (target == 'swf') {
                save(newSWF); // export swf
            } else {
                packEXE(newSWF); // replaced project
            }
        } else {
            packEXE(ba); // empty project
        }
    }

    private function packEXE(ba:ByteArray):void {
        var fp:ByteArray = new Resources.FlashPlayer();

        var fp_len:Number = fp.length;
        var swf_len:Number = ba.length;

        var exe:ByteArray = new ByteArray();
        exe.writeBytes(fp, 0, fp_len);
        exe.position = fp_len;
        exe.writeBytes(ba, 0, swf_len);
//        exe.endian = Endian.BIG_ENDIAN;
//        exe.writeUnsignedInt(0x563412FA);
        exe.endian = Endian.LITTLE_ENDIAN;
        exe.writeUnsignedInt(0xFA123456);
        exe.writeUnsignedInt(swf_len);

        save(exe);
    }

    private function save(ba:ByteArray):void {
        var defaultName:String = StringUtil.trim(TempStatic.projectName);
        if (defaultName.length == 0) defaultName = 'project';
        defaultName = defaultName.replace('.wit', '.'+target);
        if (defaultName.indexOf('.'+target) == -1) {
            defaultName += '.'+target;
        }
        var file:FileReference = new FileReference();
        file.addEventListener(Event.COMPLETE, fileSaved);
        file.save(ba, TempStatic.fixFileName(defaultName));
    }

    private function fileSaved(evt:Event):void {
        var fr:FileReference = evt.target as FileReference;
        fr.removeEventListener(Event.COMPLETE, fileSaved);
    }
}
}
