package wp.editor.controllers {
import flash.display.DisplayObject;
import flash.events.IEventDispatcher;
import flash.geom.Rectangle;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

use namespace AsGettext;

use namespace Locale;

public class ShowOnStageCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var w:DisplayObject = evt.payload;

        if (w.parent == null) setInitialPosition(w);
        w.visible = true;
        TempStatic.witStage.addChild(w);
    }

    private function setInitialPosition(watcher:DisplayObject):void {
        var wList:Array = TempStatic.witStage.watchers();
        var w:int = watcher.width;
        var h:int = watcher.height;
        var x:int = 5;
        while (x < 400) {
            var maxX:int = 0;
            var y:int = 5;
            while (y < 320) {
                var otherWatcher:DisplayObject = watcherIntersecting(wList, new Rectangle(x, y, w, h));
                if (!otherWatcher) {
                    watcher.x = x;
                    watcher.y = y;
                    return;
                }
                y = otherWatcher.y + otherWatcher.height + 5;
                maxX = otherWatcher.x + otherWatcher.width;
            }
            x = maxX + 5;
        }
        // Couldn't find an unused place, so pick a random spot
        watcher.x = 5 + Math.floor(400 * Math.random());
        watcher.y = 5 + Math.floor(320 * Math.random());
    }

    private function watcherIntersecting(watchers:Array, r:Rectangle):DisplayObject {
        for each (var w:DisplayObject in watchers) {
            if (r.intersects(w.getBounds(TempStatic.witStage))) return w;
        }
        return null;
    }
}
}
