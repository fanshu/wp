package wp.editor.controllers {
import flash.events.IEventDispatcher;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.ui.parts.TabsPart;
import wp.editor.wit.WitCostume;
import wp.editor.wit.WitSprite;

public class AddNewSpriteCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    [Inject]
    public var evt:MyEvent;

    override public function execute():void {
        super.execute();

        var spr:WitSprite = evt.payload.shift();
        var showImages:Boolean = evt.payload.shift();
        var atMouse:Boolean = evt.payload.shift();

        var c:WitCostume;
        var byteCount:int = 0;
        for each (c in spr.costumes) {
            if (!c.baseLayerData) c.prepareToSave();
            byteCount += c.baseLayerData.length;
        }
        if (!WitProgram.okayToAdd(byteCount)) return; // not enough room
        spr.objName = TempStatic.witStage.unusedSpriteName(spr.objName);
        spr.indexInLibrary = 1000000; // add at end of library
        spr.setScratchXY(int(200 * Math.random() - 100), int(100 * Math.random() - 50));
        if (atMouse) spr.setScratchXY(TempStatic.witStage.scratchMouseX(), TempStatic.witStage.scratchMouseY());
        TempStatic.witStage.addChild(spr);
        spr.updateCostume();
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, spr));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_TAB, showImages ? TabsPart.TAB_IMAGES : TabsPart.TAB_SCRIPTS));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true));
        WitProgram.libraryPart.refresh();
        for each (c in spr.costumes) {
            if (WitCostume.isSVGData(c.baseLayerData)) c.setSVGData(c.baseLayerData, false);
        }
    }
}
}
