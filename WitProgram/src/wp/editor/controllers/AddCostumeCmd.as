package wp.editor.controllers {
import flash.events.IEventDispatcher;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import wp.editor.ui.parts.TabsPart;
import wp.editor.wit.WitCostume;
import wp.editor.wit.WitObj;

public class AddCostumeCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    [Inject]
    public var evt:MyEvent;

    override public function execute():void {
        super.execute();

        var c:WitCostume = evt.payload.shift();
        var targetObj:WitObj = evt.payload.shift();

        if (!c.baseLayerData) c.prepareToSave();
        if (!WitProgram.okayToAdd(c.baseLayerData.length)) return; // not enough room
        if (!targetObj) targetObj = WitProgram.viewedObj;
        c.costumeName = targetObj.unusedCostumeName(c.costumeName);
        targetObj.costumes.push(c);
        targetObj.showCostumeNamed(c.costumeName);
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true));
        if (targetObj == WitProgram.viewedObj) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_TAB, TabsPart.TAB_IMAGES));
        }
    }
}
}
