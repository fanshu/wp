package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.interpreter.Thread;

use namespace AsGettext;

use namespace Locale;

public class ShowAllRunFeedbackCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();

        for each (var t:Thread in TempStatic.interp.threads) {
            t.topBlock.showRunFeedback();
        }
    }
}
}
