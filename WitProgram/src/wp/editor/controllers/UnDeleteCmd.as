package wp.editor.controllers {
import flash.display.DisplayObject;
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.ui.media.MediaInfo;
import wp.editor.ui.parts.TabsPart;
import wp.editor.wit.WitComment;
import wp.editor.wit.WitObj;
import wp.editor.wit.WitSprite;

use namespace AsGettext;

use namespace Locale;

public class UndeleteCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();

        if (!TempStatic.runtime.lastDelete) return;
        var lastDelete:Array = TempStatic.runtime.lastDelete;
        var obj:* = lastDelete[0];
        var x:int = lastDelete[1];
        var y:int = lastDelete[2];
        var previousOwner:* = lastDelete[4];
        doUndelete(obj, x, y, previousOwner);
        TempStatic.runtime.lastDelete = null;
    }

    private function doUndelete(obj:*, x:int, y:int, prevOwner:*):void {
        if (obj is MediaInfo) {
            if (prevOwner is WitObj) {
                evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, prevOwner));
                if (obj.mycostume) {
                    evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_COSTUME, [obj.mycostume, null]));
                }
                if (obj.mysound) {
//                    TempStatic.addSound(obj.mysound as WitSound);
                    evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_SOUND, [obj.mysound, null]));
                }
            }
        } else if (obj is WitSprite) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_NEW_SPRITE, [obj, false, false]));
            obj.setScratchXY(x, y);
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, obj));
        } else if ((obj is Array) || (obj is Block) || (obj is WitComment)) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, prevOwner));
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_TAB, TabsPart.TAB_SCRIPTS));
            var b:DisplayObject = obj is Array ? obj[0] : obj;
            b.x = WitProgram.scriptsPane.padding;
            b.y = WitProgram.scriptsPane.padding;
            if (b is Block) b.cacheAsBitmap = true;
            WitProgram.scriptsPane.addChild(b);
            if (obj is Array) {
                for each (var c:WitComment in obj[1]) {
                    WitProgram.scriptsPane.addChild(c);
                }
            }
            WitProgram.scriptsPane.saveScripts();
            if (b is Block) {
                evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
            }
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES));
        }
    }
}
}
