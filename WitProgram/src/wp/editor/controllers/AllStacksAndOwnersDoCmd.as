package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.wit.WitObj;

import wp.editor.wit.WitStage;

use namespace AsGettext;

use namespace Locale;

public class AllStacksAndOwnersDoCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var f:Function = evt.payload;

        // Call the given function on every stack in the project, passing the stack and owning sprite/stage.
        // This method is used by broadcast, so enumerate sprites/stage from front to back to match WitProgram.
        var stage:WitStage = TempStatic.witStage;
        var stack:Block;
        for (var i:int = stage.numChildren - 1; i >= 0; i--) {
            var o:* = stage.getChildAt(i);
            if (o is WitObj) {
                for each (stack in WitObj(o).scripts) f(stack, o);
            }
        }
        for each (stack in stage.scripts) f(stack, stage);
    }
}
}
