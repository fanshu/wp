package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.interpreter.Variable;
import wp.editor.uiwidgets.DialogBox;
import wp.editor.wit.WitObj;
import wp.editor.wit.WitStage;

use namespace AsGettext;

use namespace Locale;

public class RenameVariableCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var payload:Array = evt.payload;
        var oldName:String = payload[0];
        var newName:String = payload[1];

        if (oldName == newName) return;
        var owner:WitObj = WitProgram.viewedObj;
        if (!owner.ownsVar(oldName)) owner = TempStatic.witStage;
        if (owner.hasName(newName)) {
            DialogBox.notify("Cannot Rename", "That name is already in use.");
            return;
        }

        var v:Variable = owner.lookupVar(oldName);
        if (v != null) {
            v.name = newName;
            if (v.watcher) v.watcher.changeVarName(newName);
        } else {
            owner.lookupOrCreateVar(newName);
        }
        updateVarRefs(oldName, newName, owner);
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null));
    }

    private function updateVarRefs(oldName:String, newName:String, owner:WitObj):void {
        // Change the variable name in all blocks that use it.
        for each (var b:Block in allUsesOfVariable(oldName, owner)) {
            if (b.op == Specs.GET_VAR) {
                b.setSpec(newName);
                b.fixExpressionLayout();
            } else {
                b.args[0].setArgValue(newName);
            }
        }
    }

    public function allUsesOfVariable(varName:String, owner:WitObj):Array {
        var variableBlocks:Array = [Specs.SET_VAR, Specs.CHANGE_VAR, "showVariable:", "hideVariable:"];
        var result:Array = [];
        var stacks:Array = (owner is WitStage) ? TempStatic.runtime.allStacks() : owner.scripts;
        for each (var stack:Block in stacks) {
            // for each block in stack
            stack.allBlocksDo(function (b:Block):void {
                if (b.op == Specs.GET_VAR && b.spec == varName) result.push(b);
                if (variableBlocks.indexOf(b.op) != -1 && b.args[0].argValue == varName) result.push(b);
            });
        }
        return result;
    }
}
}
