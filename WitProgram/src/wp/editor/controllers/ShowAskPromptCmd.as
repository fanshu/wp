package wp.editor.controllers {
import flash.events.IEventDispatcher;
import flash.utils.ByteArray;
import flash.utils.setTimeout;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.wit.AskPrompter;
import wp.editor.wit.WitObj;

use namespace AsGettext;

use namespace Locale;

public class ShowAskPromptCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var question:String = evt.payload;

        var p:AskPrompter = new AskPrompter(question);
        TempStatic.interp.askThread = TempStatic.interp.activeThread;
        p.x = 15;
        p.y = WitObj.STAGEH - p.height - 5;
        TempStatic.witStage.addChild(p);
        setTimeout(p.grabKeyboardFocus, 100); // workaround for Window keyboard event handling
    }
}
}
