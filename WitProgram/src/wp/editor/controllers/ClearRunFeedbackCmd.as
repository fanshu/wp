package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.blocks.Block;

use namespace AsGettext;

use namespace Locale;

public class ClearRunFeedbackCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();

        if (TempStatic.editMode) {
            for each (var stack:Block in TempStatic.runtime.allStacks()) {
                stack.allBlocksDo(function (b:Block):void {
                    b.hideRunFeedback();
                });
            }
        }
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null));
    }
}
}
