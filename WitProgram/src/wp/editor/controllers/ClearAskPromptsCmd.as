package wp.editor.controllers {
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.wit.AskPrompter;

use namespace AsGettext;

use namespace Locale;

public class ClearAskPromptsCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();

        TempStatic.interp.askThread = null;
        var allPrompts:Array = [];
        var uiLayer:Sprite = TempStatic.witStage.getUILayer();
        var c:DisplayObject;
        for (var i:int = 0; i < uiLayer.numChildren; i++) {
            if ((c = uiLayer.getChildAt(i)) is AskPrompter) allPrompts.push(c);
        }
        for each (c in allPrompts) uiLayer.removeChild(c);
    }
}
}
