package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

use namespace AsGettext;

use namespace Locale;

public class DeleteVariableCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var varName:String = evt.payload;

        if (WitProgram.viewedObj.ownsVar(varName)) {
            WitProgram.viewedObj.deleteVar(varName);
        } else {
            TempStatic.witStage.deleteVar(varName);
        }
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null));
    }
}
}
