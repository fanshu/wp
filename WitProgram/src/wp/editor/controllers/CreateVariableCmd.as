package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

use namespace AsGettext;

use namespace Locale;

public class CreateVariableCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var varName:String = evt.payload;

        WitProgram.viewedObj.lookupOrCreateVar(varName);
    }
}
}
