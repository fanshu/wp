package wp.editor.controllers {
import flash.events.IEventDispatcher;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import wp.editor.ui.parts.TabsPart;
import wp.editor.wit.WitObj;
import wp.editor.wit.WitSound;

public class AddSoundCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    [Inject]
    public var evt:MyEvent;

    override public function execute():void {
        super.execute();

        var snd:WitSound = evt.payload.shift();
        var targetObj:WitObj = evt.payload.shift();

        if (snd.soundData && !WitProgram.okayToAdd(snd.soundData.length)) return; // not enough room
        if (!targetObj) targetObj = WitProgram.viewedObj;
        snd.soundName = targetObj.unusedSoundName(snd.soundName);
        targetObj.sounds.push(snd);
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true));
        if (targetObj == WitProgram.viewedObj) {
            WitProgram.editorPart.soundsPart.selectSound(snd);
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_TAB, TabsPart.TAB_SOUNDS));
        }
    }
}
}
