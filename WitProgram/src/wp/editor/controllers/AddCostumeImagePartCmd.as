package wp.editor.controllers {
import flash.events.IEventDispatcher;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import wp.editor.ui.parts.TabsPart;
import wp.editor.wit.WitCostume;
import wp.editor.wit.WitObj;
import wp.editor.wit.WitSprite;

public class AddCostumeImagePartCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    [Inject]
    public var evt:MyEvent;

    override public function execute():void {
        super.execute();
        var costumeOrSprite:* = evt.payload;

        var c:WitCostume = costumeOrSprite as WitCostume;
        if (c) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_AND_SELECT_COSTUME, c));
            return;
        }
        var spr:WitSprite = costumeOrSprite as WitSprite;
        if (spr) {
            // If a sprite was selected, add all it's costumes to this sprite.
            for each (c in spr.costumes) {
                evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_AND_SELECT_COSTUME, c));
            }
            return;
        }
        var costumeList:Array = costumeOrSprite as Array;
        if (costumeList) {
            for each (c in costumeList) {
                evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_AND_SELECT_COSTUME, c));
            }
        }
    }
}
}
