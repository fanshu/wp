package wp.editor.controllers {
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.interpreter.Variable;
import wp.editor.watchers.ListWatcher;
import wp.editor.watchers.Watcher;
import wp.editor.wit.WitObj;

use namespace AsGettext;

use namespace Locale;

public class ToggleVarOrListForCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var payload:Object = evt.payload;
        var varName:String = payload.varName;
        var isList:Boolean = payload.isList;
        var targetObj:WitObj = payload.targetObj;
        var visible:Boolean = payload.visible;

        var w:DisplayObject;
        if (visible) {
            if (targetObj.isClone) {
                // Clone's can't show local variables/lists (but can show global ones)
                if (!isList && targetObj.ownsVar(varName)) return;
                if (isList && targetObj.ownsList(varName)) return;
            }
            w = isList ? watcherForList(targetObj, varName) : watcherForVar(targetObj, varName);
            if (w is ListWatcher) ListWatcher(w).prepareToShow();
            if (w != null && (!w.visible || !w.parent)) {
                evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SHOW_ON_STAGE, w));
                evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
            }
        } else {
            w = isList ? watcherForList(targetObj, varName) : watcherForVar(targetObj, varName);
            if (w != null && w.visible) {
                w.visible = false;
                evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
            }
        }
    }

    private function watcherForList(targetObj:WitObj, listName:String):DisplayObject {
        var w:ListWatcher;
        for each (w in targetObj.lists) {
            if (w.listName == listName) return w;
        }
        for each (w in TempStatic.witStage.lists) {
            if (w.listName == listName) return w;
        }
        return null;
    }

    private function watcherForVar(targetObj:WitObj, vName:String):DisplayObject {
        var v:Variable = targetObj.lookupVar(vName);
        if (v == null) return null; // variable is not defined
        if (v.watcher == null) {
            if (TempStatic.witStage.ownsVar(vName)) targetObj = TempStatic.witStage; // global
            var existing:Watcher = existingWatcherForVar(targetObj, vName);
            if (existing != null) {
                v.watcher = existing;
            } else {
                v.watcher = new Watcher();
                Watcher(v.watcher).initForVar(targetObj, vName);
            }
        }
        return v.watcher;
    }

    private function existingWatcherForVar(target:WitObj, vName:String):Watcher {
        var uiLayer:Sprite = TempStatic.witStage.getUILayer();
        for (var i:int = 0; i < uiLayer.numChildren; i++) {
            var c:* = uiLayer.getChildAt(i);
            if ((c is Watcher) && (c.isVarWatcherFor(target, vName))) return c;
        }
        return null;
    }
}
}
