package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;
import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.blocks.BlockArg;
import wp.editor.watchers.ListWatcher;

import wp.editor.wit.WitCostume;

import wp.editor.wit.WitObj;
import wp.editor.wit.WitStage;

use namespace gettext;

use namespace AsGettext;

use namespace Locale;

public class RenameSpriteCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    private const _:Function = gettext;

    override public function execute():void {
        super.execute();
        var newName:String = evt.payload;

        var obj:WitObj = WitProgram.viewedObj;
        var oldName:String = obj.objName;
        obj.objName = '';
        newName = TempStatic.witStage.unusedSpriteName(newName || 'Sprite1');
        obj.objName = newName;
        for each (var lw:ListWatcher in WitProgram.viewedObj.lists) {
            lw.updateTitle();
        }
        var vo:Object = {
            args: TempStatic.runtime.allUsesOfSprite(oldName),
            newValue: newName
        };
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_ARGS, vo));
    }
}
}
