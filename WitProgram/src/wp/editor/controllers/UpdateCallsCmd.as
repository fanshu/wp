package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import wp.editor.blocks.Block;
import wp.editor.wit.WitObj;

use namespace AsGettext;

use namespace Locale;

public class UpdateCallsCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();

        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ALL_STACKS_AND_OWNERS_DO,
                function (b:Block, target:WitObj):void {
                    if (b.op == Specs.CALL) {
                        if (target.lookupProcedure(b.spec) == null) {
                            b.base.setColor(0xFF0000);
                            b.base.redraw();
                        }
                        else b.base.setColor(Specs.procedureColor);
                    }
                }));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null));
    }
}
}
