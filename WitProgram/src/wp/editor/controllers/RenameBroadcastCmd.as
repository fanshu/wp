package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;
import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.uiwidgets.DialogBox;
import wp.editor.wit.WitObj;

use namespace gettext;

use namespace AsGettext;

use namespace Locale;

public class RenameBroadcastCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    private const _:Function = gettext;

    override public function execute():void {
        super.execute();
        var payload:Array = evt.payload;
        var oldMsg:String = payload[0];
        var newMsg:String = payload[1];

        if (oldMsg == newMsg) return;

        if (TempStatic.runtime.allSendersOfBroadcast(newMsg).length > 0 ||
                TempStatic.runtime.allReceiversOfBroadcast(newMsg).length > 0) {
            DialogBox.notify("Cannot Rename", "That name is already in use.");
            return;
        }

        for each(var obj:Block in allBroadcastBlocksWithMsg(oldMsg)) {
            Block(obj).broadcastMsg = newMsg;
        }

        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null));
    }

    private function allBroadcastBlocksWithMsg(msg:String):Array {
        var result:Array = [];
        for each (var o:WitObj in TempStatic.witStage.allObjects()) {
            for each (var stack:Block in o.scripts) {
                stack.allBlocksDo(function (b:Block):void {
                    if (b.op == 'broadcast:' || b.op == 'doBroadcastAndWait' || b.op == 'whenIReceive') {
                        if (b.broadcastMsg == msg) result.push(b);
                    }
                });
            }
        }
        return result;
    }
}
}
