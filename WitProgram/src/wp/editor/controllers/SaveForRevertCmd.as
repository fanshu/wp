package wp.editor.controllers {
import flash.events.IEventDispatcher;
import flash.utils.ByteArray;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

use namespace AsGettext;

use namespace Locale;

public class SaveForRevertCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var projData:ByteArray = evt.payload;

        TempStatic.originalProj = projData;
        TempStatic.revertUndo = null;
    }
}
}
