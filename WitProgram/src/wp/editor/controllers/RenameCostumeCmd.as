package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;
import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.blocks.BlockArg;

import wp.editor.wit.WitCostume;

import wp.editor.wit.WitObj;
import wp.editor.wit.WitStage;

use namespace gettext;

use namespace AsGettext;

use namespace Locale;

public class RenameCostumeCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    private const _:Function = gettext;

    override public function execute():void {
        super.execute();
        var newName:String = evt.payload;

        var obj:WitObj = WitProgram.viewedObj;
        var costume:WitCostume = obj.currentCostume();
        costume.costumeName = '';
        var oldName:String = costume.costumeName;
        newName = obj.unusedCostumeName(newName || _('costume1'));
        costume.costumeName = newName;
        var vo:Object = {
            args: (obj is WitStage) ? allUsesOfBackdrop(oldName) : allUsesOfCostume(oldName),
            newValue: newName
        };
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_ARGS, vo));
    }

    private function allUsesOfBackdrop(backdropName:String):Array {
        var result:Array = [];
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ALL_STACKS_AND_OWNERS_DO,
                function (stack:Block, target:WitObj):void {
                    stack.allBlocksDo(function (b:Block):void {
                        for each (var a:* in b.args) {
                            if (a is BlockArg && a.menuName == 'backdrop' && a.argValue == backdropName) result.push(a);
                        }
                    });
                }));
        return result;
    }

    private function allUsesOfCostume(costumeName:String):Array {
        var result:Array = [];
        for each (var stack:Block in WitProgram.viewedObj.scripts) {
            stack.allBlocksDo(function (b:Block):void {
                for each (var a:* in b.args) {
                    if (a is BlockArg && a.menuName == 'costume' && a.argValue == costumeName) result.push(a);
                }
            });
        }
        return result;
    }
}
}
