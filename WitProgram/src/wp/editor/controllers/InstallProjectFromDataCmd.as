package wp.editor.controllers {
import flash.events.IEventDispatcher;
import flash.utils.ByteArray;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.ProjectIO;
import util.TempStatic;

import wp.editor.wit.WitStage;

use namespace AsGettext;

use namespace Locale;

public class InstallProjectFromDataCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var payload:* = evt.payload;
        var data:ByteArray = payload.data;
        var $saveForRevert:Boolean = payload.saveForRevert;

        var newProject:WitStage;
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.RUNTIME_STOP_ALL));
        data.position = 0;
        var version:String = "";
        if (data.length > 8) version = data.readUTFBytes(8);
        if (version != 'ScratchV') {
            data.position = 0;
            new ProjectIO().decodeProjectFromZipFile(data, function(proj:WitStage):void {
                newProject = proj;
                if (newProject) {
                    if ($saveForRevert) {
                        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SAVE_FOR_REVERT, data));
                    }
                    TempStatic.extensionManager.clearImportedExtensions();
                    TempStatic.witStage = newProject;
                    decodeImagesAndInstall(newProject);
                    newProject.setVideoState(newProject.info.videoOn ? 'on' : 'off');
                } else {
                    projectLoadFailed();
                }
            });
        }
    }

    private function projectLoadFailed(ignore:* = null):void {
        //DialogBox.notify('Error!', 'Project did not load.', app.stage);
        TempStatic.loadInProgress = false;
        if (!CONFIG::editor) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.INSTALL_PROJECT, null));
        }
    }

    private function decodeImagesAndInstall(newProject:WitStage):void {
        new ProjectIO().decodeAllImages(newProject.allObjects(),
                function ():void {
                    evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.INSTALL_PROJECT, newProject));
                },
                function ():void {
                    trace('faled');
                }
        );
    }
}
}
