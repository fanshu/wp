package wp.editor.controllers {
import flash.events.IEventDispatcher;
import flash.system.System;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.ui.parts.TabsPart;
import wp.editor.wit.WitObj;
import wp.editor.wit.WitSprite;
import wp.editor.wit.WitStage;

use namespace AsGettext;

use namespace Locale;

public class InstallProjectCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var witStage:WitStage = evt.payload;

        // stop all
        if (TempStatic.witStage != null) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.RUNTIME_STOP_ALL));
        }
        if (WitProgram.scriptsPane) WitProgram.scriptsPane.viewScriptsFor(null);

        // new
        TempStatic.witStage = witStage;

        for each (var obj:WitObj in witStage.allObjects()) {
            obj.showCostume(obj.currentCostumeIndex);
            var spr:WitSprite = obj as WitSprite;
            if (spr) spr.setDirection(spr.direction);
        }

        TempStatic.extensionManager.clearImportedExtensions();
        TempStatic.extensionManager.loadSavedExtensions(witStage.info.savedExtensions);
        /* install stage */
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.INSTALL_STAGE, witStage));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_TAB, TabsPart.TAB_SCRIPTS));
        WitProgram.editorPart.scriptsPart.resetCategory();
        // set the active sprite
        var allSprites:Array = TempStatic.witStage.sprites();
        if (allSprites.length > 0) {
            allSprites = allSprites.sortOn('indexInLibrary');
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, allSprites[0]));
        } else {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, TempStatic.witStage));
        }
        TempStatic.extensionManager.step();
        projectLoaded();
    }

    public function projectLoaded():void {
        System.gc();
        if (!CONFIG::editor) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.START_GREEN_FLAGS));
        }
        TempStatic.loadInProgress = false;
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, false));

        // translate the blocks of the newly loaded project
        for each (var o:WitObj in TempStatic.witStage.allObjects()) {
            o.updateScriptsAfterTranslation();
        }
    }
}
}
