package wp.editor.controllers {
import flash.events.IEventDispatcher;
import flash.utils.ByteArray;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.blocks.BlockArg;

use namespace AsGettext;

use namespace Locale;

public class UpdateArgsCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var payload:Object = evt.payload;
        var args:Array = payload.args;
        var newValue:* = payload.newValue;

        for each (var a:BlockArg in args) {
            a.setArgValue(newValue);
        }
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
    }
}
}
