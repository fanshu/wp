package wp.editor.controllers {
import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.net.SharedObject;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.ISO_3166;
import gnu.as3.gettext.ISO_639_1;
import gnu.as3.gettext.Locale;
import gnu.as3.gettext.services.IGettextService;
import gnu.as3.gettext.services.URLLoaderService;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

use namespace AsGettext;

use namespace Locale;

public class PrepareLocaleCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();

        // langs
        const en_US:String = mklocale(ISO_639_1.EN, ISO_3166.US);
        const zh_CN:String = mklocale(ISO_639_1.ZH, ISO_3166.CN);
        const tw_CN:String = mklocale(ISO_639_1.TW, ISO_3166.CN);

        // sharedobject langs
        var lang:String;
        var so:Object = SharedObject.getLocal('WitProgram');
        if (so.data.lang) {
            lang = so.data.lang;
        } else {
            lang = zh_CN;
            so.data.lang = lang;
            so.data.langs = {};
            so.data.langs[en_US] = "English";
            so.data.langs[zh_CN] = "简体中文";
            so.data.langs[tw_CN] = "正體中文";
            // flush
            so.flush();
        }

        if (!CONFIG::editor) {
            lang = en_US;
        }
        // setlocale
        Locale.LANGUAGE = lang;
        setlocale(Locale.LC_MESSAGES, lang);
        AsGettext.addEventListener(Event.COMPLETE, function (evt:Event):void {
            AsGettext.removeEventListener(Event.COMPLETE, arguments.callee);
            evtDispatcher.dispatchEvent(new MyEvent(Event.INIT));
        });

        if (CONFIG::editor) {
            // bind domain
            var gtService:IGettextService = new URLLoaderService("res/locale");
            AsGettext.bindtextdomain("witprogram", "", gtService);
            AsGettext.textdomain("witprogram");
        } else {
//            AsGettext.bindtextdomain("witprogram", "");
            AsGettext.textdomain("witprogram");
            AsGettext.ignore("witprogram");
            evtDispatcher.dispatchEvent(new MyEvent(Event.INIT));
        }
    }
}
}
