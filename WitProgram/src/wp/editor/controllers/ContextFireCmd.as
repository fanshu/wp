package wp.editor.controllers {
import eu.alebianco.robotlegs.utils.impl.SequenceMacro;

import flash.events.IEventDispatcher;

public class ContextFireCmd extends SequenceMacro {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function prepare():void {
        add(ResourceInitCmd);
        registerCompleteCallback(onComplete);
    }

    private function onComplete(success:Boolean):void {
    }
}
}
