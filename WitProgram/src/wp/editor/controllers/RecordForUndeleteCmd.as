package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.wit.WitComment;

use namespace AsGettext;

use namespace Locale;

public class RecordForUndeleteCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var payload:Object = evt.payload;
        var obj:* = payload.obj;
        var x:int = payload.x;
        var y:int = payload.y;
        var index:int = payload.index;
        var owner:* = payload.owner;

        if (obj is Block) {
            var comments:Array = (obj as Block).attachedCommentsIn(WitProgram.scriptsPane);
            if (comments.length) {
                for each (var c:WitComment in comments) {
                    c.parent.removeChild(c);
                }
                WitProgram.scriptsPane.fixCommentLayout();
                obj = [obj, comments];
            }
        }
        TempStatic.runtime.lastDelete = [obj, x, y, index, owner];
    }
}
}
