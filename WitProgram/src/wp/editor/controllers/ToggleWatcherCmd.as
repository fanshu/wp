package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.watchers.Watcher;

use namespace AsGettext;

use namespace Locale;

public class ToggleWatcherCmd extends Command {
    [Inject]
    public var evt:MyEvent;

    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();
        var payload:Object = evt.payload;
        var data:Object = payload.data;
        var showFlag:Boolean = payload.showFlag;

        if ('variable' == data.type) {
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.TOGGLE_VAR_OR_LIST_FOR,
                    {
                        varName: data.varName,
                        isList: data.isList,
                        targetObj: data.targetObj,
                        visible: showFlag
                    }
            ));
        }
        if ('reporter' == data.type) {
            var w:Watcher = TempStatic.runtime.findReporterWatcher(data);
            if (w) {
                w.visible = showFlag;
            } else {
                if (showFlag) {
                    w = new Watcher();
                    w.initWatcher(data.targetObj, data.cmd, data.param, data.color);
                    evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SHOW_ON_STAGE, w));
                }
            }
        }

        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
    }
}
}
