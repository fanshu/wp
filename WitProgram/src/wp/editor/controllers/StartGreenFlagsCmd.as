package wp.editor.controllers {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.wit.WitObj;

use namespace AsGettext;

use namespace Locale;

public class StartGreenFlagsCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();

        function startIfGreenFlag(stack:Block, target:WitObj):void {
            if (stack.op == 'whenGreenFlag') TempStatic.interp.toggleThread(stack, target);
        }

        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.RUNTIME_STOP_ALL));
        TempStatic.runtime.lastAnswer = '';
        clearEdgeTriggeredHats();
        TempStatic.runtime.timerReset();
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ALL_STACKS_AND_OWNERS_DO, startIfGreenFlag));
    }

    private function clearEdgeTriggeredHats():void {
        TempStatic.runtime.edgeTriggersEnabled = true;
        TempStatic.runtime.triggeredHats = []
    }
}
}
