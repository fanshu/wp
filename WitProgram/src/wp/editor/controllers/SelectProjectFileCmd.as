package wp.editor.controllers {
import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.net.FileFilter;
import flash.net.FileReference;
import flash.utils.ByteArray;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

use namespace AsGettext;

use namespace Locale;

public class SelectProjectFileCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    override public function execute():void {
        super.execute();

        function fileSelected(event:Event):void {
            file.addEventListener(Event.COMPLETE, fileLoadHandler);
            file.load();
        }

        function fileLoadHandler(event:Event):void {
            var fileName:String, data:ByteArray;
            var file:FileReference = FileReference(event.target);
            fileName = file.name;
            data = file.data;
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_PROJECT_NAME, fileName));
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.INSTALL_PROJECT_FROM_DATA,
                    {
                        data: data,
                        saveForRevert: true
                    }
            ));
            TempStatic.windowTitle(fileName);
        }

        var filter:FileFilter;
        filter = new FileFilter('WitProgram Project', '*.sb;*.sb2;*.kp;*.wit');
        var file:FileReference = new FileReference();
        file.addEventListener(Event.SELECT, fileSelected);
        try {
            // Ignore the exception that happens when you call browse() with the file browser open
            file.browse([filter]);
        } catch (e:*) {
        }
    }
}
}
