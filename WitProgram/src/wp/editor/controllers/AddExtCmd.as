package wp.editor.controllers {
import flash.events.IEventDispatcher;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.Command;

import util.TempStatic;

import wp.editor.extensions.WitExtension;

public class AddExtCmd extends Command {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    [Inject]
    public var evt:MyEvent;

    override public function execute():void {
        super.execute();

        var ext:WitExtension = evt.payload;

        if (ext.isInternal) {
            TempStatic.extensionManager.setEnabled(ext.name, true);
        } else {
            TempStatic.extensionManager.loadCustom(ext);
        }
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES, null));
    }
}
}
