package wp.editor.config {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.framework.api.IConfig;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.api.IInjector;
import robotlegs.bender.framework.api.LogLevel;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.controllers.InstallProjectFromDataCmd;
import wp.editor.controllers.PrepareLocaleCmd;
import wp.editor.extensions.ExtensionManager;
import wp.editor.interpreter.Interpreter;
import wp.editor.views.WitEncryptMediator;
import wp.editor.wit.WitRuntime;

use namespace AsGettext;

use namespace Locale;

public class EncryptConfig implements IConfig {
    [Inject]
    public var context:IContext;

    [Inject]
    public var injector:IInjector;

    [Inject]
    public var eventCommandMap:IEventCommandMap;

    [Inject]
    public var mediatorMap:IMediatorMap;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var contextView:ContextView;

    public function configure():void {
        context.logLevel = LogLevel.DEBUG;

        /* singletons */
        injector.map(Interpreter).asSingleton(true);
        injector.map(WitRuntime).asSingleton(true);
       injector.map(ExtensionManager).asSingleton(true);

        /* controls */
        eventCommandMap.map(ContextEvtConst.PREPARE_LOCALE, MyEvent).toCommand(PrepareLocaleCmd);
        eventCommandMap.map(ContextEvtConst.INSTALL_PROJECT_FROM_DATA, MyEvent).toCommand(InstallProjectFromDataCmd);

        /* views */
        mediatorMap.map(WitEncrypt).toMediator(WitEncryptMediator);

        /* fire */
        context.afterInitializing(fire);
    }

    private function fire():void {
        dispatcher.dispatchEvent(new MyEvent(ContextEvtConst.PREPARE_LOCALE));
    }
}
}
