package wp.editor.config {
import flash.events.IEventDispatcher;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.framework.api.IConfig;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.api.IInjector;
import robotlegs.bender.framework.api.LogLevel;

import util.GestureHandler;

import wp.editor.controllers.AddCostumeCmd;
import wp.editor.controllers.AddCostumeImagePartCmd;
import wp.editor.controllers.AddExtCmd;
import wp.editor.controllers.AddNewSpriteCmd;
import wp.editor.controllers.AddSoundCmd;
import wp.editor.controllers.AddSpriteCmd;
import wp.editor.controllers.AllStacksAndOwnersDoCmd;
import wp.editor.controllers.ClearAskPromptsCmd;
import wp.editor.controllers.ClearRunFeedbackCmd;
import wp.editor.controllers.ContextEvtConst;
import wp.editor.controllers.CreateVariableCmd;
import wp.editor.controllers.DeleteListCmd;
import wp.editor.controllers.DeleteVariableCmd;
import wp.editor.controllers.ExportProductCmd;
import wp.editor.controllers.HideAskPromptCmd;
import wp.editor.controllers.InstallProjectCmd;
import wp.editor.controllers.InstallProjectFromDataCmd;
import wp.editor.controllers.PrepareLocaleCmd;
import wp.editor.controllers.RecordForUndeleteCmd;
import wp.editor.controllers.RenameBroadcastCmd;
import wp.editor.controllers.RenameCostumeCmd;
import wp.editor.controllers.RenameSoundCmd;
import wp.editor.controllers.RenameSpriteCmd;
import wp.editor.controllers.RenameVariableCmd;
import wp.editor.controllers.RuntimeStopAllCmd;
import wp.editor.controllers.SaveForRevertCmd;
import wp.editor.controllers.SelectProjectFileCmd;
import wp.editor.controllers.ShowAllRunFeedbackCmd;
import wp.editor.controllers.ShowAskPromptCmd;
import wp.editor.controllers.ShowOnStageCmd;
import wp.editor.controllers.StartGreenFlagsCmd;
import wp.editor.controllers.StartKeyHatsCmd;
import wp.editor.controllers.ToggleVarOrListForCmd;
import wp.editor.controllers.ToggleWatcherCmd;
import wp.editor.controllers.UndeleteCmd;
import wp.editor.controllers.UpdateArgsCmd;
import wp.editor.controllers.UpdateCallsCmd;
import wp.editor.extensions.ExtensionManager;
import wp.editor.interpreter.Interpreter;
import wp.editor.ui.parts.EditorPart;
import wp.editor.ui.parts.ImagesPart;
import wp.editor.ui.parts.LibraryPart;
import wp.editor.ui.parts.ScriptsPart;
import wp.editor.ui.parts.SoundsPart;
import wp.editor.ui.parts.StagePart;
import wp.editor.ui.parts.TopBarPart;
import wp.editor.views.EditorPartMediator;
import wp.editor.views.ImagesPartMediator;
import wp.editor.views.LibraryPartMediator;
import wp.editor.views.ScriptsPartMediator;
import wp.editor.views.SoundsPartMediator;
import wp.editor.views.StagePartMediator;
import wp.editor.views.TopBarPartMediator;
import wp.editor.views.WitProgramMediator;
import wp.editor.views.WitStageMediator;
import wp.editor.wit.WitRuntime;
import wp.editor.wit.WitStage;

use namespace AsGettext;

use namespace Locale;

public class ProgramConfig implements IConfig {
    [Inject]
    public var context:IContext;

    [Inject]
    public var injector:IInjector;

    [Inject]
    public var eventCommandMap:IEventCommandMap;

    [Inject]
    public var mediatorMap:IMediatorMap;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var contextView:ContextView;

    public function configure():void {
        context.logLevel = LogLevel.DEBUG;

        /* singletons */
        injector.map(GestureHandler).asSingleton(true);
        injector.map(Interpreter).asSingleton(true);
        injector.map(WitRuntime).asSingleton(true);
        injector.map(ExtensionManager).asSingleton(true);

        /* controls */
        eventCommandMap.map(ContextEvtConst.PREPARE_LOCALE, MyEvent).toCommand(PrepareLocaleCmd);
        eventCommandMap.map(ContextEvtConst.ADD_COSTUME, MyEvent).toCommand(AddCostumeCmd);
        eventCommandMap.map(ContextEvtConst.ADD_NEW_SPRITE, MyEvent).toCommand(AddNewSpriteCmd);
        eventCommandMap.map(ContextEvtConst.ADD_SOUND, MyEvent).toCommand(AddSoundCmd);
        eventCommandMap.map(ContextEvtConst.INSTALL_PROJECT, MyEvent).toCommand(InstallProjectCmd);
        eventCommandMap.map(ContextEvtConst.INSTALL_PROJECT_FROM_DATA, MyEvent).toCommand(InstallProjectFromDataCmd);
        eventCommandMap.map(ContextEvtConst.SELECT_PROJECT_FILE, MyEvent).toCommand(SelectProjectFileCmd);
        eventCommandMap.map(ContextEvtConst.UNDELETE, MyEvent).toCommand(UndeleteCmd);
        eventCommandMap.map(ContextEvtConst.RECORD_FOR_UNDELETE, MyEvent).toCommand(RecordForUndeleteCmd);
        eventCommandMap.map(ContextEvtConst.TOGGLE_VAR_OR_LIST_FOR, MyEvent).toCommand(ToggleVarOrListForCmd);
        eventCommandMap.map(ContextEvtConst.SHOW_ON_STAGE, MyEvent).toCommand(ShowOnStageCmd);
        eventCommandMap.map(ContextEvtConst.TOGGLE_WATCHER, MyEvent).toCommand(ToggleWatcherCmd);
        eventCommandMap.map(ContextEvtConst.ALL_STACKS_AND_OWNERS_DO, MyEvent).toCommand(AllStacksAndOwnersDoCmd);
        eventCommandMap.map(ContextEvtConst.SAVE_FOR_REVERT, MyEvent).toCommand(SaveForRevertCmd);
        eventCommandMap.map(ContextEvtConst.SHOW_ASK_PROMPT, MyEvent).toCommand(ShowAskPromptCmd);
        eventCommandMap.map(ContextEvtConst.HIDE_ASK_PROMPT, MyEvent).toCommand(HideAskPromptCmd);
        eventCommandMap.map(ContextEvtConst.RUNTIME_STOP_ALL, MyEvent).toCommand(RuntimeStopAllCmd);
        eventCommandMap.map(ContextEvtConst.START_GREEN_FLAGS, MyEvent).toCommand(StartGreenFlagsCmd);
        eventCommandMap.map(ContextEvtConst.START_KEY_HATS, MyEvent).toCommand(StartKeyHatsCmd);
        eventCommandMap.map(ContextEvtConst.CLEAR_ASK_PROMPTS, MyEvent).toCommand(ClearAskPromptsCmd);
        eventCommandMap.map(ContextEvtConst.CREATE_VARIABLE, MyEvent).toCommand(CreateVariableCmd);
        eventCommandMap.map(ContextEvtConst.DELETE_VARIABLE, MyEvent).toCommand(DeleteVariableCmd);
        eventCommandMap.map(ContextEvtConst.RENAME_VARIABLE, MyEvent).toCommand(RenameVariableCmd);
        eventCommandMap.map(ContextEvtConst.DELETE_LIST, MyEvent).toCommand(DeleteListCmd);
        eventCommandMap.map(ContextEvtConst.RENAME_COSTUME, MyEvent).toCommand(RenameCostumeCmd);
        eventCommandMap.map(ContextEvtConst.RENAME_SPRITE, MyEvent).toCommand(RenameSpriteCmd);
        eventCommandMap.map(ContextEvtConst.RENAME_SOUND, MyEvent).toCommand(RenameSoundCmd);
        eventCommandMap.map(ContextEvtConst.CLEAR_RUN_FEEDBACK, MyEvent).toCommand(ClearRunFeedbackCmd);
        eventCommandMap.map(ContextEvtConst.RENAME_BROADCAST, MyEvent).toCommand(RenameBroadcastCmd);
        eventCommandMap.map(ContextEvtConst.UPDATE_ARGS, MyEvent).toCommand(UpdateArgsCmd);
        eventCommandMap.map(ContextEvtConst.UPDATE_CALLS, MyEvent).toCommand(UpdateCallsCmd);
        eventCommandMap.map(ContextEvtConst.SHOW_ALL_RUN_FEEDBACK, MyEvent).toCommand(ShowAllRunFeedbackCmd);
        eventCommandMap.map(ContextEvtConst.ADD_EXT, MyEvent).toCommand(AddExtCmd);
        eventCommandMap.map(ContextEvtConst.ADD_SPRITE, MyEvent).toCommand(AddSpriteCmd);
        eventCommandMap.map(ContextEvtConst.ADD_COSTUME_IMAGE_PART, MyEvent).toCommand(AddCostumeImagePartCmd);
        eventCommandMap.map(ContextEvtConst.EXPORT_PRODUCT, MyEvent).toCommand(ExportProductCmd);

        /* views */
        mediatorMap.map(WitProgram).toMediator(WitProgramMediator);
        mediatorMap.map(TopBarPart).toMediator(TopBarPartMediator);
        mediatorMap.map(ScriptsPart).toMediator(ScriptsPartMediator);
        mediatorMap.map(ImagesPart).toMediator(ImagesPartMediator);
        mediatorMap.map(SoundsPart).toMediator(SoundsPartMediator);
        mediatorMap.map(LibraryPart).toMediator(LibraryPartMediator);
        mediatorMap.map(EditorPart).toMediator(EditorPartMediator);
        mediatorMap.map(WitStage).toMediator(WitStageMediator);
        mediatorMap.map(StagePart).toMediator(StagePartMediator);

        /* fire */
        context.afterInitializing(fire);
    }

    private function fire():void {
        dispatcher.dispatchEvent(new MyEvent(ContextEvtConst.PREPARE_LOCALE));
    }
}
}
