package wp.editor.translation {
import flash.events.Event;
import flash.net.SharedObject;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.ISO_3166;
import gnu.as3.gettext.ISO_639_1;
import gnu.as3.gettext.Locale;
import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.*;

import wp.editor.blocks.Block;
import wp.editor.controllers.ContextEvtConst;

use namespace Locale;

public class Translator {
    private static const _:Function = gettext;

    public static var rightToLeft:Boolean;
    public static var rightToLeftMath:Boolean; // true only for Arabic
    public static var lang:String;

    private static var dictionary:Object = {};

    public static function setLanguage($lang:String):void {
        dictionary = {}; // default to English (empty dictionary) if there's no .po file
        // langs
        const en_US:String = mklocale(ISO_639_1.EN, ISO_3166.US);
        const zh_CN:String = mklocale(ISO_639_1.ZH, ISO_3166.CN);
        const tw_CN:String = mklocale(ISO_639_1.TW, ISO_3166.CN);

        // sharedobject lang
        var so:SharedObject = SharedObject.getLocal('WitProgram');
        so.data.lang = $lang;
        so.flush();

        // setlocale
        Locale.LANGUAGE = $lang;
        setlocale(Locale.LC_MESSAGES, $lang);
        WitProgram.step_allow = false;
        AsGettext.addEventListener(Event.COMPLETE, function (evt:Event):void {
            AsGettext.removeEventListener(Event.COMPLETE, arguments.callee);
            var lang_map:Object = {};
            lang_map[en_US] = 'en';
            lang_map[zh_CN] = 'zh-cn';
            lang_map[tw_CN] = 'zh-tw';
            lang = lang_map[$lang];
            if ('en' !== lang) {
                checkBlockTranslations();
            }
            setFontsFor(lang);
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE, null, true));
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_TRANSLATION, null, true));
            WitProgram.step_allow = true;
        });
    }

    private static function setFontsFor(lang:String):void {
        // Set the rightToLeft flag and font sizes the given language.

        rightToLeft = false;
        rightToLeftMath = false;
        if (['zh_CN', 'zh-cn'].indexOf(lang) > -1) {
            // zh
            CSS.font = CSS.font_zh;
            Block.setFonts(CSS.font, 14, 13, false, 0);
        } else {
            // en
            CSS.font = CSS.font_en;
            Block.setFonts(CSS.font, 16, 15, false, 0);
        }
    }

    private static function checkBlockTranslations():void {
        for each (var entry:Array in Specs.commands) checkBlockSpec(entry[0]);
        for each (var spec:String in Specs.extensionSpecs) checkBlockSpec(spec);
    }

    private static function checkBlockSpec(spec:String):void {
        var translatedSpec:String = _(spec);
        if (translatedSpec == spec) return; // not translated
        if (!argsMatch(extractArgs(spec), extractArgs(translatedSpec))) {
            trace('Block argument mismatch:');
            trace('    ' + spec);
            trace('    ' + translatedSpec);
            delete dictionary[spec]; // remove broken entry from dictionary
        }
    }

    private static function argsMatch(args1:Array, args2:Array):Boolean {
        if (args1.length != args2.length) return false;
//        for (var i:int = 0; i < args1.length; i++) {
//            if (args1[i] != args2[i]) return false;
//        }
        return true;
    }

    private static function extractArgs(spec:String):Array {
        var result:Array = [];
        var tokens:Array = ReadStream.tokenize(spec);
        for each (var s:String in tokens) {
            if ((s.length > 1) && ((s.charAt(0) == '%') || (s.charAt(0) == '@'))) result.push(s);
        }
        return result;
    }
}
}
