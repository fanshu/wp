// WitProgram extension manager. Maintains a dictionary of all extensions in use and manages
// socket-based communications with local and server-based extension helper applications.

package wp.editor.extensions {
import com.adobe.utils.StringUtil;

import flash.events.*;
import flash.net.*;
import flash.utils.Dictionary;
import flash.utils.getTimer;

import jijunzeng.MyEvent;

import mx.utils.URLUtil;

import util.*;

import wp.editor.blocks.Block;
import wp.editor.controllers.ContextEvtConst;
import wp.editor.interpreter.*;
import wp.editor.uiwidgets.IndicatorLight;

public class ExtensionManager {
    [Inject]
    public var evtDispatcher:IEventDispatcher;

    protected var extensionDict:Object = {}; // extension name -> extension record
    private var justStartedWait:Boolean;
    private var pollInProgress:Dictionary = new Dictionary(true);
    static public const wedoExt:String = 'LEGO WeDo';

    public function ExtensionManager() {
        TempStatic.extensionManager = this;
        clearImportedExtensions();
    }

    public function extensionActive(extName:String):Boolean {
        return extensionDict.hasOwnProperty(extName);
    }

    public function isInternal(extName:String):Boolean {
        return (extensionDict.hasOwnProperty(extName) && extensionDict[extName].isInternal);
    }

    public function clearImportedExtensions():void {
        for each(var ext:WitExtension in extensionDict) {
            if (ext.showBlocks)
                setEnabled(ext.name, false);
        }

        // Clear imported extensions before loading a new project.
        extensionDict = {};
        extensionDict['PicoBoard'] = WitExtension.PicoBoard();
        extensionDict[wedoExt] = WitExtension.WeDo();
    }

    // -----------------------------
    // Block Specifications
    //------------------------------

    public function specForCmd(op:String):Array {
        // Return a command spec array for the given operation or null.
        for each (var ext:WitExtension in extensionDict) {
            var prefix:String = ext.useScratchPrimitives ? '' : (ext.name + '.');
            for each (var spec:Array in ext.blockSpecs) {
                if ((spec.length > 2) && ((prefix + spec[2]) == op)) {
                    return [spec[1], spec[0], Specs.extensionsCategory, op, spec.slice(3)];
                }
            }
        }
        return null;
    }

    // -----------------------------
    // Enable/disable/reset
    //------------------------------

    public function setEnabled(extName:String, flag:Boolean):void {
        var ext:WitExtension = extensionDict[extName];
        if (ext && ext.showBlocks != flag) {
            ext.showBlocks = flag;
        }
    }

    public function isEnabled(extName:String):Boolean {
        var ext:WitExtension = extensionDict[extName];
        return ext ? ext.showBlocks : false;
    }

    public function enabledExtensions():Array {
        // Answer an array of enabled extensions, sorted alphabetically.
        var result:Array = [];
        for each (var ext:WitExtension in extensionDict) {
            if (ext.showBlocks) result.push(ext);
        }
        result.sortOn('name');
        return result;
    }

    public function stopButtonPressed():* {
        // Send a reset_all command to all active extensions.
        for each (var ext:WitExtension in enabledExtensions()) {
            call(ext.name, 'reset_all', []);
        }
    }

    public function extensionsToSave():Array {
        // Answer an array of extension descriptor objects for imported extensions to be saved with the project.
        var result:Array = [];
        for each (var ext:WitExtension in extensionDict) {
            if (!ext.showBlocks) continue;

            var descriptor:Object = {};
            descriptor.extensionName = ext.name;
            descriptor.blockSpecs = ext.blockSpecs;
            descriptor.menus = ext.menus;
            if (ext.port) descriptor.extensionPort = ext.port;
            else if (ext.javascriptURL) descriptor.javascriptURL = ext.javascriptURL;
            result.push(descriptor);
        }
        return result;
    }

    // -----------------------------
    // Communications
    //------------------------------

    public function callCompleted(extensionName:String, id:Number):void {
        var ext:WitExtension = extensionDict[extensionName];
        if (ext == null) return; // unknown extension

        var index:int = ext.busy.indexOf(id);
        if (index > -1) ext.busy.splice(index, 1);
    }

    public function reporterCompleted(extensionName:String, id:Number, retval:*):void {
        var ext:WitExtension = extensionDict[extensionName];
        if (ext == null) return; // unknown extension

        WitProgram.topBarPart.refresh();

        var index:int = ext.busy.indexOf(id);
        if (index > -1) {
            ext.busy.splice(index, 1);
            for (var b:Object in ext.waiting) {
                if (ext.waiting[b] == id) {
                    delete ext.waiting[b];
                    (b as Block).response = retval;
                    (b as Block).requestState = 2;
                }
            }
        }
    }

    // -----------------------------
    // Loading
    //------------------------------

    public function loadCustom(ext:WitExtension):void {
        if (!extensionDict[ext.name] && ext.javascriptURL) {
            extensionDict[ext.name] = ext;
            ext.showBlocks = false;
            setEnabled(ext.name, true);
        }
    }

    public function loadRawExtension(extObj:Object):WitExtension {
        var ext:WitExtension = extensionDict[extObj.extensionName];
        if (!ext)
            ext = new WitExtension(extObj.extensionName, extObj.extensionPort);
        ext.port = extObj.extensionPort;
        ext.blockSpecs = extObj.blockSpecs;
        if (ext.port == 0) {
            // Fix up block specs to force reporters to be treated as requesters.
            // This is because the offline JS interface doesn't support returning values directly.
            for each(var spec:Object in ext.blockSpecs) {
                if (spec[0] == 'r') {
                    // 'r' is reporter, 'R' is requester, and 'rR' is a reporter forced to act as a requester.
                    spec[0] = 'rR';
                }
            }
        }
        if (extObj.url) ext.url = extObj.url;
        ext.showBlocks = true;
        ext.menus = extObj.menus;
        ext.javascriptURL = extObj.javascriptURL;
        if (extObj.host) ext.host = extObj.host; // non-local host allowed but not saved in project
        extensionDict[extObj.extensionName] = ext;
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_TRANSLATION));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES));

        // Update the indicator
        for (var i:int = 0; i < WitProgram.palette.numChildren; i++) {
            var indicator:IndicatorLight = WitProgram.palette.getChildAt(i) as IndicatorLight;
            if (indicator && indicator.target === ext) {
                updateIndicator(indicator, indicator.target, true);
                break;
            }
        }

        return ext;
    }

    public function loadSavedExtensions(savedExtensions:Array):void {
        function extensionRefused(extObj:Object, reason:String):void {
            trace('SWF Error: Refusing to load project extension "' + extObj.extensionName + '": ' + reason);
        }

        // Reset the system extensions and load the given array of saved extensions.
        if (!savedExtensions) return; // no saved extensions
        for each (var extObj:Object in savedExtensions) {
            if (isInternal(extObj.extensionName)) {
                setEnabled(extObj.extensionName, true);
                continue; // internal extension overrides one saved in project
            }

            if (!('extensionName' in extObj)) {
                trace('SWF Error: Refusing to load project extension without a name.');
                continue;
            }

            if (!('extensionPort' in extObj) && !('javascriptURL' in extObj)) {
                extensionRefused(extObj, 'No location specified.');
                continue;
            }

            if (!('blockSpecs' in extObj)) {
                // TODO: resolve potential confusion when the project blockSpecs don't match those in the JS.
                extensionRefused(extObj, 'No blockSpecs.');
                continue;
            }

            var ext:WitExtension = new WitExtension(extObj.extensionName, extObj.extensionPort || 0);
            ext.blockSpecs = extObj.blockSpecs;
            ext.showBlocks = true;
            ext.isInternal = false;
            ext.menus = extObj.menus;
            if (extObj.javascriptURL) {
                if (true) {
                    extensionRefused(extObj, 'Experimental extensions are only supported on ScratchX.');
                    continue;
                }
                if (!StringUtil.endsWith(URLUtil.getServerName(extObj.javascriptURL).toLowerCase(), '.github.io')) {
                    extensionRefused(extObj, 'Experimental extensions must be hosted on GitHub Pages.');
                    continue;
                }
                ext.javascriptURL = extObj.javascriptURL;
                ext.showBlocks = false;
                if (extObj.id) ext.id = extObj.id;
            }

            extensionDict[extObj.extensionName] = ext;
            setEnabled(extObj.extensionName, true);
        }
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES));
    }

    // -----------------------------
    // Menu Support
    //------------------------------

    public function menuItemsFor(op:String, menuName:String):Array {
        // Return a list of menu items for the given menu of the extension associated with op or null.
        var i:int = op.indexOf('.');
        if (i < 0) return null;
        var ext:WitExtension = extensionDict[op.slice(0, i)];
        if (!ext || !ext.menus) return null; // unknown extension
        return ext.menus[menuName];
    }

    // -----------------------------
    // Status Indicator
    //------------------------------

    public function updateIndicator(indicator:IndicatorLight, ext:WitExtension, firstTime:Boolean = false):void {
        if (ext.port > 0) {
            var msecsSinceLastResponse:uint = getTimer() - ext.lastPollResponseTime;
            if (msecsSinceLastResponse > 500) indicator.setColorAndMsg(0xE00000, 'Cannot find helper app');
            else if (ext.problem != '') indicator.setColorAndMsg(0xE0E000, ext.problem);
            else indicator.setColorAndMsg(0x00C000, ext.success);
        }
    }

    // -----------------------------
    // Execution
    //------------------------------

    public function primExtensionOp(b:Block):* {
        var i:int = b.op.indexOf('.');
        var extName:String = b.op.slice(0, i);
        var ext:WitExtension = extensionDict[extName];
        if (ext == null) return 0; // unknown extension
        var primOrVarName:String = b.op.slice(i + 1);
        var args:Array = [];
        for (i = 0; i < b.args.length; i++) {
            args.push(TempStatic.interp.arg(b, i));
        }

        var value:*;
        if (b.isReporter) {
            if (b.isRequester) {
                if (b.requestState == 2) {
                    b.requestState = 0;
                    return b.response;
                }
                else if (b.requestState == 0) {
                    request(extName, primOrVarName, args, b);
                }

                // Returns null if we just made a request or we're still waiting
                return null;
            }
            else {
                var sensorName:String = primOrVarName;
                if (ext.port > 0) {  // we were checking ext.isInternal before, should we?
                    sensorName = encodeURIComponent(sensorName);
                    for each (var a:* in args) sensorName += '/' + encodeURIComponent(a); // append menu args
                    value = ext.stateVars[sensorName];
                }
                if (value == undefined) value = 0; // default to zero if missing
                if ('b' == b.type) value = (ext.port > 0 ? 'true' == value : true == value); // coerce value to a boolean
                return value;
            }
        } else {
            if ('w' == b.type) {
                var activeThread:Thread = TempStatic.interp.activeThread;
                if (activeThread.firstTime) {
                    var id:int = ++ext.nextID; // assign a unique ID for this call
                    ext.busy.push(id);
                    activeThread.tmp = id;
                    TempStatic.interp.doYield();
                    justStartedWait = true;

                    if (ext.port == 0) {
                        activeThread.firstTime = false;
                        ext.busy.pop();

                        return;
                    }

                    args.unshift(id); // pass the ID as the first argument
                } else {
                    if (ext.busy.indexOf(activeThread.tmp) > -1) {
                        TempStatic.interp.doYield();
                    } else {
                        activeThread.tmp = 0;
                        activeThread.firstTime = true;
                    }
                    return;
                }
            }
            call(extName, primOrVarName, args);
        }
    }

    public function call(extensionName:String, op:String, args:Array):void {
        var ext:WitExtension = extensionDict[extensionName];
        if (ext == null) return; // unknown extension
        if (ext.port > 0) {
            var activeThread:Thread = TempStatic.interp.activeThread;
            if (activeThread && op != 'reset_all') {
                if (activeThread.firstTime) {
                    httpCall(ext, op, args);
                    activeThread.firstTime = false;
                    TempStatic.interp.doYield();
                }
                else {
                    activeThread.firstTime = true;
                }
            }
            else
                httpCall(ext, op, args);
        } else {
            if (op == 'reset_all') op = 'resetAll';

            // call a JavaScript extension function with the given arguments
            TempStatic.interp.redraw(); // make sure interpreter doesn't do too many extension calls in one cycle
        }
    }

    public function request(extensionName:String, op:String, args:Array, b:Block):void {
        var ext:WitExtension = extensionDict[extensionName];
        if (ext == null) {
            // unknown extension, skip the block
            b.requestState = 2;
            return;
        }

        if (ext.port > 0) {
            httpRequest(ext, op, args, b);
        }
    }

    private function httpRequest(ext:WitExtension, op:String, args:Array, b:Block):void {
        function responseHandler(e:Event):void {
            if (e.type == Event.COMPLETE)
                b.response = loader.data;
            else
                b.response = '';

            b.requestState = 2;
            b.requestLoader = null;
        }

        var loader:URLLoader = new URLLoader();
        loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, responseHandler);
        loader.addEventListener(IOErrorEvent.IO_ERROR, responseHandler);
        loader.addEventListener(Event.COMPLETE, responseHandler);

        b.requestState = 1;
        b.requestLoader = loader;

        var url:String = 'http://' + ext.host + ':' + ext.port + '/' + encodeURIComponent(op);
        for each (var arg:* in args) {
            url += '/' + ((arg is String) ? encodeURIComponent(arg) : arg);
        }
        loader.load(new URLRequest(url));
    }

    private function httpCall(ext:WitExtension, op:String, args:Array):void {
        function errorHandler(e:Event):void {
        } // ignore errors
        var url:String = 'http://' + ext.host + ':' + ext.port + '/' + encodeURIComponent(op);
        for each (var arg:* in args) {
            url += '/' + ((arg is String) ? encodeURIComponent(arg) : arg);
        }
        var loader:URLLoader = new URLLoader();
        loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler);
        loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
        loader.load(new URLRequest(url));
    }

    public function getStateVar(extensionName:String, varName:String, defaultValue:*):* {
        var ext:WitExtension = extensionDict[extensionName];
        if (ext == null) return defaultValue; // unknown extension
        var value:* = ext.stateVars[encodeURIComponent(varName)];
        return (value == undefined) ? defaultValue : value;
    }

    // -----------------------------
    // Polling
    //------------------------------

    public function step():void {
        // Poll all extensions.
        for each (var ext:WitExtension in extensionDict) {
            if (ext.showBlocks) {
                if (!ext.isInternal && ext.port > 0) {
                    if (ext.blockSpecs.length == 0) httpGetSpecs(ext);
                    httpPoll(ext);
                }
            }
        }
    }

    private function httpGetSpecs(ext:WitExtension):void {
        // Fetch the block specs (and optional menu specs) from the helper app.
        function completeHandler(e:Event):void {
            var specsObj:Object;
            try {
                specsObj = JSON.parse(loader.data);
            } catch (e:*) {
            }
            if (!specsObj) return;
            // use the block specs and (optionally) menu returned by the helper app
            if (specsObj.blockSpecs) ext.blockSpecs = specsObj.blockSpecs;
            if (specsObj.menus) ext.menus = specsObj.menus;
        }

        function errorHandler(e:Event):void {
        } // ignore errors
        var url:String = 'http://' + ext.host + ':' + ext.port + '/get_specs';
        var loader:URLLoader = new URLLoader();
        loader.addEventListener(Event.COMPLETE, completeHandler);
        loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler);
        loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
        loader.load(new URLRequest(url));
    }

    private function httpPoll(ext:WitExtension):void {

        if (pollInProgress[ext]) {
            // Don't poll again if there's already one in progress.
            // This can happen a lot if the connection is timing out.
            return;
        }

        // Poll via HTTP.
        function completeHandler(e:Event):void {
            delete pollInProgress[ext];
            processPollResponse(ext, loader.data);
        }

        function errorHandler(e:Event):void {
            // ignore errors
            delete pollInProgress[ext];
        }

        var url:String = 'http://' + ext.host + ':' + ext.port + '/poll';
        var loader:URLLoader = new URLLoader();
        loader.addEventListener(Event.COMPLETE, completeHandler);
        loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler);
        loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
        pollInProgress[ext] = true;
        loader.load(new URLRequest(url));
    }

    private function processPollResponse(ext:WitExtension, response:String):void {
        if (response == null) return;
        ext.lastPollResponseTime = getTimer();
        ext.problem = '';

        // clear the busy list unless we just started a command that waits
        if (justStartedWait) justStartedWait = false;
        else ext.busy = [];

        var i:int;
        var lines:Array = response.split('\n');
        for each (var line:String in lines) {
            i = line.indexOf(' ');
            if (i == -1) i = line.length;
            var key:String = line.slice(0, i);
            var value:String = decodeURIComponent(line.slice(i + 1));
            switch (key) {
                case '_busy':
                    for each (var token:String in value.split(' ')) {
                        var id:int = parseInt(token);
                        if (ext.busy.indexOf(id) == -1) ext.busy.push(id);
                    }
                    break;
                case '_problem':
                    ext.problem = value;
                    break;
                case '_success':
                    ext.success = value;
                    break;
                default:
                    var n:Number = Interpreter.asNumber(value);
                    var path:Array = key.split('/');
                    for (i = 0; i < path.length; i++) {
                        // normalize URL encoding for each path segment
                        path[i] = encodeURIComponent(decodeURIComponent(path[i]));
                    }
                    ext.stateVars[path.join('/')] = n == n ? n : value;
            }
        }
    }

    public function hasExperimentalExtensions():Boolean {
        for each (var ext:WitExtension in extensionDict) {
            if (!ext.isInternal && ext.javascriptURL) {
                return true;
            }
        }
        return false;
    }
}
}
