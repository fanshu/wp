package wp.editor.extensions {
import flash.display.Sprite;
import flash.display.Stage;
import flash.events.*;
import flash.net.*;
import flash.utils.clearInterval;
import flash.utils.setInterval;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.TempStatic;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.uiwidgets.Button;
import wp.editor.uiwidgets.DialogBox;

public class ExtensionDevManager extends ExtensionManager {
    public var localExt:WitExtension = null;
    public var localFilePoller:uint = 0;
    protected var localFileRef:FileReference;

    public function ExtensionDevManager() {
        super();
    }

    public function getLocalFileName(ext:WitExtension = null):String {
        if (localFileRef && (ext === localExt || ext == null)) return localFileRef.name;

        return null;
    }

    public function isLocalExtensionDirty(ext:WitExtension = null):Boolean {
        return (!ext || ext == localExt) && localExt && localFileDirty;
    }

    // Override so that we can keep the reference to the local extension
    private var rawExtensionLoaded:Boolean = false;

    override public function loadRawExtension(extObj:Object):WitExtension {
        var ext:WitExtension = extensionDict[extObj.extensionName];
        var isLocalExt:Boolean = (localExt && ext == localExt) || (localFilePoller && !localExt);
        ext = super.loadRawExtension(extObj);
        if (isLocalExt) {
            if (!localExt) {
                DialogBox.notify('Extensions', 'Your local extension "' + ext.name +
                        '" is now loaded.The editor will notice when ' + localFileRef.name +
                        ' is\nsaved and offer you to reload the extension. Reloading an extension will stop the project.');
            }
            localExt = ext;
            localExtensionLoaded();
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE));
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES));
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true));
        }

        rawExtensionLoaded = true;
        return ext;
    }

    protected function localExtensionLoaded():void {
    }

    public function makeLoadExperimentalExtensionButton():Button {
        function showShiftMenu(evt:MouseEvent):void {
            loadAndWatchExtensionFile();
        }

        // TODO: button tip link
        const _:Function = gettext;
        var button:Button = new Button(_('Load Experimental Extension'));
        button.setEventAction(function (evt:MouseEvent):void {
            if (evt.shiftKey) {
                showShiftMenu(evt);
            } else {
                setModalOverlay(true);
            }
        });

        return button;
    }

    private var modalOverlay:Sprite;

    public function setModalOverlay(enableOverlay:Boolean):void {
        var currentlyEnabled:Boolean = !!modalOverlay;
        if (enableOverlay != currentlyEnabled) {
            var stage:Stage = TempStatic.stagePart.stage;
            if (enableOverlay) {
                function eatEvent(event:MouseEvent):void {
                    event.stopImmediatePropagation();
                    event.stopPropagation();
                }

                modalOverlay = new Sprite();
                modalOverlay.graphics.beginFill(CSS.backgroundColor_ScratchX, 0.8);
                modalOverlay.graphics.drawRect(0, 0, stage.width, stage.height);
                modalOverlay.addEventListener(MouseEvent.CLICK, eatEvent);
                modalOverlay.addEventListener(MouseEvent.MOUSE_DOWN, eatEvent);
                stage.addChild(modalOverlay);
            }
            else {
                stage.removeChild(modalOverlay);
                modalOverlay = null;
            }
        }
    }

    // -----------------------------
    // Javascript Extension Development
    //------------------------------

    protected var localFileDirty:Boolean;

    public function loadAndWatchExtensionFile(ext:WitExtension = null):void {
        if (localExt || localFilePoller > 0) {
            var msg:String = 'Sorry, a new extension cannot be created while another extension is connected to a file. ' +
                    'Please save the project and disconnect from ' + localFileRef.name + ' first.';
            DialogBox.notify('Extensions', msg);
            return;
        }

        var filter:FileFilter = new FileFilter('WitProgram 2.0 Javascript Extension', '*.js');
        var self:ExtensionDevManager = this;

        function foo(e:Event):void {
            FileReference(e.target).removeEventListener(Event.COMPLETE, arguments.callee);
            FileReference(e.target).addEventListener(Event.COMPLETE, self.extensionFileLoaded);
            self.localExt = ext;
            self.extensionFileLoaded(e);
        }

        function fileSelected(event:Event):void {
            if (fileList.fileList.length > 0) {
                var file:FileReference = FileReference(fileList.fileList[0]);
                file.addEventListener(Event.COMPLETE, foo);
                file.load();
            }
        }

        var fileList:FileReferenceList = new FileReferenceList();
        fileList.addEventListener(Event.SELECT, fileSelected);
        try {
            // Ignore the exception that happens when you call browse() with the file browser open
            fileList.browse([filter]);
        } catch (e:*) {
        }
    }

    public function stopWatchingExtensionFile():void {
        if (localFilePoller > 0) clearInterval(localFilePoller);
        localExt = null;
        localFilePoller = 0;
        localFileDirty = false;
        localFileRef = null;
        localExtCodeDate = null;
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES));
    }

    private var localExtCodeDate:Date = null;

    protected function extensionFileLoaded(e:Event):void {
        localFileRef = FileReference(e.target);
        var lastModified:Date = localFileRef.modificationDate;
        var self:ExtensionDevManager = this;
        localFilePoller = setInterval(function ():void {
            if (lastModified.getTime() != self.localFileRef.modificationDate.getTime()) {
                lastModified = self.localFileRef.modificationDate;
                self.localFileDirty = true;
                clearInterval(self.localFilePoller);
                // Shutdown the extension
                self.localFileRef.load();
            }
        }, 200);

        if (localFileDirty && localExt) {
            //DialogBox.confirm('Reload the "' + localExt.name + '" from ' + localFileRef.name + '?', null, loadLocalCode);
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE));
            evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES));
        }
        else
            loadLocalCode();
    }

    public function getLocalCodeDate():Date {
        return localExtCodeDate;
    }

    public function loadLocalCode(db:DialogBox = null):void {
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.RUNTIME_STOP_ALL));

        localFileDirty = false;
        rawExtensionLoaded = false;
        localExtCodeDate = localFileRef.modificationDate;
//		if(!rawExtensionLoaded)
//			DialogBox.notify('Extensions', 'There was a problem loading your extension code. Please check your javascript console and fix the code.');

        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UPDATE_PALETTE));
        evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.CLEAR_WITSTAGE_ALL_CACHES));
    }

    override public function setEnabled(extName:String, flag:Boolean):void {
        var ext:WitExtension = extensionDict[extName];
        if (ext && localExt === ext && !flag) {
            stopWatchingExtensionFile();
        }

        super.setEnabled(extName, flag);
    }

    public function getExperimentalExtensionNames():Array {
        var names:Array = [];
        for each (var ext:WitExtension in extensionDict) {
            if (!ext.isInternal && ext.javascriptURL) {
                names.push(ext.name);
            }
        }
        return names;
    }
}
}
