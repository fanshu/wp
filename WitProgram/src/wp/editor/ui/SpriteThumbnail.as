package wp.editor.ui {
import flash.display.*;
import flash.events.*;
import flash.filters.GlowFilter;
import flash.text.*;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.Resources;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.media.MediaInfo;
import wp.editor.ui.parts.LibraryPart;
import wp.editor.uiwidgets.*;
import wp.editor.wit.*;

public class SpriteThumbnail extends Sprite {
    private static const _:Function = gettext;

    private const frameW:int = 73;
    private const frameH:int = 73;

    private const thumbnailW:int = 68;
    private const thumbnailH:int = 51;

    public var targetObj:WitObj;

    private var thumbnail:Bitmap;
    private var label:TextField;
    private var sceneInfo:TextField;
    private var selectedFrame:Shape;
    private var highlightFrame:Shape;
    private var infoSprite:Sprite;
    private var detailsButton:IconButton;

    private var lastSrcImg:DisplayObject;
    private var lastName:String = '';

    public function SpriteThumbnail(targetObj:WitObj) {
        this.targetObj = targetObj;

        addFrame();
        addSelectedFrame();
        addHighlightFrame();

        thumbnail = new Bitmap();
        thumbnail.x = 3;
        thumbnail.y = 3;
        thumbnail.filters = [grayOutlineFilter()];
        addChild(thumbnail);

        label = Resources.makeLabel('', CSS.thumbnailFormat);
        label.width = frameW;
        addChild(label);

        if (targetObj is WitStage) {
            sceneInfo = Resources.makeLabel('', CSS.thumbnailExtraInfoFormat);
            sceneInfo.width = frameW;
            addChild(sceneInfo);
        }

        addDetailsButton();
        updateThumbnail();
    }

    private function addDetailsButton():void {
        detailsButton = new IconButton(showSpriteDetails, 'spriteInfo');
        detailsButton.x = detailsButton.y = -2;
        detailsButton.isMomentary = true;
        detailsButton.visible = false;
        addChild(detailsButton);
    }

    private function addFrame():void {
        if (targetObj is WitStage) return;

        var frame:Shape = new Shape();
        var g:Graphics = frame.graphics;
        g.lineStyle(NaN);
        g.beginFill(0xFFFFFF);
        g.drawRoundRect(0, 0, frameW, frameH, 12, 12);
        g.endFill();
        addChild(frame);
    }

    private function addSelectedFrame():void {
        selectedFrame = new Shape();
        var g:Graphics = selectedFrame.graphics;
        var h:int = frameH;
        g.lineStyle(3, CSS.overColor, 1, true);
        g.beginFill(CSS.itemSelectedColor);
        g.drawRoundRect(0, 0, frameW, h, 12, 12);
        g.endFill();
        selectedFrame.visible = false;
        addChild(selectedFrame);
    }

    private function addHighlightFrame():void {
        const highlightColor:int = 0xE0E000;
        highlightFrame = new Shape();
        var g:Graphics = highlightFrame.graphics;
        var h:int = frameH;
        g.lineStyle(2, highlightColor, 1, true);
        g.drawRoundRect(1, 1, frameW - 1, h - 1, 12, 12);
        highlightFrame.visible = false;
        addChild(highlightFrame);
    }

    public function setTarget(obj:WitObj):void {
        targetObj = obj;
        updateThumbnail();
    }

    public function select(flag:Boolean):void {
        if (selectedFrame.visible == flag) return;
        selectedFrame.visible = flag;
        detailsButton.visible = flag && !(targetObj is WitStage);
    }

    public function showHighlight(flag:Boolean):void {
        // Display a highlight if flag is true (e.g. to show broadcast senders/receivers).
        highlightFrame.visible = flag;
    }

    public function showInfo(flag:Boolean):void {
        if (infoSprite) {
            removeChild(infoSprite);
            infoSprite = null;
        }
        if (flag) {
            infoSprite = makeInfoSprite();
            addChild(infoSprite);
        }
    }

    public function makeInfoSprite():Sprite {
        var result:Sprite = new Sprite();
        var bm:Bitmap = Resources.createBmp('hatshape');
        bm.x = (frameW - bm.width) / 2;
        bm.y = 20;
        result.addChild(bm);
        var tf:TextField = Resources.makeLabel(String(targetObj.scripts.length), CSS.normalTextFormat);
        tf.x = bm.x + 20 - (tf.textWidth / 2);
        tf.y = bm.y + 4;
        result.addChild(tf);
        return result;
    }

    public function updateThumbnail(translationChanged:Boolean = false):void {
        if (targetObj == null) return;
        updateName();

        if (targetObj.img.numChildren == 0) return; // shouldn't happen
        if (targetObj.currentCostume().svgLoading) return; // don't update thumbnail while loading SVG bitmaps
        var src:DisplayObject = targetObj.img.getChildAt(0);
        if (src == lastSrcImg) return; // thumbnail is up to date

        var c:WitCostume = targetObj.currentCostume();
        thumbnail.bitmapData = c.thumbnail(thumbnailW, thumbnailH, (targetObj is WitStage));
        lastSrcImg = src;
    }

    private function grayOutlineFilter():GlowFilter {
        // Filter to provide a gray outline even around totally white costumes.
        var f:GlowFilter = new GlowFilter(CSS.onColor);
        f.strength = 1;
        f.blurX = f.blurY = 2;
        f.knockout = false;
        return f;
    }

    private function updateName():void {
        var s:String = (targetObj is WitStage) ? _('Stage') : targetObj.objName;
        if (s == lastName) return;
        lastName = s;
        label.text = s;
        while ((label.textWidth > 60) && (s.length > 0)) {
            s = s.substring(0, s.length - 1);
            label.text = s + '\u2026'; // truncated name with ellipses
        }
        label.x = ((frameW - label.textWidth) / 2) - 2;
        label.y = 57;
    }

    // -----------------------------
    // Grab and Drop
    //------------------------------

    public function objToGrab(evt:MouseEvent):MediaInfo {
        if (targetObj is WitStage) return null;
        var result:MediaInfo = new MediaInfo(targetObj);
        result.removeDeleteButton();
        result.computeThumbnail();
        result.hideTextFields();
        return result;
    }

    public function handleDrop(obj:*):Boolean {
        function addCostume(c:WitCostume):void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_COSTUME, [c, targetObj], true));
        }

        function addSound(snd:WitSound):void {
//            TempStatic.addSound(snd, targetObj)
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_SOUND, [snd, targetObj], true));
        }

        var item:MediaInfo = obj as MediaInfo;
        if (item) {
            // accept dropped costumes and sounds from another sprite, but not yet from Backpack
            if (item.mycostume) {
                addCostume(item.mycostume.duplicate());
                return true;
            }
            if (item.mysound) {
                addSound(item.mysound.duplicate());
                return true;
            }
        }
        if (obj is Block) {
            // copy a block/stack to this sprite
            if (targetObj == WitProgram.viewedObj) return false; // dropped on my own thumbnail; do nothing
            var copy:Block = Block(obj).duplicate(false, (targetObj is WitStage));
            copy.x = WitProgram.scriptsPane.padding;
            copy.y = WitProgram.scriptsPane.padding;
            targetObj.scripts.push(copy);
            return false; // do not consume the original block
        }
        return false;
    }

    // -----------------------------
    // User interaction
    //------------------------------

    public function click(evt:Event):void {
        if (!(targetObj is WitStage) && (targetObj is WitSprite)) TempStatic.stagePart.flashSprite(targetObj as WitSprite);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_SPRITE, targetObj, true));
    }

    public function menu(evt:MouseEvent):Menu {
        function hideInScene():void {
            t.visible = false;
            t.updateBubble();
        }

        function showInScene():void {
            t.visible = true;
            t.updateBubble();
        }

        if (targetObj is WitStage) return null;
        var t:WitSprite = targetObj as WitSprite;
        var m:Menu = t.menu(evt); // basic sprite menu
        m.addLine();
        if (t.visible) {
            m.addItem('hide', hideInScene);
        } else {
            m.addItem('show', showInScene);
        }
        return m;
    }

    public function handleTool(tool:String, evt:MouseEvent):void {
        if (tool == 'help') trace('scratchUI');
        var spr:WitSprite = targetObj as WitSprite;
        if (!spr) return;
        if (tool == 'copy') spr.duplicateSprite();
        if (tool == 'cut') spr.deleteSprite();
    }

    private function showSpriteDetails(ignore:*):void {
        var lib:LibraryPart = parent.parent.parent as LibraryPart;
        if (lib) lib.showSpriteDetails(true);
    }
}
}
