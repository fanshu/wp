package wp.editor.ui.media {
import flash.display.*;
import flash.events.MouseEvent;
import flash.text.*;

import gnu.as3.gettext.gettext;

import util.Resources;

public class MediaFilter extends Sprite {
    private static const _:Function = gettext;

    private const titleFormat:TextFormat = new TextFormat(CSS.font, 15, CSS.buttonLabelOverColor, false);
    private const selectorFormat:TextFormat = new TextFormat(CSS.font, 14, CSS.textColor);

    private const unselectedColor:int = CSS.overColor; // 0x909090;
    private const selectedColor:int = CSS.textColor;
    private const rolloverColor:int = CSS.buttonLabelOverColor;

    private var title:TextField;
    private var selectorNames:Array = []; // strings representing tags/themes/categories
    private var selectors:Array = []; // TextFields (translated)
    private var selection:String = '';
    private var whenChanged:Function;

    public function MediaFilter(filterName:String, elements:Array, whenChanged:Function = null) {
        addChild(title = Resources.makeLabel(_(filterName), titleFormat));
        this.whenChanged = whenChanged;
        for each (var selName:String in elements) addSelector(selName);
        select(0); // select first selector by default
        fixLayout();
    }

    public function set currentSelection(s:String):void {
        select(selectorNames.indexOf(s))
    }

    public function get currentSelection():String {
        return selection
    }

    private function fixLayout():void {
        title.x = title.y = 0;
        var nextY:int = title.height + 2;
        for each (var sel:TextField in selectors) {
            sel.x = 15;
            sel.y = nextY;
            nextY += sel.height;
        }
    }

    private function addSelectors(selList:Array):void {
        for each (var selName:String in selList) addSelector(selName);
    }

    private function addSelector(selName:String):void {
        function mouseDown(ignore:*):void {
            select(selectorNames.indexOf(selName));
            if (whenChanged != null) whenChanged(sel.parent);
        }

        var sel:TextField = Resources.makeLabel(_(selName), selectorFormat);
        sel.addEventListener(MouseEvent.MOUSE_OVER, mouseOver);
        sel.addEventListener(MouseEvent.MOUSE_OUT, mouseOver);
        sel.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
        selectorNames.push(selName);
        selectors.push(sel);
        addChild(sel);
    }

    private function mouseOver(evt:MouseEvent):void {
        var sel:TextField = evt.target as TextField;
        if (sel.textColor != selectedColor) {
            sel.textColor = (evt.type == MouseEvent.MOUSE_OVER) ? rolloverColor : unselectedColor;
        }
    }

    private function select(index:int):void {
        // Highlight the new selection and unlight all others.
        selection = ''; // nothing selected
        var fmt:TextFormat = new TextFormat();
        for (var i:int = 0; i < selectors.length; i++) {
            if (i == index) {
                selection = selectorNames[i];
                fmt.bold = true;
                selectors[i].setTextFormat(fmt);
                selectors[i].textColor = selectedColor;
            } else {
                fmt.bold = false;
                selectors[i].setTextFormat(fmt);
                selectors[i].textColor = unselectedColor;
            }
        }
    }
}
}
