package wp.editor.ui.media {
import flash.display.Sprite;
import flash.geom.Point;
import flash.text.TextField;

import jijunzeng.MyEvent;

import util.Resources;
import util.TempStatic;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.parts.SoundsPart;
import wp.editor.uiwidgets.*;
import wp.editor.wit.*;

public class MediaPane extends ScrollFrameContents {
    private var isSound:Boolean;
    private var lastCostume:WitCostume;

    public function MediaPane(type:String):void {
        isSound = (type == 'sounds');
        refresh();
    }

    public function refresh():void {
        if (WitProgram.viewedObj == null) return;
        replaceContents(isSound ? soundItems() : costumeItems());
        updateSelection();
    }

    // Returns true if we might need to save
    public function updateSelection():Boolean {
        if (isSound) {
            updateSoundSelection();
            return true;
        }

        return updateCostumeSelection();
    }

    private function replaceContents(newItems:Array):void {
        while (numChildren > 0) removeChildAt(0);
        var nextY:int = 3;
        var n:int = 1;
        for each (var item:Sprite in newItems) {
            var numLabel:TextField = Resources.makeLabel('' + n++, CSS.thumbnailExtraInfoFormat);
            numLabel.x = 9;
            numLabel.y = nextY + 1;
            item.x = 7;
            item.y = nextY;
            nextY += item.height + 3;
            addChild(item);
            addChild(numLabel);
        }
        updateSize();
        lastCostume = null;
        x = y = 0; // reset scroll offset
    }

    private function costumeItems():Array {
        var result:Array = [];
        var viewedObj:WitObj = WitProgram.viewedObj;
        for each (var c:WitCostume in viewedObj.costumes) {
            result.push(new MediaInfo(c, viewedObj));
        }
        return result;
    }

    private function soundItems():Array {
        var result:Array = [];
        var viewedObj:WitObj = WitProgram.viewedObj;
        for each (var snd:WitSound in viewedObj.sounds) {
            result.push(new MediaInfo(snd, viewedObj));
        }
        return result;
    }

    // Returns true if the costume changed
    private function updateCostumeSelection():Boolean {
        var viewedObj:WitObj = WitProgram.viewedObj;
        if ((viewedObj == null) || isSound) return false;
        var current:WitCostume = viewedObj.currentCostume();
        if (current == lastCostume) return false;
        var oldCostume:WitCostume = lastCostume;
        for (var i:int = 0; i < numChildren; i++) {
            var ci:MediaInfo = getChildAt(i) as MediaInfo;
            if (ci != null) {
                if (ci.mycostume == current) {
                    ci.highlight();
                    scrollToItem(ci);
                } else {
                    ci.unhighlight();
                }
            }
        }
        lastCostume = current;
        return (oldCostume != null);
    }

    private function scrollToItem(item:MediaInfo):void {
        var frame:ScrollFrame = parent as ScrollFrame;
        if (!frame) return;
        var itemTop:int = item.y + y - 1;
        var itemBottom:int = itemTop + item.height;
        y -= Math.max(0, itemBottom - frame.visibleH());
        y -= Math.min(0, itemTop);
        frame.updateScrollbars();
    }

    private function updateSoundSelection():void {
        var viewedObj:WitObj = WitProgram.viewedObj;
        if ((viewedObj == null) || !isSound) return;
        if (viewedObj.sounds.length < 1) return;
        if (!this.parent || !this.parent.parent) return;
        var sp:SoundsPart = this.parent.parent as SoundsPart;
        if (sp == null) return;
        sp.currentIndex = Math.min(sp.currentIndex, viewedObj.sounds.length - 1);
        var current:WitSound = viewedObj.sounds[sp.currentIndex] as WitSound;
        for (var i:int = 0; i < numChildren; i++) {
            var si:MediaInfo = getChildAt(i) as MediaInfo;
            if (si != null) {
                if (si.mysound == current) si.highlight();
                else si.unhighlight();
            }
        }
    }

    // -----------------------------
    // Dropping
    //------------------------------

    public function handleDrop(obj:*):Boolean {
        var item:MediaInfo = obj as MediaInfo;
        if (item && item.owner == WitProgram.viewedObj) {
            changeMediaOrder(item);
            return true;
        }
        return false;
    }

    private function changeMediaOrder(dropped:MediaInfo):void {
        var inserted:Boolean = false;
        var newItems:Array = [];
        var dropY:int = globalToLocal(new Point(dropped.x, dropped.y)).y;
        for (var i:int = 0; i < numChildren; i++) {
            var item:MediaInfo = getChildAt(i) as MediaInfo;
            if (!item) continue; // skip item numbers
            if (!inserted && (dropY < item.y)) {
                newItems.push(dropped);
                inserted = true;
            }
            if (!sameMedia(item, dropped)) newItems.push(item);
        }
        if (!inserted) newItems.push(dropped);
        replacedMedia(newItems);
        // update the target object with the new costume/sound list
        // refresh();
    }

    private function sameMedia(item1:MediaInfo, item2:MediaInfo):Boolean {
        if (item1.mycostume && (item1.mycostume == item2.mycostume)) return true;
        if (item1.mysound && (item1.mysound == item2.mysound)) return true;
        return false;
    }

    private function replacedMedia(newList:Array):void {
        // Note: Clones can share the costume and sound arrays with their prototype,
        // so this method mutates those arrays in place rather than replacing them.
        var el:MediaInfo;
        var scratchObj:WitObj = WitProgram.viewedObj;
        if (isSound) {
            scratchObj.sounds.splice(0); // remove all
            for each (el in newList) {
                if (el.mysound) scratchObj.sounds.push(el.mysound);
            }
        } else {
            var oldCurrentCostume:WitCostume = scratchObj.currentCostume();
            scratchObj.costumes.splice(0); // remove all
            for each (el in newList) {
                if (el.mycostume) scratchObj.costumes.push(el.mycostume);
            }
            var cIndex:int = scratchObj.costumes.indexOf(oldCurrentCostume);
            if (cIndex > -1) scratchObj.currentCostumeIndex = cIndex;
        }
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
        refresh();
    }
}
}
