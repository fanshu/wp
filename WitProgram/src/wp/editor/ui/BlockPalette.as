// A BlockPalette holds the blocks for the selected category.
// The mouse handling code detects when a Block's parent is a BlocksPalette and
// creates a copy of that block when it is dragged out of the palette.
package wp.editor.ui {
import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.interpreter.Interpreter;
import wp.editor.uiwidgets.*;
import wp.editor.wit.WitComment;
import wp.editor.wit.WitObj;

public class BlockPalette extends ScrollFrameContents {
    public const isBlockPalette:Boolean = true;

    public function BlockPalette():void {
        super();
        this.color = CSS.backgroundColor_default;
    }

    override public function clear(scrollToOrigin:Boolean = true):void {
        var interp:Interpreter = TempStatic.interp;
        var targetObj:WitObj = WitProgram.viewedObj;
        while (numChildren > 0) {
            var b:Block = getChildAt(0) as Block;
            if (interp.isRunning(b, targetObj)) interp.toggleThread(b, targetObj);
            removeChildAt(0);
        }
        if (scrollToOrigin) x = y = 0;
    }

    public function handleDrop(obj:*):Boolean {
        // Delete blocks and stacks dropped onto the palette.
        var c:WitComment = obj as WitComment;
        if (c) {
            c.x = c.y = 20; // position for undelete
            c.deleteComment();
            return true;
        }
        var b:Block = obj as Block;
        if (b) {
            return b.deleteStack();
        }
        return false;
    }
}
}
