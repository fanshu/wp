// PaletteSelector is a UI widget that holds set of PaletteSelectorItems
// and supports changing the selected category. When the category is changed,
// the blocks palette is filled with the blocks for the selected category.
package wp.editor.ui {
import flash.display.*;

import wp.editor.wit.PaletteBuilder;

public class PaletteSelector extends Sprite {
    private static const categories:Array = [
        'Motion', 'Looks', 'Sound', 'Pen', 'Data', // column 1
        'Events', 'Control', 'Sensing', 'Operators', 'More Blocks']; // column 2

    public var selectedCategory:int = 0;

    public function PaletteSelector() {
        initCategories();
    }

    public function updateTranslation():void {
        initCategories()
    }

    public function select(id:int, shiftKey:Boolean = false):void {
        for (var i:int = 0; i < numChildren; i++) {
            var item:PaletteSelectorItem = getChildAt(i) as PaletteSelectorItem;
            item.setSelected(item.categoryID == id);
        }
        var oldID:int = selectedCategory;
        selectedCategory = id;
        new PaletteBuilder().showBlocksForCategory(selectedCategory, (id != oldID), shiftKey);
    }

    private function initCategories():void {
        const numberOfRows:int = 5;
        const w:int = 245;
        const startX:int = 20;
        const startY:int = 11;
        var itemH:int;
        var x:int, i:int;
        var y:int = startY;
        while (numChildren > 0) removeChildAt(0); // remove old contents

        for (i = 0; i < categories.length; i++) {
            if (i == numberOfRows) {
                x = (w / 2) - 3;
                y = startY;
            }
            var entry:Array = Specs.entryForCategory(categories[i]);
            var item:PaletteSelectorItem = new PaletteSelectorItem(entry[0], entry[1], entry[2]);
            itemH = item.height + 10;
            item.x = x + startX;
            item.y = y;
            addChild(item);
            y += itemH;
        }
        setWidthHeightColor(w, startY + (numberOfRows * itemH) + 5);
    }

    private function setWidthHeightColor(w:int, h:int):void {
        var g:Graphics = graphics;
        g.clear();
        g.beginFill(0xFFFF00, 0); // invisible (alpha = 0) rectangle used to set size
        g.drawRect(0, 0, w, h);
    }
}
}
