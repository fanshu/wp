package wp.editor.ui.parts {
import flash.display.*;
import flash.events.MouseEvent;
import flash.geom.*;
import flash.text.*;
import flash.utils.setTimeout;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;
import util.UIPartUtil;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.svgeditor.*;
import wp.editor.svgutils.*;
import wp.editor.ui.CameraDialog;
import wp.editor.ui.media.*;
import wp.editor.uiwidgets.*;
import wp.editor.wit.*;

public class ImagesPart extends UIPart {
    private static const _:Function = gettext;
    [Embed(source='/../assets/UI/newsprite/camera.png')]
    private const CAMERA:Class;

    [Embed(source='/../assets/UI/newsprite/import.png')]
    private const IMPORT:Class;

    [Embed(source='/../assets/UI/newsprite/library.png')]
    private const LIBRARY:Class;

    [Embed(source='/../assets/UI/newsprite/paintbrush.png')]
    private const PAINT:Class;

    public var editor:ImageEdit;

    private const columnWidth:int = 145;
    private const contentsX:int = columnWidth + 13;
    private const topButtonSize:Point = new Point(24, 22);
    private const smallSpace:int = 3;
    private var bigSpace:int;

    private var shape:Shape;
    private var listFrame:ScrollFrame;
    private var nameField:EditableLabel;
    private var undoButton:IconButton;
    private var redoButton:IconButton;
    private var clearButton:Button;
    private var libraryButton:Button;
    private var editorImportButton:Button;
    private var cropButton:IconButton;
    private var flipHButton:IconButton;
    private var flipVButton:IconButton;
    private var centerButton:IconButton;

    private var newCostumeLabel:TextField;
    private var costumeLibraryButton:MyButton;
    private var paintButton:MyButton;
    private var importButton:MyButton;
    private var cameraButton:MyButton;

    public var cameraDialog:CameraDialog;

    public function ImagesPart() {
        addChild(shape = new Shape());

        addChild(newCostumeLabel = UIPartUtil.makeLabel('', new TextFormat(CSS.font, 12, CSS.textColor, true)));
        addNewCostumeButtons();

        addListFrame();
        addChild(nameField = new EditableLabel(nameChanged));

        addEditor(true);

        addUndoButtons();
        addFlipButtons();
        addCenterButton();
        updateTranslation();
    }

    protected function addEditor(isSVG:Boolean):void {
        if (isSVG) {
            addChild(editor = new SVGEdit(this));
        }
        else {
            addChild(editor = new BitmapEdit(this));
        }
    }

    public function updateTranslation():void {
        clearButton.setLabel(_('Clear'));
        libraryButton.setLabel(_('Add'));
        editorImportButton.setLabel(_('Import'));
        if (editor) editor.updateTranslation();
        updateLabel();
        fixlayout();
    }

    public function refresh(fromEditor:Boolean = false):void {
        updateLabel();
        (listFrame.contents as MediaPane).refresh();
        if (!fromEditor) selectCostume(); // this refresh is because the editor just saved the costume; do nothing
    }

    private function updateLabel():void {
//        newCostumeLabel.text = _(isStage() ? 'New backdrop:' : 'New costume:');
//
//        SimpleTooltips.add(backdropLibraryButton, {text: 'Choose backdrop from library', direction: 'bottom'});
//        SimpleTooltips.add(costumeLibraryButton, {text: 'Choose costume from library', direction: 'bottom'});
//        if (isStage()) {
//            SimpleTooltips.add(paintButton, {text: 'Paint new backdrop', direction: 'bottom'});
//            SimpleTooltips.add(importButton, {text: 'Upload backdrop from file', direction: 'bottom'});
//            SimpleTooltips.add(cameraButton, {text: 'New backdrop from camera', direction: 'bottom'});
//        }
//        else {
//            SimpleTooltips.add(paintButton, {text: 'Paint new costume', direction: 'bottom'});
//            SimpleTooltips.add(importButton, {text: 'Upload costume from file', direction: 'bottom'});
//            SimpleTooltips.add(cameraButton, {text: 'new costume from camera', direction: 'bottom'});
//        }
    }

    private function isStage():Boolean {
        return WitProgram.viewedObj && (WitProgram.viewedObj is WitStage);
    }

    public function step():void {
        (listFrame.contents as MediaPane).updateSelection();
        listFrame.updateScrollbars();
    }

    public function setWidthHeight(w:int, h:int):void {
        this.w = w;
        this.h = h;
        var g:Graphics = shape.graphics;
        g.clear();

        g.lineStyle(0.5, CSS.borderColor, 1, true);
        g.beginFill(CSS.backgroundColor_default);
        g.drawRect(0, 0, w, h);
        g.endFill();

        g.lineStyle(0.5, CSS.borderColor, 1, true);
        g.beginFill(CSS.backgroundColor_default);
        g.drawRect(columnWidth + 1, 5, w - columnWidth - 6, h - 10);
        g.endFill();

        fixlayout();
    }

    private function fixlayout():void {
        var extraSpace:int = Math.max(0, (w - 590) / 3);
        bigSpace = smallSpace + extraSpace;

        newCostumeLabel.x = 7;
        newCostumeLabel.y = 7;

        listFrame.x = 1;
        listFrame.y = 150;
        listFrame.setWidthHeight(columnWidth, h - listFrame.y);

        var contentsW:int = w - contentsX - 15;
        nameField.setWidth(Math.min(135, contentsW));
        nameField.x = contentsX;
        nameField.y = 15;

        // undo buttons
        undoButton.x = nameField.x + nameField.width + bigSpace;
        redoButton.x = undoButton.right() + smallSpace;
        clearButton.x = redoButton.right() + bigSpace;
        clearButton.y = nameField.y;
        undoButton.y = redoButton.y = nameField.y - 2;

        fixEditorLayout();
        if (parent) refresh();
    }

    public function selectCostume():void {
        var contents:MediaPane = listFrame.contents as MediaPane;
        var changed:Boolean = contents.updateSelection();
        var obj:WitObj = WitProgram.viewedObj;
        if (obj == null) return;
        nameField.setContents(obj.currentCostume().costumeName);

        var zoomAndScroll:Array = editor.getZoomAndScroll();
        editor.shutdown();
        var c:WitCostume = obj.currentCostume();
        useBitmapEditor(c.isBitmap() && !c.text);
        editor.editCostume(c, (obj is WitStage));
        editor.setZoomAndScroll(zoomAndScroll);
        if (changed) {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
        }
    }

    private function addListFrame():void {
        listFrame = new ScrollFrame();
        listFrame.setContents(new MediaPane('costumes'));
        listFrame.contents.color = CSS.backgroundColor_default;
        listFrame.allowHorizontalScrollbar = false;
        addChild(listFrame);
    }

    private function nameChanged():void {
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.RENAME_COSTUME, nameField.contents, true));
        nameField.setContents(WitProgram.viewedObj.currentCostume().costumeName);
        (listFrame.contents as MediaPane).refresh();
    }

    private function addNewCostumeButtons():void {
        var left:int = 8;
        var buttonY:int = 32;
        addChild(costumeLibraryButton = new MyButton(new LIBRARY(), 0x31cbff, _('Choose backdrop from library')));
        addChild(paintButton = new MyButton(new PAINT(), 0x9ac969, _('Paint new costume')));
        addChild(importButton = new MyButton(new IMPORT(), 0xfc9832, _('Upload costume from file')));
        addChild(cameraButton = new MyButton(new CAMERA(), 0xfe6665, _('New backdrop from camera')));
        costumeLibraryButton.x = left + 1;
        costumeLibraryButton.y = buttonY - 2;
        costumeLibraryButton.setWitdh(130);
        paintButton.x = costumeLibraryButton.x;
        paintButton.y = costumeLibraryButton.y + 28;
        paintButton.setWitdh(130);
        importButton.x = paintButton.x;
        importButton.y = paintButton.y + 28;
        importButton.setWitdh(130);
        cameraButton.x = importButton.x;
        cameraButton.y = importButton.y + 28;
        cameraButton.setWitdh(130);
        costumeLibraryButton.addEventListener(MouseEvent.CLICK, function(evt:MouseEvent):void {
            costumeFromLibrary();
        });
        paintButton.addEventListener(MouseEvent.CLICK, function(evt:MouseEvent):void {
            paintCostume();
        });
        importButton.addEventListener(MouseEvent.CLICK, function(evt:MouseEvent):void {
            costumeFromComputer();
        });
        cameraButton.addEventListener(MouseEvent.CLICK, function(evt:MouseEvent):void {
            costumeFromCamera();
        });
    }

    public function useBitmapEditor(flag:Boolean):void {
        // Switch editors based on flag. Do nothing if editor is already of the correct type.
        // NOTE: After switching editors, the caller must install costume and other state in the new editor.
        var oldSettings:DrawProperties, oldZoomAndScroll:Array;
        if (editor) {
            oldSettings = editor.getShapeProps();
            oldZoomAndScroll = editor.getWorkArea().getZoomAndScroll();
        }
        if (flag) {
            if (editor is BitmapEdit) return;
            if (editor && editor.parent) removeChild(editor);
            addEditor(false);
        }
        else {
            if (editor is SVGEdit) return;
            if (editor && editor.parent) removeChild(editor);
            addEditor(true);
        }
        if (oldSettings) {
            editor.setShapeProps(oldSettings);
            editor.getWorkArea().setZoomAndScroll([oldZoomAndScroll[0], 0.5, 0.5]);
        }
        editor.registerToolButton('setCenter', centerButton);
        fixEditorLayout();
    }

    private function fixEditorLayout():void {
        var contentsW:int = w - contentsX - 15;
        if (editor) {
            editor.x = contentsX;
            editor.y = 45;
            editor.setWidthHeight(contentsW, h - editor.y - 14);
        }

        contentsW = w - 16;
        // import button
        libraryButton.x = clearButton.x + clearButton.width + smallSpace;
        libraryButton.y = clearButton.y;
        editorImportButton.x = libraryButton.x + libraryButton.width + smallSpace;
        editorImportButton.y = clearButton.y;

        // buttons in the upper right
        centerButton.x = contentsW - centerButton.width;
        flipVButton.x = centerButton.x - flipVButton.width - smallSpace;
        flipHButton.x = flipVButton.x - flipHButton.width - smallSpace;
        cropButton.x = flipHButton.x - cropButton.width - smallSpace;
        cropButton.y = flipHButton.y = flipVButton.y = centerButton.y = nameField.y - 1;
    }

    // -----------------------------
    // Button Creation
    //------------------------------

    private function makeButton(fcn:Function, iconName:String, x:int, y:int):IconButton {
        var b:IconButton = new IconButton(fcn, iconName);
        b.isMomentary = true;
        b.x = x;
        b.y = y;
        return b;
    }

    private function makeTopButton(fcn:Function, iconName:String, isRadioButton:Boolean = false):IconButton {
        return new IconButton(
                fcn, SoundsPart.makeButtonImg(iconName, true, topButtonSize),
                SoundsPart.makeButtonImg(iconName, false, topButtonSize), isRadioButton);
    }

    // -----------------------------
    // Bitmap/Vector Conversion
    //------------------------------

    public function convertToBitmap():void {
        function finishConverting():void {
            var c:WitCostume = editor.targetCostume;
            var forStage:Boolean = editor.isScene;
            var zoomAndScroll:Array = editor.getZoomAndScroll();
            useBitmapEditor(true);

            var bm:BitmapData = c.bitmapForEditor(forStage);
            c.setBitmapData(bm, 2 * c.rotationCenterX, 2 * c.rotationCenterY);

            editor.editCostume(c, forStage, true);
            editor.setZoomAndScroll(zoomAndScroll);
            editor.saveContent();
        }

        if (editor is BitmapEdit) return;
        editor.shutdown();
        setTimeout(finishConverting, 300); // hack: allow time for SVG embedded bitmaps to be rendered before rendering
    }

    public function convertToVector():void {
        if (editor is SVGEdit) return;
        editor.shutdown();
        editor.setToolMode('select', true);
        var c:WitCostume = editor.targetCostume;
        var forStage:Boolean = editor.isScene;
        var zoomAndScroll:Array = editor.getZoomAndScroll();
        useBitmapEditor(false);

        var svg:SVGElement = new SVGElement('svg');
        svg.subElements.push(SVGElement.makeBitmapEl(c.baseLayerBitmap, 1 / c.bitmapResolution));
        c.rotationCenterX /= c.bitmapResolution;
        c.rotationCenterY /= c.bitmapResolution;
        c.setSVGData(new SVGExport(svg).svgData(), false, false);

        editor.editCostume(c, forStage, true);
        editor.setZoomAndScroll(zoomAndScroll);
//		editor.saveContent();
    }

    // -----------------------------
    // Undo/Redo
    //------------------------------

    private function addUndoButtons():void {
        addChild(undoButton = makeTopButton(undo, 'undo'));
        addChild(redoButton = makeTopButton(redo, 'redo'));
        addChild(clearButton = new Button(_('Clear'), clear, true));
        addChild(libraryButton = new Button(_('Add'), importFromLibrary, true));
        addChild(editorImportButton = new Button(_('Import'), importIntoEditor, true));
        undoButton.isMomentary = true;
        redoButton.isMomentary = true;
        SimpleTooltips.add(undoButton, {text: 'Undo', direction: 'bottom'});
        SimpleTooltips.add(redoButton, {text: 'Redo', direction: 'bottom'});
        SimpleTooltips.add(clearButton, {text: 'Erase all', direction: 'bottom'});
    }

    private function undo(b:*):void {
        editor.undo(b)
    }

    private function redo(b:*):void {
        editor.redo(b)
    }

    private function clear():void {
        editor.clearCanvas()
    }

    private function importFromLibrary():void {
        var t:String = isStage() ? 'backdrop' : 'costume';
        var lib:MediaLibrary = new MediaLibrary(t, addCostume);
        lib.open();
    }

    private function importIntoEditor():void {
        var lib:MediaLibrary = new MediaLibrary('', addCostume);
        lib.importFromDisk();
    }

    private function addCostume(c:WitCostume):void {
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_COSTUME_IMAGE_EDITOR, c));
    }

    public function refreshUndoButtons():void {
        undoButton.setDisabled(!editor.canUndo(), 0.5);
        redoButton.setDisabled(!editor.canRedo(), 0.5);
        if (editor.canClearCanvas()) {
            clearButton.alpha = 1;
            clearButton.mouseEnabled = true;
        }
        else {
            clearButton.alpha = 0.5;
            clearButton.mouseEnabled = false;
        }
    }

    public function setCanCrop(enabled:Boolean):void {
        if (enabled) {
            cropButton.alpha = 1;
            cropButton.mouseEnabled = true;
        }
        else {
            cropButton.alpha = 0.5;
            cropButton.mouseEnabled = false;
        }

    }

    // -----------------------------
    // Flip and costume center buttons
    //------------------------------

    private function addFlipButtons():void {
        addChild(cropButton = makeTopButton(crop, 'crop'));
        addChild(flipHButton = makeTopButton(flipH, 'flipH'));
        addChild(flipVButton = makeTopButton(flipV, 'flipV'));
        cropButton.isMomentary = true;
        flipHButton.isMomentary = true;
        flipVButton.isMomentary = true;
        SimpleTooltips.add(cropButton, {text: 'Crop to selection', direction: 'bottom'});
        SimpleTooltips.add(flipHButton, {text: 'Flip left-right', direction: 'bottom'});
        SimpleTooltips.add(flipVButton, {text: 'Flip up-down', direction: 'bottom'});
        setCanCrop(false);
    }

    private function crop(ignore:*):void {
        var bitmapEditor:BitmapEdit = editor as BitmapEdit;
        if (bitmapEditor) {
            bitmapEditor.cropToSelection();
        }
    }

    private function flipH(ignore:*):void {
        editor.flipContent(false);
    }

    private function flipV(ignore:*):void {
        editor.flipContent(true);
    }

    private function addCenterButton():void {
        function setCostumeCenter(b:IconButton):void {
            editor.setToolMode('setCenter');
            b.lastEvent.stopPropagation();
        }

        centerButton = makeTopButton(setCostumeCenter, 'setCenter', true);
        SimpleTooltips.add(centerButton, {text: 'Set costume center', direction: 'bottom'});
        editor.registerToolButton('setCenter', centerButton);
        addChild(centerButton);
    }

    // -----------------------------
    // New costume/backdrop
    //------------------------------

    private function costumeFromComputer(ignore:* = null):void {
        importCostume(true)
    }

    private function costumeFromLibrary(ignore:* = null):void {
        importCostume(false)
    }

    private function importCostume(fromComputer:Boolean):void {
        function addCostume(costumeOrSprite:*):void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_COSTUME_IMAGE_PART, costumeOrSprite));
        }

        var t:String = isStage() ? 'backdrop' : 'costume';
        var lib:MediaLibrary = new MediaLibrary(t, addCostume);
        if (fromComputer) lib.importFromDisk();
        else lib.open();
    }

    private function paintCostume(ignore:* = null):void {
        addAndSelectCostume(WitCostume.emptyBitmapCostume('', isStage()));
    }

    protected function savePhotoAsCostume(photo:BitmapData):void {
        WitProgram.editorPart.imagesPart.closeCameraDialog();
        var obj:WitObj = WitProgram.viewedObj;
        if (obj == null) return;
        if (obj is WitStage) { // resize photo to stage
            var scale:Number = WitObj.STAGEW / photo.width;
            var m:Matrix = new Matrix();
            m.scale(scale, scale);
            var scaledPhoto:BitmapData = new BitmapData(WitObj.STAGEW, WitObj.STAGEH, true, 0);
            scaledPhoto.draw(photo, m);
            photo = scaledPhoto;
        }
        var c:WitCostume = new WitCostume(_('photo1'), photo);
        addAndSelectCostume(c);
        editor.getWorkArea().zoom();
    }

    private function costumeFromCamera(ignore:* = null):void {
        openCameraDialog(savePhotoAsCostume);
    }

    public function openCameraDialog(savePhoto:Function):void {
        closeCameraDialog();
        cameraDialog = new CameraDialog(savePhoto);
        cameraDialog.fixLayout();
        cameraDialog.x = (TempStatic.stagePart.stage.stageWidth - cameraDialog.width) / 2;
        cameraDialog.y = (TempStatic.stagePart.stage.stageHeight - cameraDialog.height) / 2;
        TempStatic.stagePart.stage.addChild(cameraDialog);
    }

    public function closeCameraDialog():void {
        if (cameraDialog) {
            cameraDialog.closeDialog();
            cameraDialog = null;
        }
    }

    public function addAndSelectCostume(c:WitCostume):void {
        var obj:WitObj = WitProgram.viewedObj;
        if (!c.baseLayerData) c.prepareToSave();
        if (!WitProgram.okayToAdd(c.baseLayerData.length)) return; // not enough room
        c.costumeName = obj.unusedCostumeName(c.costumeName);
        obj.costumes.push(c);
        obj.showCostume(obj.costumes.length - 1);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
        refresh();
    }

    // -----------------------------
    // Help tool
    //------------------------------

    public function handleTool(tool:String, evt:MouseEvent):void {
        var localP:Point = globalToLocal(new Point(stage.mouseX, stage.mouseY));
        if (tool == 'help') {
            if (localP.x > columnWidth) trace('paint');
            else trace('scratchUI');
        }
    }
}
}
