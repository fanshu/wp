package wp.editor.ui.parts {
import flash.display.Bitmap;
import flash.display.Shape;
import flash.display.Sprite;
import flash.text.TextField;

public class MyButton extends Sprite {
    public static const SIZE:uint = 22;

    private var color:uint;
    private var msk:Shape;
    private var summer:TextField;

    public function MyButton(icon:Bitmap, color:uint, summer:String) {
        mouseChildren = false;
        // icon
        icon.x = SIZE - icon.width >> 1;
        addChild(icon);
        // color
        this.color = color;
        // summer
        msk = new Shape();
        addChild(msk);
        this.summer = new TextField();
        this.summer.text = summer;
        this.summer.textColor = 0xffffff;
        this.summer.width = 100;
        addChildAt(this.summer, 0);

        // fire
        setWitdh(SIZE);
    }

    public function setWitdh(w:Number):void {
        // color
        graphics.clear();
        graphics.beginFill(color);
        graphics.drawRoundRect(0, 0, w, SIZE, 5, 5);
        graphics.endFill();
        // summer
        msk.graphics.clear();
        msk.graphics.beginFill(0, 1);
        msk.graphics.drawRect(0, 0, w - SIZE, SIZE);
        msk.graphics.endFill();
        msk.x = SIZE;
        summer.x = SIZE + 2;
        summer.mask = msk;
    }
}
}
