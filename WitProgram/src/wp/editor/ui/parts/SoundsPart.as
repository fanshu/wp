package wp.editor.ui.parts {
import flash.display.*;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.geom.*;
import flash.text.*;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.Resources;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.UIPartUtil;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.sound.WAVFile;
import wp.editor.soundedit.SoundEditor;
import wp.editor.ui.media.*;
import wp.editor.uiwidgets.*;
import wp.editor.wit.*;

public class SoundsPart extends UIPart {
    private static const _:Function = gettext;

    public var editor:SoundEditor;
    public var currentIndex:int;

    private const columnWidth:int = 106;

    private var shape:Shape;
    private var listFrame:ScrollFrame;
    private var nameField:EditableLabel;
    private var undoButton:IconButton;
    private var redoButton:IconButton;

    private var newSoundLabel:TextField;
    private var libraryButton:IconButton;
    private var importButton:IconButton;
    private var recordButton:IconButton;

    public function SoundsPart() {
        addChild(shape = new Shape());

        addChild(newSoundLabel = UIPartUtil.makeLabel('', new TextFormat(CSS.font, 12, CSS.textColor, true)));
        addNewSoundButtons();

        addListFrame();
        addChild(nameField = new EditableLabel(nameChanged));
        addChild(editor = new SoundEditor(this));
        addUndoButtons();
        addEventListener(Event.ADDED_TO_STAGE, function (evt:Event):void {
            removeEventListener(Event.ADDED_TO_STAGE, arguments.callee);
            stage.addEventListener(KeyboardEvent.KEY_DOWN, editor.keyDown);
            addEventListener(Event.REMOVED_FROM_STAGE, function (evt:Event):void {
                removeEventListener(Event.REMOVED_FROM_STAGE, arguments.callee);
                stage.removeEventListener(KeyboardEvent.KEY_DOWN, editor.keyDown);
            });
        });
        updateTranslation();
    }

    public function updateTranslation():void {
        newSoundLabel.text = _('New sound:');
        editor.updateTranslation();
        SimpleTooltips.add(libraryButton, {text: 'Choose sound from library', direction: 'bottom'});
        SimpleTooltips.add(recordButton, {text: 'Record new sound', direction: 'bottom'});
        SimpleTooltips.add(importButton, {text: 'Upload sound from file', direction: 'bottom'});
        fixlayout();
    }

    public function selectSound(snd:WitSound):void {
        var obj:WitObj = WitProgram.viewedObj;
        if (obj == null) return;
        if (obj.sounds.length == 0) return;
        currentIndex = 0;
        for (var i:int = 0; i < obj.sounds.length; i++) {
            if ((obj.sounds[i] as WitSound) == snd) currentIndex = i;
        }
        (listFrame.contents as MediaPane).updateSelection();
        refresh(false);
    }

    public function refresh(refreshListContents:Boolean = true):void {
        if (refreshListContents) {
            var contents:MediaPane = listFrame.contents as MediaPane;
            contents.refresh();
        }

        nameField.setContents('');
        var viewedObj:WitObj = WitProgram.viewedObj;
        if (viewedObj.sounds.length < 1) {
            nameField.visible = false;
            editor.visible = false;
            undoButton.visible = false;
            redoButton.visible = false;
            return;
        } else {
            nameField.visible = true;
            editor.visible = true;
            undoButton.visible = true;
            redoButton.visible = true;
            refreshUndoButtons();
        }

        editor.waveform.stopAll();
        var snd:WitSound = viewedObj.sounds[currentIndex];
        if (snd) {
            nameField.setContents(snd.soundName);
            editor.waveform.editSound(snd);
        }
    }

    public function setWidthHeight(w:int, h:int):void {
        this.w = w;
        this.h = h;
        var g:Graphics = shape.graphics;
        g.clear();

        g.lineStyle(0.5, CSS.borderColor, 1, true);
        g.beginFill(CSS.backgroundColor_default);
        g.drawRect(0, 0, w, h);
        g.endFill();

        g.lineStyle(0.5, CSS.borderColor, 1, true);
        g.beginFill(CSS.backgroundColor_default);
        g.drawRect(columnWidth + 1, 5, w - columnWidth - 6, h - 10);
        g.endFill();

        fixlayout();
    }

    private function fixlayout():void {
        newSoundLabel.x = 7;
        newSoundLabel.y = 7;

        listFrame.x = 1;
        listFrame.y = 58;
        listFrame.setWidthHeight(columnWidth, h - listFrame.y);

        var contentsX:int = columnWidth + 13;
        var contentsW:int = w - contentsX - 15;

        nameField.setWidth(Math.min(135, contentsW));
        nameField.x = contentsX;
        nameField.y = 15;

        // undo buttons
        undoButton.x = nameField.x + nameField.width + 30;
        redoButton.x = undoButton.right() + 8;
        undoButton.y = redoButton.y = nameField.y - 2;

        editor.setWidthHeight(contentsW, 200);
        editor.x = contentsX;
        editor.y = 50;
    }

    private function addNewSoundButtons():void {
        var left:int = 16;
        var buttonY:int = 31;
        addChild(libraryButton = makeButton(soundFromLibrary, 'soundlibrary', left, buttonY));
        addChild(recordButton = makeButton(recordSound, 'record', left + 34, buttonY));
        addChild(importButton = makeButton(soundFromComputer, 'import', left + 61, buttonY - 1));
    }

    private function makeButton(fcn:Function, iconName:String, x:int, y:int):IconButton {
        var b:IconButton = new IconButton(fcn, iconName);
        b.isMomentary = true;
        b.x = x;
        b.y = y;
        return b;
    }

    private function addListFrame():void {
        listFrame = new ScrollFrame();
        listFrame.setContents(new MediaPane('sounds'));
        listFrame.contents.color = CSS.backgroundColor_default;
        listFrame.allowHorizontalScrollbar = false;
        addChild(listFrame);
    }

    // -----------------------------
    // Sound Name
    //------------------------------

    private function nameChanged():void {
        currentIndex = Math.min(currentIndex, WitProgram.viewedObj.sounds.length - 1);
        var current:WitSound = WitProgram.viewedObj.sounds[currentIndex] as WitSound;
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.RENAME_SOUND,
                {
                    s: current,
                    newName: nameField.contents
                },
                true));
        nameField.setContents(current.soundName);
        (listFrame.contents as MediaPane).refresh();
    }

    // -----------------------------
    // Undo/Redo
    //------------------------------

    private function addUndoButtons():void {
        addChild(undoButton = new IconButton(editor.waveform.undo, makeButtonImg('undo', true), makeButtonImg('undo', false)));
        addChild(redoButton = new IconButton(editor.waveform.redo, makeButtonImg('redo', true), makeButtonImg('redo', false)));
        undoButton.isMomentary = true;
        redoButton.isMomentary = true;
    }

    public function refreshUndoButtons():void {
        undoButton.setDisabled(!editor.waveform.canUndo(), 0.5);
        redoButton.setDisabled(!editor.waveform.canRedo(), 0.5);
    }

    public static function makeButtonImg(iconName:String, isOn:Boolean, buttonSize:Point = null):Sprite {
        var icon:Bitmap = Resources.createBmp(iconName + (isOn ? 'On' : 'Off'));
        var buttonW:int = Math.max(icon.width, buttonSize ? buttonSize.x : 24);
        var buttonH:int = Math.max(icon.height, buttonSize ? buttonSize.y : 24);

        var img:Sprite = new Sprite();
        var g:Graphics = img.graphics;
        g.clear();
        g.lineStyle(0.5, CSS.borderColor, 1, true);
        if (isOn) {
            g.beginFill(CSS.overColor);
        } else {
            var m:Matrix = new Matrix();
            m.createGradientBox(24, 24, Math.PI / 2, 0, 0);
            g.beginGradientFill(GradientType.LINEAR, CSS.buttonColors, [100, 100], [0x00, 0xFF], m);
        }
        g.drawRoundRect(0, 0, buttonW, buttonH, 8);
        g.endFill();

        icon.x = (buttonW - icon.width) / 2;
        icon.y = (buttonH - icon.height) / 2;
        img.addChild(icon);
        return img;
    }

    // -----------------------------
    // Menu
    //------------------------------

    private function showNewSoundMenu(b:IconButton):void {
        var m:Menu = new Menu(null, 'New Sound', 0xB0B0B0, 28);
        m.minWidth = 90;
        m.addItem('Library', soundFromLibrary);
        m.addItem('Record', recordSound);
        m.addItem('Import', soundFromComputer);
        var p:Point = b.localToGlobal(new Point(0, 0));
        m.showOnStage(stage, p.x - 1, p.y + b.height - 2);
    }

    public function soundFromLibrary(b:* = null):void {
        new MediaLibrary("sound", addSound).open();
    }

    public function soundFromComputer(b:* = null):void {
        new MediaLibrary("sound", addSound).importFromDisk();
    }

    private function addSound(snd:WitSound, targetObj:WitObj = null):void {
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_SOUND, [snd, targetObj], true));
    }

    public function recordSound(b:* = null):void {
        var newName:String = WitProgram.viewedObj.unusedSoundName(_('recording1'));
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_SOUND, [new WitSound(newName, WAVFile.empty()), null], true));
    }
}
}
