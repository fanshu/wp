package wp.editor.ui.parts {
import com.greensock.TweenLite;

import flash.display.*;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.*;
import flash.utils.*;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.TempStatic;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.SpriteThumbnail;
import wp.editor.ui.media.*;
import wp.editor.uiwidgets.*;
import wp.editor.wit.*;

public class LibraryPart extends UIPart {
    private static const _:Function = gettext;

    private const bgColor:int = CSS.backgroundColor_default;
    private const updateInterval:int = 200; // msecs between thumbnail updates

    private var lastUpdate:uint; // time of last thumbnail update

    public var expand:Boolean = true;

    private var shape:Shape;

    private var stageThumbnail:SpriteThumbnail;
    private var spritesFrame:ScrollFrame;
    protected var spritesPane:ScrollFrameContents;
    private var spriteDetails:SpriteInfoPart;

    private var libBtns:LibraryButtonGroup;

    private var sliderButton:IconButton;

    private var videoLabel:TextField;

    public function LibraryPart() {
        shape = new Shape();
        addChild(shape);

        addStageArea();
        addSpritesArea();

        spriteDetails = new SpriteInfoPart();
        addChild(spriteDetails);
        spriteDetails.visible = false;

        addChild(libBtns=new LibraryButtonGroup());

        addChild(sliderButton = makeButton(null, 'slider'));

        updateTranslation();

        // slider
        sliderButton.addEventListener(MouseEvent.MOUSE_DOWN, sliderDown);
        expand = false;
        addEventListener(Event.ADDED_TO_STAGE, function (evt:Event):void {
            removeEventListener(Event.ADDED_TO_STAGE, arguments.callee);
            TweenLite.delayedCall(.6, sliderUp);
        });
    }

    public function updateTranslation():void {
        if (videoLabel) videoLabel.text = _('Video on:');
        if (stageThumbnail) stageThumbnail.updateThumbnail(true);
        spriteDetails.updateTranslation();

        fixLayout();
    }

    public function setWidthHeight(w:int, h:int):void {
        if (w==this.w && h==this.h) return;

        this.w = w;
        this.h = h;
        fixLayout();
        if (WitProgram.viewedObj) refresh(); // refresh, but not during initialization
    }

    public function fixLayout():void {
        var toggleStagePart:Boolean = TempStatic.stagePart.parent == this;
        var nextY:Number = 0;

        // stagePart
        if (toggleStagePart) {
            var stagePart:StagePart = TempStatic.stagePart;
            var scale:Number = w / WitObj.STAGEW;
            stagePart.setWidthHeight(WitObj.STAGEW * scale, WitObj.STAGEH * scale);
            stagePart.x = 0;
            stagePart.y = 0;
            nextY = stagePart.h;
        }

        // bg
        var g:Graphics = shape.graphics;
        g.clear();
        g.beginFill(CSS.backgroundColor_default);
        g.drawRect(1, nextY, w, h - CSS.titleBarH - 1);
        g.endFill();

        // buttons
        libBtns.x = w - 300;
        libBtns.y = nextY + 4;

        // spritesFrame
        nextY += CSS.titleBarH + 1;
        spritesFrame.x = 2;
        spritesFrame.y = nextY;
        spritesFrame.allowHorizontalScrollbar = false;
        spritesFrame.setWidthHeight(w - spritesFrame.x, h - spritesFrame.y);

        // spriteDetails
        spriteDetails.x = spritesFrame.x;
        spriteDetails.y = spritesFrame.y;
        spriteDetails.setWidthHeight(w - spritesFrame.x, h - spritesFrame.y);

        // sliderButton
        sliderButton.scaleX = -1;
        sliderButton.x = sliderButton.width - 8;
        sliderButton.y = h - 20 - sliderButton.height;

        if (stage) {
            x = expand ? stage.stageWidth - w : stage.stageWidth - 5;
        }
    }

    public function highlight(highlightList:Array):void {
        // Highlight each ScratchObject in the given list to show,
        // for example, broadcast senders or receivers. Passing an
        // empty list to this function clears all highlights.
        for each (var tn:SpriteThumbnail in allThumbnails()) {
            tn.showHighlight(highlightList.indexOf(tn.targetObj) >= 0);
        }
    }

    public function refresh():void {
        // Create thumbnails for all sprites. This function is called
        // after loading project, or adding or deleting a sprite.
        if (WitProgram.viewedObj && (WitProgram.viewedObj is WitStage)) showSpriteDetails(false);
        if (spriteDetails.visible) spriteDetails.refresh();
        spritesPane.clear(false);
        var sortedSprites:Array = TempStatic.witStage.sprites();
        sortedSprites.sort(
                function (spr1:WitSprite, spr2:WitSprite):int {
                    return spr1.indexInLibrary - spr2.indexInLibrary
                });
        const inset:int = 2;
        var rightEdge:int = w - spritesFrame.x;
        var nextX:int = inset, nextY:int = inset;
        if (stageThumbnail) {
            stageThumbnail.setTarget(TempStatic.witStage);
            stageThumbnail.x = nextX;
            stageThumbnail.y = nextY;
            nextX += stageThumbnail.width;
            spritesPane.addChild(stageThumbnail);
        }
        var index:int = 1;
        for each (var spr:WitSprite in sortedSprites) {
            spr.indexInLibrary = index++; // renumber to ensure unique indices
            var tn:SpriteThumbnail = new SpriteThumbnail(spr);
            tn.x = nextX;
            tn.y = nextY;
            spritesPane.addChild(tn);
            nextX += tn.width;
            if ((nextX + tn.width) > rightEdge) { // start new line
                nextX = inset;
                nextY += tn.height;
            }
        }
        spritesPane.updateSize();
        scrollToSelectedSprite();
        step();
    }

    private function scrollToSelectedSprite():void {
        var viewedObj:WitObj = WitProgram.viewedObj;
        var sel:SpriteThumbnail;
        for (var i:int = 0; i < spritesPane.numChildren; i++) {
            var tn:SpriteThumbnail = spritesPane.getChildAt(i) as SpriteThumbnail;
            if (tn && (tn.targetObj == viewedObj)) sel = tn;
        }
        if (sel) {
            var selTop:int = sel.y + spritesPane.y - 1;
            var selBottom:int = selTop + sel.height;
            spritesPane.y -= Math.max(0, selBottom - spritesFrame.visibleH());
            spritesPane.y -= Math.min(0, selTop);
            spritesFrame.updateScrollbars();
        }
    }

    public function showSpriteDetails(flag:Boolean):void {
        spriteDetails.visible = flag;
        if (spriteDetails.visible) spriteDetails.refresh();
    }

    public function step():void {
        // Update thumbnails and sprite details.
        var viewedObj:WitObj = WitProgram.viewedObj;
        var updateThumbnails:Boolean = ((getTimer() - lastUpdate) > updateInterval);
        for each (var tn:SpriteThumbnail in allThumbnails()) {
            if (updateThumbnails) tn.updateThumbnail();
            tn.select(tn.targetObj == viewedObj);
        }
        if (updateThumbnails) lastUpdate = getTimer();
        if (spriteDetails.visible) spriteDetails.step();
    }

    private function addStageArea():void {
        stageThumbnail = new SpriteThumbnail(TempStatic.witStage);
    }

    private function addSpritesArea():void {
        spritesPane = new ScrollFrameContents();
        spritesPane.color = bgColor;
        spritesPane.hExtra = spritesPane.vExtra = 0;
        spritesFrame = new ScrollFrame();
        spritesFrame.setContents(spritesPane);
        addChild(spritesFrame);
    }

    private function makeButton(fcn:Function, iconName:String):IconButton {
        var b:IconButton = new IconButton(fcn, iconName);
        b.isMomentary = true;
        return b;
    }

    // -----------------------------
    // New Sprite Operations
    //------------------------------

    // -----------------------------
    // Dropping
    //------------------------------

    public function handleDrop(obj:*):Boolean {
        return false;
    }

    protected function changeThumbnailOrder(dropped:WitSprite, dropX:int, dropY:int):void {
        // Update the order of library items based on the drop point. Update the
        // indexInLibrary field of all sprites, then refresh the library.
        dropped.indexInLibrary = -1;
        var inserted:Boolean = false;
        var nextIndex:int = 1;
        for (var i:int = 0; i < spritesPane.numChildren; i++) {
            var th:SpriteThumbnail = spritesPane.getChildAt(i) as SpriteThumbnail;
            var spr:WitSprite = th.targetObj as WitSprite;
            if (!inserted) {
                if (dropY < (th.y - (th.height / 2))) { // insert before this row
                    dropped.indexInLibrary = nextIndex++;
                    inserted = true;
                } else if (dropY < (th.y + (th.height / 2))) {
                    if (dropX < th.x) { // insert before the current thumbnail
                        dropped.indexInLibrary = nextIndex++;
                        inserted = true;
                    }
                }
            }
            if (spr != dropped) spr.indexInLibrary = nextIndex++;
        }
        if (dropped.indexInLibrary < 0) dropped.indexInLibrary = nextIndex++;
        refresh();
    }

    // -----------------------------
    // slider
    //------------------------------
    private var downX:Number;
    private var dragged:Boolean;

    private function sliderDown(evt:MouseEvent = null):void {
        dragged = false;
        stage.addEventListener(MouseEvent.MOUSE_UP, sliderUp);
        if (expand) {
            downX = mouseX;
            stage.addEventListener(MouseEvent.MOUSE_MOVE, sliderMove);
        }
    }

    private function sliderMove(evt:MouseEvent):void {
        dragged = true;
        var resize:Number = downX - mouseX;
        resize += w;
        if (resize < 320) resize = 320;
        if (resize > stage.stageWidth / 2) resize = stage.stageWidth / 2;
        if (resize > 550) resize = 550;
        if (resize == w) return;
        setWidthHeight(resize, h);
        x = stage.stageWidth - w;
        WitProgram.editorPart.fixLayout();
    }

    private function sliderUp(evt:MouseEvent = null):void {
        if (!stage) return;

        stage.removeEventListener(MouseEvent.MOUSE_MOVE, sliderMove);
        stage.removeEventListener(MouseEvent.MOUSE_UP, sliderUp);

        if (!dragged) {
            expand = !expand;
            var toX:Number = expand ? stage.stageWidth - w : stage.stageWidth - 5;
            TweenLite.to(this, .1, {
                x: toX, onUpdate: function ():void {
                    if (WitProgram.editorPart) WitProgram.editorPart.fixLayout();
                }
            });
        }
    }

    // -----------------------------
    // Misc
    //------------------------------

    private function allThumbnails():Array {
        // Return a list containing all thumbnails.
        var result:Array = [];
        for (var i:int = 0; i < spritesPane.numChildren; i++) {
            result.push(spritesPane.getChildAt(i));
        }
        return result;
    }
}
}
