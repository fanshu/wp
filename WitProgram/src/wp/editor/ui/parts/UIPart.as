// This is the superclass for the main parts of the WitProgram UI.
// It holds drawing style constants and code shared by all parts.
// Subclasses often implement one or more of the following:
//
//		refresh() - update this part after a change (e.g. changing the selected object)
//		step() - do background tasks

package wp.editor.ui.parts {
import flash.display.Sprite;

public class UIPart extends Sprite {
    public var w:int, h:int;
}
}
