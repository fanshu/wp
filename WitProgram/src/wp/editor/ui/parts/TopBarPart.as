package wp.editor.ui.parts {
import com.adobe.utils.StringUtil;

import flash.display.*;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.net.FileReference;
import flash.net.SharedObject;
import flash.text.*;
import flash.utils.ByteArray;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.ProjectIO;
import util.Resources;
import util.TempStatic;
import util.TempStatic;
import util.UIPartUtil;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.translation.Translator;
import wp.editor.uiwidgets.*;
import wp.editor.wit.WitStage;

public class TopBarPart extends UIPart {
    private static const _:Function = gettext;

    private var shape:Shape;
    private var logoButton:IconButton;
    private var languageButton:IconButton;

    private var fileMenu:IconButton;
    private var editMenu:IconButton;

    private var _saveNeeded:Boolean = false;

    private var deviceID:EditableLabel;

    private var copyTool:IconButton;
    private var cutTool:IconButton;
    private var growTool:IconButton;
    private var shrinkTool:IconButton;
    private var helpTool:IconButton;
    private var toolOnMouseDown:String;

    private var offlineNotice:TextField;

    private var loadExperimentalButton:Button;
    private var exportButton:Button;
    private var extensionLabel:TextField;

    public function TopBarPart() {
        // size shape bg
        addChild(shape = new Shape());
        // language button
        addChild(languageButton = new IconButton(
                function (b:IconButton):void {
                    var so:SharedObject = SharedObject.getLocal('WitProgram');
                    if (!so.data.langs) return; // empty language list
                    var m:Menu = new Menu(
                            function (lang:String):void {
                                Translator.setLanguage(lang);
                            },
                            'Language', CSS.titleBarColors[0], 28
                    );
                    for (var id:String in so.data.langs) {
                        m.addItem(so.data.langs[id], id);
                    }
                    m.showOnStage(stage, b.x, y + height - 1);
                },
                'languageButton'
        ));

        addChild(deviceID = new EditableLabel(function ():void {
            if (deviceID.visible) TempStatic.usbID = StringUtil.trim(deviceID.contents);
        }));
        deviceID.setWidth(150);
        deviceID.visible = false;
        languageButton.isMomentary = true;
        // text buttons
        addTextButtons();
        // tool buttons
        addToolButtons();
        refresh();
    }

    private function removeTextButtons():void {
        if (fileMenu.parent) {
            removeChild(fileMenu);
            removeChild(editMenu);
        }
    }

    public function updateTranslation():void {
        removeTextButtons();
        addTextButtons();
        if (offlineNotice) offlineNotice.text = _('Offline Editor');
        refresh();
    }

    public function setWidthHeight(w:int, h:int):void {
        this.w = w;
        this.h = h;
        var g:Graphics = shape.graphics;
        g.clear();
        UIPartUtil.drawTopBar(g, CSS.titleBarColors, UIPartUtil.getTopBarPath(w, h), w, h, CSS.borderColor);
        g.drawRect(0, 0, w, h);
        g.endFill();
        fixLayout();
    }

    private function fixLogoLayout():int {
        var nextX:int = 9;
        if (logoButton) {
            logoButton.x = nextX;
            logoButton.y = 5;
            nextX += logoButton.width + buttonSpace;
        }
        return nextX;
    }

    private const buttonSpace:int = 12;

    private function fixLayout():void {
        const buttonY:int = 10;

        var nextX:int = fixLogoLayout();

        languageButton.x = nextX;
        languageButton.y = buttonY - 1;
        nextX += languageButton.width + buttonSpace;

        // new/more/tips buttons
        fileMenu.x = nextX;
        fileMenu.y = buttonY;
        nextX += fileMenu.width + buttonSpace;

        editMenu.x = nextX;
        editMenu.y = buttonY;
        nextX += editMenu.width + buttonSpace;

        deviceID.x = nextX;
        deviceID.y = buttonY;

        // cursor tool buttons
        var space:int = 5;
        copyTool.x = 290;
        cutTool.x = copyTool.right() + space;
        growTool.x = cutTool.right() + space;
        shrinkTool.x = growTool.right() + space;
        helpTool.x = shrinkTool.right() + space;
        copyTool.y = cutTool.y = shrinkTool.y = growTool.y = helpTool.y = buttonY - 1;

        if (offlineNotice) {
            offlineNotice.x = w - offlineNotice.width - 5;
            offlineNotice.y = 5;
        }

        // From here down, nextX is the next item's right edge and decreases after each item
        nextX = w - 5;

        if (loadExperimentalButton) {
            loadExperimentalButton.x = nextX - loadExperimentalButton.width;
            loadExperimentalButton.y = h + 5;
            // Don't upload nextX: we overlap with other items. At most one set should show at a time.
        }

        if (exportButton) {
            exportButton.x = nextX - exportButton.width;
            exportButton.y = h + 5;
            nextX = exportButton.x - 5;
        }

        if (extensionLabel) {
            extensionLabel.x = nextX - extensionLabel.width;
            extensionLabel.y = h + 5;
            nextX = extensionLabel.x - 5;
        }
    }

    public function refresh():void {
        helpTool.visible = false;
        fixLayout();
    }

    private function addTextButtons():void {
        addChild(fileMenu = UIPartUtil.makeMenuButton('File', showFileMenu, true));
        addChild(editMenu = UIPartUtil.makeMenuButton('Edit', showEditMenu, true));
    }

    private function showFileMenu(b:*):void {
        var m:Menu = new Menu(null, 'Files', CSS.titleBarColors[0], 28);
        m.addItem(
                'New', function (callback:Function = null):void {
                    function clearProject():void {
                        /* startNewProj */
                        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SAVE_FOR_REVERT, null, true));
                        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.INSTALL_PROJECT, new WitStage(), true));
                        /* startNewProj */
                        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_PROJECT_NAME, 'Untitled', true));
                        WitProgram.topBarPart.refresh();
                        TempStatic.stagePart.refresh();
                        if (callback != null) callback();
                    }

                    saveProjectAndThen(clearProject);
                });

        /* Derived class will handle this */
        m.addItem('Load Project', function ():void {
            function selectProjectFile():void {
                TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SELECT_PROJECT_FILE, null, true));
            }
            if (TempStatic.witStage.isEmpty()) {
                selectProjectFile();
            } else {
                DialogBox.confirm('替换当前项目？', stage, selectProjectFile);
//                DialogBox.confirm('Replace contents of the current project?', stage, selectProjectFile);
            }
        });
        m.addItem('Save Project', exportProjectToFile);
        if (CONFIG::editor) {
            m.addItem('Export swf', exportProjectToSWF);
            m.addItem('Export exe', exportProjectToEXE);
        }
        if (TempStatic.revertUndo != null) {
            m.addLine();
            m.addItem('Undo Revert', function ():void {
                if (!TempStatic.revertUndo) return;
                TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.INSTALL_PROJECT_FROM_DATA,
                        {
                            data: TempStatic.revertUndo,
                            saveForRevert: false
                        }, true
                ));
                TempStatic.revertUndo = null;
            });
        } else if (TempStatic.originalProj != null) {
            m.addLine();
            m.addItem('Revert', function ():void {
                function preDoRevert():void {
                    TempStatic.revertUndo = new ProjectIO().encodeProjectAsZipFile(TempStatic.witStage);
                    TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.INSTALL_PROJECT_FROM_DATA,
                            {
                                data: TempStatic.originalProj,
                                saveForRevert: false
                            }, true
                    ));
                }

                if (!TempStatic.originalProj) return;
                DialogBox.confirm('Throw away all changes since opening this project?', TempStatic.stagePart.stage, preDoRevert);
            });
        }
        m.showOnStage(stage, b.x, WitProgram.topBarPart.y + WitProgram.topBarPart.height - 1);
    }

    private function saveProjectAndThen(postSaveAction:Function = null):void {
        // Give the user a chance to save their project, if needed, then call postSaveAction.
        function doNothing():void {
        }

        function cancel():void {
            d.cancel();
        }

        function proceedWithoutSaving():void {
            d.cancel();
            postSaveAction()
        }

        function save():void {
            d.cancel();
            exportProjectToFile(postSaveAction);
        }

        if (postSaveAction == null) postSaveAction = doNothing;
        if (!_saveNeeded) {
            postSaveAction();
            return;
        }
        var d:DialogBox = new DialogBox();
        d.addTitle('Save project?');
        d.addButton('Save', save);
        d.addButton('Don\'t save', proceedWithoutSaving);
        d.addButton('Cancel', cancel);
        d.showOnStage(stage);
    }

    private function showEditMenu(b:*):void {
        var m:Menu = new Menu(null, 'More', CSS.titleBarColors[0], 28);
        m.addItem('Undelete', function ():void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.UNDELETE, null, true));
        }, TempStatic.runtime.canUndelete());
        m.addItem('Turbo mode', toggleTurboMode, true, TempStatic.interp.turboMode);
        m.showOnStage(stage, b.x, WitProgram.topBarPart.y + WitProgram.topBarPart.height - 1);
    }

    private function toggleTurboMode():void {
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.TOGGLE_TURBO_MODE));
    }


    public function exportProjectToFile(saveCallback:Function = null):void {
        function squeakSoundsConverted():void {
            WitProgram.scriptsPane.saveScripts(false);
            var projectType:String = TempStatic.extensionManager.hasExperimentalExtensions() ? '.sbx' : '.wit';
            var defaultName:String = StringUtil.trim(TempStatic.projectName);
            if (defaultName.length == 0) defaultName = 'project';
            defaultName = defaultName.replace('.wit', projectType);
            var zipData:ByteArray = projIO.encodeProjectAsZipFile(TempStatic.witStage);
            var file:FileReference = new FileReference();
            file.addEventListener(Event.COMPLETE, fileSaved);
            file.save(zipData, TempStatic.fixFileName(defaultName));
        }

        function fileSaved(e:Event):void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_PROJECT_NAME, e.target.name, true));
            if (saveCallback != null) saveCallback();
        }

        if (TempStatic.loadInProgress) return;
        var projIO:ProjectIO = new ProjectIO();
        projIO.convertSqueakSounds(TempStatic.witStage, squeakSoundsConverted);
    }

    private function exportProjectToSWF():void {
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.EXPORT_PRODUCT, 'swf'));
    }

    private function exportProjectToEXE():void {
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.EXPORT_PRODUCT, 'exe'));
    }

    private function addToolButtons():void {
        function selectTool(b:IconButton):void {
            var newTool:String = '';
            if (b == copyTool) newTool = 'copy';
            if (b == cutTool) newTool = 'cut';
            if (b == growTool) newTool = 'grow';
            if (b == shrinkTool) newTool = 'shrink';
            if (b == helpTool) newTool = 'help';
            if (newTool == toolOnMouseDown) {
                clearToolButtons();
                CursorTool.setTool(null);
            } else {
                clearToolButtonsExcept(b);
                CursorTool.setTool(newTool);
            }
        }

        addChild(copyTool = makeToolButton('copyTool', selectTool));
        addChild(cutTool = makeToolButton('cutTool', selectTool));
        addChild(growTool = makeToolButton('growTool', selectTool));
        addChild(shrinkTool = makeToolButton('shrinkTool', selectTool));
        addChild(helpTool = makeToolButton('helpTool', selectTool));

        SimpleTooltips.add(copyTool, {text: 'Duplicate', direction: 'bottom'});
        SimpleTooltips.add(cutTool, {text: 'Delete', direction: 'bottom'});
        SimpleTooltips.add(growTool, {text: 'Grow', direction: 'bottom'});
        SimpleTooltips.add(shrinkTool, {text: 'Shrink', direction: 'bottom'});
        SimpleTooltips.add(helpTool, {text: 'Block help', direction: 'bottom'});
    }

    public function clearToolButtons():void {
        CursorTool.setTool(null);
        clearToolButtonsExcept(null)
    }

    public function set saveNeeded(bool:Boolean):void {
        _saveNeeded = bool;
        if (bool) TempStatic.revertUndo = null;
    }

    private function clearToolButtonsExcept(activeButton:IconButton):void {
        for each (var b:IconButton in [copyTool, cutTool, growTool, shrinkTool, helpTool]) {
            if (b != activeButton) b.turnOff();
        }
    }

    private function makeToolButton(iconName:String, fcn:Function):IconButton {
        function mouseDown(evt:MouseEvent):void {
            toolOnMouseDown = CursorTool.tool
        }

        var onImage:Sprite = toolButtonImage(iconName, CSS.overColor, 1);
        var offImage:Sprite = toolButtonImage(iconName, 0, 0);
        var b:IconButton = new IconButton(fcn, onImage, offImage);
        b.actOnMouseUp();
        b.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown); // capture tool on mouse down to support deselecting
        return b;
    }

    private function toolButtonImage(iconName:String, color:int, alpha:Number):Sprite {
        const w:int = 23;
        const h:int = 24;
        var img:Bitmap;
        var result:Sprite = new Sprite();
        var g:Graphics = result.graphics;
        g.clear();
        g.beginFill(color, alpha);
        g.drawRoundRect(0, 0, w, h, 8, 8);
        g.endFill();
        result.addChild(img = Resources.createBmp(iconName));
        img.x = Math.floor((w - img.width) / 2);
        img.y = Math.floor((h - img.height) / 2);
        return result;
    }
}
}
