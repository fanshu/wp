package wp.editor.ui.parts {
import com.greensock.TweenLite;

import flash.display.*;
import flash.events.*;
import flash.geom.Rectangle;
import flash.text.*;
import flash.ui.Keyboard;

import jijunzeng.MyEvent;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;
import util.UIPartUtil;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.uiwidgets.*;
import wp.editor.wit.*;

public class StagePart extends UIPart {
    private var lastX:int, lastY:int;

    private var topBarHeight:int = 39;

    private var outline:Shape;
    private var runButton:IconButton;
    private var stopButton:IconButton;
    private var fullscreenButton:IconButton;

    // x-y readouts
    private var readouts:Sprite; // readouts that appear below the stage
    private var xLabel:TextField;
    private var xReadout:TextField;
    private var yLabel:TextField;
    private var yReadout:TextField;

    private var logo:IconButton;

    public function StagePart() {
        outline = new Shape();
        addChild(outline);

        /* start button */
        function startAll(b:IconButton):void {
            onStartButton(b.lastEvent)
        }

        runButton = new IconButton(startAll, 'greenflag');
        runButton.actOnMouseUp();
        addChild(runButton);
        /* stop button */
        function stopAll():void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.RUNTIME_STOP_ALL, null, true));
        }

        stopButton = new IconButton(stopAll, 'stop');
        addChild(stopButton);
        /* toggle fullscreen button */
        function toggleFullscreen(b:IconButton):void {
            TempStatic.witStage.setPresentationMode(b.isOn());
            drawOutline();
        }

        fullscreenButton = new IconButton(toggleFullscreen, 'fullscreen');
        fullscreenButton.disableMouseover();
        fullscreenButton.actOnMouseUp();
        addChild(fullscreenButton);
        /* logo */
        logo = new IconButton(null, 'logo');
        logo.disableMouseover();
        addChild(logo);
        /* xy readouts */
        const readoutLabelFormat:TextFormat = new TextFormat(CSS.font, 12, CSS.white, true);
        const readoutFormat:TextFormat = new TextFormat(CSS.font, 10, CSS.white);
        readouts = new Sprite();
        addChild(readouts);
        xLabel = UIPartUtil.makeLabel('x:', readoutLabelFormat);
        readouts.addChild(xLabel);
        xReadout = UIPartUtil.makeLabel('-888', readoutFormat);
        readouts.addChild(xReadout);
        yLabel = UIPartUtil.makeLabel('y:', readoutLabelFormat);
        readouts.addChild(yLabel);
        yReadout = UIPartUtil.makeLabel('-888', readoutFormat);
        readouts.addChild(yReadout);

        // onStage
        addEventListener(Event.ADDED_TO_STAGE, function (evt:Event):void {
            removeEventListener(Event.ADDED_TO_STAGE, arguments.callee);
            stage.addEventListener(KeyboardEvent.KEY_DOWN,
                    function (evt:KeyboardEvent):void {
                        if ((evt.charCode == Keyboard.ESCAPE) && (fullscreenButton.visible && fullscreenButton.isOn())) {
                            // Escape exists presentation mode.
                            TempStatic.witStage.setPresentationMode(false);
                            fullscreenButton.setOn(false);
                            drawOutline();
                            refresh();
                        } else if (evt.keyCode == Keyboard.ENTER && !stage.focus) {
                            // Handle enter key
                            onStartButton(null);
                            evt.preventDefault();
                            evt.stopImmediatePropagation();
                        }
                    }
            );
        });
    }

    public function setWidthHeight(w:int, h:int):void {
        this.w = w;
        this.h = h;
        topBarHeight = (TempStatic.editMode ? 26 : 39);
        drawOutline();
        fixLayout();
    }

    public function installStage(witStage:WitStage):void {
        var child:DisplayObject;
        for (var i:int = numChildren - 1; i >= 0; i--) {
            child = getChildAt(i);
            if (child is WitStage) removeChild(child);
        }
        topBarHeight = (TempStatic.editMode ? 26 : 39);
        addChild(witStage);
        TempStatic.witStage = witStage;
        TweenLite.delayedCall(.2, function ():void {
            witStage.width = w;
            witStage.height = h - topBarHeight;
            witStage.y = topBarHeight;
        });
    }

    public function refresh():void {
        readouts.visible = TempStatic.editMode;
        if (TempStatic.editMode) {
            fullscreenButton.setOn(false);
        }
    }

    // -----------------------------
    // Layout
    //------------------------------

    private function drawOutline():void {
        var g:Graphics = outline.graphics;
        g.clear();
        UIPartUtil.drawTopBar(g, CSS.titleBarColors, UIPartUtil.getTopBarPath(w, topBarHeight), w, topBarHeight, CSS.borderColor);
//        g.lineStyle(1, CSS.borderColor, 1, true);
//        g.drawRect(0, topBarHeight, w, h-topBarHeight);
    }

    public function fixLayout():void {
        if (TempStatic.witStage) {
            var witStage:WitStage = TempStatic.witStage;
            TweenLite.delayedCall(.2, function ():void {
                witStage.width = w;
                witStage.height = h - topBarHeight;
                witStage.y = topBarHeight;
            });
        }

        runButton.x = w - 60;
        runButton.y = int((topBarHeight - runButton.height) / 2);
        stopButton.x = runButton.x + 32;
        stopButton.y = runButton.y + 1;

        fullscreenButton.x = 11;
        fullscreenButton.y = stopButton.y - 1;
        fullscreenButton.visible = CONFIG::editor;

        logo.x = 11;
        logo.y = stopButton.y + 2;
        logo.visible = !CONFIG::editor;

        // x-y readouts
        var left:int = 98;
        xLabel.x = left;
        xReadout.x = left + 16;
        yLabel.x = left + 43;
        yReadout.x = left + 60;
        xReadout.y = yReadout.y = 5;
        xLabel.y = yLabel.y = 3;
    }

    // -----------------------------
    // Stepping
    //------------------------------

    public function step():void {
        /* update run/stop buttons */
        if (TempStatic.interp.threadCount() > 0) threadStarted();
        else {
            runButton.turnOff();
            stopButton.turnOn();
        }
        /* update run/stop buttons */
        if (TempStatic.editMode) {
            // Update the mouse readouts. Do nothing if they are up-to-date (to minimize CPU load).
            if (stage.mouseX != lastX) {
                lastX = TempStatic.witStage.scratchMouseX();
                xReadout.text = String(lastX);
            }
            if (stage.mouseY != lastY) {
                lastY = TempStatic.witStage.scratchMouseY();
                yReadout.text = String(lastY);
            }
        }
    }

    // -----------------------------
    // Run/Stop/Fullscreen Buttons
    //------------------------------

    public function threadStarted():void {
        runButton.turnOn();
        stopButton.turnOff();
    }

    public function flashSprite(spr:WitSprite):void {
        function doFade():void {
            box.visible = !box.visible;
        }

        function deleteBox():void {
            if (box.parent) {
                box.parent.removeChild(box)
            }
        }

        var r:Rectangle = spr.getVisibleBounds(this);
        var box:Shape = new Shape();
        box.graphics.lineStyle(3, CSS.overColor, 1, true);
        box.graphics.beginFill(0x808080);
        box.graphics.drawRoundRect(0, 0, r.width, r.height, 12, 12);
        box.x = r.x;
        box.y = r.y;
        addChild(box);
        TweenLite.to(box, 6, {onUpdate:doFade, useFrames:true, onComplete:deleteBox});
    }

    public function toggleTurboMode():void {
        TempStatic.interp.turboMode = !TempStatic.interp.turboMode;
        refresh();
    }

    private function onStartButton(evt:MouseEvent):void {
        stopEvent(evt);
        if (TempStatic.loadInProgress) {
            return;
        }

        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.START_GREEN_FLAGS, null, true));
    }

    private function stopEvent(e:Event):void {
        if (e) {
            e.stopImmediatePropagation();
            e.preventDefault();
        }
    }
}
}
