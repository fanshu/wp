package wp.editor.ui.parts {
import com.greensock.TweenLite;
import com.greensock.easing.Linear;

import flash.display.*;
import flash.text.*;
import flash.utils.getTimer;

import util.TempStatic;
import util.UIPartUtil;

import wp.editor.ui.*;
import wp.editor.uiwidgets.*;
import wp.editor.wit.*;

public class ScriptsPart extends UIPart {
    private var shape:Shape;
    private var selector:PaletteSelector;
    private var spriteWatermark:Bitmap;
    protected var paletteFrame:ScrollFrame;
    protected var scriptsFrame:ScrollFrame;
    private var zoomWidget:ZoomWidget;
    private var sliderButton:IconButton;
    private var expand:Boolean;

    private const readoutLabelFormat:TextFormat = new TextFormat(CSS.font, 12, CSS.textColor, true);
    private const readoutFormat:TextFormat = new TextFormat(CSS.font, 12, CSS.textColor);

    private var xyDisplay:Sprite;
    private var xLabel:TextField;
    private var yLabel:TextField;
    private var xReadout:TextField;
    private var yReadout:TextField;
    private var lastX:int = -10000000; // impossible value to force initial update
    private var lastY:int = -10000000; // impossible value to force initial update

    public function ScriptsPart() {
        addChild(shape = new Shape());
        addChild(spriteWatermark = new Bitmap());
        addXYDisplay();
        addChild(selector = new PaletteSelector());

        spriteWatermark.alpha = .7;

        var palette:BlockPalette = new BlockPalette();
        paletteFrame = new ScrollFrame();
        paletteFrame.allowHorizontalScrollbar = false;
        paletteFrame.setContents(palette);
        addChild(paletteFrame);

        WitProgram.palette = palette;
        WitProgram.scriptsPane = addScriptsPane();

        addChild(zoomWidget = new ZoomWidget(WitProgram.scriptsPane));

        addChild(sliderButton = makeButton(sliderClick, 'slider'));
        expand = false;
        selector.x = 1 - selector.width;
        TweenLite.delayedCall(.5, sliderClick, [sliderButton]);
    }

    private function makeButton(fcn:Function, iconName:String):IconButton {
        var b:IconButton = new IconButton(fcn, iconName);
        b.isMomentary = true;
        return b;
    }

    protected function addScriptsPane():ScriptsPane {
        var scriptsPane:ScriptsPane = new ScriptsPane();
        scriptsFrame = new ScrollFrame();
        scriptsFrame.setContents(scriptsPane);
        addChild(scriptsFrame);

        return scriptsPane;
    }

    public function resetCategory():void {
        selector.select(Specs.motionCategory);
    }

    public function updatePalette():void {
        selector.updateTranslation();
        selector.select(selector.selectedCategory);
    }

    public function updateSpriteWatermark():void {
        var target:WitObj = WitProgram.viewedObj;
        if (target && !(target is WitStage)) {
            spriteWatermark.bitmapData = target.currentCostume().thumbnail(40, 40, false);
        } else {
            spriteWatermark.bitmapData = null;
        }
        if (spriteWatermark.parent) {
            spriteWatermark.parent.addChild(spriteWatermark);
        }
    }

    public function step():void {
        // Update the mouse readouts. Do nothing if they are up-to-date (to minimize CPU load).
        var target:WitObj = WitProgram.viewedObj;
        if (!target || (target is WitStage)) {
            if (xyDisplay.visible) xyDisplay.visible = false;
        } else {
            if (!xyDisplay.visible) xyDisplay.visible = true;

            var spr:WitSprite = target as WitSprite;
            if (!spr) return;
            if (spr.scratchX != lastX) {
                lastX = spr.scratchX;
                xReadout.text = String(lastX);
            }
            if (spr.scratchY != lastY) {
                lastY = spr.scratchY;
                yReadout.text = String(lastY);
            }
        }
        updateExtensionIndicators();
    }

    private var lastUpdateTime:uint;

    private function updateExtensionIndicators():void {
        if ((getTimer() - lastUpdateTime) < 500) return;
        for (var i:int = 0; i < WitProgram.palette.numChildren; i++) {
            var indicator:IndicatorLight = WitProgram.palette.getChildAt(i) as IndicatorLight;
            if (indicator) TempStatic.extensionManager.updateIndicator(indicator, indicator.target);
        }
        lastUpdateTime = getTimer();
    }

    public function setWidthHeight(w:int, h:int):void {
        this.w = w;
        this.h = h;
        fixlayout();
        redraw();
    }

    private function fixlayout():void {
        selector.y = 5;

        paletteFrame.x = selector.x;
        paletteFrame.y = selector.y + selector.height + 2;
        paletteFrame.setWidthHeight(selector.width + 1, h - paletteFrame.y - 2); // 5

        scriptsFrame.x = selector.x + selector.width + 2;
        scriptsFrame.y = selector.y + 1;

        scriptsFrame.setWidthHeight(w - scriptsFrame.x - 5, h - scriptsFrame.y - 5);

        // zoom
        zoomWidget.x = w - zoomWidget.width - 15;
        zoomWidget.y = h - zoomWidget.height - 15;

        // water
        spriteWatermark.x = w - 60;
        spriteWatermark.y = scriptsFrame.y + 10;
        xyDisplay.x = spriteWatermark.x + 1;
        xyDisplay.y = spriteWatermark.y + 43;

        sliderButton.x = selector.x + selector.width + 10 - sliderButton.width;
        sliderButton.y = h - 20 - sliderButton.height;
    }

    private function redraw():void {
        var paletteW:int = paletteFrame.visibleW();
        var paletteH:int = paletteFrame.visibleH();
        var scriptsW:int = scriptsFrame.visibleW();
        var scriptsH:int = scriptsFrame.visibleH();

        var g:Graphics = shape.graphics;
        g.clear();
        g.lineStyle(1, CSS.borderColor, 1, true);
        g.beginFill(CSS.backgroundColor_default);
        g.drawRect(0, 0, w, h);
        g.endFill();

        var lineY:int = selector.y + selector.height;
        var darkerBorder:int = CSS.borderColor - 0x141414;
        var lighterBorder:int = 0xF2F2F2;
        g.lineStyle(1, darkerBorder, 1, true);
        hLine(g, paletteFrame.x + 8, lineY, paletteW - 20);
        g.lineStyle(1, lighterBorder, 1, true);
        hLine(g, paletteFrame.x + 8, lineY + 1, paletteW - 20);

        g.lineStyle(1, darkerBorder, 1, true);
        g.drawRect(scriptsFrame.x - 1, scriptsFrame.y - 1, scriptsW + 1, scriptsH + 1);
    }

    private function hLine(g:Graphics, x:int, y:int, w:int):void {
        g.moveTo(x, y);
        g.lineTo(x + w, y);
    }

    private function addXYDisplay():void {
        xyDisplay = new Sprite();
        xyDisplay.addChild(xLabel = UIPartUtil.makeLabel('x:', readoutLabelFormat, 0, 0));
        xyDisplay.addChild(xReadout = UIPartUtil.makeLabel('-888', readoutFormat, 15, 0));
        xyDisplay.addChild(yLabel = UIPartUtil.makeLabel('y:', readoutLabelFormat, 0, 13));
        xyDisplay.addChild(yReadout = UIPartUtil.makeLabel('-888', readoutFormat, 15, 13));
        addChild(xyDisplay);
    }

    private function sliderClick(b:IconButton):void {
        expand = !expand;

        var toX:Number = expand ? 1 : 1 - selector.width;
        TweenLite.to(selector, .1, {x: toX, ease: Linear.easeNone, onUpdate: setWidthHeight, onUpdateParams: [w, h]});
    }
}
}
