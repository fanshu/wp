// This part holds the tab buttons to view scripts, costumes/scenes, or sounds.

package wp.editor.ui.parts {
import flash.display.*;
import flash.text.*;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import util.UIPartUtil;

import util.UIPartUtil;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.uiwidgets.IconButton;
import wp.editor.wit.WitStage;

public class TabsPart extends UIPart {
    public static const TAB_SCRIPTS:String = "EditorPart.tabScrits";
    public static const TAB_IMAGES:String = "EditorPart.tabImages";
    public static const TAB_SOUNDS:String = "EditorPart.tabSounds";

    private var scriptsTab:IconButton;
    private var imagesTab:IconButton;
    private var soundsTab:IconButton;

    public function TabsPart() {
        function selectScripts(b:IconButton):void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_TAB, TAB_SCRIPTS, true));
        }

        function selectImages(b:IconButton):void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_TAB, TAB_IMAGES, true));
        }

        function selectSounds(b:IconButton):void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_TAB, TAB_SOUNDS, true));
        }

        scriptsTab = makeTab('Scripts', selectScripts);
        imagesTab = makeTab('Images', selectImages); // changed to 'Costumes' or 'Scenes' by refresh()
        soundsTab = makeTab('Sounds', selectSounds);
        addChild(scriptsTab);
        addChild(imagesTab);
        addChild(soundsTab);
        scriptsTab.turnOn();
    }

    public function refresh():void {
        var label:String = ((WitProgram.viewedObj != null) && (WitProgram.viewedObj is WitStage)) ? 'Backdrops' : 'Costumes';
        imagesTab.setImage(makeTabImg(label, true), makeTabImg(label, false));
        fixLayout();
    }

    public function selectTab(tabName:String):void {
        scriptsTab.turnOff();
        imagesTab.turnOff();
        soundsTab.turnOff();
        if (tabName == TAB_SCRIPTS) scriptsTab.turnOn();
        if (tabName == TAB_IMAGES) imagesTab.turnOn();
        if (tabName == TAB_SOUNDS) soundsTab.turnOn();
    }

    public function fixLayout():void {
        scriptsTab.x = 0;
        scriptsTab.y = 0;
        imagesTab.x = scriptsTab.x + scriptsTab.width + 1;
        imagesTab.y = 0;
        soundsTab.x = imagesTab.x + imagesTab.width + 1;
        soundsTab.y = 0;
        this.w = soundsTab.x + soundsTab.width;
        this.h = scriptsTab.height;
    }

    public function updateTranslation():void {
        scriptsTab.setImage(makeTabImg('Scripts', true), makeTabImg('Scripts', false));
        soundsTab.setImage(makeTabImg('Sounds', true), makeTabImg('Sounds', false));
        refresh(); // updates imagesTabs
    }

    private function makeTab(label:String, action:Function):IconButton {
        return new IconButton(action, makeTabImg(label, true), makeTabImg(label, false), true);
    }

    private function makeTabImg(label:String, isSelected:Boolean):Sprite {
        const _:Function = gettext;
        var img:Sprite = new Sprite();
        var tf:TextField = new TextField();
        tf.defaultTextFormat = new TextFormat(CSS.font, 12, isSelected ? CSS.onColor : CSS.offColor, false);
        tf.text = _(label);
        tf.width = tf.textWidth + 5;
        tf.height = tf.textHeight + 5;
        tf.x = 10;
        tf.y = 4;
        img.addChild(tf);

        var g:Graphics = img.graphics;
        var w:int = tf.width + 20;
        var h:int = 28;
        UIPartUtil.cornerRadius = 13;
        if (isSelected) UIPartUtil.drawTopBar(g, [CSS.white, CSS.white], UIPartUtil.getTopBarPath(w, h), w, h);
        else UIPartUtil.drawSelected(g, CSS.tabsColors, UIPartUtil.getTopBarPath(w, h), w, h);
        UIPartUtil.cornerRadius = 8;
        return img;
    }
}
}
