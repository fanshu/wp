package wp.editor.ui.parts {
import flash.display.DisplayObject;

import jijunzeng.MyEvent;

import util.TempStatic;

import util.TempStatic;

import wp.editor.controllers.ContextEvtConst;

public class EditorPart extends UIPart {
    // UI Parts
    public var tabsPart:TabsPart;
    public var scriptsPart:ScriptsPart;
    public var imagesPart:ImagesPart;
    public var soundsPart:SoundsPart;

    public function EditorPart() {
        super();
        tabsPart = new TabsPart();
        scriptsPart = new ScriptsPart();
        imagesPart = new ImagesPart();
        soundsPart = new SoundsPart();
    }

    public function fixLayout():void {
        tabsPart.fixLayout();

        if (!stage) return;
        // the content area shows the part associated with the currently selected tab:
        var w:int = stage.stageWidth;
        var h:int = stage.stageHeight - 1; // fix to show bottom border...
        w = Math.ceil(w / scaleX);
        h = Math.ceil(h / scaleY);
        var contentY:int = tabsPart.y + 27;
        if (WitProgram.libraryPart) w = WitProgram.libraryPart.x;
        updateContentArea(tabsPart.x, contentY, w - tabsPart.x - 6, h - y - contentY - 5);
    }

    public function setTab(tabName:String):void {
        if (!TempStatic.editMode) tabName = "";

        if (isShowing(imagesPart)) imagesPart.editor.shutdown();
        if (isShowing(soundsPart)) soundsPart.editor.shutdown();
        hide(scriptsPart);
        hide(imagesPart);
        hide(soundsPart);
        if (tabName == TabsPart.TAB_IMAGES) {
            imagesPart.refresh();
            addChild(imagesPart);
        } else if (tabName == TabsPart.TAB_SOUNDS) {
            soundsPart.refresh();
            addChild(soundsPart);
        } else if (tabName && (tabName.length > 0)) {
            tabName = TabsPart.TAB_SCRIPTS;
            scriptsPart.updatePalette();
            WitProgram.scriptsPane.viewScriptsFor(WitProgram.viewedObj);
            scriptsPart.updateSpriteWatermark();
            addChild(scriptsPart);
        }
        addChild(tabsPart);
        tabsPart.selectTab(tabName);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_SAVE_NEEDED, true, true));
    }

    private function updateContentArea(contentX:int, contentY:int, contentW:int, contentH:int):void {
        imagesPart.x = soundsPart.x = scriptsPart.x = contentX;
        imagesPart.y = soundsPart.y = scriptsPart.y = contentY;
        imagesPart.setWidthHeight(contentW, contentH);
        soundsPart.setWidthHeight(contentW, contentH);
        scriptsPart.setWidthHeight(contentW, contentH);
    }

    private function isShowing(obj:DisplayObject):Boolean {
        return obj.parent != null;
    }

    private function hide(obj:DisplayObject):void {
        if (obj.parent) obj.parent.removeChild(obj);
    }
}
}
