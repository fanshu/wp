package wp.editor.ui.parts {
import com.greensock.TweenLite;

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.MouseEvent;

import gnu.as3.gettext.gettext;

import jijunzeng.MyEvent;

import util.TempStatic;

import util.TempStatic;

import util.TempStatic;

import wp.editor.controllers.ContextEvtConst;
import wp.editor.ui.media.MediaLibrary;

import wp.editor.wit.WitCostume;

import wp.editor.wit.WitSprite;

use namespace gettext;

public class LibraryButtonGroup extends Sprite {
    private static const _:Function = gettext;
    [Embed(source='/../assets/UI/newsprite/camera.png')]
    private const CAMERA:Class;

    [Embed(source='/../assets/UI/newsprite/import.png')]
    private const IMPORT:Class;

    [Embed(source='/../assets/UI/newsprite/library.png')]
    private const LIBRARY:Class;

    [Embed(source='/../assets/UI/newsprite/paintbrush.png')]
    private const PAINT:Class;

    private const right:uint = 300;
    private const gap:uint = 8;

    public function LibraryButtonGroup() {
        var btn:MyButton;
        var icons:Array = [new CAMERA(), new IMPORT(), new PAINT(), new LIBRARY()];
        var colors:Array = [0xfe6665, 0xfc9832, 0x9ac969, 0x31cbff];
        var summer:Array = [_('New sprite from camera'), _('Upload sprite from file'), _('Paint new sprite'), _('Choose sprite from library')];
        for (var i:uint = 0; i < icons.length; i++) {
            btn = new MyButton(icons[i], colors[i], summer[i]);
            btn.x = right - 20 - MyButton.SIZE - i * (MyButton.SIZE + 5);
            addChild(btn);
        }

        select();
        addEventListener(MouseEvent.MOUSE_OVER, function (evt:MouseEvent):void {
            var btn:MyButton = evt.target as MyButton;
            if (!btn) return;
            select(getChildIndex(btn));
        });
        addEventListener(MouseEvent.MOUSE_OUT, function (evt:MouseEvent):void {
            select();
        });
        addEventListener(MouseEvent.CLICK, function (evt:MouseEvent):void {
            var btn:MyButton = evt.target as MyButton;
            if (!btn) return;
            switch (getChildIndex(btn)) {
                case 0:
                    spriteFromCamera();
                    break;
                case 1:
                    spriteFromComputer();
                    break;
                case 2:
                    paintSprite();
                    break;
                case 3:
                    spriteFromLibrary();
                    break;
            }
        });
    }

    private function select(id:int = -1):void {
        var toX:uint;
        var btn:MyButton;
        for (var i:uint = 0; i < numChildren; i++) {
            btn = getChildAt(i) as MyButton;
            toX = right - 20 - MyButton.SIZE - i * (MyButton.SIZE + gap);
            if (id != -1 && i >= id) {
                toX -= 120;
            }
            TweenLite.killTweensOf(btn);
            TweenLite.to(btn, .2, {
                x: toX, onUpdateParams: [i], onUpdate: function (idx:uint):void {
                    var btn:MyButton;
                    btn = getChildAt(idx) as MyButton;
                    btn.setWitdh(((idx == 0) ? right - 20 : getChildAt(idx - 1).x - gap) - btn.x);
                }
            });
        }
    }

    private function spriteFromComputer():void {
        importSprite(true)
    }

    private function spriteFromLibrary():void {
        importSprite(false)
    }

    private function importSprite(fromComputer:Boolean):void {
        function addSprite(costumeOrSprite:*):void {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_SPRITE, costumeOrSprite));
        }

        var lib:MediaLibrary = new MediaLibrary('sprite', addSprite);
        if (fromComputer) lib.importFromDisk();
        else lib.open();
    }

    private function paintSprite():void {
        var spr:WitSprite = new WitSprite();
        spr.setInitialCostume(WitCostume.emptyBitmapCostume(_('costume1'), false));
//        TempStatic.addNewSprite(spr, true);
        TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_NEW_SPRITE, [spr, true, false], true));
    }

    protected function spriteFromCamera():void {
        function savePhoto(photo:BitmapData):void {
            var s:WitSprite = new WitSprite();
            s.setInitialCostume(new WitCostume(_('photo1'), photo));
//            TempStatic.addNewSprite(s);
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.ADD_NEW_SPRITE, [s, false, false], true));
            WitProgram.editorPart.imagesPart.closeCameraDialog();
        }

        WitProgram.editorPart.imagesPart.openCameraDialog(savePhoto);
    }
}
}
