package wp.editor.ui {
import flash.display.*;
import flash.media.*;

import gnu.as3.gettext.gettext;

import wp.editor.uiwidgets.*;

public class CameraDialog extends DialogBox {
    private static const _:Function = gettext;

    private var saveFunc:Function;
    private var picture:Bitmap;
    private var video:Video;

    public function CameraDialog(saveFunc:Function) {
        super();
        this.saveFunc = saveFunc;

        addTitle(_('Camera'));

        var container:Sprite = new Sprite();
        addWidget(container);

        picture = new Bitmap();
        picture.bitmapData = new BitmapData(320, 240, true);
        picture.visible = false;
        container.addChild(picture);

        video = new Video(320, 240);
        video.smoothing = true;
        video.attachCamera(Camera.getCamera());
        container.addChild(video);

        var b:Button;
        addChild(b = new Button(_('Save'), savePicture));
        buttons.push(b);
        addChild(b = new Button(_('Close'), closeDialog));
        buttons.push(b);
    }

    private function savePicture():void {
        picture.bitmapData.draw(video);
        if (saveFunc != null) (saveFunc(picture.bitmapData.clone()));
    }

    public function closeDialog():void {
        if (video) video.attachCamera(null);
        if (parent) parent.removeChild(this);
    }
}
}
