// A PaletteSelectorItem is a text button for a named category in a PaletteSelector.
// It handles mouse over, out, and up events and changes its appearance when selected.
package wp.editor.ui {
import com.greensock.TweenLite;
import com.greensock.easing.Back;

import flash.display.*;
import flash.events.MouseEvent;
import flash.text.*;

import gnu.as3.gettext.gettext;

import util.Resources;

use namespace gettext;

public class PaletteSelectorItem extends Sprite {
    private static const _:Function = gettext;

    public var categoryID:int;
    public var bg:Shape;
    public var icon:Bitmap;
    public var label:TextField;
    public var isSelected:Boolean;

    private var color:uint;

    public function PaletteSelectorItem(id:int, s:String, c:uint) {
        categoryID = id;
        color = c;
        addLabel(s);
        addBG();
        setSelected(false);
        addEventListener(MouseEvent.MOUSE_OVER, mouseOver);
        addEventListener(MouseEvent.MOUSE_OUT, mouseOut);
        addEventListener(MouseEvent.CLICK, mouseUp);
    }

    private function addLabel(s:String):void {
        icon = Resources.createBmp(s.split(' ').shift());
        icon.x = 3;
        addChild(icon);

        label = new TextField();
        label.autoSize = TextFieldAutoSize.LEFT;
        label.selectable = false;
        label.text = _(s);
        label.x = 37;
        label.y = icon.height - label.height >> 1;
        label.y -= 2;
        addChild(label);
    }

    private function addBG():void {
        var w:int = 100;
        var h:int = icon.height;
        // mouse area
        var g:Graphics = this.graphics;
        g.clear();
        g.beginFill(0xd9d9d9);
        g.drawRoundRect(0, 0, w, h, 20);
        g.endFill();
        // msk
        var msk:Shape = new Shape();
        msk.graphics.beginFill(0);
        msk.graphics.drawRoundRect(0, 0, w, h, 20);
        msk.graphics.endFill();
        addChildAt(msk, 0);
        // bg
        bg = new Shape();
        bg.graphics.beginFill(color);
        bg.graphics.drawRect(0, 0, 1, h);
        bg.graphics.endFill();
        bg.mask = msk;
        addChildAt(bg, 0);
    }

    public function setSelected(flag:Boolean):void {
        isSelected = flag;

        // label
        var fmt:TextFormat = new TextFormat();
        fmt.font = CSS.font;
        fmt.size = 14;
        fmt.color = isSelected ? CSS.white : CSS.offColor;
        fmt.bold = isSelected;
        label.setTextFormat(fmt);

        // bg
        TweenLite.killTweensOf(bg);
        TweenLite.to(bg, .5, {width:isSelected?100:icon.width,ease: Back.easeInOut});
    }

    private function mouseOver(event:MouseEvent):void {
        label.textColor = isSelected ? CSS.white : CSS.buttonLabelOverColor;
    }

    private function mouseOut(event:MouseEvent):void {
        label.textColor = isSelected ? CSS.white : CSS.offColor;
    }

    private function mouseUp(event:MouseEvent):void {
        if (parent is PaletteSelector) {
            PaletteSelector(parent).select(categoryID, event.shiftKey);
        }
    }
}
}
