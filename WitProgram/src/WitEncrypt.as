package {
import com.adobe.utils.StringUtil;
import com.greensock.TweenLite;

import flash.desktop.Clipboard;
import flash.desktop.ClipboardFormats;
import flash.desktop.NativeDragManager;
import flash.display.*;
import flash.events.Event;
import flash.events.NativeDragEvent;
import flash.filesystem.File;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.MVCSBundle;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.impl.Context;

import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.config.EncryptConfig;

[SWF(width=500, height=300, backgroundColor=0xf0f0f0)]
public class WitEncrypt extends Sprite {
    // RL2 holder
    private var context:IContext;

    private var _rect:Sprite;
    private var _files:Vector.<File> = new <File>[];
    public var curr_file:File;
    private var _prompt:TextField;

    public function WitEncrypt() {
        // stage
        stage.align = StageAlign.TOP_LEFT;
        stage.scaleMode = StageScaleMode.NO_SCALE;
        // invoke file
        // RL2
        context = new Context()
                .install(MVCSBundle)
                .configure(EncryptConfig)
                .configure(new ContextView(this));
    }

    public function initialize():void {
        // TODO rm
        Block.setFonts("", 14, 13, false, 0);


        _rect = new Sprite();
        _rect.graphics.beginFill(0x232323);
        _rect.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
        _rect.graphics.endFill();
        addChild(_rect);

        var usb:TextField = new TextField();
        var fmt:TextFormat = new TextFormat(null, 24, 0xffffff, null, null, null, null, null, TextFormatAlign.CENTER);
        usb.defaultTextFormat = fmt;
        usb.addEventListener(Event.CHANGE, function(evt:Event):void {
            var str:String = StringUtil.trim(usb.text);
            if (str.length > 0) TempStatic.usbID = str;
        });
        usb.type = TextFieldType.INPUT;
        usb.border = true;
        usb.borderColor = 0xff0000;
        usb.width = 400;
        usb.height = 30;
        usb.x = stage.stageWidth - usb.width >> 1;
        usb.y = 50;
        addChild(usb);
        usb.appendText("VID_048D&PID_1234");
        usb.dispatchEvent(new Event(Event.CHANGE));

        _prompt = new TextField();
        _prompt.defaultTextFormat = fmt;
        _prompt.width = usb.width;
        _prompt.height = usb.height;
        _prompt.x = usb.x;
        _prompt.y = usb.y + 100;
        addChild(_prompt);
        _prompt.text = 'Ready';

        addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, onDragEnter);
        addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onDragDrop);
    }

    public function encrypt():void {
        if (_files.length == 0) {
            _prompt.text = 'Ready';
            return;
        }
        curr_file = _files.pop();
        _prompt.text = curr_file.nativePath.split('\\').pop();
        var ext:String = curr_file.nativePath.split('.').pop();
        TweenLite.delayedCall(.1, function():void {
            dispatchEvent(new MyEvent("encrypt", ext));
        });
    }

    private function onDragEnter(evt:NativeDragEvent):void {
        var clip:Clipboard = evt.clipboard;
        if (clip.hasFormat(ClipboardFormats.FILE_LIST_FORMAT)) {
            NativeDragManager.acceptDragDrop(_rect);
        }
    }

    private function onDragDrop(evt:NativeDragEvent):void {
        _files.length = 0;
        for each (var file:File in evt.clipboard.getData(ClipboardFormats.FILE_LIST_FORMAT)) {
            getFiles(file);
        }

        encrypt();
    }

    private function getFiles(file:File):void {
        if (file.isDirectory) {
            for each (var f:File in file.getDirectoryListing()) {
                getFiles(f);
            }
        } else {
            var ext:String = file.nativePath.split('.').pop();
            if ("wit spr".indexOf(ext) != -1) _files.push(file);
        }
    }
}
}
