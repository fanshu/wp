package {
import com.greensock.TweenLite;

import flash.display.*;
import flash.events.*;
import flash.ui.Keyboard;

import gnu.as3.gettext.AsGettext;
import gnu.as3.gettext.Locale;

import jijunzeng.MyEvent;

import robotlegs.bender.bundles.mvcs.MVCSBundle;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.impl.Context;

import util.Resources;
import util.Server;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;
import util.TempStatic;

import wp.editor.blocks.Block;
import wp.editor.config.ProgramConfig;
import wp.editor.controllers.ContextEvtConst;
import wp.editor.svgeditor.tools.SVGTool;
import wp.editor.ui.parts.StagePart;
import wp.editor.uiwidgets.DialogBox;
import wp.editor.uiwidgets.Menu;
import wp.editor.wit.WitCostume;
import wp.editor.wit.WitObj;
import wp.editor.wit.WitSound;
import wp.editor.wit.WitStage;

use namespace Locale;

use namespace AsGettext;

[SWF(width=1280, height=800, backgroundColor=0xf0f0f0)]
public class WitProject extends Sprite {
    public static var step_allow:Boolean = true;
    public static var viewedObj:WitObj;
    // UI Elements
    private var playerBG:Shape;

    // RL2 holder
    private var context:IContext;

    public function WitProject() {
        // uncaught error
        if (loaderInfo.hasOwnProperty("uncaughtErrorEvents")) {
            IEventDispatcher(loaderInfo["uncaughtErrorEvents"]).addEventListener("uncaughtError",
                    function (event:UncaughtErrorEvent):void {
                        trace(event.error);
                    }
            );
        }

        // stage
        stage.align = StageAlign.TOP_LEFT;
        stage.scaleMode = StageScaleMode.NO_SCALE;

        // RL2
        context = new Context()
                .install(MVCSBundle)
                .configure(ProgramConfig)
                .configure(new ContextView(this));
    }

    public function initialize():void {
        SVGTool.setStage(stage);
        Server.instance.app = this;

        CSS.font = CSS.font_zh;
        Block.setFonts(CSS.font, 14, 13, false, 0);

        TempStatic.witStage = new WitStage();

        playerBG = new Shape(); // create, but don't add
        /* addParts */
        TempStatic.stagePart = new StagePart();
        addChild(TempStatic.stagePart);


        /* addParts */

        stage.addEventListener(MouseEvent.MOUSE_DOWN, TempStatic.gh.mouseDown);
        stage.addEventListener(MouseEvent.MOUSE_MOVE, TempStatic.gh.mouseMove);
        stage.addEventListener(MouseEvent.MOUSE_UP, TempStatic.gh.mouseUp);
        stage.addEventListener(MouseEvent.MOUSE_WHEEL, TempStatic.gh.mouseWheel);
        stage.addEventListener('rightClick', TempStatic.gh.rightMouseClick);
        stage.addEventListener(
                KeyboardEvent.KEY_DOWN,
                function (evt:KeyboardEvent):void {
                    if (!evt.shiftKey && evt.charCode == Keyboard.ESCAPE) {
                        TempStatic.gh.escKeyDown();
                    } else {
                        TempStatic.runtime.keyDown(evt);
                    }
                }
        );
        stage.addEventListener(KeyboardEvent.KEY_UP, TempStatic.runtime.keyUp);
        stage.addEventListener(Event.ENTER_FRAME, step);
        stage.addEventListener(Event.RESIZE, fixLayout);
        TempStatic.windowTitle();
        if (CONFIG::editor) {
            stage["nativeWindow"].maximize();
        }

        // install project before calling fixLayout()
        TweenLite.delayedCall(
                .2,
                function ():void {
                    TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.INSTALL_PROJECT_FROM_DATA,
                            {
                                data: new Resources.Project(),
                                saveForRevert: true
                            }
                    ));
                    TweenLite.delayedCall(.2, setEditMode, [false]);
                }
        );
    }

    protected function step(e:Event):void {
        if (!step_allow) return;
        // Step the runtime system and all UI components.
        TempStatic.gh.step();
        TempStatic.runtime.stepRuntime();
        TempStatic.stagePart.step();
    }

    // -----------------------------
    // UI Modes and Resizing
    //------------------------------

    public function setEditMode(newMode:Boolean):void {
        Menu.removeMenusFrom(stage);
        TempStatic.editMode = newMode;
        if (TempStatic.editMode) {
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SHOW_ALL_RUN_FEEDBACK));
            hide(playerBG);
            addChild(TempStatic.stagePart);
            TempStatic.runtime.edgeTriggersEnabled = true;
        } else {
            addChildAt(playerBG, 0); // behind everything
            playerBG.visible = false;
            TempStatic.runtime.evtDispatcher.dispatchEvent(new MyEvent(ContextEvtConst.SET_TAB, "")); // hides scripts, images, and sounds
        }
        TempStatic.witStage.updateListWatchers();
        addChild(TempStatic.stagePart); // put stage in front
        fixLayout();
        TempStatic.stagePart.refresh();
    }

    protected static function hide(obj:DisplayObject):void {
        if (obj.parent) obj.parent.removeChild(obj)
    }

    public function fixLayout(evt:Event = null):void {
        var w:int = stage.stageWidth;
        var h:int = stage.stageHeight - 1; // fix to show bottom border...

        w = Math.ceil(w / scaleX);
        h = Math.ceil(h / scaleY);

        var extraW:int = 2;
        var extraH:int = (TempStatic.editMode ? 26 : 39) + 1;
        /* drawBG */
        var g:Graphics = playerBG.graphics;
        g.clear();
        g.beginFill(0);
        g.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
        g.endFill();
        /* drawBG */
        var pad:int = (w > 550) ? 16 : 0; // add padding for full-screen mode
        var scale:Number = Math.min((w - extraW - pad) / WitObj.STAGEW, (h - extraH - pad) / WitObj.STAGEH);
        scale = Math.max(0.01, scale);
        var scaledW:int = Math.floor((scale * WitObj.STAGEW) / 4) * 4; // round down to a multiple of 4
        scale = scaledW / WitObj.STAGEW;
        var playerW:Number = (scale * WitObj.STAGEW) + extraW;
        var playerH:Number = (scale * WitObj.STAGEH) + extraH;
        TempStatic.stagePart.setWidthHeight(playerW, playerH);
        TempStatic.stagePart.x = int((w - playerW) / 2);
        TempStatic.stagePart.y = int((h - playerH) / 2);
    }

    public static function okayToAdd(newAssetBytes:int):Boolean {
        // Return true if there is room to add an asset of the given size.
        // Otherwise, return false and display a warning dialog.
        const assetByteLimit:int = 50 * 1024 * 1024; // 50 megabytes
        var assetByteCount:int = newAssetBytes;
        for each (var obj:WitObj in TempStatic.witStage.allObjects()) {
            for each (var c:WitCostume in obj.costumes) {
                if (!c.baseLayerData) c.prepareToSave();
                assetByteCount += c.baseLayerData.length;
            }
            for each (var snd:WitSound in obj.sounds) assetByteCount += snd.soundData.length;
        }
        if (assetByteCount > assetByteLimit) {
            var overBy:int = Math.max(1, (assetByteCount - assetByteLimit) / 1024);
            DialogBox.notify(
                    'Sorry!',
                    'Adding that media asset would put this project over the size limit by ' + overBy + ' KB\n' +
                    'Please remove some costumes, backdrops, or sounds before adding additional media.', TempStatic.witStage.stage);
            return false;
        }
        return true;
    }
}
}
