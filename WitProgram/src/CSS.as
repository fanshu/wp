// Styles for WitProgram Editor based on the Upstatement design.
package {
import flash.text.*;

import util.Resources;

public class CSS {
    // ScratchX
    public static const backgroundColor_ScratchX:int = 0x3f5975;

    // Colors
    public static const white:int = 0xFFFFFF;
    public static const black:int = 0;
    public static const backgroundColor_default:int = 0xE2E2E2;
    public static const topBarColor_default:int = 0x80B23F;
    public static const tabColor:int = 0xD8D8D8;
    public static const itemSelectedColor:int = 0xFFFFFF;
    public static const borderColor:int = 0xD6D6D6;
    public static const textColor:int = 0x5C5D5F; // 0x6C6D6F
    public static const buttonLabelColor:int = textColor;
    public static const buttonLabelOverColor:int = 0xFBA939;
    public static const offColor:int = 0x8F9193; // 0x9FA1A3
    public static const onColor:int = textColor; // 0x4C4D4F
    public static const overColor:int = 0x179FD7;
    public static const arrowColor:int = 0xA6A8AC;

    // Fonts
    public static const font_en:String = Resources.chooseFont(['Donegal', 'Gloria', 'Helvetica', 'Mystery', 'Marker']);
    public static const font_zh:String = "Microsoft yahei";
    public static var font:String = font_zh;
    public static const menuFontSize:int = 12;
    public static const normalTextFormat:TextFormat = new TextFormat(font, 12, textColor);
    public static const topBarButtonFormat:TextFormat = new TextFormat(font, 12, white, true);
    public static const titleFormat:TextFormat = new TextFormat(font, 14, textColor);
    public static const thumbnailFormat:TextFormat = new TextFormat(font, 11, textColor);
    public static const thumbnailExtraInfoFormat:TextFormat = new TextFormat(font, 9, textColor);
    public static const projectTitleFormat:TextFormat = new TextFormat(font, 13, textColor);
    public static const projectInfoFormat:TextFormat = new TextFormat(font, 12, textColor);

    // Section title bars
//    public static const titleBarColors:Array = [0x87bd43, 0x7aab3c]; // 绿色
//    public static const titleBarColors:Array = [0xFF9934, 0xFF9934]; // 橙色
    public static const titleBarColors:Array = [0x31A7EF, 0x4ABCFB]; // 蓝色
    public static const tabsColors:Array = [0xD6D6D6, 0xD6D6D6];
    public static const buttonColors:Array = [0xF5F5F5, 0xFFFFFF];
    public static const titleBarH:int = 30;

}
}
