#!/bin/sh

find ../../../src | egrep -i '\.as$' > files
xgettext --package-name witprogram --package-version 0.1 --default-domain witprogram --output witprogram.pot --from-code=UTF-8 -L C --keyword=_:1 -f files
sed --in-place witprogram.pot --expression='s/CHARSET/UTF-8/'
rm files
