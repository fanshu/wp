|  v2.3   | v2.4 | v2.5 | v2.6 | v3 |
|  ------ | ------ | ------ | ------ | ------ |
|  2015-12-23 | 2016-1-1 | 2016-1-8 | 2016-1-15 | 2016-2-8 |
|  独立打包 | UI改进 | 舞台高清 | 网站集成加密 | 联机版 |
|  加密工具 |  | swf打包apk |  |  |
|  绿色版bat文件关联 |  |  |  |  |  |


##### 程序优化
- [~~库面板宽度缩小~~](http://www.hostedredmine.com/issues/504400)
- [~~积木面板宽度缩小~~](http://www.hostedredmine.com/issues/504401)
- [~~积木面板中分类积木增大~~](http://www.hostedredmine.com/issues/504402)
- [~~舞台合并到库面板~~](http://www.hostedredmine.com/issues/504403)
- [~~积木颜色立体化~~](http://www.hostedredmine.com/issues/504404)
- [~~UI色彩调整~~](http://www.hostedredmine.com/issues/504405)
- [~~图标替换~~](http://www.hostedredmine.com/issues/504406)
- [舞台高清](http://www.hostedredmine.com/issues/504407)

##### 体积优化
- [~~独立打包【程序、插件、素材库】~~](http://www.hostedredmine.com/issues/504398)
- [~~绿色版bat批处理安装文件，用于关联.wit .spr~~](http://www.hostedredmine.com/issues/504408)

##### 加密
- [~~独立加密工具~~](http://www.hostedredmine.com/issues/504399)
- [网站集成](http://www.hostedredmine.com/issues/504409)

##### 联机版
- [素材库采用缓存、同步模式](http://www.hostedredmine.com/issues/504410)

##### 移动端
- [~~单独swf打包apk~~](http://www.hostedredmine.com/issues/504411)
- HTML5

##### 创客
- Arduino

##### BUG、Features处理
